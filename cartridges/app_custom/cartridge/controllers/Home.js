'use strict';

var server = require('server');

server.extend(module.superModule);

/**
 * Any customization on this endpoint, also requires update for Default-Start endpoint
 */
server.prepend('Show', function (req, res, next) {
    // Get page scope
    var viewData = res.getViewData();

    // Extend scope object with my data
    viewData.customTitle = 'Custom Title';
    viewData.customDescription = 'Custom Description';

    // Update page scope
    res.setViewData(viewData);

    next();
});

module.exports = server.exports();
