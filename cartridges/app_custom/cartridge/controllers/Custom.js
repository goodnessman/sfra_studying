'use strict';

const server = require('server');
const pref = require('pref');
const HookMgr = require('dw/system/HookMgr');
const csrfProtection = require('*/cartridge/scripts/middleware/csrf');
const cache = require('*/cartridge/scripts/middleware/cache');
const CatalogMgr = require('dw/catalog/CatalogMgr');
const ProductMgr = require('dw/catalog/ProductMgr');

server.get('Show', function (req, res, next) {
    var currentCustomer = require('*/cartridge/models/currentCustomer');
    var currentCustomerData = currentCustomer(request);
    let testModulesPref = pref.get('test.modules');
    res.render('custom', {
        testData: 1,
        currentCustomer: currentCustomerData,
        testModulesPref: testModulesPref,
        updated: false
    });
    let viewData = res.getViewData();
    HookMgr.callHook('app.update', 'update', viewData);
    next();
});

server.get('IncludeUrl', function (req, res, next) {
    res.render('custom/remoteInclude', {
        testData: 2
    });
    next();
});

// test url: https://dev16-na01-lesschwab.demandware.net/on/demandware.store/Sites-lesus-Site/default/Custom-PrintProductInfo?pid=106352226
server.get('PrintProductInfo', function (req, res, next) {
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var product = ProductFactory.get(req.querystring);
    res.render('custom/product', {
        product: product
    });
    next();
});

server.get(
    'CustomerDetails',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        let customerDetailsForm = server.forms.getForm('customerDetailsForm');
        customerDetailsForm.clear();
        res.render('components/customerDetails', {
            form: customerDetailsForm
        });
        next();
});

server.get(
    'PageCaching',
    server.middleware.https,
    cache.applyDefaultCache,
    function (req, res, next) {
        res.render('components/pagecaching/page');
        next();
});

server.get(
    'NonCachedBlock',
    server.middleware.https,
    function (req, res, next) {
        res.render('components/pagecaching/page');
        next();
});

server.get(
    'SetCustomCach',
    server.middleware.https,
    function (req, res, next) {
        let CacheMgr = require('dw/system/CacheMgr');
        let cacheStorage = CacheMgr.getCache('test');
        cacheStorage.put('testID', { test: 1 });
        session.custom.test = 'test';
        res.json({ success: true });
        next();
});

server.get(
    'GetCustomCach',
    server.middleware.https,
    function (req, res, next) {
        let CacheMgr = require('dw/system/CacheMgr');
        let cacheStorage = CacheMgr.getCache('test');
        cacheStorage.put('testID', { test: 1 });
        var myCachedData = cacheStorage.get('testID');
        res.json({ customCach: myCachedData, session: session.custom.test});
        next();
});

// https://dev16-na01-lesschwab.demandware.net/on/demandware.store/Sites-lesus-Site/default/Custom-RenderContent?cid=overnight-review-step-message
server.get(
    'RenderContent',
    server.middleware.https,
    function (req, res, next) {
        let contentID = request.httpParameterMap.cid.value;
        if (contentID) {
            const ContentMgr = require('dw/content/ContentMgr');
            const ContentModel = require('*/cartridge/models/content');
            let apiContent = ContentMgr.getContent(contentID);
            if (apiContent) {
                let content = new ContentModel(apiContent, 'components/content/contentAssetInc');
                res.render(content.template, { content: content });
            } else {
                res.render('/components/content/offlineContent');
            }
        }
        next();
});

server.get(
    'GetSiteCatalog',
    function (req, res, next) {
        var siteCatalog = CatalogMgr.getSiteCatalog();

        // get catalog display name
        var catalogName = siteCatalog.getDisplayName();

        res.print(catalogName);

        next();
    }
);

// https://dev16-na01-lesschwab.demandware.net/on/demandware.store/Sites-lesus-Site/default/Custom-GetCategory?cid=tires
server.get(
    'GetCategory',
    function (req, res, next) {
        let categoryData = CatalogMgr.getCategory(req.querystring.cid);

        res.render('components/content/categorydata', { category: categoryData });

        next();
    }
);

// https://dev16-na01-lesschwab.demandware.net/on/demandware.store/Sites-lesus-Site/default/Custom-GetProduct?pid=776559
server.get(
    'GetProduct',
    function (req, res, next) {
        let product = ProductMgr.getProduct(req.querystring.pid);
        let categoryID;
        if (product && product.isOnline()) {
            let category = product.getPrimaryCategory();
            if (category) {
                categoryID = category.getID();
            }
        }

        res.print(categoryID);

        next();
    }
);

// https://dev16-na01-lesschwab.demandware.net/on/demandware.store/Sites-lesus-Site/default/Custom-GetVariations?pid=111202665
server.get(
    'GetVariations',
    function (req, res, next) {
        let variations = [];
        let product = ProductMgr.getProduct(req.querystring.pid);

        if (product.isMaster() || product.isVariant()) {
            // get the collection of product variants of this variation model
            variations = product.getVariationModel().getVariants();
        }

        res.render('components/content/variations', { variations: variations });

        next();
    }
);

// https://dev16-na01-lesschwab.demandware.net/on/demandware.store/Sites-lesus-Site/default/Custom-GetInventoryAndPrice?pid=710916
server.get(
    'GetInventoryAndPrice',
    function (req, res, next) {
        let variations = [];
        let product = ProductMgr.getProduct(req.querystring.pid);
        let productInstance;
        if (product.isVariant()) {
            // get the collection of product variants of this variation model
            variations = product.getVariationModel().getVariants();
            const productModel = require('*/cartridge/models/product2/product');
            productInstance = productModel(Object.create(null), product);
        }

        res.render('components/inventoryAndPrice', { product: productInstance });

        next();
    }
);

server.get(
    'SearchProducts',
    function (req, res, next) {
        var ProductSearchModel = require('dw/catalog/ProductSearchModel');
        var psm = new ProductSearchModel();

        psm.addRefinementValues('sfraTest', 'test');

        psm.search();

        let productHits = psm.getProductSearchHits();

        res.print(psm.count);

        next();
    }
);

server.get(
    'RenderSlotContent',
    server.middleware.https,
    function (req, res, next) {
        res.render('/components/slotContent');
        next();
});

server.post(
    'ChangeCustomerDetails',
    server.middleware.https,
    csrfProtection.validateRequest,
    function (req, res, next) {
        let CustomerMgr = require('dw/customer/CustomerMgr');
        let customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
        let profile = customer.getProfile();

        let customerDetailsForm = server.forms.getForm('customerDetailsForm');
        let firstName = customerDetailsForm.firstName.htmlValue;
        let lastName = customerDetailsForm.lastName.htmlValue;

        if (!empty(firstName) && !empty(lastName)) {
            let Transaction = require('dw/system/Transaction');
            Transaction.wrap(function () {
                profile.setFirstName(firstName);
                profile.setLastName(lastName);
            });
        }

        let URLUtils = require('dw/web/URLUtils');
        res.redirect(URLUtils.url('Custom-CustomerDetails'));
        next();
});

module.exports = server.exports();
