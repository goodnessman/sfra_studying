'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'price', {
        enumerable: true,
        value: (function() {
            let priceModel = apiProduct.getPriceModel();
            return apiProduct.getPriceModel().getPrice().getValue();
        })()
    });
};
