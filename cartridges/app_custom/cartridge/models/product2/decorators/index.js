'use strict';

module.exports = {
    base: require('*/cartridge/models/product2/decorators/base'),
    availability: require('*/cartridge/models/product2/decorators/availability'),
    price: require('*/cartridge/models/product2/decorators/price')
};
