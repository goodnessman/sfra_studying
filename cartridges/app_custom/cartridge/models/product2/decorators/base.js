'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'id', {
        enumerable: true,
        value: apiProduct.getID()
    });
};
