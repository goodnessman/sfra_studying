'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'available', {
        enumerable: true,
        value: true
    });
};
