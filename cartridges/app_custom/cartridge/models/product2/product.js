'use strict';

var decorators = require('*/cartridge/models/product2/decorators/index');

module.exports = function product(product, apiProduct) {
    decorators.base(product, apiProduct);
    decorators.price(product, apiProduct);
    decorators.availability(product, apiProduct);

    return product;
};
