'use strict';

var decorators = require('*/cartridge/models/customer/decorators/index');

module.exports = function currentCustomer(request) {
    var customer = {};
    decorators.ip(customer, request);

    return customer;
};
