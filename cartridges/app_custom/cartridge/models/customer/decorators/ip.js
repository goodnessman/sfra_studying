'use strict';

module.exports = function (object, request) {
    Object.defineProperty(object, 'ip', {
        enumerable: true,
        value: request.httpRemoteAddress
    });
    Object.defineProperty(object, 'lastVisitedTime', {
        enumerable: true,
        value: Date.now()
    });
};