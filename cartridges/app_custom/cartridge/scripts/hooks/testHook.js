
'use strict';

exports.update = function (viewData) {
    if (viewData && typeof(viewData.updated) === 'boolean') {
        viewData.updated = true;
    }
}