/* eslint-disable no-undef */
module.exports = {
    /**
     * @description Retrieves configuration value by Id form SitePreferences
     * @param {string} prefName The name of the preference to get value
     * @returns {string|number|boolean|string[]|Date|EnumValue<string|number>[]|number[]|MarkupText} value
     */
    getValue: function (prefName) {
        return empty(prefName)
            ? null
            : require('dw/system/Site').getCurrent().getCustomPreferenceValue(prefName);
    },

    /**
     * @description Retrieves configuration value by Id form SitePreferences and try to parse it as JSON
     * @param {string} prefName The name of the preference to get value
     * @returns {object} value
     */
    getJSONValue: function (prefName) {
        var pref = this.getValue(prefName);
        var result = null;
        if (!pref) {
            return result;
        }

        try {
            result = JSON.parse(pref);
        } catch (e) {
            return null;
        }

        return result;
    }
};
