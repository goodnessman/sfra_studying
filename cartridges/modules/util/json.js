module.exports = {

    /**
     * @description General wrapper for JSON.parse(...) with error catching
     * @param {string} stringified The string object representation to parse
     * @param {string} defaultObject The default object
     * @returns {Object} Parsed object
     */
    parse: function (stringified, defaultObject) {
        var parsed = {};

        if (!stringified) {
            return defaultObject;
        }

        try {
            parsed = JSON.parse(stringified);
        } catch (e) {
            return defaultObject;
        }

        return parsed;
    },

    /**
     * @description Test if string could be parsed to JSON
     * @param  {string} str The string object
     * @returns {boolean} isJSON flag
     */
    isJSON: function (str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }

        return true;
    }
};
