/**
 * _preferences.properties helper module
 * @module pref
 */
const Resource = require('dw/web/Resource');

module.exports = {
    /**
     * @description get preference from resource bundle defined in _preference.properties file
     * @param {string} key Key from properties file
     * @param {string} defaultValue Default value will be returned if there is no such key
     * @returns {string} Preference value
     */
    get: function (key, defaultValue) {
        return Resource.msg(key, '_preferences', (typeof defaultValue !== 'undefined') ? defaultValue : '');
    },

    /**
     * @description Get formated preference from resource bundle defined in _preference.properties file
     * @param {string} key Key from properties file
     * @param {string} defaultValue Default value will be returned if there is no such key
     * @param {Object} args Arguments to format, can be single value or an Array
     * @returns {string} Formated string value of preference
     */
    getf: function (key, defaultValue, args) {
        return Resource.msgf(key, '_preferences', (typeof defaultValue !== 'undefined') ? defaultValue : '', args);
    },

    /**
     * @description Checking boolean preference from resource bundle defined in _preference.properties file
     * @param {string} key Key from properties file
     * @param {string} bundle Resource bundle
     * @returns {string} Returns true if value of resource bundle is boolean true
     */
    is: function (key, bundle) {
        return Resource.msg(key, bundle || '_preferences', '') === 'true';
    },

    /**
     * @description Checking if value exists and not empty in resource bundle  defined in _preference.properties file
     * @param {string} key Key from properties file
     * @param {string} bundle Resource bundle
     * @returns {boolean} Returns true if value of resource bundle doesn't exists or empty
     */
    isEmpty: function (key, bundle) {
        // eslint-disable-next-line no-undef
        return empty(Resource.msg(key, bundle || '_preferences', ''));
    }
};
