const type = require('./type');

/**
 * Object.assign polyfill
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 * @param {Object} target
 * @returns {Object}
 */
function assign(target) {
    if (type.isNull(target) || type.isUndefined(target)) {
        throw new TypeError('Cannot convert undefined or null to object');
    }

    const to = Object(target);

    for (let index = 1; index < arguments.length; index++) {
        let nextSource = arguments[index];

        if (type.isNull(nextSource) || type.isUndefined(nextSource)) {
            continue;
        }

        // eslint-disable-next-line no-restricted-syntax
        for (let nextKey in nextSource) {
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                to[nextKey] = nextSource[nextKey];
            }
        }
    }

    return to;
}

/**
 * @param {Object} target
 * @returns {Object}
 */
function deepAssign(target) {
    const sources = Array.prototype.slice.call(arguments, 1);

    return sources.reduce(function (prev, obj) {
        Object.keys(obj).forEach(function (key) {
            const pVal = prev[key];
            const oVal = obj[key];

            if (Array.isArray(pVal) && Array.isArray(oVal)) {
                prev[key] = pVal.concat(oVal);
            } else if (type.isObject(pVal) && type.isObject(oVal)) {
                prev[key] = deepAssign(pVal, oVal);
            } else {
                prev[key] = oVal;
            }
        });

        return prev;
    }, target);
}

/**
 * Object.keys
 * @param {Object} obj
 * @returns {Object}
 */
function keys(obj) {
    return Object.keys(obj);
}

/**
 * Object.values
 * @param {Object} obj
 * @returns {Object}
 */
function values(obj) {
    return Object.keys(obj).map(function (key) {
        return obj[key];
    });
}

/**
 * Object.entries
 * @param {Object} obj
 * @returns {Object}
 */
function entries(obj) {
    const ownProps = Object.keys(obj);
    let i = ownProps.length;
    const resArray = new Array(i);

    while (i--) {
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
    }

    return resArray;
}

/**
 * @param {Object} obj
 * @returns {dw.util.HashMap}
 */
function toHashMap(obj) {
    const HashMap = require('dw/util/HashMap');
    const result = new HashMap();

    Object.keys(obj).forEach(function (key) {
        result.put(key, obj[key]);
    });

    return result;
}

/**
 * @param {dw.util.HashMap} obj
 * @returns {Object}
 */
function fromHashMap(obj) {
    const result = {};
    const entryIterator = obj.entrySet().iterator();

    while (entryIterator.hasNext()) {
        let entry = entryIterator.next();
        let key = entry[0];
        let value = entry[1];

        result[key] = value;
    }

    return result;
}

/**
 * Access given properties of an object recursively
 * @param {Object} obj The object
 * @param {string} path The property string, i.e. 'data.myValue.prop1'
 * @return {Object} The value of the given property or undefined
 */
function get(obj, path) {
    const propPath = (path || '').split('.');
    let result = obj;

    propPath.forEach(function (prop) {
        if (result && prop in result) {
            result = result[prop];
        } else {
            result = undefined;
        }
    });

    return result;
}

/**
 * Sets the value at path of object.
 * If a portion of path doesn't exist, it's created.
 * @param {Object} obj
 * @param {string} path
 * @param {*} value
 * @return {Object}
 */
function set(obj, path, value) {
    const propPath = (path || '').split('.');
    let nested = obj;
    let lastIndex = propPath.length - 1;

    propPath.forEach(function (prop, i) {
        if (!nested[prop]) {
            nested[prop] = {};
        }

        if (i !== lastIndex) {
            nested = nested[prop];
        } else {
            nested[prop] = value;
        }
    });

    return obj;
}

module.exports = {
    assign: assign,
    deepAssign: deepAssign,
    keys: keys,
    values: values,
    entries: entries,
    toHashMap: toHashMap,
    fromHashMap: fromHashMap,
    get: get,
    set: set
};
