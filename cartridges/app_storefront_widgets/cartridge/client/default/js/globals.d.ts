/**
* Is production build?
*/
declare const PRODUCTION: boolean

declare interface HTMLElement {
    ['@@_widget_instance_@@']: undefined | import('widgets/Widget').default;
    ['@@_widget_events_disposable_@@']: Function[] | undefined;
    ['@@@_template']: string | undefined;
    ['@_hideTimer']: NodeJS.Timeout;
}

declare type NoReadonlyKeys<T> = { [P in keyof T]: 'readonly' extends keyof T[P] ? never : P }[keyof T]
