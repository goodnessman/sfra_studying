// TO BE REVIEWED
// TODO: we need to replace basicForm usage inside all template to switch to data-widget="form"
// BasicForm is more like abstract layer.
// AjaxForm and Form are concrete implementations in case of XHR and POST submissions
// It's more accurate from the architecture perspective - it will show us that basicForm - it’s interface object
/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicForm').default>} BasicForm
 */

/**
 * @description Base Form implementation
 * @param {BasicForm} BasicForm Base widget for extending
 * @returns {typeof Form} Form class
 */
export default function (BasicForm) {
    /**
     * @class Form
     * @augments BasicForm
     * @classdesc Form widget implementation. Mostly inherits generic functionality.
     * @property {string} data-widget - Widget name `form`
     * @property {string} data-event-submit - Event listener for form submission
     * @example
     * // use this code to display widget
     * <form
     *     action="${URLUtils.url('Order-Track')}"
     *     method="GET"
     *     data-widget="form"
     *     data-event-submit="handleSubmit"
     * >
     *     ... form contents
     * </form>
     */
    class Form extends BasicForm {

    }

    return Form;
}
