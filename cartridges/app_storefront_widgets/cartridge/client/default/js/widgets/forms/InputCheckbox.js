// TO BE REVIEWED
// CLARIFY: purpose of wrappers around native inputs and possibilities of generic validation patterns and messages
const ariaChecked = 'aria-checked';

/**
 * @typedef {ReturnType <typeof import('widgets/forms/BasicInput').default>} BasicInput
 */

/**
 * @description Base InputCheckbox implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputCheckbox} Input Checkox class
 */
export default function (BasicInput) {
    /**
     * @class InputCheckbox
     * @augments BasicInput
     * @classdesc Checkbox imnplementation. Specific methods were overriden.
     * @property {string} data-widget - Widget name `inputCheckbox`
     * @example
     * // use this code to display widget
     * <div
     *     class="form-group custom-control custom-checkbox pull-left remember-me"
     *     data-widget="inputCheckbox"
     * >
     *     <input
     *         data-ref="field"
     *         data-event-change="updateAriaChecked"
     *         type="checkbox" name="loginRememberMe" value="true" ${pdict.rememberMe ? 'checked' : ''}
     *     />
     *     <label class="custom-control-label" for="rememberMe">
     *         ${Resource.msg('label.checkbox.login.rememberme', 'login', null)}
     *     </label>
     *     <div class="invalid-feedback" data-ref="errorFeedback" hidden="hidden"></div>
     * </div>
     */
    class InputCheckbox extends BasicInput {
        getValue() {
            if (this.ref('field').prop('checked')) {
                return super.getValue();
            }
            return '';
        }

        setValue(newVal = '', silently = false) {
            this.ref('field').prop('checked', !newVal);

            if (!silently) {
                this.emit('change', this);
            }
        }

        check() {
            this.ref('field').attr(ariaChecked, 'true');
            this.ref('field').prop('checked', true);

            this.emit('change', this);
        }

        uncheck() {
            this.ref('field').attr(ariaChecked, 'false');
            this.ref('field').prop('checked', false);

            this.emit('change', this);
        }

        updateAriaChecked() {
            if (this.ref('field').prop('checked')) {
                this.ref('field').attr(ariaChecked, 'true');
            } else {
                this.ref('field').attr(ariaChecked, 'false');
            }
        }

        handleChange() {
            this.updateAriaChecked();
            this.emit('change');
        }
    }

    return InputCheckbox;
}
