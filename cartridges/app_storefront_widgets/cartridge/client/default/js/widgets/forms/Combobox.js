// TO BE REVIEWED
import { clickOutside } from 'widgets/toolbox/util';

const KEY_DOWN = 40;
const KEY_UP = 38;
const KEY_ESCAPE = 27;
const KEY_RETURN = 13;
const KEY_TAB = 9;

/**
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 */

/**
 * @description Base Combobox implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof Combobox} Combobox class
 */
export default function (BasicInput) {
    /*
     * This content is based on w3.org design pattern examples and licensed according to the
     * W3C Software License at
     * https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
     * Please see specification:
     * https://www.w3.org/TR/wai-aria-practices/#combobox
     */

    /**
     * @class Combobox
     * @augments BasicInput
     * @classdesc Combobox implementation in accordance with accessibility statements
     * <br>Class could be extended to implement any provider-specific
     * remote/local data fetching for Combobox inner elements.
     * <br>It is needed to implement `getSuggestions` method in a way,
     * that is specific for subclasses
     * <br>Supports keyboard navigation among Combobox items.
     * <br>As far as Combobox could be used only in a subclasses, example, given below, is related to known subclass `searchBox`
     * @property {string} data-widget - Widget name `searchBox`
     * @property {string} data-url - URL to obtain serach suggestions from server based on customer's input
     * @property {boolean} data-close-on-tab - If true - `tab` keypress will close listbox
     * @property {string} data-widget-event-closesearch - An event, fired when `close` element was pressed
     * @property {string} data-event-keydown - event handler for `keydown` event
     * @property {boolean} data-close-from-outside - config, which shows, if combobox should be closed when click outside
     * @property {number} data-min-chars - minimum characters to trigger show combobox
     * @property {number} data-max-chars - maximum characters to enter
     * @property {number} data-updating-delay - update listbox delay
     * @property {string} data-classes-focused-item - class for currently focused item
     * @property {string} data-classes-active-list-box - active listbox class
     * @example
     * // use this code to display minicart widget
     * <div
     *     data-widget="searchBox"
     *     data-url="${URLUtils.url('SearchServices-GetSuggestions')}"
     *     data-close-on-tab="true"
     * >
     *     <form>
     *         ... serach input
     *     </form>
     *     ...
     *     <div data-ref="listbox" data-event-mouseenter="markHover" data-event-mouseleave="unMarkHover">
     *         <div role="none" class="b-suggestions-inner" data-ref="listboxInner"></div>
     *     </div>
     *     <script data-ref="template" type="template/mustache">
     *         <div data-ref="listboxInner">
     *             ... search suggestions
     *         </div>
     *     </script>
     * </div>
     */
    class Combobox extends BasicInput {
        prefs() {
            return {
                minChars: 3,
                maxChars: 50,
                updatingDelay: 300,
                closeFromOutside: true,
                closeOnTab: true,
                classesFocusedItem: 'm-focused',
                classesActiveListBox: 'm-active',
                ...super.prefs()
            };
        }

        init() {
            super.init();

            this.lastSearchedTerm = '';
            this.selectedIndex = -1;
            this.resultsCount = 0;
            this.isListboxOpen = false;
        }

        handleInput() {
            const inputLength = this.ref('input').prop('value').length;
            if (inputLength >= this.prefs().minChars && inputLength <= this.prefs().maxChars) {
                this.updateListbox();
            } else {
                clearTimeout(this.timeout);
                this.closeListbox();
            }
        }

        markHover() {
            this.hasHoverWithin = true;
        }

        unMarkHover() {
            this.hasHoverWithin = false;
        }

        handleFocus() {
            this.updateListbox();
        }

        handleBlur() {
            if (this.hasHoverWithin || this.selectedIndex < 0) {
                return;
            }
            this.closeListbox();
        }

        /**
         * @param {HTMLElement} _ - source of keydown event
         * @param {Event} event - keydown event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case KEY_ESCAPE:
                    this.closeListbox();
                    preventEventActions = true;
                    break;
                case KEY_UP:
                    this.setSelectToNextItem();
                    this.selectItem(this.getItemByIndex());
                    preventEventActions = true;
                    break;
                case KEY_DOWN:
                    this.setSelectToPreviousItem();
                    this.selectItem(this.getItemByIndex());
                    preventEventActions = true;
                    break;
                case KEY_RETURN:
                    this.activateItem(this.getItemByIndex());
                    return;
                case KEY_TAB:
                    if (this.prefs().closeOnTab) {
                        this.closeListbox();
                    }
                    return;
                default:
                    return;
            }

            if (preventEventActions) {
                event.stopPropagation();
                event.preventDefault();
            }
        }

        setSelectToPreviousItem() {
            if (this.selectedIndex === -1 || this.selectedIndex >= this.resultsCount - 1) {
                this.selectedIndex = 0;
            } else {
                this.selectedIndex += 1;
            }
        }

        setSelectToNextItem() {
            if (this.selectedIndex <= 0) {
                this.selectedIndex = this.resultsCount - 1;
            } else {
                this.selectedIndex -= 1;
            }
        }

        /**
         * @param {refElement} selectedItem - item, selected by user
         */
        selectItem(selectedItem) {
            if (this.currentSelected) {
                this.deselectItem(this.currentSelected);
            }

            if (selectedItem) {
                this.ref('input').attr('aria-activedescendant', `result-item-${this.selectedIndex}`);
                selectedItem
                    .addClass(this.prefs().classesFocusedItem)
                    .attr('aria-selected', 'true');
                this.afterItemSelected(selectedItem);
            } else {
                this.ref('input').attr('aria-activedescendant', '');
            }

            this.currentSelected = selectedItem;
        }

        /**
         * @param {refElement} item - item, selected by user
         */
        afterItemSelected(item) {
            this.ref('input').prop('value', item.data('suggestionValue'));
        }

        /**
         * @param {refElement} item - item, selected by user
         */
        deselectItem(item) {
            item
                .removeClass(this.prefs().classesFocusedItem)
                .attr('aria-selected', 'false');
        }

        /**
         * @param {HTMLElement} item - item, selected by user
         */
        activateItem(activeItem) { // "Activate" item by press Enter on selected item or mouse click on item
            if (activeItem) {
                this.ref('input')
                    .prop('value', activeItem.data('suggestionValue'))
                    .get().focus();
                this.closeListbox();
            }
        }

        updateListbox() {
            const inputValue = this.ref('input').prop('value');

            if (this.lastSearchedTerm === inputValue || inputValue.length < this.minChars) {
                // could be triggered from focus so we need additionally check minChars
                return;
            }

            clearTimeout(this.timeout);
            this.timeout = setTimeout(this.getSuggestions.bind(this, inputValue), this.prefs().updatingDelay);
        }

        /**
         * @param {string} query - a query, which needs to be processed in subclasses
         */
        getSuggestions(query) {
            // This is example of getSuggestion method to implement
            // Comboboxes like search or address should implement his own methods
            // of how to get suggestion and render it. Template should follow this:
            // <li role="option" id="result-item-${0}">${results[0]}</li>
            // ...
            // <li role="status" aria-live="polite">${results.length} suggestion found</li>
            // You should always include ids of suggestion and status about how much
            // suggestion total.
            const resultsTotal = 1;
            this.afterSuggestionsUpdate(query, resultsTotal);
        }

        /**
         * @param {string} query - a query, used for fetching Combobox inner items
         * @param {number} resultsCount - obtained inner items count
         */
        afterSuggestionsUpdate(query, resultsCount) {
            this.resultsCount = resultsCount;
            this.selectedIndex = -1;
            this.lastSearchedTerm = query;
            this.openListbox();
        }

        openListbox() {
            this.isListboxOpen = true;

            this.ref('listbox')
                .addClass(this.prefs().classesActiveListBox)
                .attr('aria-hidden', 'false');
            this.ref('input')
                .attr('aria-activedescendant', '')
                .attr('aria-expanded', 'true'); // Should be combobox node by specs
            this.toggleOverlay(true);

            if (this.prefs().closeFromOutside) {
                this.bodyClickListener = clickOutside(this.ref('self'), this.closeListbox.bind(this));
                this.onDestroy(this.bodyClickListener);
            }
        }

        closeListbox() {
            this.resultsCount = 0;
            this.selectedIndex = -1;
            this.lastSearchedTerm = '';
            this.isListboxOpen = false;

            this.ref('listbox')
                .removeClass(this.prefs().classesActiveListBox)
                .attr('aria-hidden', 'true');
            this.ref('input')
                .attr('aria-activedescendant', '')
                .attr('aria-expanded', 'false'); // Should be combobox nore by specs
            this.toggleOverlay(false);

            if (this.bodyClickListener) {
                this.bodyClickListener();
                this.bodyClickListener = undefined;
            }

            this.afterCloseListbox();
        }

        afterCloseListbox() {
            this.ref('listbox').empty();
        }

        /**
         * @param {boolean} isShown - does overlay should be shown
         */
        toggleOverlay(isShown) {
            this.isOverlayVisible = isShown;
        }

        getItemByIndex() {
            if (this.selectedIndex < 0) {
                return null;
            }

            const listItem = this.ref(`result-item-${this.selectedIndex}`);
            if (listItem.length) {
                return listItem;
            }

            return null;
        }
    }

    return Combobox;
}
