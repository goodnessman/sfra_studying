// TO BE REVIEWED

// @ts-ignore
import { RefElement } from 'widgets/toolbox/RefElement';

/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 * @typedef {InstanceType <typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 */

/**
 * @description Base InputSelect implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputSelect} Input Select class
 */
export default function (BasicInput) {
    /**
     * @class InputSelect
     * @augments BasicInput
     * @classdesc Select input imnplementation. Specific methods were overriden.
     * @property {string} data-widget - Widget name `inputSelect`
     * @example
     * // use this code to display widget
     * <div class="form-group
     *     ${billingFields.country.htmlName}"
     *     data-widget="inputSelect"
     *     data-id="${billingFields.country.htmlName}"
     * >
     *     <label class="form-control-label" for="billingCountry">
     *          ${Resource.msg('field.billing.address.country','address',null)}
     *     </label>
     *         <select
     *             data-ref="field"
     *             data-event-blur="validate"
     *             id="billingCountry"
     *         >
     *             <isloop items=${billingFields.country.options} var="country">
     *                 <option id="${country.id}" value="${country.htmlValue}">${country.label}</option>
     *             </isloop>
     *         </select>
     *         <div class="invalid-feedback" data-ref="errorFeedback"></div>
     *     </div>
     * </div>
     */
    class InputSelect extends BasicInput {
        init() {
            super.init();
            const field = /** @type {HTMLSelectElement|undefined} */(this.ref('field').get());
            // Reset value when for some reason no option preselected, even first (default)
            if (field && !field.value) {
                const option = Array.from(field.options).find(elem => elem.hasAttribute('selected'));
                let val = option && option.value;

                if (!val) {
                    val = this.getValue();
                }

                this.setValue(val, true);
            }
        }

        /**
         * @param {RefElement} el source of event
         * @param {(Event|undefined)} event event instance if DOM event
         */
        onChange(el, event) {
            if (!this.config.dontPrevent && event && event instanceof Event) {
                event.preventDefault();
            }

            this.update();
        }

        /**
         * @param {string} newVal new value
         * @param {boolean} silently validate after change
         */
        setValue(newVal, silently) {
            let val = newVal;
            if (this.initValue && val === undefined) {
                val = this.initValue;
            }
            super.setValue(val, silently);
        }

        getValue() {
            const selectedOption = this.getSelectedOptions();
            if (selectedOption) {
                if (selectedOption.length) {
                    return /** @type {string} */(selectedOption.val());
                }
                const field = /** @type {HTMLSelectElement|undefined} */(this.ref('field').get());

                if (field && field.options.length) {
                    const item = field.options.item(0);
                    if (item) {
                        return item.value;
                    }
                }
            }
            return '';
        }

        getSelectedOptions() {
            const field = /** @type {HTMLSelectElement|undefined} */(this.ref('field').get());

            if (field && field.selectedOptions) {
                return new RefElement(Array.from(field.selectedOptions));
            }
            return undefined;
        }

        getText() {
            const field = this.ref('field').get();

            if (field) {
                const selectedNode = Object.values(field.childNodes)
                    .find(node => /** @type {HTMLOptionElement} */(node).selected);

                if (selectedNode) {
                    return selectedNode.textContent || '';
                }
                return '';
            }
            return '';
        }

        lock() {
            if (!this.locked) {
                super.lock();
                this.ref('field').attr('disabled', true);
            }
        }

        unlock() {
            if (this.locked) {
                super.unlock();
                this.ref('field').attr('disabled', false);
            }
        }

        changeAttribute() {} // eslint-disable-line
    }

    return InputSelect;
}
