/**
 * @typedef {ReturnType<typeof import('./InputText').default>} InputText
 */
/**
 * @description Base InputTel implementation
 * @param {InputText} InputText Base widget for extending
 * @returns {typeof InputTel} Input Tel class
 */
export default function (InputText) {
    /**
     * @class InputTel
     * @augments InputText
     * @classdesc Input type Tel implementation
     * @property {string} data-widget - Widget name `inputTel`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputTel"
     * >
     *     <label class="form-control-label" for="tel">
     *         <isprint value="${pdict.profileForm.customer.tel.label}" encoding="htmlcontent" />
     *     </label>
     *     <input
     *         type="tel"
     *         data-ref="field"
     *         data-event-blur="validate"
     *     >
     *     <div class="invalid-feedback"></div>
     * </div>
     */
    class InputTel extends InputText {

    }

    return InputTel;
}
