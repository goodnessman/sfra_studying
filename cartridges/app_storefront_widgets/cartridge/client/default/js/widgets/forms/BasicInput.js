// TO BE REVIEWED
import { scrollIntoView } from 'widgets/toolbox/scroll';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base BasicInput implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof BasicInput} Basic Input class
 */
export default function (Widget) {
    /**
     * @class BasicInput
     * @augments Widget
     * @classdesc Basic Input Widget (like abstract ancestor), contains basic validation and input management logic.
     * <br>Supposes valid unified input markup. Contains also error messaging.
     * <br>Usually is not used directly, and relevant subclasses should be used.
     * <br>Below data attributes and example will be provided for `InputText` subclass
     * @property {string} data-widget - Widget name `inputText`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputText" class="form-group
     *     <isif condition="${pdict.profileForm.customer.firstname.mandatory === true}">required</isif>"
     * >
     *     <label class="form-control-label" for="firstName">
     *         <isprint value="${pdict.profileForm.customer.firstname.label}" encoding="htmlcontent" />
     *     </label>
     *     <input type="text" class="form-control" id="firstName"
     *         data-ref="field"
     *         data-event-blur="validate"
     *         <isprint value="${pdict.profileForm.customer.firstname.attributes}" encoding="off" />
     *     >
     *     <div class="invalid-feedback" data-ref="errorFeedback"></div>
     * </div>
     */
    class BasicInput extends Widget {
        prefs() {
            return {
                skipValidation: false,
                classesErrorState: 'm-invalid',
                classesValidState: 'm-valid',
                classesDisabled: 'm-disabled',
                classesLocked: 'm-locked',
                classesWrapperErrorState: 'm-invalid',
                classesWrapperValidState: 'm-valid',
                validationConfig: {},
                ...super.prefs()
            };
        }

        init() {
            this.initValue = this.getValue();
            if (!this.id && this.ref('field').attr('name')) {
                this.id = this.ref('field').attr('name');
            }
            this.disabled = this.ref('field').attr('disabled') === 'disabled';
        }

        /**
         * @returns {string} - return input name
         */
        getValue() {
            // @ts-ignore
            return (this.ref('field').val());
        }

        /**
         * @returns {string} - return input name
         */
        getName() {
            // @ts-ignore
            return this.ref('field').attr('name');
        }

        focus() {
            var field = this.ref('field').get();
            if (field) {
                field.focus();
            }
        }

        setFocus() {
            const elementToScroll = this.ref('self').get();

            if (elementToScroll) {
                scrollIntoView(elementToScroll);
            }

            this.focus();
        }

        blur() {
            var field = this.ref('field').get();
            if (field) {
                field.blur();
            }
        }

        /**
         * @param {(string|number|undefined)} [newVal] - set this value to input
         * @param {(boolean|undefined)} [silently] - if set to `true` - input
         * should not be validated against a new value
         */
        setValue(newVal = '', silently = false) {
            this.ref('field').val(String(newVal));
            if (!silently) {
                this.update();
            }
        }

        update() {
            this.validate();
            this.emit('change', this);
        }

        clearError() {
            this.ref('field').removeClass(this.prefs().classesErrorState);
            this.ref('field').removeClass(this.prefs().classesValidState);
            this.ref('self').removeClass(this.prefs().classesWrapperErrorState);
            this.ref('self').removeClass(this.prefs().classesWrapperValidState);

            this.ref('errorFeedback').hide();

            this.emit('inputstatechanged');
        }

        /**
         * @param {string} [error] error message
         */
        setError(error) {
            if (error) {
                this.ref('field').removeClass(this.prefs().classesValidState);
                this.ref('self').removeClass(this.prefs().classesWrapperValidState);

                this.ref('field').addClass(this.prefs().classesErrorState);
                this.ref('self').addClass(this.prefs().classesWrapperErrorState);

                this.ref('errorFeedback').setText(error).show();
            } else {
                this.ref('field').removeClass(this.prefs().classesErrorState);
                this.ref('self').removeClass(this.prefs().classesWrapperErrorState);

                this.ref('field').addClass(this.prefs().classesValidState);
                this.ref('self').addClass(this.prefs().classesWrapperValidState);

                this.ref('errorFeedback').hide();
            }

            this.emit('inputstatechanged');
        }

        isValid() {
            const field = this.ref('field');
            var { state, msg } = field.getValidity()
                || { msg: '', state: /** @type {ValidityState} */({ valid: true }) };

            const validation = this.prefs().validationConfig;

            if ((state.patternMismatch || state.typeMismatch)) {
                msg = validation.errors.parse || validation.errors.security;
            } else if (
                (state.rangeOverflow || state.rangeUnderflow || state.tooLong || state.tooShort)
            ) {
                if (state.rangeOverflow || state.tooLong) {
                    msg = validation.errors.maxLength;
                } else if (state.rangeUnderflow || state.tooShort) {
                    msg = validation.errors.minLength;
                }
            } else if (state.valueMissing) {
                msg = validation.errors.required;
            }

            if (state.valid && this.widgetToMatch
                && this.widgetToMatchOpts
                && this.widgetToMatch.data('getValue') !== this.getValue()
            ) {
                this.error = this.widgetToMatchOpts.msg;
                return false;
            }

            this.error = msg;
            return state.valid;
        }
        // /**
        //  * @private
        //  * @param {number} minLength minLength
        //  * @param {number} maxLength maxLength
        //  * @param {any} value value
        //  * @param {string} error error
        //  * @deprecated
        //  */
        // _doValidate(minLength, maxLength, value, error) {
        //     if (this.config.rangelengthtext && minLength && maxLength &&
        //         (value.length < minLength || value.length > maxLength)) {
        //         error = format((this.config.rangelengthtext + ''), String(minLength), String(maxLength));
        //     } else if (minLength && value.length < minLength) {
        //         error = format(String(this.config.minlengthtext), String(minLength));
        //     } else if (maxLength && value.length > maxLength) {
        //         error = format(String(this.config.maxlengthtext), String(maxLength));
        //     } else if (this.config.regEx && !(new RegExp(String(this.config.regEx), 'g')).test(value)) {
        //         error = String(this.config.parseError);
        //     }
        //     return error;
        // }

        validate() {
            if (!this.shown || this.disabled || this.prefs().skipValidation) {
                return true;
            }

            const valid = this.isValid();

            if (valid) {
                this.setError();
            } else {
                this.setError(this.error);
            }

            return valid;
        }

        disable() {
            this.disabled = true;
            this.ref('field').disable();
            this.ref('self').addClass(this.prefs().classesDisabled);
            return this;
        }

        enable() {
            this.disabled = false;
            this.ref('field').enable();
            this.ref('self').removeClass(this.prefs().classesDisabled);
            return this;
        }

        lock() {
            this.locked = true;
            this.ref('field').attr('readonly', true);
            this.ref('self').addClass(this.prefs().classesLocked);
        }

        unlock() {
            this.locked = false;
            this.ref('field').attr('readonly', false);
            this.ref('self').removeClass(this.prefs().classesLocked);
        }

        isDisabled() {
            return this.disabled;
        }

        /**
         * @param {BasicInput} widgetToMatch cmp
         * @param {{[x: string]: string|undefined}} options to compare
         */
        setMatchCmp(widgetToMatch, options = {}) {
            this.widgetToMatch = widgetToMatch;
            this.widgetToMatchOpts = options;
        }

        skipSubmission() {
            return false;
        }
    }

    return BasicInput;
}
