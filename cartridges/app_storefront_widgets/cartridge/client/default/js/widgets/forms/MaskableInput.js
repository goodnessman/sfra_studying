/**
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @typedef {ReturnType<typeof import('./InputText').default>} InputText
*/

/**
 * @description Base MaskableInput implementation
 * @param {InputText} InputText Base widget for extending
 * @returns {typeof MaskableInput} Input Email class
 */
export default function (InputText) {
    /**
     * @class MaskableInput
     * @augments InputText
     * @classdesc Adds maskable InputMask (customised library) implementation.
     * As a basis was taken [this library](https://github.com/ViniChab/supermask.js).
     * On widget level you mustr specify 4 event listeners, as described in properties.
     * @property {string} data-mask - Mask for widget in format "+99(9)99-999-999", "9-999-9999-999" etc
     * @property {string} data-event-input - Field `input` event handler
     * @property {string} data-event-keypress - Field `keypress` event handler
     * @property {string} data-event-change - Field `change` event handler
     * @property {string} data-event-paste - Field `paste` event handler
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputTel" class="form-group" required
     *     data-mask="9-999-999-9999"
     * >
     *     <label class="form-control-label" for="tel">
     *         <isprint value="${pdict.profileForm.customer.tel.label}" encoding="htmlcontent" />
     *     </label>
     *     <input type="text" class="form-control" id="tel"
     *         data-ref="field"
     *         data-event-input="onInput"
     *         data-event-keypress="onKeypress"
     *         data-event-change="onChange"
     *         data-event-paste.prevent="onPaste"
     *         <isprint value="${pdict.profileForm.customer.tel.attributes}" encoding="off" />
     *     >
     *     <div class="invalid-feedback" data-ref="errorFeedback"></div>
     * </div>
     * @returns {typeof MaskableInput} MaskableInput class;
     */
    class MaskableInput extends InputText {
        prefs() {
            return {
                mask: '',
                ...super.prefs()
            };
        }

        /**
         * @description Overloaded `setValue` in order to trigger mask revalidation
         * @param {(string|number|undefined)} [newVal] - set this value to input
         * @param {(boolean|undefined)} [silently] - if set to `true` - input
         * should not be validated against a new value
         */
        setValue(newVal = '', silently = false) {
            super.setValue(newVal, silently);

            const { field, mask } = this.getFieldAndMask();
            if (!this.inputMask || !field) {
                return;
            }

            this.inputMask.validate(field, mask);
        }

        init() {
            super.init();

            import(/* webpackChunkName: 'inputmask' */'widgets/toolbox/InputMask').then((InputMask) => {
                // eslint-disable-next-line new-cap
                this.inputMask = new InputMask.default();
                this.onDestroy(() => {
                    this.inputMask = undefined;
                });

                const { field, mask } = this.getFieldAndMask();
                if (!field) {
                    return;
                }

                this.inputMask.validate(field, mask);
            });
        }

        /**
         * @description Get field and mask for current maskable widget instance
         * @returns {{field: HTMLElement|undefined, mask: string}} Object, containig field and mask and field ref element
         */
        getFieldAndMask() {
            const field = this.ref('field');
            return {
                field: field.get(),
                mask: this.prefs().mask
            };
        }

        /**
         * @description On input logic
         * @param {RefElement} element - target input
         * @param {Event} event - target event
         */
        onInput(element, event) {
            super.onInput(element, event);

            const { field, mask } = this.getFieldAndMask();
            if (!this.inputMask || !field) {
                return;
            }

            this.inputMask.onInput(field, mask, event);
        }

        /**
         * @description On keypress logic
         * @param {RefElement} element - target input
         * @param {Event} event - target event
         */
        onKeypress(element, event) {
            const { field, mask } = this.getFieldAndMask();
            if (!this.inputMask || !field) {
                return;
            }

            this.inputMask.onKeypress(field, mask, event);
        }

        /**
         * @description On change logic
         */
        onChange() {
            const { field, mask } = this.getFieldAndMask();
            if (!this.inputMask || !field) {
                return;
            }

            this.inputMask.onChange(field, mask);
        }

        /**
         * @description On paste logic
         * @param {RefElement} element - target input
         * @param {Event} event - target event
         */
        onPaste(element, event) {
            const { field, mask } = this.getFieldAndMask();
            if (!this.inputMask || !field) {
                return;
            }

            this.inputMask.onPaste(field, mask, event);
        }
    }

    return MaskableInput;
}
