/**
 * @typedef {ReturnType<typeof import('widgets/forms/InputText').default>} InputText
 */

/**
 * @description Base InputTextarea implementation
 * @param {InputText} InputText Base widget for extending
 * @returns {typeof InputTextarea} Input Textarea class
 */
export default function (InputText) {
    /**
     * @class InputTextarea
     * @augments InputText
     * @classdesc Input type InputTextarea implementation
     * @property {string} data-widget - Widget name `inputTextarea`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputText"
     * >
     *     <label class="form-control-label" for="inputText">
     *         <isprint value="${pdict.profileForm.customer.firstname.label}" encoding="htmlcontent" />
     *     </label>
     *     <textarea
     *         type="text"
     *         id="inputText"
     *         data-ref="field"
     *         data-event-blur="validate"
     *     ></textarea>
     *     <div class="invalid-feedback"></div>
     * </div>
     */
    class InputTextarea extends InputText {
        onInput() {
            if (this.has('maxcounter')) {
                const maxLength = Number(this.ref('field').attr('maxlength'));
                const currentInputLength = this.ref('field').val().length;

                this.ref('maxcounter').setText(String(maxLength - currentInputLength));
            }
        }
    }

    return InputTextarea;
}
