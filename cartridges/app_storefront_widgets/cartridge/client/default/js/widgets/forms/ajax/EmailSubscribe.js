// TODO: should be binded to signup component
// TODO: review backend functionality
/**
 * @typedef {ReturnType<typeof import('widgets/forms/AjaxForm').default>} AjaxForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputText').default>>} inputText
 */

/**
 * @description Base EmailSubscribe implementation
 * @param {AjaxForm} AjaxForm Base widget for extending
 * @returns {typeof EmailSubscribe} Email Subscribe Form class
 */
export default function (AjaxForm) {
    /**
     * @class EmailSubscribe
     * @augments AjaxForm
     * @classdesc Serves email subscription form in footer, sends AJAX request to server.
     * @property {string} data-widget - Widget name `emailSubscribe`
     * @property {string} data-event-submit - Event listener for form submission
     * @example
     * // use this code to display widget
     * <form action="${URLUtils.url('EmailSubscribe-Subscribe')}" method="POST" name="subscribe-form"
     *     data-widget="emailSubscribe"
     *     data-event-submit.prevent="handleSubmit">
     *     <div class="input-group" data-ref="formContent">
     *         ... subscription form content
     *     </div>
     *     <div data-ref="successMessage"></div>
     * </form>
     */
    class EmailSubscribe extends AjaxForm {
        prefs() {
            return {
                signupEmail: 'dwfrm_emailsubscribe_email',
                formContent: 'formContent',
                successBlock: 'successBlock',
                successMessage: 'successMessage',
                agreeToPrivacy: 'agreeToPrivacy',
                accessibilityAnnouncementMsg: '',
                ...super.prefs()
            };
        }

        /**
         * @param {inputText} el event source element
         */
        onEmailInput(el) {
            const entered = el && el.getValue();
            if (entered && entered.length) {
                this.showAgreeToPrivacy();
            }
        }

        showAgreeToPrivacy() {
            this.ref('agreeToPrivacy').show();
        }

        /**
         * @param {object} data Server JSON response once form submitted
         */
        onSubmitted(data) {
            if (data.success) {
                this.ref('formContent').hide();
                this.ref('successMessage')
                    .setText(data.msg)
                    .show();
                this.eventBus().emit('alert.show', {
                    accessibilityAlert: this.prefs().accessibilityAnnouncementMsg
                });
            }

            if (!data.success) {
                if (data.fieldErrors) {
                    Object.entries(data.fieldErrors).forEach(([name, errorMsg]) => {
                        this.getById(name, formField => formField.setError(errorMsg));
                    });
                }
                if (data.msg) {
                    this.getById(this.prefs().signupEmail, inputEmail => inputEmail.setError(data.msg));
                }
            }
        }
    }

    return EmailSubscribe;
}
