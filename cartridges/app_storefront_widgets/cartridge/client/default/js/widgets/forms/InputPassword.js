
import { get } from 'widgets/toolbox/util';
/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 */

/**
 * @description Base InputPassword implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputPassword} Input Password class
 */
export default function (BasicInput) {
    /**
     * @class InputPassword
     * @augments BasicInput
     * @classdesc Specific Password input, contains specific password validation logic
     * @property {string} data-widget - Widget name `inputPassword`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputPassword"
     * >
     *     <label for="currentPassword">
     *         <isprint value="${pdict.profileForm.login.currentpassword.label}" encoding="htmlcontent" />
     *     </label>
     *     <input type="password" autocomplete="current-password"
     *         data-ref="field"
     *         data-event-blur="validate"
     *     >
     *     <div class="invalid-feedback" data-ref="errorFeedback"></div>
     * </div>
     */
    class InputPassword extends BasicInput {
        prefs() {
            return {
                upperCaseAmount: 0,
                upperCaseAmountMsg: '',
                lowerCaseAmount: 0,
                lowerCaseAmountMsg: '',
                numbersAmount: 0,
                numbersAmountMsg: '',
                specialCharsAmount: 0,
                specialCharsAmountMsg: '',
                specialCharsList: '!@#$%^&*()_=/-+";\'.,~',
                ...super.prefs()
            };
        }

        /**
         *
         * @param {string} val - entered password
         */
        isNumber(val) {
            return '0123456789'.includes(val);
        }

        /**
         *
         * @param {string} val - entered password
         */
        isSimpleChar(val) {
            return !this.isNumber(val) && !this.prefs().specialCharsList.includes(val);
        }

        isValid() {
            const val = this.getValue();

            if (typeof val === 'string' && val) {
                const valChars = val.split('');

                if (
                    this.checkUpperCaseAmount(valChars)
                    && this.checkLowerCaseAmount(valChars)
                    && this.checkNumberAmount(valChars)
                    && this.checkSpecialCharsAmount(valChars)
                ) {
                    if (this.widgetToMatch && this.widgetToMatchOpts && this.widgetToMatch.data('getValue') !== val) {
                        this.error = this.widgetToMatchOpts.msg;
                        return false;
                    }
                } else {
                    const validation = this.prefs().validationConfig;
                    this.error = get(validation, 'errors.passwordFormatError') || '';
                    return false;
                }
            }
            return super.isValid();
        }

        /**
         * @param {string[]} valChars - array of entered password symbols
         */
        checkSpecialCharsAmount(valChars) {
            if (this.prefs().specialCharsAmount) {
                const specialCharsList = this.prefs().specialCharsList;
                const specialCharsLetters = valChars.filter(char => specialCharsList.includes(char));
                if (specialCharsLetters.length < this.prefs().specialCharsAmount) {
                    this.error = this.prefs().specialCharsAmountMsg;
                    return false;
                }
            }
            return true;
        }

        /**
         * @param {string[]} valChars - array of entered password symbols
         */
        checkNumberAmount(valChars) {
            if (this.prefs().numbersAmount) {
                const numberLetters = valChars.filter(char => this.isNumber(char));
                if (numberLetters.length < this.prefs().numbersAmount) {
                    this.error = this.prefs().numbersAmountMsg;
                    return false;
                }
            }
            return true;
        }

        /**
         * @param {string[]} valChars - array of entered password symbols
         */
        checkLowerCaseAmount(valChars) {
            if (this.prefs().lowerCaseAmount) {
                const lowerCaseLetters = valChars.filter(char => char === char.toLowerCase() && this.isSimpleChar(char));
                if (lowerCaseLetters.length < this.prefs().lowerCaseAmount) {
                    this.error = this.prefs().lowerCaseAmountMsg;
                    return false;
                }
            }
            return true;
        }

        /**
         * @param {string[]} valChars - array of entered password symbols
         */
        checkUpperCaseAmount(valChars) {
            if (this.prefs().upperCaseAmount) {
                const upperCaseLetters = valChars.filter(char => char === char.toUpperCase() && this.isSimpleChar(char));
                if (upperCaseLetters.length < this.prefs().upperCaseAmount) {
                    this.error = this.prefs().upperCaseAmountMsg;
                    return false;
                }
            }
            return true;
        }

        showMaskButton() {
            this.ref('showButton').show();
        }

        /**
         * @description Handle click on button Show. Toggle type based on input type.
         */

        toggleMask() {
            const input = this.ref('field');
            const inputType = input.attr('type');
            const showButton = this.ref('showButton');
            const showText = showButton.data('buttonTextShow');
            const hideText = showButton.data('buttonTextHide');

            if (inputType === 'password') {
                input.attr('type', 'text');
                showButton.attr('aria-pressed', 'true');
                showButton.setText(hideText);
            } else {
                input.attr('type', 'password');
                showButton.attr('aria-pressed', 'false');
                showButton.setText(showText);
            }
        }

        /**
         * @param {RefElement} el event source element
         */
        onPasswordInput(el) {
            const entered = el && el.val();
            if (entered && entered.length) {
                this.showMaskButton();
            }
        }
    }

    return InputPassword;
}
