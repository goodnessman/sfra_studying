/**
 * @typedef {ReturnType<typeof import('widgets/forms/InputText').default>} InputText
 */

import {
    initGoogleMapsApi,
    initGoogleAutocomplete,
    parseAddress
} from 'widgets/toolbox/googleMgr';

const DEFAULT_COUNTRY_CODE = 'US';

/**
 * @description Base GoogleAddressLookupField implementation
 * @param {InputText} InputText Base widget for extending
 * @returns {typeof GoogleAutocompleteField} Input Textarea class
 */
export default function (InputText) {
    /**
     * @class GoogleAddressLookupField
     * @augments InputText
     * @classdesc Input type GoogleAddressLookupField implementation
     */
    class GoogleAutocompleteField extends InputText {
        init() {
            super.init();

            this.inited = false;
        }

        /**
         * @description Inits google API once field is focused
         */
        handleFocus() {
            if (this.inited) {
                return;
            }

            initGoogleMapsApi(this.config.apiKey).then(() => {
                const field = /** @type {HTMLInputElement} */ (this.ref('field').get());

                if (field) {
                    initGoogleAutocomplete(field, this.config.countryCode || DEFAULT_COUNTRY_CODE)
                        .then(this.initEvents.bind(this))
                        .then(() => {
                            this.inited = true;
                        });
                }
            });
        }

        /**
         * @description Inits google API events
         * @param {object} autocomplete - google autocomplete
         */
        initEvents(autocomplete) {
            if (!autocomplete) {
                return;
            }

            this.autocomplete = autocomplete;

            this.autocomplete.addListener('place_changed', () => {
                var place = autocomplete.getPlace();
                var addressComponents = place.address_components;

                if (addressComponents) {
                    parseAddress(addressComponents).then((parsedAddress) => {
                        if (parsedAddress) {
                            this.emit('placechanged', parsedAddress);
                        }
                    });
                }
            });
        }

        /**
         * @description Updates google API restrictions
         * @param {string} countryCode - current country code
         */
        updateState(countryCode) {
            if (countryCode && this.autocomplete) {
                this.autocomplete.setComponentRestrictions({
                    country: countryCode
                });
            }
        }
    }

    return GoogleAutocompleteField;
}
