// TO BE REVIEWED

const DATA_FIELD = '[data-ref="field"]';

/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 */

/**
 * @description Base InputRadio implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputRadio} Input Radio class
 */
export default function (BasicInput) {
    /**
     * @class InputRadio
     * @augments BasicInput
     * @classdesc Radio input imnplementation. Specific methods were overriden.
     * <br>Could be also used in complex structures (see example)
     * @property {string} data-widget - Widget name `inputRadio`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputRadio"
     *     data-widget-event-change="selectShippingMethod"
     * >
     *     <div data-ref="field" hidden></div>
     *     <script data-ref="template" type="template/mustache">
     *         <div
     *             class="row leading-lines shipping-method-list"
     *             data-widget="inputRadio"
     *             data-widget-event-change="selectShippingMethod"
     *         >
     *             {{${'#'}shipping.0.applicableShippingMethods}}
     *                 <div>
     *                     {{${'#'}selected}}
     *                         <input
     *                             id="shippingMethod-{{ID}}" name="${shippingAddress.shippingMethodID.htmlName}"
     *                             type="radio" class="form-check-input" data-value="{{ID}}" value="{{ID}}"
     *                             data-ref="field" data-event-change="update"
     *                             checked
     *                         >
     *                     {{/selected}}
     *                     {{^selected}}
     *                         <input id="shippingMethod-{{ID}}"
     *                             data-event-change="update"
     *                             name="${shippingAddress.shippingMethodID.htmlName}"
     *                             data-ref="field"
     *                             type="radio" value="{{ID}}" data-value="{{ID}}"
     *                          />
     *                      {{/selected}}
     *                      <label class="form-check-label shipping-method-option" for="shippingMethod-{{ID}}">
     *                          <span class="display-name">{{displayName}}</span>
     *                      </label>
     *                  </div>
     *                  <div class="col-3 text-right shipping-method-pricing end-lines">
     *                      <span class="shipping-cost">{{shippingCost}}</span>
     *                  </div>
     *              {{/shipping.0.applicableShippingMethods}}
     *              <div class="invalid-feedback" data-ref="errorFeedback" hidden></div>
     *          </div>
     *      </script>
     * </div>
     */
    class InputRadio extends BasicInput {
        getValue() {
            const el = (this.ref('self').get());

            if (el) {
                return Array.from(el.querySelectorAll(DATA_FIELD))
                    .reduce((val, elField) => {
                        const tmpEl = /** @type {HTMLInputElement} */(elField);
                        return val || ((tmpEl.checked || '') && tmpEl.value);
                    }, '');
            }
            return '';
        }

        getDataValue() {
            const el = (this.ref('self').get());

            if (el) {
                return Array.from(el.querySelectorAll(DATA_FIELD))
                    .reduce((val, elInner) => {
                        const tmpEl = /** @type {HTMLInputElement} */(elInner);

                        return val || ((tmpEl.checked || '') && (tmpEl.getAttribute('data-value') || ''));
                    }, '');
            }
            return '';
        }

        validate() {
            return true;
        }

        /**
         * @param {string} value - value to be set to radio button
         */
        setValue(value) {
            const el = (this.ref('self').get());

            if (el) {
                Array.from(el.querySelectorAll(DATA_FIELD))
                    .forEach((elField) => {
                        const tmpEl = /** @type {HTMLInputElement} */(elField);

                        if (tmpEl.value === value) {
                            tmpEl.checked = true;
                            tmpEl.setAttribute('checked', 'checked');
                        } else {
                            tmpEl.checked = false;
                            tmpEl.removeAttribute('checked');
                        }
                    });
            }
        }

        getDataSet() {
            const el = this.ref('self').get();

            if (el) {
                /** @type {HTMLInputElement} */
                const currentEl = (Array.from(el.querySelectorAll(DATA_FIELD))
                    .filter(field => {
                        const tmpField = /** @type {HTMLInputElement} */(field);

                        return tmpField.checked;
                    })[0] || el.querySelectorAll(DATA_FIELD)[0]);

                return currentEl.dataset;
            }

            return null;
        }
    }

    return InputRadio;
}
