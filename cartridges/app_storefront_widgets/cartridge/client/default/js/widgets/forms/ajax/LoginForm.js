/**
 * @typedef {ReturnType<typeof import('widgets/forms/AjaxForm').default>} AjaxForm
 */

/**
 * @description Base LoginForm implementation
 * @param {AjaxForm} AjaxForm Base widget for extending
 * @returns {typeof LoginForm} Email Subscribe Form class
 */
export default function (AjaxForm) {
    /**
     * @class LoginForm
     * @augments AjaxForm
     * @classdesc Serves email subscription form in footer, sends AJAX request to server.
     * @property {string} data-widget - Widget name `loginForm`
     * @property {string} data-event-submit - Event listener for form submission
     */
    class LoginForm extends AjaxForm {
        passwordReset() {
            this.getById('passwordResetModal', passwordResetModal => passwordResetModal.showModal({}));
        }
    }

    return LoginForm;
}
