// TO BE REVIEWED
/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 */

/**
 * @description Base InputText implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputText} Input Text class
 */
export default function (BasicInput) {
    /**
     * @class InputText
     * @augments BasicInput
     * @classdesc Input type Text implementation
     * @property {string} data-widget - Widget name `inputText`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputText"
     * >
     *     <label class="form-control-label" for="inputText">
     *         <isprint value="${pdict.profileForm.customer.firstname.label}" encoding="htmlcontent" />
     *     </label>
     *     <input
     *         type="text"
     *         id="inputText"
     *         data-ref="field"
     *         data-event-blur="validate"
     *     >
     *     <div class="invalid-feedback"></div>
     * </div>
     */
    class InputText extends BasicInput {
        /**
         * @param {refElement} el source of event
         * @param {(Event|undefined)} event event instance if DOM event
         */
        onInput(el, event) { // eslint-disable-line no-unused-vars
            this.emit('input');
        }
    }

    return InputText;
}
