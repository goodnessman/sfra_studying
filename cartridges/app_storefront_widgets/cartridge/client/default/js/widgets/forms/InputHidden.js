// TO BE REVIEWED
/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 */
/**
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputHidden} Input Hidden class
 */
export default function (BasicInput) {
    /**
     * @class InputHidden
     * @augments BasicInput
     * @classdesc Widget used to handle hidden inputs
     * @property {string} data-widget - Widget name `inputHidden`
     */
    class InputHidden extends BasicInput {
        prefs() {
            return {
                skipValidation: true,
                ...super.prefs()
            };
        }
    }

    return InputHidden;
}
