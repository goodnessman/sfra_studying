// TO BE REVIEWED

import { submitFormJson } from 'widgets/toolbox/ajax';
import { scrollWindowTo, scrollToTop } from 'widgets/toolbox/scroll';
import { timeout } from 'widgets/toolbox/util';

/**
 * @typedef {ReturnType <typeof import('widgets/forms/BasicForm').default>} BasicForm
 */

/**
 * @description Base AjaxForm implementation
 * @param {BasicForm} BasicForm Base widget for extending
 * @returns {typeof AjaxForm} Ajax Form class
 */
export default function (BasicForm) {
    /**
     * @class AjaxForm
     * @augments BasicForm
     * @classdesc Widget, that contains all needed logic to submit form with AJAX.
     * @property {string} data-widget - Widget name `ajaxform`
     * @property {string} data-event-submit - Event listener for form submission
     * @example
     * // use this code to display widget
     * <form
     *     data-widget="ajaxform"
     *     data-event-submit.prevent="handleSubmit" novalidate
     *     action="${URLUtils.url('Account-SavePassword')}" method="POST
     * >
     *     <div class="alert alert-danger" hidden="hidden" data-ref="errorMessageLabel"></div>
     *     ... form fields
     *     <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>
     *     <div>
     *         <button
     *              type="submit"
     *              name="save"
     *              data-widget="button"
     *              data-widget-event-click="handleSubmit"
     *              data-event-click.prevent="handleClick"
     *              data-id="submitButton"
     *         >
     *              ${Resource.msg('button.save','account',null)}
     *         </button>
     *     </div>
     * </form>
     */
    class AjaxForm extends BasicForm {
        handleSubmit() {
            if (this.isChildsValid() && !this.submitting) {
                this.submitting = true;
                this.showProgressBar();
                this.getById(this.prefs().submitButton, submitButton => submitButton.busy());
                this.ref(this.prefs().errorMessageLabel).hide();

                submitFormJson(
                    this.getFormUrl(),
                    this.getFormFields(),
                    this.ref('self').attr('method') === 'GET' ? 'GET' : 'POST'
                )
                    .then(this.onSubmitted.bind(this))
                    .catch(this.onError.bind(this))
                    .finally(this.afterSubmission.bind(this));
            }

            return false;
        }

        /**
         * @param {object} data Server JSON response once form submitted
         * @param {string} data.success - If operation was success.
         * @param {string} data.redirectUrl - if not empty - redirect to specified location should be executed
         * @param {(string|string[])} data.error - error messages/message from server
         * @param {object[]} data.fields - form fields objects
         */
        onSubmitted(data) {
            if (data.success && data.redirectUrl) {
                window.location.assign(data.redirectUrl);
            } else if (data.error) {
                if (data.redirectUrl) {
                    window.location.assign(data.redirectUrl);
                } else {
                    this.setError(Array.isArray(data.error) ? data.error.join('<br/>') : data.error, '');
                }
            } else if (data.fields) {
                Object.entries(data.fields).forEach(([name, errorMsg]) => {
                    this.getById(name, (input) => {
                        input.setError(errorMsg);
                    });
                });
            } else {
                timeout(() => { // lets hideProgress finishes first
                    this.emit('submit', data);
                });
            }
        }

        /**
         * @description Handles an error, which happens during request (for ex. 500 response)
         * @param {Error} e - error, which happend during request
         */
        onError(e) {
            this.setError(e.message, '');
            const errorElement = this.ref(this.prefs().errorMessageLabel).get();
            if (errorElement) {
                scrollWindowTo(errorElement, true);
            } else {
                scrollToTop();
            }
        }

        afterSubmission() {
            this.getById(this.prefs().submitButton, submitButton => submitButton.unbusy());
            this.hideProgressBar();
            this.submitting = false;
        }

        /**
         * @param {string} msg - Generic error message, if no custom message - show generic one
         * @param {string} [msg2] - Custom error message to be displayed
         * @param {boolean} [scrollToError] - defines using of scroll to error
         */
        setError(msg, msg2, scrollToError = false) {
            const errorMessageLabel = this.ref('errorMessageLabel')
                .setText(msg2 || msg)
                .show();

            if (!scrollToError) {
                return;
            }

            const element = errorMessageLabel.get();

            if (element) {
                scrollWindowTo(element, true);
            } else {
                scrollToTop();
            }
        }
    }

    return AjaxForm;
}
