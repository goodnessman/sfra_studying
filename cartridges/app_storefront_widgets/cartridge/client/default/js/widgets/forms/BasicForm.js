// TO BE REVIEWED
/* eslint-disable no-shadow */

import { timeout } from '../toolbox/util';
import { getJSONByUrl } from 'widgets/toolbox/ajax';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 */

/**
 * @description Base Form implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof BasicForm} Basic Form class
 */
export default function (Widget) {
    /**
     * @class BasicForm
     * @augments Widget
     * @classdesc Basic Form widget implementation. Mostly contains generic functionality.
     * <br>Usage: use known subclasses `Form` and `AjaxForm`
     * <br>Below data attributes and example will be provided for `Form` subclass
     * @property {string} data-widget - Widget name `form`
     * @property {string} data-event-submit - Event listener for form submission
     * @example
     * // use this code to display widget
     * <form
     *     action="${URLUtils.url('Order-Track')}"
     *     method="GET"
     *     data-widget="form"
     *     data-event-submit="handleSubmit"
     * >
     *     ... form contents
     * </form>
     */
    class BasicForm extends Widget {
        prefs() {
            return {
                submitEmpty: true,
                emptyFormErrorMsg: '',
                submitButton: 'submitButton',
                errorMessageLabel: 'errorMessageLabel',
                SUBMITTING_TIMEOUT: 5000,
                formDefinitionUrl: '',
                ...super.prefs()
            };
        }

        showProgressBar() {
            this.ref('self').attr('aria-busy', 'true');
        }

        hideProgressBar() {
            this.ref('self').attr('aria-busy', 'false');
        }

        getSubmitButtonName() {
            return this.submitButtonName;
        }

        init() {
            this.submitButtonName = null;
            super.init();
            // this.eventMgr('FormSubmitter.submit.' + this.id, this.submit);

            /**
             * Below code is needed to initiate correct comparison configuration for certain fields
             * It iterates all BasicInput childs and if will find in validation compare data - will
             * sets up needed data on a proper child BasicInput widgets
            */
            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(widget => {
                if (widget instanceof BasicInput) {
                    let widgetValidation = widget.prefs().validationConfig || {};
                    let compareFieldId = widgetValidation.compareWith || '';
                    if (compareFieldId) {
                        this.getById(compareFieldId, targetWidget => {
                            if (targetWidget.length) {
                                let compareOptions = {
                                    field: compareFieldId,
                                    msg: widgetValidation.errors.compareWith || ''
                                };
                                widget.data('setMatchCmp', targetWidget, compareOptions);
                            }
                        });
                    }
                }
            });
        }

        /**
         * @param {HTMLInputElement} el submit button element
         */
        saveSubmitButton(el) {
            this.submitButtonName = el.name;
        }

        submit() {
            /**
             * @type {HTMLElement|undefined}
             */
            var elem = this.ref(this.prefs().submitButton).get();

            if (elem) {
                elem.click();
            }
        }

        /**
         * @param {refElement} _el event source element
         * @param {(Event|undefined)} event event instance if DOM event
         */
        handleSubmit(_el, event) {
            this.clearError();
            const valid = this.isChildsValid();

            if ((!valid || this.submitting) && (event && event instanceof Event)) {
                event.preventDefault();
                return;
            }
            this.ref(this.prefs().submitButton).disable();

            this.submitting = true;

            this.onDestroy(timeout(() => {
                this.submitting = false;
            }, this.prefs().SUBMITTING_TIMEOUT));

            this.emit('submit');
        }

        getFormFields() {
            /**
             * @type {{[x: string]: string}}
             */
            var fields = {};
            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(widget => {
                if (widget instanceof BasicInput && !(widget.skipSubmission && widget.skipSubmission())) {
                    const name = widget.getName && widget.getName();

                    if (name) {
                        fields[name.toString()] = widget.getValue();
                    }
                }
            });
            return fields;
        }

        /**
         * @description Sets related fields values, can be executed silently, without triggering `change` event
         * @param {object} formFields - Structured object with name: value pairs for input fields
         * @param {(boolean|undefined)} [silently] - if set to `true` - input should not be
         * validated against a new value
         */
        setFormFields(formFields, silently = false) {
            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(widget => {
                if (widget instanceof BasicInput) {
                    let name = widget.getName && widget.getName();
                    widget.setValue(formFields[name], silently);
                }
            });
        }

        /**
         * @param {Function} [cb] callback called if child inputs are valid
         * @returns {boolean} - boolean value is Form input valid
         */
        isChildsValid(cb) {
            var valid = true;
            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(item => {
                if (item instanceof BasicInput && typeof item.validate === 'function' && !item.validate()) {
                    if (valid && item.setFocus) {
                        item.setFocus();
                    }
                    valid = false;
                }
            });

            if (valid && typeof cb === 'function') {
                cb();
            }

            if (!this.prefs().submitEmpty) {
                const fieldsValues = this.getFormFields();

                if (Object.keys(fieldsValues).every((key) => !fieldsValues[key])) {
                    valid = false;

                    this.ref(this.prefs().errorMessageLabel)
                        .setText(this.prefs().emptyFormErrorMsg);

                    this.ref(this.prefs().errorMessageLabel).show();
                }
            }
            return valid;
        }

        validate() {
            return this.isChildsValid();
        }

        isValid() {
            var valid = true;
            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(itemCmp => {
                if (itemCmp instanceof BasicInput && typeof itemCmp.isValid === 'function' && !itemCmp.isValid()) {
                    valid = false;
                    return false;
                }
                return true;
            });
            return valid;
        }

        setFocus() {
        }

        /**
         * @returns {string} form action url
         */
        getFormUrl() {
            // @ts-ignore
            return this.ref('self').attr('action');
        }

        clearError() {
            this.ref(this.prefs().errorMessageLabel)
                .hide()
                .setText('');
        }

        /**
         * @description Updates form html body by sending an AJAX request to server with params object
         * <br>(possible param key is `countryCode`)
         * <br>Obtained template injected instead of old fields
         * <br>Data, entered in fields previosly will be restored
         * @param {object} params - request parameters
         * @param {string} params.countryCode - A country code to get country-specific form
         * @returns {InstanceType <Promise>} - new Promise
         */
        updateFormData(params) {
            let formDefinitionUrl = this.prefs().formDefinitionUrl;

            if (formDefinitionUrl && params) {
                return new Promise((resolve) => {
                    getJSONByUrl(formDefinitionUrl, params, true).then((response) => {
                        if (response.formDefinition) {
                            const formFields = this.getFormFields();
                            this.render('', {}, this.ref('fieldset'), response.formDefinition).then(() => {
                                this.setFormFields(formFields, true);
                                resolve();
                                this.formDataUpdated();
                            });
                        }
                    });
                });
            }

            return new Promise((resolve) => {
                resolve();
                this.formDataUpdated();
            });
        }

        /**
         * @description Template method called once form definitions were reloaded
         */
        formDataUpdated() {

        }
    }

    return BasicForm;
}
