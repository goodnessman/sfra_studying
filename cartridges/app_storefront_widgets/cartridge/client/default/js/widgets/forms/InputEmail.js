// TO BE REVIEWED
/**
 * @typedef {ReturnType<typeof import('./InputText').default>} InputText
 */
/**
 * @description Base InputCheckbox implementation
 * @param {InputText} InputText Base widget for extending
 * @returns {typeof InputEmail} Input Email class
 */
export default function (InputText) {
    /**
     * @class InputEmail
     * @augments InputText
     * @classdesc Input type Email implementation
     * @property {string} data-widget - Widget name `inputEmail`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="inputEmail"
     * >
     *     <label class="form-control-label" for="email">
     *         <isprint value="${pdict.profileForm.customer.email.label}" encoding="htmlcontent" />
     *     </label>
     *     <input
     *         type="email"
     *         autocomplete="email"
     *         id="email"
     *         data-ref="field"
     *         data-event-blur="validate"
     *     >
     *     <div class="invalid-feedback"></div>
     * </div>
     */
    class InputEmail extends InputText {

    }

    return InputEmail;
}
