import { submitFormJson } from 'widgets/toolbox/ajax';

/**
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @typedef {ReturnType<typeof import('widgets/forms/AjaxForm').default>} AjaxForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/Button').default>>} button
 */

/**
 * @description Base CouponForm implementation
 * @param {AjaxForm} AjaxForm Base widget for extending
 * @returns {typeof CouponForm} Coupon Form class
 */
export default function (AjaxForm) {
    /**
     * @class CouponForm
     * @augments AjaxForm
     * @classdesc Cart page promo code apopolication AJAX form
     * @property {string} data-widget - Widget name `couponform`
     * @property {string} data-error-message-label - Specific error message label
     * @property {string} data-event-submit - Event listener for submit form event
     * @example
     * // use this code to display widget
     * <form
     *     data-widget="couponform"
     *     data-id="couponForm"
     *     data-error-message-label="errorFeedback"
     *     data-event-submit.prevent="handleSubmit"
     *     action="{{actionUrls.submitCouponCodeUrl}}"
     *     method="GET"
     * >
     *     ... form contents
     *     <button type="submit">${Resource.msg('button.submit.promo.code', 'cart', null)}</button>
     * </form>
     */
    class CouponForm extends AjaxForm {
        prefs() {
            return {
                msgEmptyCoupon: '',
                couponInput: 'dwfrm_coupon_couponCode',
                couponCodeContent: 'couponCodeContent',
                couponDeletionMsg: 'couponDeletionMsg',
                removeCouponUrl: '',
                classesShow: 'show',
                ...super.prefs()
            };
        }

        /**
         * @param {Object} data server JSON response
         */
        onSubmitted(data) {
            if (!data) {
                return;
            }

            if (data.error && data.errorMessage) {
                this.getById(this.prefs().couponInput, (/** @type {basicInput} */ input) => input.setError(data.errorMessage));
            } else if (data.error && data.fieldErrors) {
                Object.keys(data.fieldErrors).forEach(errorKey => {
                    this.getById(errorKey, (/** @type {basicInput} */ input) => input.setError(data.fieldErrors[errorKey]));
                });
            } else {
                this.getById(this.prefs().couponInput, (/** @type {basicInput} */ input) => input.setValue(''));
                data.couponAction = 'applied';
                this.eventBus().emit('coupon.updated', data);
            }
        }

        /**
         * @description Invalidates coupon code input with relevant error message
         * @param {Error} error - error, happened during request
         */
        onError(error) {
            this.getById(this.prefs().couponInput, (/** @type {basicInput} */ input) => input.setError(error.message));
        }

        /**
         * @description Removes coupon code from cart
         * @param {button} button - triggering button
         */
        removeCoupon(button) {
            this.showProgressBar();
            this.has(this.prefs().couponDeletionMsg, function (errMsg) {
                errMsg.setText('').hide();
            });

            submitFormJson(this.prefs().removeCouponUrl, {
                couponCode: button.data('code'),
                uuid: button.data('uuid')
            }, 'GET')
                .then((response) => {
                    response.couponAction = 'removed';
                    this.eventBus().emit('coupon.updated', response);
                })
                .catch((err) => {
                    this.has(this.prefs().couponDeletionMsg, function (errMsg) {
                        errMsg.setText(err.message).show();
                    });
                })
                .finally(() => {
                    this.hideProgressBar();
                });
        }

        focusInput() {
            this.getById(this.prefs().couponInput, (/** @type {basicInput} */ input) => input.focus());
        }
    }

    return CouponForm;
}
