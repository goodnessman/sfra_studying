/**
 * @typedef {ReturnType<typeof import('widgets/forms/AjaxForm').default>} AjaxForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 *
 */

/**
 * @description Base AjaxForm implementation
 * @param {AjaxForm} AjaxForm Base widget for extending
 * @returns {typeof AddressForm} Address Form class
 */
export default function (AjaxForm) {
    /**
     * @class AddressForm
     * @augments AjaxForm
     * @property {string} data-form-definition-url - Update form definition URL
     * @example
     * // use this code to display widget
     * <form
     *     data-widget="addressForm"
     *     data-event-submit.prevent="handleSubmit" novalidate
     *     action="${URLUtils.url('Address-SaveAddress')}" method="POST
     *     data-form-definition-url="${URLUtils.url('Forms-FormDefinition', 'form', 'address')}"
     *     data-address-fields="${[
     *           addressFields.address1.htmlName,
     *           addressFields.address2.htmlName,
     *           addressFields.country.htmlName,
     *           addressFields.states.stateCode.htmlName,
     *           addressFields.city.htmlName,
     *           addressFields.postalCode.htmlName
     *     ].join(',')}"
     * >
     *     <div class="alert alert-danger" hidden="hidden" data-ref="errorMessageLabel"></div>
     *     ... form fields
     *     <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>
     *     <div>
     *         <button
     *              type="submit"
     *              name="save"
     *              data-widget="button"
     *              data-widget-event-click="handleSubmit"
     *              data-event-click.prevent="handleClick"
     *              data-id="submitButton"
     *         >
     *              ${Resource.msg('button.save','account',null)}
     *         </button>
     *     </div>
     * </form>
     */
    class AddressForm extends AjaxForm {
        prefs() {
            return {
                addressFields: '',
                ...super.prefs()
            };
        }

        /**
         * @param {basicInput} el event source element
         */
        onCountryChange(el) {
            this.config.countryCode = el.getValue();

            this.updateFormData({ countryCode: this.config.countryCode });
        }

        getAddressFields() {
            return this.prefs().addressFields.split(',');
        }

        /**
         *
         * @param {HTMLInputElement} el - input element
         * @param {object} address - address object
         */
        handlePlaceChange(el, address) {
            this.getAddressFields().forEach(item => {
                this.getById(item, (/** @type {basicInput} */addressField) => {
                    const name = addressField.getName() || '';
                    const shortName = name.split('_').pop();

                    if (shortName && (shortName in address)) {
                        addressField.setValue(address[shortName] || '', true);
                    }
                });
            });
        }
    }

    return AddressForm;
}
