import { interval } from 'widgets/toolbox/util';

/**
 * @description Class represents the simple Countdown. Periodically runs `onProgressCallback` callback
 * with current countdown data. When countdown ends - executes `onEndCallback`.
 */
class SimpleCountdown {
    /**
     * @param {Function|undefined} [onProgressCallback] function to be called periodically with current countdown time as a parameter
     * @param {Function|undefined} [onEndCallback] function to be called once countdown finishes
     * @param {Object} [settings] settings for countdown.
     * @param {number|undefined} [settings.time] time for countdown in seconds
     * @param {number|undefined} [settings.period] period of countdown refresh, specified in milliseconds;
     */
    constructor(onProgressCallback, onEndCallback, settings) {
        this.countdownDisposable = undefined;

        let timer = (settings && settings.time) || 0;
        let period = (settings && settings.period) || 1000;
        let minutes;
        let seconds;

        this.countdownDisposable = interval(() => {
            minutes = parseInt(String(timer / 60), 10);
            seconds = parseInt(String(timer % 60), 10);
            seconds = seconds < 10 ? '0' + seconds : seconds;

            if (onProgressCallback) {
                onProgressCallback({
                    minutes,
                    seconds
                });
            }

            timer -= (period / 1000);
            if (timer < 0) {
                this.destroy();
                if (onEndCallback) {
                    onEndCallback();
                }
            }
        }, period);
    }

    /**
     * @description Clear resources on demand
     */
    destroy() {
        if (this.countdownDisposable) {
            this.countdownDisposable();
            this.countdownDisposable = undefined;
        }
    }
}

export default SimpleCountdown;
