/**
 * @description Original library `supermask.js` was taken from
 * [this library](https://github.com/ViniChab/supermask.js).
 * It was re-factored to conform Widget's appropach, because OOTB implementation is too much different
 * form widgets useage concepts.
 * Underlying original logic was not touched, just formatted for project specific linter rules.
 */

/**
 * @description To check if string is symbol
 */
function isSymbol(str) {
    return !!str.match(/(?![Ã])(?![Á])[_\W]/);
}

/**
 * @description To check if string is lowercase letter
 */
function isLowerCaseLetter(str) {
    return !!str.match(/[a-z]/);
}

/**
 * @description To check if string is uppercase letter
 */
function isUpperCaseLetter(str) {
    return !!str.match(/[A-Z]/);
}

/**
 * @description To check if string is letter
 */
function isLetter(str) {
    // eslint-disable-next-line
    return !!str.match(/[a-z\A-Z]/);
}

/**
 * @description To check if digit should be here according to mask
 */
function shouldThisInvalidDigitBeHere(digit, mask) {
    return digit === mask;
}

/**
 * @description Will validate a number in a specific position
 */
function numericValidation(digitMask, key, field) {
    if (field.hasAttribute('inverted-numbers')) {
        return (+key >= digitMask);
    } else if (+key <= digitMask) {
        return true;
    }

    return false;
}

/**
 * @description Function to concat symbols at the end of mask
 */
function concatSymbols(field, splitMask, lastDigitPosition) {
    let symbols = '';
    for (let i = lastDigitPosition; i < splitMask.length; i++) {
        if (isSymbol(splitMask[i])) {
            symbols = symbols.concat(splitMask[i]);
        } else {
            break;
        }
    }

    return field.value.concat(symbols);
}

/**
 * @description Will validate a specific digit of the mask
 */
function validateDigit(splitMask, lastDigitPosition, key, field, isFullValidation = false) {
    let digitMask = splitMask[lastDigitPosition];

    if (key === ' ') {
        return false;
    }

    if (isSymbol(digitMask)) {
        if (!isFullValidation) {
            field.value = concatSymbols(field, splitMask, lastDigitPosition);
            return validateDigit(splitMask, field.value.length, key, field);
        }
        return validateDigit(splitMask, (lastDigitPosition + 1), key, field, true);
    }

    if (+digitMask || digitMask === '0') {
        return numericValidation(digitMask, key, field);
    }
    if (isUpperCaseLetter(digitMask) && isUpperCaseLetter(key)) {
        return true;
    }
    if (isLowerCaseLetter(digitMask) && isLowerCaseLetter(key)) {
        return true;
    }
    if (digitMask === 'Á' && isLetter(key)) {
        return true;
    }
    if (digitMask === 'Ã') {
        return true;
    }

    return undefined;
}

/**
 * @description Function to validate mask on type, it'll also add symbols
 */
function validateMask(field, mask, event) {
    let lastDigitPosition = field.value.length;
    let splitMask = mask.split('');

    try {
        if (!validateDigit(splitMask, lastDigitPosition, event.key, field)) {
            event.preventDefault();
        }
    } catch (e) {
        event.preventDefault();
    }
}

/**
 * @description Function used to catch any symbols a mask might have at the end (such as AAA---)
 */
function getFinalMaskSymbols(mask) {
    let finalSymbols = [];
    let reverseSplitMask = mask.split('').reverse();
    try {
        reverseSplitMask.forEach(digit => {
            if (isSymbol(digit)) {
                finalSymbols.push(digit);
            } else {
                throw new Error('Reached a non-symbolic value');
            }
        });
    } catch (e) { } // eslint-disable-line

    return finalSymbols;
}

/**
 * @description Function to validate every digit
 */
function fullyValidateMask(field, mask) {
    let validatedMask = '';
    let finalSymbols = getFinalMaskSymbols(mask);

    try {
        field.value.split('').forEach((digit, index) => {
            if (validateDigit(mask.split(''), index, digit, field, true)) {
                validatedMask = validatedMask.concat(digit);
                return validateMask;
            }
            if (shouldThisInvalidDigitBeHere(digit, mask[index])) {
                validatedMask = validatedMask.concat(digit);
                return validateMask;
            }
            throw new Error('invalid input');
        });
    } catch (e) {
        field.value = validatedMask + finalSymbols.join('');
        return field.value;
    }

    return undefined;
}

/**
 * @description Function used to insert symbol at specific position
 */
function insertSymbol(fieldValue, index, symbol) {
    let splitValue = fieldValue.split('');
    let firstHalf = fieldValue.substr(0, index);
    let secondHalf = splitValue.splice(index).join('');
    return firstHalf + symbol + secondHalf;
}

/**
 * @description Will add every symbol the mask should have
 */
function addSymbols(field, mask) {
    if (field.value.length === 0) {
        return field.value;
    }

    let splitMask = mask.split('');
    let splitValue = field.value.split('');

    try {
        splitMask.forEach((digit, index) => {
            if (index > splitValue.length) {
                throw new Error('mask is bigger than input value');
            }
            if (isSymbol(digit) && splitValue[index] !== digit) {
                splitValue = insertSymbol(splitValue.join(''), index, digit).split('');
            }
        });
    } catch (e) { } // eslint-disable-line

    field.value = splitValue.join('');
    return undefined;
}

/**
 * @description Capsule function to validate field value after focusout
 */
function fullyValidateChange(field, mask) {
    addSymbols(field, mask);
    fullyValidateMask(field, mask);
}

/**
 * @description Will get the carret position
 */
function getCaretPos(field) {
    let range;
    let bookmark;
    let caretPos;

    // @ts-ignore
    if (document.selection && document.selection.createRange) {
        // @ts-ignore
        range = document.selection.createRange();
        bookmark = range.getBookmark();
        caretPos = bookmark.charCodeAt(2) - 2;
    } else if (field.setSelectionRange) {
        caretPos = field.selectionStart;
    }

    return caretPos;
}

/**
 * @description Will set the carret position
 */
function setCaretPosition(field, caretPos) {
    if (field.createTextRange) {
        let range = field.createTextRange();
        range.move('character', caretPos);
        range.select();
    } else if (field.selectionStart) {
        field.focus();
        field.setSelectionRange(caretPos, caretPos);
    } else {
        field.focus();
    }
}

/**
 * @description Function to make backspace unable to delete a symbol at the middle of the field
 */
function validateBackspaceButton(field, mask, deletionType = 'backspace') {
    let caretPosition = getCaretPos(field);
    let deletedCharacter = mask[caretPosition];

    if (isSymbol(deletedCharacter) && field.value.split('')[caretPosition]) {
        field.value = insertSymbol(field.value, caretPosition, deletedCharacter);

        switch (deletionType) {
            case 'backspace':
                setCaretPosition(field, caretPosition);
                break;
            case 'delete':
                setCaretPosition(field, caretPosition + 1);
                break;
            default:
                break;
        }
    }
}

/**
 * @description Class represents an input mask.
 * Originally was taken from [this library](https://github.com/ViniChab/supermask.js).
 */
class InputMask {
    /**
     * @description On keypress logic
     * @param {HTMLElement} field - masked field
     * @param {string} mask - current field mask
     * @param {Event} event - target event
     */
    onKeypress(field, mask, event) {
        validateMask(field, mask, event);
    }

    /**
     * @description On change logic
     * @param {HTMLElement} field - masked field
     * @param {string} mask - current field mask
     */
    onChange(field, mask) {
        fullyValidateChange(field, mask);
    }

    /**
     * @description On change logic
     * @param {HTMLElement} field - masked field
     * @param {string} mask - current field mask
     */
    onInput(field, mask, event) {
        // @ts-ignore
        switch (event.inputType) {
            case 'deleteContentForward':
                validateBackspaceButton(field, mask, 'delete');
                break;
            case 'deleteContentBackward':
                validateBackspaceButton(field, mask);
                break;
            case 'insertFromDrop':
                fullyValidateChange(field, mask);
                break;
            default:
                break;
        }
    }

    /**
     * @description On paste logic
     * @param {HTMLElement} field - masked field
     * @param {string} mask - current field mask
     * @param {Event} event - target event
     */
    onPaste(field, mask, event) {
        if (!field.hasAttribute('blockpasting')) {
            // @ts-ignore
            field.value += (event.clipboardData || window.clipboardData).getData('text');
            fullyValidateChange(field, mask);
        }
    }

    /**
     * @description Validate applied change. Might be triggered on form definition reloaded, etc
     * @param {HTMLElement} field - masked field
     * @param {string} mask - current field mask
     */
    validate(field, mask) {
        // @ts-ignore
        if (field.value) {
            fullyValidateChange(field, mask);
        }
    }
}

export default InputMask;
