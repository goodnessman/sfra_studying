// TODO: describe in JSDoc or documentation extended purpose of usage eventemitter library and used methods
/**
 * @description Create and returns an instance of  EventEmitter.
 *
 * Refer eventemitter3 documentation
 * @module eventBus
 * @category widgets
 * @subcategory toolbox
 */

import { EventEmitter } from 'eventemitter3';
const EventBus = new EventEmitter();
export default EventBus;
