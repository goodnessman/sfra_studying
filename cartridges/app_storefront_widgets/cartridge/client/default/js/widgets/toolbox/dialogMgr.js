/**
 * @typedef {InstanceType <ReturnType <typeof import('widgets/global/Modal').default>>} modal
 */

/**
 * @class DialogMgr
 * @classdesc Contains all needed logic for managing dialog stack.
 * @category widgets
 * @subcategory toolbox
 * @augments Widget
 */
class DialogMgr {
    constructor() {
        this.dialogsStack = [];
    }

    /**
     * @description Open Modal
     * @param {modal} dialog - Dialog object
     */
    openDialog(dialog) {
        const topDialog = this.dialogsStack.length ? this.dialogsStack[this.dialogsStack.length - 1] : null;

        if (topDialog !== dialog) {
            if (topDialog) {
                topDialog.moveBehind();
            }

            dialog.open();

            this.dialogsStack.push(dialog);
        }
    }

    /**
     * @description Close Modal
     */
    closeDialog() {
        const dialog = this.dialogsStack.pop();

        if (dialog) {
            dialog.close();

            if (this.dialogsStack.length) {
                const previousDialog = this.dialogsStack[this.dialogsStack.length - 1];

                previousDialog.moveToTop();
            } else {
                dialog.removeGlobalDialogClass();
            }
        }
    }
}

export const dialogMgr = new DialogMgr();
