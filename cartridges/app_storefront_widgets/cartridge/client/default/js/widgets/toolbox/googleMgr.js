let googleApi;
let resolveGMapsFn;
let initialized = false;

const initializationPromise = new Promise((resolve) => {
    resolveGMapsFn = resolve;
});

/**
 * @description callback fires once google maps api loaded
 */
// @ts-ignore
window.googleMapsApiCallback = () => {
    // @ts-ignore
    googleApi = window.google;

    resolveGMapsFn();
};

/**
 * @description Extracts address field value from google address components
 * @param {Array} fields - fields list to extract value
 * @param {Array} address - address
 * @returns {string} result
 */
function findAddressFieldsValue(fields, address) {
    return fields.map((field) => {
        var value = address.find((item) => {
            if (item.types.indexOf(field) !== -1) {
                return item;
            }

            return null;
        });

        if ((['country', 'administrative_area_level_1'].indexOf(field) !== -1) && value) {
            return value.short_name;
        }

        return value ? value.long_name : '';
    }).filter(component => component).join(' ');
}

/**
 * @description Loads google maps API
 * @param {string} apiKey - api key
 * @returns {Promise} result
 */
export function initGoogleMapsApi(apiKey) {
    if (!initialized) {
        let url = new URL('https://maps.googleapis.com/maps/api/js');

        url.searchParams.append('key', apiKey);
        url.searchParams.append('libraries', 'places');
        url.searchParams.append('callback', 'googleMapsApiCallback');

        let script = document.createElement('script');
        script.src = url.toString();
        script.async = true;
        script.type = 'text/javascript';

        document.body.appendChild(script);

        initialized = true;
    }

    return initializationPromise;
}

/**
 * @description Inits google autocomplete for the field
 * @param {HTMLInputElement} field - input field
 * @param {string} countryCode - country code to restrict search results
 * @returns {Promise} result
 */
export function initGoogleAutocomplete(field, countryCode) {
    return Promise.resolve(new googleApi.maps.places.Autocomplete(
        field, {
            componentRestrictions: {
                country: countryCode
            }
        }
    ));
}

/**
 * @description Returns parsed address object
 * @param {Array} addressComponents - array with google address components
 * @returns {object} result
 */
export function parseAddress(addressComponents) {
    return Promise.resolve({
        address1: findAddressFieldsValue(['street_number', 'route'], addressComponents),
        city: findAddressFieldsValue(['postal_town', 'locality'], addressComponents),
        county: findAddressFieldsValue(['administrative_area_level_1'], addressComponents),
        stateCode: findAddressFieldsValue(['administrative_area_level_1'], addressComponents),
        postalCode: findAddressFieldsValue(['postal_code'], addressComponents)
    });
}
