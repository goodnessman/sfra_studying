/**
 * @module widgetsToolkit
 * @category widgets
 * @subcategory framework
 */

/**
 * @typedef {InstanceType<typeof import('widgets/Widget').default>} widget
 */

// eslint-disable-next-line max-len
const colorsList = ['AliceBlue', 'AntiqueWhite', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque', 'Black', 'BlanchedAlmond', 'Blue', 'BlueViolet', 'Brown', 'BurlyWood', 'CadetBlue', 'Chartreuse', 'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson', 'Cyan', 'DarkBlue', 'DarkCyan', 'DarkGoldenRod', 'DarkGray', 'DarkGrey', 'DarkGreen', 'DarkKhaki', 'DarkMagenta', 'DarkOliveGreen', 'DarkOrange', 'DarkOrchid', 'DarkRed', 'DarkSalmon', 'DarkSeaGreen', 'DarkSlateBlue', 'DarkSlateGray', 'DarkSlateGrey', 'DarkTurquoise', 'DarkViolet', 'DeepPink', 'DeepSkyBlue', 'DimGray', 'DimGrey', 'DodgerBlue', 'FireBrick', 'FloralWhite', 'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'Gold', 'GoldenRod', 'Gray', 'Grey', 'Green', 'GreenYellow', 'HoneyDew', 'HotPink', 'IndianRed ', 'Indigo ', 'Ivory', 'Khaki', 'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue', 'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey', 'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue', 'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime', 'LimeGreen', 'Linen', 'Magenta', 'Maroon', 'MediumAquaMarine', 'MediumBlue', 'MediumOrchid', 'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen', 'MediumTurquoise', 'MediumVioletRed', 'MidnightBlue', 'MintCream', 'MistyRose', 'Moccasin', 'NavajoWhite', 'Navy', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed', 'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed', 'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple', 'RebeccaPurple', 'Red', 'RosyBrown', 'RoyalBlue', 'SaddleBrown', 'Salmon', 'SandyBrown', 'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue', 'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'Tan', 'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White', 'WhiteSmoke', 'Yellow', 'YellowGreen'];
const WIDGET_PROP_NAME = '@@_widget_instance_@@';
const DATA_WIDGET = 'data-widget';

/**
 * @description add event listener to element
 * @param {HTMLElement} element to add
 * @param {string} eventName name of event
 * @param {EventListenerOrEventListenerObject} fn callback function
 * @returns {() => void} disposible callback
 */
// eslint-disable-next-line no-inner-declarations
function addDisposableEventListener(element, eventName, fn) {
    element.addEventListener(eventName, fn);
    return () => element.removeEventListener(eventName, fn);
}

/**
 * @description Get widget path
 * @param {HTMLElement} element widget DOM element
 * @returns {widget[]} widets path
 */
// eslint-disable-next-line no-inner-declarations
function widgetsPath(element) {
    /**
     * @type {widget[]}
     */
    var widgets = [];
    var el = element;
    while (el) {
        var widget = el[WIDGET_PROP_NAME];
        if (widget) {
            widgets.push(widget);
        }

        if (el.parentElement) {
            el = el.parentElement;
        } else {
            break;
        }
    }
    return widgets;
}

var colorIdx = -1;

/**
 * @type {Function[]}
 */
let toolKitListeners = [];

/**
 * @param {typeof import('widgets/widgetsMgr').default} widgetsMgr Instance of WidgetsMgr class
 */
export function initToolkit(widgetsMgr) {
    colorIdx = -1;
    const errorLayout = document.querySelector('#errorLayout');
    toolKitListeners.forEach(l => l());
    toolKitListeners = [];
    document.querySelectorAll('[' + DATA_WIDGET + ']').forEach((element) => { // TODO : fix query for viewtype related widgets
        if (element instanceof HTMLElement) {
            colorIdx += 1;
            element.style.backgroundColor = '' + colorsList[colorIdx];

            if (colorIdx >= colorsList.length) {
                colorIdx = 0;
            }

            if (errorLayout) {
                toolKitListeners.push(addDisposableEventListener(element, 'mouseenter', () => {
                    const widgets = widgetsPath(element).map(widget => widget.config.widget
                        + (widget.id ? `[${widget.id}]` : ''));

                    errorLayout.innerHTML = `<div class="" style="
                        top: 0;
                        right: 0;
                        position: fixed;
                        background-color: #ff0000ee;
                        border: black;
                        padding: 5px;
                        z-index: 9999999;
                        border-radius: 10px;
                    ">
                        ${widgets.join(' < ')}
                    </div>`;
                }));
            }


            toolKitListeners.push(addDisposableEventListener(element, 'click', () => {
                // eslint-disable-next-line no-console
                console.log(element[WIDGET_PROP_NAME]);
            }));
        }
    });
    document.querySelectorAll('[data-ref]').forEach((element) => {
        if (element instanceof HTMLElement) {
            // element.style.padding = '1px';
            element.style.border = '1px solid blue';
        }
        if (colorIdx > colorsList.length) {
            colorIdx = 0;
        }
    });

    /* eslint-disable no-proto, no-console */
    Object.keys(widgetsMgr.getAll()).forEach(item => {
        const topClass = widgetsMgr.get(item);
        const hierarchy = [[topClass.name, topClass.constructor]];
        let currentClass = topClass;
        // @ts-ignore
        while (currentClass.__proto__ && currentClass.__proto__.name) {
            // @ts-ignore
            hierarchy.push([currentClass.__proto__.name, currentClass.__proto__.prototype.constructor]);
            // @ts-ignore
            currentClass = currentClass.__proto__;
        }
        console.log({
            widgetID: item,
            classesHierarchy: hierarchy
        });
    });
    /* eslint-enable */
}
