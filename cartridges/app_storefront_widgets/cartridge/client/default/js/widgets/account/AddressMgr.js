// TODO: notify blind user about all not/applied changes with alert messages.
// TODO: Global nofication component should be implemented
// TODO: keep track that focus stays on the same place during cart update

import { getJSONByUrl } from 'widgets/toolbox/ajax';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/ConfirmDialog').default>>} confirmDialog
 */

/**
 * @description Base AccountAddressForm implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof AddressMgr} Address Manager class
 */

export default function (Widget) {
    /**
     * @class AccountAddressForm
     * @augments Widget
     */
    class AddressMgr extends Widget {
        prefs() {
            return {
                accessibilityAlerts: {},
                ...super.prefs()
            };
        }

        removeAddress(address) {
            this.addressToRemove = address;
            this.getById('confirmDialog', (/** @type {confirmDialog} */confirmDialog) => {
                confirmDialog.showModal({ addressTitle: address.data('id') });
            });
        }

        confirmedRemoveAddress() {
            getJSONByUrl(this.addressToRemove.data('url'), {
                addressId: this.addressToRemove.data('id'),
                isDefault: this.addressToRemove.data('default')
            }).then(() => {
                const addressCardId = this.addressToRemove.data('id');
                const addressCard = this.ref(addressCardId);
                if (addressCard) {
                    addressCard.remove();
                    delete this.addressToRemove;
                    if (addressCard.data('isDefaultAddress')) {
                        const accessibilityAlert = this.prefs().accessibilityAlerts.deleteDefaultAddress;
                        this.eventBus().emit('alert.show', {
                            accessibilityAlert
                        });
                    }
                }
            });
        }
    }

    return AddressMgr;
}
