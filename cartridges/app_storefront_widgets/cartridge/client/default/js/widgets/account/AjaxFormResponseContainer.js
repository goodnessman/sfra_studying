/**
 * @description Base AjaxFormResponseContainer implementation
 * Used to rerender part of page after success response from server
 * @param {typeof import('widgets/Widget').default} Widget Base widget for extending
 * @returns {typeof AjaxFormResponseContainer} Password reset modal class
 */
export default function (Widget) {
    /**
     * @class AjaxFormResponseContainer
     * @augments Widget
     */
    class AjaxFormResponseContainer extends Widget {
        /**
         * @description Get response from server and render it
         * @param {typeof import('widgets/forms/AjaxForm').default} _form - ajaxForm Widget
         * @param {object} data - response form server
         */
        onSuccessSubmit(_form, data) {
            this.render(undefined, data);
        }
    }

    return AjaxFormResponseContainer;
}
