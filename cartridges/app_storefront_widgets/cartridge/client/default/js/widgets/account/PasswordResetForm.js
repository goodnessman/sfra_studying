// TODO: non working reset form along with PasswordResetModal widget
/**
 * @typedef {ReturnType<typeof import('widgets/forms/AjaxForm').default>} AjaxForm
 */

/**
 * @description Base PasswordResetForm implementation
 * @param {AjaxForm} AjaxForm Base widget for extending
 * @returns {typeof PasswordResetForm} Password reset form
 */
export default function (AjaxForm) {
    /**
     * @class PasswordResetForm
     * @augments AjaxForm
     */
    class PasswordResetForm extends AjaxForm {
        /**
         *
         * @param {object} data - response data
         */
        onSubmitted(data) {
            if (data.success) {
                this.emitWithoutContext('submitted', data);
            } else {
                super.onSubmitted(data);
            }
        }

        closeForm() {
            this.emit('closed');
        }
    }

    return PasswordResetForm;
}
