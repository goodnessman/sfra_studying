// TODO: notify blind user about all not/applied changes with alert messages.
// TODO: Global nofication component should be implemented
// TODO: keep track that focus stays on the same place during cart update

import { getJSONByUrl } from 'widgets/toolbox/ajax';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 *  * @typedef {InstanceType<ReturnType<typeof import('widgets/global/ConfirmDialog').default>>} confirmDialog
 */

/**
 * @description Base PaymentMgr implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof PaymentMgr} Payment Manager class
 */

export default function (Widget) {
    /**
     * @class PaymentMgr
     * @augments Widget
     */
    class PaymentMgr extends Widget {
        removeCard(card) {
            this.cardToRemove = card;
            this.getById('confirmDialog', (/** @type {confirmDialog} */confirmDialog) => {
                confirmDialog.showDialog({ cardNumber: card.data('card') });
            });
        }

        confirmedRemoveCard() {
            getJSONByUrl(this.cardToRemove.data('url'), { UUID: this.cardToRemove.data('id') }).then(() => {
                this.ref('self').remove();
                delete this.cardToRemove;
            });
        }
    }

    return PaymentMgr;
}
