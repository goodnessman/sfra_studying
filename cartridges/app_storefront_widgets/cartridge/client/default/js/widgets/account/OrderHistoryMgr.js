import { getJSONByUrl, getContentByUrl } from 'widgets/toolbox/ajax';
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 * @typedef {ReturnType<typeof import('widgets/forms/InputSelect').default>} InputSelect
 */

/**
 * @description Base OrderHistoryMgr implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof OrderHistoryMgr} Order History Manager class
 */

export default function (Widget) {
    /**
     * @class OrderHistoryMgr
     * @augments Widget
     */
    class OrderHistoryMgr extends Widget {
        prefs() {
            return {
                classesBusy: 'm-busy',
                accessibilityAlerts: {},
                ...super.prefs()
            };
        }

        /**
         * @description Load more orders
         * @param {refElement} button Target element
         * @returns {void}
        */
        loadMore(button) {
            const href = String(button.attr('href'));

            this.busy();
            getJSONByUrl(href).then(response => {
                Promise.all([
                    this.ref('orderList').append(response.orderListItems),
                    this.render(undefined, undefined, this.ref('listFooter'), response.orderListFooter)
                ]).then(()=> {
                    const accessibilityAlert = this.prefs().accessibilityAlerts.orderlistupdated;
                    this.eventBus().emit('alert.show', {
                        accessibilityAlert
                    });
                }).finally(() => {
                    this.unbusy();
                });
            });
        }


        /**
         * @description Update Order List
         * @param {InstanceType<InputSelect>} select Target element
         * @returns {void}
         */
        updateOrderList(select) {
            const selectedSorting = select.getSelectedOptions();
            if (selectedSorting) {
                const url = String(selectedSorting.val());

                this.busy();
                getContentByUrl(url).then(response => {
                    if (typeof response === 'string') {
                        this.render(undefined, undefined, this.ref('grid'), response);
                    }
                }).then(()=> {
                    const accessibilityAlert = this.prefs().accessibilityAlerts.orderlistupdated;
                    this.eventBus().emit('alert.show', {
                        accessibilityAlert
                    });
                }).finally(() => {
                    this.unbusy();
                });
            }
        }

        /**
         * @description Busy
         * @returns {void}
         */
        busy() {
            this.ref('grid').addClass(this.prefs().classesBusy).attr('aria-busy', 'true');
        }

        /**
         * @description Unbusy
         * @returns {void}
         */
        unbusy() {
            this.ref('grid').removeClass(this.prefs().classesBusy).attr('aria-busy', false);
        }
    }

    return OrderHistoryMgr;
}
