// TODO: non working modal dialog (calling Account-PasswordResetDialogForm via GET instead of POST and so on)
/**
 * @description Base PasswordResetModal implementation
 * @param {ReturnType<typeof import('widgets/global/Modal').default>} Modal Base widget for extending
 * @returns {typeof PasswordResetModal} Password reset modal class
 */
export default function (Modal) {
    /**
     * @class PasswordResetModal
     * @augments Modal
     */
    class PasswordResetModal extends Modal {
        /**
         * @description Get response from server and render it
         */
        onSuccessSubmit(form, data) {
            this.render(undefined, data, this.ref('container'));
            return this;
        }
    }

    return PasswordResetModal;
}
