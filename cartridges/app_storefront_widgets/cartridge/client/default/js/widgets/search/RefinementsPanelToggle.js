// TODO: Create PanelToggleMixin
const keyCode = Object.freeze({
    RETURN: 13,
    SPACE: 32,
    UP: 38,
    DOWN: 40
});

/**
 * @description Base RefinementsPanelToggle implementation
 * @param {typeof import('widgets/Widget').default} Widget Base widget for extending
 */
export default function (Widget) {
    /**
     * @class RefinementsPanelToggle
     * @augments Widget
     * @classdesc Widget, which triggers touch/mobile refinements panel to open/close
     * @property {string} data-widget - Widget name `refinementsPanelToggle`
     * @property {string} data-event-click - Event handler method for click event
     * @property {string} data-event-keydown - Event handler method for keydown event
     */
    class RefinementsPanelToggle extends Widget {
        /**
         * @description Init widget logic
         * @returns {void}
         */
        init() {
            this.eventBus().on('refinement.panel.close', 'closePanel');
            this.eventBus().on('refinement.panel.open', 'openPanel');
        }

        /**
         * @description Focus
         * @returns {void}
         */
        focus() {
            this.ref('self').focus();
        }

        /**
         * @description Show panel
         * @param {boolean} [focusToLastItem] Focus to last item flag
         * @returns {void}
         */
        showPanel(focusToLastItem) {
            this.openPanel();
            this.eventBus().emit('refinement.panel.open', { focusToLastItem });
        }

        /**
         * @description Handle Click
         * @returns {void}
         */
        handleClick() {
            this.showPanel();
        }

        /**
         * @description Open panel
         * @returns {void}
         */
        openPanel() {
            this.ref('self').attr('aria-expanded', 'true');
        }

        /**
         * @description Close panel
         * @returns {void}
         */
        closePanel() {
            this.focus();
            this.ref('self').attr('aria-expanded', 'false');
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.UP:
                    this.showPanel(true);
                    preventEventActions = true;

                    break;

                case keyCode.RETURN:
                case keyCode.SPACE:
                case keyCode.DOWN:
                    this.showPanel();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return RefinementsPanelToggle;
}
