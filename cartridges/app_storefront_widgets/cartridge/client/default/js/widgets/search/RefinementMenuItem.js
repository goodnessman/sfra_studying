import {
    appendParamToURL,
    getUrlParams
} from 'widgets/toolbox/util';

const keyCode = Object.freeze({
    ESC: 27,
    PAGEUP: 33,
    PAGEDOWN: 34,
    END: 35,
    HOME: 36,
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39
});

/**
 * @description Base RefinementMenuItem implementation
 * @param {ReturnType <typeof import('widgets/global/listAccessibility').default>} ListAccessibility Base widget for extending
 * @typedef {ReturnType <typeof import('widgets/global/Refinement').default>} Refinement Refinement widget
 * @typedef {InstanceType <Refinement>} refinement Refinement widget instance
 * @returns {typeof RefinementMenuItem} RefinementMenuItem widget
 */
export default function (ListAccessibility) {
    /**
     * @class RefinementMenuItem
     * @augments ListAccessibility
     * @classdesc Represents Refinement Menu Item with specific logic for submenus, keyboard navigation.
     * @property {string} data-widget - Widget name `refinementMenuItem`
     * @property {string} data-event-keydown - Event listener for `handleKeydown` method
     * @property {boolean} data-prevent-show-submenu - prevent showing submenu
     * @property {string} data-refinebar-url - refine bar url
     * @property {string} data-widget-event-previous - Previous panel handler
     * @property {string} data-update-url - update url
     */

    class RefinementMenuItem extends ListAccessibility {
        prefs() {
            return {
                preventShowSubmenu: false,
                refinementSelected: false,
                submenu: 'submenu',
                updateUrl: '',
                currentUrl: '',
                refinebarUrl: '',
                title: '',
                ...super.prefs()
            };
        }

        /**
         * @description Has submenu
         * @returns {boolean} Return true if submenu exist
         */
        hasSubmenu() {
            return this.has(this.prefs().submenu) && !this.prefs().preventShowSubmenu;
        }

        /**
         * @description Initialize widget logic
         * @returns {void}
         */
        init() {
            this.defineItems();
            this.isSubmenuOpen = false;
        }

        /**
         * @description Initialize widget logic
         * @returns {Array<refinement>} Array of refinement widgets
         */
        getRefinements() {
            const Refinement = /** @type {Refinement} */(this.getConstructor('refinement'));

            let refinement = [];

            if (this.items) {
                refinement = this.items.filter((item) => {
                    let valid = false;

                    if (item instanceof Refinement) {
                        return item.isAttributeRefinement();
                    }

                    return valid;
                });
            }

            return refinement;
        }

        /**
         * @description Has Checked Refinements
         * @returns {boolean} Has Checked Refinements Flag
         */
        hasCheckedRefinements() {
            return this.getRefinements().some((refinement) => refinement.selected);
        }

        /**
         * @description Uncheck Refinements
         * @returns {void}
         */
        uncheckRefinements() {
            this.getRefinements().forEach((refinement) => {
                refinement.uncheck();
            });
        }

        /**
         * @description Clear Refinements
         * @returns {void}
         */
        clearRefinements() {
            this.uncheckRefinements();
            this.hideRefinementControls(true);
            this.defineItems();
            this.setFocusToFirstItem();
        }

        /**
         * @description Mark Submenu Opened
         * @returns {void}
         */
        markSubmenuOpened() {
            this.isSubmenuOpen = true;
            this.ref('self').attr('aria-expanded', 'true');
        }

        /**
         * @description Has opened submenu
         * @returns {boolean|undefined} Return true if submenu opened
         */
        hasOpenedSubmenu() {
            return this.isSubmenuOpen;
        }

        /**
         * @description Open next menu level if exist
         * @returns {void}
         */
        openMenu() {
            if (this.hasSubmenu()) {
                let itemName = this.prefs().title;
                let submenuHTML;

                this.has(this.prefs().submenu, (submenu) => {
                    const submenuElement = submenu.get();

                    if (submenuElement) {
                        submenuHTML = submenuElement.innerHTML;
                    }
                });
                this.eventBus().emit('refinement.panel.next.level', {
                    panelName: itemName,
                    htmlMarkup: submenuHTML,
                    showRefinementControls: this.prefs().refinementSelected
                });
            }
        }

        /**
         * @description Show Refinement Controls
         * @returns {void}
         */
        showRefinementControls() {
            this.getById('clearBtn', (button) => button.show());
            this.getById('applyBtn', (button) => button.show());
            this.getById('backBtn', (button) => button.hide());
        }

        /**
         * @description Hide Refinement Controls
         * @param {boolean} [showApply] Show Apply button flag
         * @returns {void}
         */
        hideRefinementControls(showApply) {
            this.getById('clearBtn', (button) => button.hide());

            if (showApply) {
                this.getById('applyBtn', (button) => button.show());
                this.getById('backBtn', (button) => button.hide());
            } else {
                this.getById('applyBtn', (button) => button.hide());
                this.getById('backBtn', (button) => button.show());
            }
        }

        /**
         * @description Set Submenu Title
         * @param {string} submenuTitle Submenu Title
         * @returns {void}
         */
        setSubmenuTitle(submenuTitle) {
            this.has('title', (submenu) => {
                submenu.setText(submenuTitle);
            });
        }

        /**
         * @description Set Submenu HTML
         * @param {string} submenuHTML Submenu HTML
         * @returns {void}
         */
        setSubmenuHTML(submenuHTML) {
            this.has(this.prefs().submenu, (submenu) => {
                let submenuElement = submenu.get();

                if (submenuElement) {
                    submenuElement.innerHTML = submenuHTML;
                }
            });
        }

        /**
         * @description Click Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {Event} event Event object
         */
        handleClick(_, event) {
            if (this.hasSubmenu()) {
                event.preventDefault();
                this.openMenu();
            }
        }

        /**
         * @description Close Submenu
         * @returns {void}
         */
        closeSubmenu() {
            this.eventBus().emit('refinement.panel.previous.level');
            this.ref('self').attr('aria-expanded', 'false');
            this.isSubmenuOpen = false;
        }

        /**
         * @description Parse Url Refinement Parameter
         * @returns {void}
         */
        parseUrlRefinementParameter() {
            /**
             * @type {object}
             */
            let paramMap = getUrlParams(this.prefs().updateUrl || this.prefs().currentUrl);

            this.refinementParameterMap = {};

            Object.keys(paramMap).forEach((paramKey) => {
                if (this.refinementParameterMap) {
                    if (paramKey.includes('prefn')) {
                        this.refinementParameterMap[paramMap[paramKey]] = paramMap[paramKey.replace('prefn', 'prefv')].split('|');
                    } else if (!paramKey.includes('prefv')) {
                        this.refinementParameterMap[paramKey] = paramMap[paramKey];
                    }
                }
            });
        }

        /**
         * @description Update Refinement Controls
         * @returns {void}
         */
        updateRefinementControls() {
            if (this.hasCheckedRefinements()) {
                this.showRefinementControls();
            } else {
                this.hideRefinementControls(true);
            }

            this.defineItems(this.currentItem);

            this.setFocusToCurrentItem();
        }

        /**
         * @description Apply change
         */
        applyChange() {
            const values = [];
            let url = this.prefs().refinebarUrl;
            let counter = 1;
            let type;
            let min;
            let max;

            this.getRefinements().forEach((refinement) => {
                type = refinement.attrId;

                if (refinement.selected) {
                    if (type === 'price') {
                        min = refinement.min;
                        max = refinement.max;
                    } else {
                        values.push(refinement.value);
                    }
                }
            });

            if (this.refinementParameterMap) {
                if (type === 'price') {
                    if (max) {
                        this.refinementParameterMap.pmin = min;
                        this.refinementParameterMap.pmax = max;
                    } else {
                        delete this.refinementParameterMap.pmin;
                        delete this.refinementParameterMap.pmax;
                    }
                } else if (values.length) {
                    this.refinementParameterMap[type] = values;
                } else {
                    delete this.refinementParameterMap[type];
                }

                Object.keys(this.refinementParameterMap).forEach((key) => {
                    if (this.refinementParameterMap && !['start', 'sz'].includes(key)) {
                        if (Array.isArray(this.refinementParameterMap[key])) {
                            url = appendParamToURL(url, 'prefn' + counter, key);
                            url = appendParamToURL(url, 'prefv' + counter, this.refinementParameterMap[key].join('|'));
                            counter++;
                        } else {
                            url = appendParamToURL(url, key, this.refinementParameterMap[key]);
                        }
                    }
                });

                this.eventBus().emit('refinement.panel.update', { url });
            }
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            if (!this.hasOpenedSubmenu()) {
                return;
            }

            switch (event.keyCode) {
                case keyCode.LEFT:
                    this.closeSubmenu();
                    preventEventActions = true;

                    break;

                case keyCode.PAGEUP:
                case keyCode.HOME:
                    this.setFocusToFirstItem();
                    preventEventActions = true;

                    break;

                case keyCode.PAGEDOWN:
                case keyCode.END:
                    this.setFocusToLastItem();
                    preventEventActions = true;

                    break;

                case keyCode.UP:
                    this.setFocusToPreviousItem();
                    preventEventActions = true;

                    break;

                case keyCode.DOWN:
                    this.setFocusToNextItem();
                    preventEventActions = true;

                    break;

                case keyCode.RIGHT:
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return RefinementMenuItem;
}
