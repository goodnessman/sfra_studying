/**
 * @typedef {ReturnType<typeof import('widgets/global/AccordionItem').default>} AccordionItem
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 */

/**
 * @description Base PaymentAccordionItem implementation
 * @param {AccordionItem} AccordionItem Base widget for extending
 * @returns {typeof PaymentAccordionItem} Payment Tab Panel class
 */
export default function (AccordionItem) {
    /**
     * @class PaymentTabPanel
     * @classdesc Implements Payment Accordion Item general behaviour
     * @augments AccordionItem
     */
    class PaymentAccordionItem extends AccordionItem {
        /**
         * @description Initialize widget logic
         * @returns {void}
         */
        init() {
            super.init();

            if (this.prefs().expanded) {
                this.openPanel();
            }
        }

        /**
         * @description Handle panel errors
         * @param {object} data - data jobject
         */
        handleError(data) {
            if (!data.fieldName) {
                return;
            }

            this.getById(data.fieldName, (/** @type {basicInput} */field) => {
                if (field.id === data.fieldName) {
                    field.setError(data.errorMsg);
                }
            });
        }

        /**
         * @description On Refresh Handler
         * @returns {void}
         */
        onRefresh() {
            super.onRefresh();

            if (this.isPanelOpen) {
                this.openPanel();
            }
        }
    }

    return PaymentAccordionItem;
}
