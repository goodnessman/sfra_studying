/**
 * @typedef {ReturnType<typeof import('widgets/forms/InputRadio').default>} InputRadio
 */

/**
 * @description Base RadioSelector implementation
 * @param {InputRadio} InputRadio Base widget for extending
 * @returns {typeof RadioSelector} Credit Card Number class
 */
export default function (InputRadio) {
    /**
     * @class RadioSelector
     * @augments InputRadio
     */
    class RadioSelector extends InputRadio {
        /**
         * @description Update  radio selector state with new data
         * @param {object} data - object with data
         * @returns {Promise} result
         */
        updateState(data) {
            return this.render(undefined, data);
        }
    }

    return RadioSelector;
}
