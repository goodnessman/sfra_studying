// TODO: JSDoc for all methods
// TODO: Get rid (or manual check and vendoring) of cleave.js for security reasons
// CLARIFY: Some library should be used instead of cleave or it should be turned back
/**
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 */
/**
 * @description Base InputCreditCardNumber implementation
 * @param {BasicInput} BasicInput Base widget for extending
 * @returns {typeof InputCreditCardNumber} Credit Card Number class
 */
export default function (BasicInput) {
    /**
     * @class InputCreditCardNumber
     * @augments BasicInput
     */
    class InputCreditCardNumber extends BasicInput {
        init() {
            super.init();

            import(/* webpackChunkName: 'cleave' */'cleave.js').then((Cleave) => {
                // eslint-disable-next-line new-cap
                this.cleave = new Cleave.default(this.ref('field').get(), {
                    creditCard: true,
                    onValueChanged: () => this.emit('keyup')
                });

                this.onDestroy(() => {
                    if (this.cleave) {
                        this.cleave.destroy();
                        this.cleave = undefined;
                    }
                });
            });
        }

        getValue() {
            if (this.cleave) {
                return this.cleave.getRawValue();
            }
            return super.getValue();
        }
        // setValue(newVal, silent) {
        //     super.setValue(newVal, silent);
        //     if (this.cleave) {
        //         this.cleave.setRawValue(newVal);
        //     }
        // }
    }

    return InputCreditCardNumber;
}
