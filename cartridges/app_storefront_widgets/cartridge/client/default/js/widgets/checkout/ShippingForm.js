// TODO: JSDoc for all methods
// TODO: togllers should be implemented according to disclosure specs
// https://www.w3.org/TR/wai-aria-practices/#disclosure
import { submitFormJson } from 'widgets/toolbox/ajax';

/**
 * @typedef {ReturnType<typeof import('widgets/checkout/AddressCheckoutForm').default>} AddressCheckoutForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputRadio').default>>} inputRadio
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputTextarea').default>>} inputTextarea
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputCheckbox').default>>} inputCheckbox
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/GoogleAutocompleteField').default>>} googleAutocompleteField
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/RadioSelector').default>>} radioSelector
 */

/**
 * @description Base ShippingForm implementation
 * @param {AddressCheckoutForm} AddressCheckoutForm Base widget for extending
 * @returns {typeof ShippingForm} Shipping Form class
 */
export default function (AddressCheckoutForm) {
    /**
     * @class ShippingForm
     * @augments AddressCheckoutForm
     * @property {string} data-select-shipping-method-url
     * @property {string} data-update-shipping-list-url
     */
    class ShippingForm extends AddressCheckoutForm {
        prefs() {
            return {
                updateShippingListUrl: '',
                selectShippingMethodUrl: '',
                ...super.prefs()
            };
        }

        formDataUpdated() {
            this.updateShippingState();
            this.updateAddressAutocomplete();
        }

        onSubmitted(data) {
            if (data.error && data.serverErrors && data.serverErrors.length) {
                this.setError(data.serverErrors.join(''), undefined, true);
            } else if (data.error && data.fieldErrors) {
                data.fieldErrors.forEach((entries) => {
                    this.updateAddress();
                    let firstError = true;
                    Object.entries(entries).forEach(([name, errorMsg]) => {
                        this.getById(name, (/** @type {basicInput} */address) => {
                            if (address.id === name) {
                                if (firstError) {
                                    address.setFocus();
                                }
                                firstError = false;
                                address.setError(errorMsg);
                            }
                        });
                    });
                });
            } else {
                super.onSubmitted(data);
            }
        }

        updateShippingState() {
            this.showProgressBar();
            submitFormJson(
                this.prefs().updateShippingListUrl,
                this.getFormFieldsShort()
            ).then((response) => {
                if (response && response.order) {
                    this.updateState(response.order);
                    this.emit('updateshippingform', response.order);
                }
            }).finally(() => {
                this.hideProgressBar();
            });
        }

        updateState(basket) {
            this.getById('shippingMethodList', (/** @type {radioSelector} */shippingMethodList) => {
                return shippingMethodList.updateState(basket);
            });
        }

        selectShippingMethod(inputRadio) {
            this.showProgressBar();
            submitFormJson(
                this.prefs().selectShippingMethodUrl,
                { methodID: inputRadio.getDataValue(), ...this.getFormFieldsShort() }
            ).then((response) => {
                if (response && response.order) {
                    this.emit('updateshippingform', response.order);
                }
            }).finally(() => {
                this.hideProgressBar();
            });
        }

        /**
         * @description Handles address autocomplete update with the current country code
         */
        updateAddressAutocomplete() {
            this.getById('googleAutocomplete', (/** @type {googleAutocompleteField} */ googleAutocompleteField) => {
                googleAutocompleteField.updateState(this.config.countryCode);
            });
        }

        /**
         * @param {inputCheckbox} inputCheckbox - input
         */
        toggleGiftWrap(inputCheckbox) {
            const checked = inputCheckbox.getValue();

            this.getById('giftMessage', (/** @type {inputTextarea} */giftMessage) => {
                giftMessage.toggle(Boolean(checked));
            });
        }
    }

    return ShippingForm;
}
