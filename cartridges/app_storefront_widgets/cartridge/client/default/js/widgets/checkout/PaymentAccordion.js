/**
 * @typedef {ReturnType<typeof import('widgets/global/Accordion').default>} Accordion
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/PaymentAccordionItem').default>>} paymentAccordionItem
 */

/**
 * @description Base PaymentTabs implementation
 * @param {Accordion} Accordion Base widget for extending
 * @returns {typeof PaymentAccordion} Payment Tabs class
 */
export default function (Accordion) {
    /**
     * @class PaymentAccordion
     * @property {string} data-field-name - payment field name
     * @augments Accordion
     */
    class PaymentAccordion extends Accordion {
        prefs() {
            return {
                fieldName: '',
                ...super.prefs()
            };
        }

        validate() {
            const currentPaymentItem = this.getCurrentPaymentItem();

            if (currentPaymentItem) {
                const isValid = currentPaymentItem.isChildsValid();

                if (!isValid && !currentPaymentItem.isPanelOpen) {
                    currentPaymentItem.openPanel();
                }

                return isValid;
            }

            return false;
        }

        process() {
            return new Promise((resolve, reject) => {
                const currentPaymentItem = this.getCurrentPaymentItem();

                if (currentPaymentItem) {
                    const tabProcessResult = currentPaymentItem.process().then(data => {
                        return { [this.prefs().fieldName]: currentPaymentItem.prefs().methodId, ...data };
                    });

                    resolve(tabProcessResult);
                } else {
                    reject(new Error('No payment'));
                }
            });
        }

        handleError(fieldName, errorMsg) {
            const currentPaymentItem = this.getCurrentPaymentItem();

            if (currentPaymentItem) {
                return currentPaymentItem.handleError({ fieldName, errorMsg });
            }

            return undefined;
        }

        cleanupSensitiveData() {
            const currentPaymentItem = this.getCurrentPaymentItem();

            if (currentPaymentItem) {
                return currentPaymentItem.data('cleanupSensitiveData');
            }

            return undefined;
        }

        getCurrentPaymentItem() {
            let currentItem;

            this.eachChild(item => {
                if (item.isPanelOpen) {
                    currentItem = item;
                }
            });

            return currentItem;
        }
    }

    return PaymentAccordion;
}
