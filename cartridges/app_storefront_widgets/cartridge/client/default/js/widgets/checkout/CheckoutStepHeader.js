/**
 * @typedef {ReturnType<typeof import('widgets/global/Label').default>} Label
 */

/**
 * @description Base CheckoutStepHeader implementation
 * @param {Label} Label Base widget for extending
 * @returns {typeof CheckoutStepHeader} Checkout Step Header class
 */
export default function (Label) {
    /**
     * @class CheckoutStepHeader
     * @augments Label
     */
    class CheckoutStepHeader extends Label {
        editStep() {
            const step = this.ref('editStepBtn').data('step');

            this.emit('editstep', step);
        }
    }

    return CheckoutStepHeader;
}
