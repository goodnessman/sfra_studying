/**
 * @typedef {ReturnType<typeof import('widgets/checkout/PaymentAccordionItem').default>} PaymentAccordionItem
 */

/**
 * @description Base ZeroPaymentAccordionItem implementation
 * @param {PaymentAccordionItem} PaymentAccordionItem Base widget for extending
 * @returns {typeof PaymentAccordionItemZEROPAYMENT} Zero Payment Accordion Item class
 */
export default function (PaymentAccordionItem) {
    /**
     * @class PaymentAccordionItemZEROPAYMENT
     * @augments PaymentAccordionItem
     */
    class PaymentAccordionItemZEROPAYMENT extends PaymentAccordionItem {
        process() {
            return Promise.resolve([]);
        }

        isChildsValid() {
            return true;
        }

        cleanupSensitiveData() { }
    }

    return PaymentAccordionItemZEROPAYMENT;
}
