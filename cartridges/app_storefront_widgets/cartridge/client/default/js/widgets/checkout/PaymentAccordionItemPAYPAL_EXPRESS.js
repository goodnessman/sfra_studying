/**
 * @typedef {ReturnType<typeof import('widgets/checkout/PaymentAccordionItem').default>} PaymentAccordionItem
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 */

/**
 * @description Base PaymentAccordionItemPAYPAL_EXPRESS implementation
 * @param {PaymentAccordionItem} PaymentAccordionItem Base widget for extending
 * @returns {typeof PaymentAccordionItemPAYPALEXPRESS } PAYPAL_EXPRESS Payment Accordion Item class
 */
export default function (PaymentAccordionItem) {
    /**
     * @class PaymentAccordionItemPAYPAL_EXPRESS
     * @augments PaymentAccordionItem
     */
    class PaymentAccordionItemPAYPALEXPRESS extends PaymentAccordionItem {
        process() {
            return new Promise(resolve => {
                const fields = [];
                /**
                 * TODO: Implement paypal process logic here
                 */

                resolve(fields);
            });
        }

        isChildsValid() {
            return true;
        }

        cleanupSensitiveData() { }
    }

    return PaymentAccordionItemPAYPALEXPRESS;
}
