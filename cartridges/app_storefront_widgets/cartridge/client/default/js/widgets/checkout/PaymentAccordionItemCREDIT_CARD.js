// TODO: JSDoc for all methods
// CLARIFY: implement partial matching. 3rd party plugins should not be used for security reasons

/**
 * @typedef {ReturnType<typeof import('widgets/checkout/PaymentAccordionItem').default>} PaymentAccordionItem
 * @typedef {ReturnType<typeof import('widgets/forms/BasicInput').default>} BasicInput
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 */

const regexToCardsMaping = {
    Bijcard: /^5100081/, // CLARIFY: full length of card ?
    Jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
    Cup: /^(62[0-9]{14,17})$/,
    Visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
    Maestro: /^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$/,
    Master: /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/,
    Amex: /^3[47][0-9]{13}$/,
    Diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
    // eslint-disable-next-line max-len
    Discover: /^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$/,
    Cartebancaire: /^(4|5|6)\d{15}/
};

/**
 *
 * @param {string} cardNumber number of card
 * @returns {Array<string>} result
 */
function determin(cardNumber) {
    let cardNumberReplaced = cardNumber.replace(/[\s-]/g, '');

    return Object.keys(regexToCardsMaping)
        .filter(key => regexToCardsMaping[key] && regexToCardsMaping[key].test(cardNumberReplaced));
}

/**
 * @description Base PaymentAccordionItemCREDITCARD implementation
 * @param {PaymentAccordionItem} PaymentAccordionItem Base widget for extending
 * @returns {typeof PaymentAccordionItemCREDITCARD} Credit Card Payment item class
 */
export default function (PaymentAccordionItem) {
    /**
     * @class PaymentAccordionItemCREDITCARD
     * @property {string} data-field-name - payment field name
     * @augments PaymentAccordionItem
     */
    class PaymentAccordionItemCREDITCARD extends PaymentAccordionItem {
        prefs() {
            return {
                creditCard: {
                    type: 'dwfrm_billing_creditCardFields_cardType',
                    number: 'dwfrm_billing_creditCardFields_cardNumber',
                    expirationMonth: 'dwfrm_billing_creditCardFields_expirationMonth',
                    expirationYear: 'dwfrm_billing_creditCardFields_expirationYear',
                    save: 'dwfrm_billing_creditCardFields_saveCard',
                    securityCode: 'dwfrm_billing_creditCardFields_securityCode'
                },
                savedCard: {
                    selectedCardID: 'dwfrm_billing_creditCardFields_selectedCardID',
                    securityCode: 'securityCode'
                },
                newPaymentBlock: 'newPaymentBlock',
                storedPaymentInstrumentsBlock: 'storedPaymentInstrumentsBlock',
                fieldName: '',
                ...super.prefs()
            };
        }

        process() {
            return new Promise(resolve => {
                const fields = this.getFormFields();

                if (!this.ref('newPaymentBlock').hasAttr('hidden')) {
                    fields[this.prefs().creditCard.type] = determin(
                        fields[this.prefs().creditCard.number]
                    ).shift() || 'Visa';

                    if (fields[this.prefs().creditCard.number]) {
                        fields[this.prefs().creditCard.number] = fields[this.prefs().creditCard.number].replace(/[\s-]/g, '');
                    }
                }

                resolve(fields);
            });
        }

        isChildsValid() {
            var valid = true;

            if (this.ref('newPaymentBlock').hasAttr('hidden')) {
                return this.validateSavedCard();
            }

            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(item => {
                if (item.getName() && !this.isCreditCardField(item.getName())) {
                    return;
                }

                if (item instanceof BasicInput && typeof item.validate === 'function' && !item.validate()) {
                    if (valid && item.setFocus) {
                        item.setFocus();
                    }
                    valid = false;
                }
            });

            return valid;
        }

        getFormFields() {
            /**
             * @type {{[x: string]: string}}
             */
            var fields = {};

            if (this.ref('newPaymentBlock').hasAttr('hidden')) {
                return this.getSavedCardFields();
            }

            const BasicInput = /** @type {BasicInput} */(this.getConstructor('basicInput'));

            this.eachChild(widget => {
                if (widget instanceof BasicInput && !(widget.skipSubmission && widget.skipSubmission())) {
                    const name = widget.getName && widget.getName();

                    if (name && !this.isSavedCardField(name)) {
                        fields[name] = widget.getValue();
                    }
                }
            });

            return fields;
        }

        isSavedCardField(name) {
            return [this.prefs().savedCard.selectedCardID, this.prefs().savedCard.securityCode].includes(name);
        }

        getSavedCardFields() {
            var fields = {};

            this.eachChild(widget => {
                const name = widget.getName && widget.getName();

                if (widget.id === this.prefs().savedCard.selectedCardID && name) {
                    fields[name] = widget.getValue();
                } else if (
                    widget.id === this.prefs().savedCard.securityCode
                    && widget.ref('self').data('selected')
                    && name
                ) {
                    fields[name] = widget.getValue();
                }
            });

            return fields;
        }

        validateSavedCard() {
            let valid = true;

            this.eachChild(item => {
                var isCurrentCVVField = (item.getName() === this.prefs().savedCard.securityCode)
                    && item.ref('self').data('selected');

                if (isCurrentCVVField && typeof item.validate === 'function' && !item.validate()) {
                    item.setFocus();

                    valid = false;
                }
            });

            return valid;
        }

        addNewCard() {
            this.ref('storedPaymentInstrumentsBlock').hide();
            this.showNewCreditCard();

            this.updateState();
        }

        clearSavedCardSensitiveData() {
            this.eachChild(child => {
                if (child.id === this.prefs().savedCard.securityCode) {
                    child.setValue('', true);
                    child.clearError();
                }
            });
        }

        showSavedPayments() {
            this.clearSavedCardSensitiveData();

            this.ref('storedPaymentInstrumentsBlock').show();
            this.ref('newPaymentBlock').hide();

            this.eachChild(child => {
                if (child.id === this.prefs().savedCard.securityCode) {
                    child.setValue('', true);
                }
            });

            this.updateState();
        }

        showNewCreditCard() {
            this.eachChild(child => {
                if (this.isCreditCardField(child.id)) {
                    child.setValue('', true);
                    child.clearError();
                }
            });

            this.ref('newPaymentBlock').show();
        }

        isCreditCardField(id) {
            var status = false;

            Object.keys(this.prefs().creditCard).forEach(field => {
                this.getById(this.prefs().creditCard[field], (/** @type {basicInput} */el) => {
                    if (el && el.id === id) {
                        status = true;
                    }
                });
            });

            return status;
        }

        selectSavedPayment(element) {
            this.getById(this.prefs().savedCard.selectedCardID, (/** @type {basicInput} */savedCardID) => {
                savedCardID.setValue(element.data('uuid'), true);
            });

            this.eachChild(child => {
                if (child.getName() === this.prefs().savedCard.securityCode) {
                    const isCurrentCard = child.ref('self').data('uuid') === element.data('uuid');

                    child.ref('self').data('selected', isCurrentCard);

                    if (!isCurrentCard) {
                        child.setValue('', true);
                        child.clearError();
                    }
                }
            });
        }

        cleanupSensitiveData() { }
    }

    return PaymentAccordionItemCREDITCARD;
}
