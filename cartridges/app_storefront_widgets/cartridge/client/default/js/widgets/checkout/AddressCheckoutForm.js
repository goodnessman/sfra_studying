// TODO: JSDoc for all methods
// TODO: notify blind user about not/applied changes
// TODO: toggle components should be implemented as disclosure or accordion
import { get } from 'widgets/toolbox/util';

/**
 * @typedef {ReturnType<typeof import('widgets/forms/AddressForm').default>} AddressForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputCheckbox').default>>} inputCheckbox
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/Label').default>>} label
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/RadioSelector').default>>} radioSelector
 *
 */

/**
 * @description Base AddressCheckoutForm implementation
 * @param {AddressForm} AddressForm Base widget for extending
 * @returns {typeof AddressCheckoutForm} Address Checkout Form
 */
export default function (AddressForm) {
    /**
     * @class AddressCheckoutForm
     * @augments AddressForm
     * @property {string} data-address-selector - saved addresses selector
     * @property {string} data-address-fields - list of address fields
     */
    class AddressCheckoutForm extends AddressForm {
        prefs() {
            return {
                addressSelector: '',
                ...super.prefs()
            };
        }

        /**
         * @description Converts `dwfrm_(shipping/billing)_addressFields_address2` to `address2`
         * @returns {object} result
         */
        getFormFieldsShort() {
            const fileds = this.getFormFields();

            return Object.keys(fileds).reduce((acc, fullKey) => {
                const shortKey = fullKey.split('_').pop();

                if (shortKey) {
                    acc[shortKey] = fileds[fullKey];
                }

                return acc;
            }, {});
        }

        hideAddressFields() {
            this.ref('addressForm').hide();
            this.ref('addressForm').disable();
        }

        showAddressFields() {
            this.ref('addressForm').show();
            this.ref('addressForm').enable();
        }

        updateAddress() {
            this.editMode = true;

            this.hideSavedAddresses();
            this.showAddressFields();
            this.fulfillAddress();
        }

        /**
         * @description Fill address based on datased
         * @param {object} dataset object with address data
         */
        fillAddressFields(dataset) {
            this.getAddressFields().forEach(item => {
                this.getById(item, (/** @type {basicInput} */addressField) => {
                    const name = addressField.getName() || '';
                    const shortName = name.split('_').pop();

                    if (shortName && shortName !== 'country') {
                        addressField.setValue(dataset[shortName] || '', true);
                    } else {
                        addressField.setValue(dataset.countryCode || '', true);
                    }
                });
            });
        }

        /**
         * @description Update form definition if form definition
         * not valid for saved address. Fill saved address into address fields
         */
        fulfillAddress() {
            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                const dataset = addressSelector.getDataSet();

                if (!dataset || (addressSelector.data('value') === 'new') || this.editMode) {
                    return;
                }

                const definitionReloaded = this.getAddressFields().some(item => {
                    let isReloaded;

                    this.getById(item, /** @type {basicInput} */addressField => {
                        const name = addressField.getName() || '';
                        const shortName = name.split('_').pop();

                        if (shortName === 'country') {
                            const countryCode = addressField.getValue();

                            if (dataset.countryCode && (countryCode !== dataset.countryCode)) {
                                this.updateFormData({ countryCode: dataset.countryCode }).then(() => {
                                    this.fillAddressFields(dataset);
                                });
                                isReloaded = true;
                            }
                        }

                        isReloaded = false;
                    });

                    return isReloaded;
                });

                if (!definitionReloaded) {
                    this.fillAddressFields(dataset);
                    this.formDataUpdated();
                }
            });
        }

        /**
         * @description Handle click on  add new address button.
         * Toggle saved and new address blocks
         */
        addNewAddress() {
            this.editMode = false;
            this.showAddressFields();

            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                addressSelector.data('value', 'new');
            });

            this.getAddressFields().forEach(item => {
                this.getById(item, (/** @type {basicInput} */addressField) => {
                    const name = addressField.getName() || '';

                    if (!name.includes('country')) {
                        addressField.clearError();
                        addressField.setValue('', true);
                    }
                });
            });

            this.hideSavedAddresses();
        }

        /**
         * @description Handle click on back to saved addresses button
         */
        backToAddresses() {
            this.showSavedAddresses();
            this.changeAddress();
        }

        hideSavedAddresses() {
            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                addressSelector.hide();
            });
        }

        showSavedAddresses() {
            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                addressSelector.show();
            });
        }

        /**
         * @description Handle saved address change. Prefill saved address to the address form
         */
        changeAddress() {
            this.editMode = false;

            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                const addressID = addressSelector.getDataValue();

                if (addressID === 'new') {
                    this.addNewAddress();
                } else {
                    this.markSelectedAddress(addressID);
                }
            });
        }

        /**
         * @description Initialize saved addresses selector
         * @returns {Promise| undefined} result
         */
        activateAddressSelector() {
            return this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                this.updateModels();

                return addressSelector.updateState({
                    order: this.order,
                    customer: this.customer,
                    isFieldsShown: false
                }).then(() => {
                    if (addressSelector.data('value') !== 'new') {
                        this.hideAddressFields();
                        this.fulfillAddress();
                    } else {
                        this.showAddressFields();
                    }

                    this.onAddressSelectorActivated();
                }).then(() => {
                    this.getById('useShippingDescription', (/** @type {label} */ useShippingDescription) => {
                        useShippingDescription.updateTemplate({
                            order: this.order
                        });
                    });
                });
            });
        }

        /**
         * @description Fix for mustache template of shipping/billing address selector
         * @param {string} selectedAddressID - unique address identifier
         */
        markSelectedAddress(selectedAddressID) {
            if (selectedAddressID && this.order && this.customer) {
                this.updateModels(selectedAddressID);

                this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                    addressSelector.updateState({
                        order: this.order,
                        customer: this.customer
                    }).then(() => {
                        this.fulfillAddress();
                        this.hideAddressFields();
                    });
                });
            }
        }

        /**
         * @description Updates models address selection to reflect in a respective Address Selector
         * as a selected item
         * @param {string|undefined} [selectedAddressID] - unique address identifier
         */
        updateModels(selectedAddressID) {
            const { order, customer } = this;

            if (!order || !customer) {
                return;
            }

            const shippingAddress = this.setShippingAddress(order, customer, selectedAddressID);

            this.setBillingAddress(order, customer, selectedAddressID || shippingAddress);
        }

        setShippingAddress(order, customer, selectedAddressID) {
            let selectedAddress;
            const shippingAddressMatcher = selectedAddressID || get(order, 'shipping.0.shippingAddress.addressUUID');

            if (order.shipping && order.shipping.length) {
                order.shipping.forEach(shipping => {
                    const shippingAddress = shipping.shippingAddress;

                    shipping.addressSelected = shippingAddress && (shippingAddress.addressUUID === shippingAddressMatcher)
                        ? 'checked'
                        : '';

                    if (shipping.addressSelected) {
                        selectedAddress = shipping.shippingAddress.addressUUID;
                    }
                });
            }

            if (!selectedAddress && customer && customer.addresses && customer.addresses.length) {
                customer.addresses.forEach(addr => {
                    addr.shippingAddressSelected = addr.addressUUID === shippingAddressMatcher
                        ? 'checked'
                        : '';

                    if (addr.shippingAddressSelected) {
                        selectedAddress = addr.addressUUID;
                    }
                });

                if (!selectedAddress) {
                    customer.addresses[0].shippingAddressSelected = 'checked';
                    selectedAddress = customer.addresses[0].addressUUID;
                }
            }

            return selectedAddress;
        }

        setBillingAddress(order, customer, selectedAddressID) {
            let billingAddressID = get(order, 'billingAddress.addressUUID');
            let selectedAddress;

            if (billingAddressID) {
                order.billingAddress.addressSelected = 'checked';
                selectedAddress = order.billingAddress.addressUUID;
            } else {
                billingAddressID = selectedAddressID;
            }

            if (!selectedAddress && customer && customer.addresses && customer.addresses.length) {
                customer.addresses.forEach(addr => {
                    addr.billingAddressSelected = addr.addressUUID === billingAddressID
                        ? 'checked'
                        : '';

                    if (addr.billingAddressSelected) {
                        selectedAddress = addr.addressUUID;
                    }
                });
            }
        }

        /**
         * @param {inputCheckbox} useShippingCheckbox
         */
        toggleUseShipping(useShippingCheckbox) {
            const checked = Boolean(useShippingCheckbox.getValue());

            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                addressSelector.toggle(!checked);
            });
        }

        /**
         * @description Address selector activated callback
         */
        onAddressSelectorActivated() {

        }

        activate({ order, customer }) {
            this.order = order;
            this.customer = customer;
            this.activateAddressSelector();
        }
    }

    return AddressCheckoutForm;
}
