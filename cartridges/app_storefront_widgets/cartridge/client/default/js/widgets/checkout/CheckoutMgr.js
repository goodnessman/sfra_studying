// TODO: JSDoc for all methods
// TODO: check if error messages are accessable
// TODO: notify blind user about all not/applied changes with alert messages.
// Global notfication component should be implemented
// TODO: freeze loaded sections during update with aria-busy
// TODO: investigate proper implementation of aria-live region for updated sections
// TODO: keep track that focus stays on the same place during checkout update

import {
    timeout,
    appendParamsToUrl,
    getUrlParams,
    get
} from 'widgets/toolbox/util';
import { submitFormJson } from 'widgets/toolbox/ajax';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/ShippingForm').default>>} shippingForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/BillingForm').default>>} billingForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputCheckbox').default>>} inputCheckbox
 *
 */

/**
 * @description Base CheckoutMgr implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof CheckoutMgr} Checkout Manager class
 */

export default function (Widget) {
    /**
     * @class CheckoutMgr
     * @augments Widget
     * @property {string} shippingFormID - shipping form ID
     * @property {string} billingFormID - billing form ID
     * @property {string} stepsList - list of checkot steps
     * @property {object} data-basket - basket data
     * @property {object} data-customer - customer data object
     * @property {string} data-url-checkout
     * @property {string} data-url-place-order
     */
    class CheckoutMgr extends Widget {
        prefs() {
            return {
                shippingFormID: 'dwfrm_shipping',
                billingFormID: 'dwfrm_billing',
                stepsList: 'shipping,billing,summary',
                basket: {},
                customer: {},
                urlCheckout: '',
                urlPlaceOrder: '',
                ...super.prefs()
            };
        }

        init() {
            this.onDestroy(timeout(() => {
                const basket = this.prefs().basket;

                this.order = basket;
                this.customer = this.prefs().customer;

                this.getById(
                    this.prefs().shippingFormID,
                    (/** @type {shippingForm} */shippingForm) => shippingForm.updateState(basket)
                );

                const urlParams = getUrlParams();
                if (basket && basket.steps && basket.steps.billing && !basket.steps.billing.iscompleted) {
                    this.currentStep = 'shipping';
                } else if (typeof urlParams.step === 'string') {
                    this.currentStep = urlParams.step;
                } else if (urlParams.stage === 'payment') {
                    this.currentStep = 'billing';
                } else {
                    this.currentStep = 'shipping';
                }

                this.activateStepByName(this.currentStep);

                this.updateSummary(basket);
            }));

            this.initEvents();
        }

        initEvents() {
            this.ev('popstate', () => {
                const urlParams = getUrlParams();
                if (typeof urlParams.step === 'string') {
                    this.activateStepByName(urlParams.step);
                }
            }, window);
        }

        /**
         *
         * @param {string} name name of step
         */
        activateStepByName(name) {
            let isStepActivated = false;
            this.prefs().stepsList.split(',').forEach(step => {
                if (step === name) {
                    isStepActivated = true;
                    this.currentStep = step;
                    this[step + 'Activate'].call(this);

                    const urlParams = getUrlParams();
                    if (urlParams.step !== step) {
                        window.history.pushState(undefined, '', appendParamsToUrl(this.prefs().urlCheckout, { step }));
                    }
                } else {
                    this[step + 'DeActivate'].call(this, !isStepActivated);
                }
            });
            this.updateSummary();
            this.eventBus().emit('checkout.' + name + '.step.activated', this);
        }

        /**
         * @param {object} [order] order data
         */
        updateSummary(order) {
            let localOrder = order;
            if (localOrder && typeof localOrder.order !== 'undefined') {
                localOrder = localOrder.order;
            }

            if (localOrder) {
                this.order = localOrder;
                if (localOrder.billing
                    && localOrder.billing.payment
                    && localOrder.billing.payment.selectedPaymentInstruments
                ) {
                    localOrder.billing.payment.selectedPaymentInstruments.forEach(pi => {
                        pi[pi.paymentMethod] = true;
                    });
                }
            }

            if (this.order) {
                this.order.isShipping = this.currentStep === 'shipping';
                this.order.isBilling = this.currentStep === 'billing';
                this.order.isSummary = this.currentStep === 'summary';
                this.eventBus().emit('order.updated', this.order);
            }

            let shipping = (localOrder || this.order || this.prefs().basket).shipping;
            let hasShippingMethods = Boolean(get(shipping, '0.applicableShippingMethods.length'));

            this.eventBus().emit('checkout.step.updated', {
                // @ts-ignore
                [this.currentStep]: true,
                disabled: hasShippingMethods ? '' : 'disabled',
                hasShippingMethods
            });
        }

        submitCurrentStep() {
            this[this.currentStep + 'Submit'].call(this);
        }

        nextStep() {
            const steps = this.prefs().stepsList.split(',');

            // eslint-disable-next-line no-empty
            while (steps.length && this.currentStep !== steps.shift()) { }

            if (steps.length) {
                this.activateStepByName(steps.shift() + '');
            } else {
                this.activateStepByName(this.prefs().stepsList.split(',').shift() + '');
            }
        }

        // / shipping
        shippingActivate() {
            this.getById(this.prefs().shippingFormID, (/** @type {shippingForm} */shippingForm) => shippingForm.activate({
                order: this.order,
                customer: this.customer
            }));

            this.ref('shippingSection').show();
            this.ref('shippingSummary').hide();
        }

        shippingDeActivate() {
            this.ref('shippingSection').hide();
            this.ref('shippingSummary').show();
        }

        shippingSubmit() {
            this.getById(this.prefs().shippingFormID, (/** @type {shippingForm} */shippingForm) => {
                if (shippingForm.validate()) {
                    shippingForm.handleSubmit();
                }
            });
        }

        /**
         *
         * @param {any} [el] element
         * @param {any} [data] data
         */
        shippingSubmitted(el, data) {
            this.updateSummaryAndNext(el, data);
        }

        // /// billing
        billingActivate() {
            this.getById(this.prefs().billingFormID, (/** @type {billingForm} */billingForm) => billingForm.activate({
                order: this.order,
                customer: this.customer
            }));

            this.ref('billingSection').show();
            this.ref('billingSummary').hide();

            const urlParams = getUrlParams();
            if (urlParams.paymentError) {
                this.ref(this.prefs().billingFormID).data('setError', urlParams.paymentError);
            }
        }

        /**
         * @param {boolean} isNextStepActive flag to detect is next step activation needed
         */
        billingDeActivate(isNextStepActive) {
            this.ref('billingSection').hide();

            if (isNextStepActive) {
                this.ref('billingSummary').show();
            } else {
                this.ref('billingSummary').hide();
            }
        }

        billingSubmit() {
            this.getById(this.prefs().billingFormID, (/** @type {billingForm} */billingForm) => {
                if (billingForm.validate()) {
                    billingForm.handleSubmit();
                }
            });
        }

        updateSummaryAndNext(el, data) {
            if (data.order) {
                this.updateSummary(data.order);
                this.nextStep();
            }
        }

        handleEditStep(el, step) {
            switch (step) {
                case 'shipping':
                    this.activateStepByName('shipping');
                    break;
                case 'billing':
                    this.activateStepByName('billing');
                    break;
                default:
                    break;
            }
        }

        /**
         *
         * @param {any} [el] element
         * @param {any} [data] data
         */
        billingSubmitted(el, data) {
            this.updateSummaryAndNext(el, data);
        }

        // summary
        summaryActivate() {
            this.ref('confirmationSummary').show();
        }

        summaryDeActivate() {
            this.ref('confirmationSummary').hide();
            this.ref('summaryError').hide();
        }

        summarySubmit() {
            this.ref('summaryError').hide();

            this.getById('agreeToPrivacy', (/** @type {inputCheckbox} */ agreeToPrivacy) => {
                if (!agreeToPrivacy.validate()) {
                    return;
                }

                submitFormJson(
                    this.prefs().urlPlaceOrder
                ).then((response) => {
                    if (response && response.redirectUrl) {
                        window.location.assign(response.redirectUrl);
                    } else if (response && response.error) {
                        if (response.errorStage) {
                            if (['billing', 'payment'].includes(response.errorStage.stage)) {
                                this.activateStepByName('billing');
                                this.getById(this.prefs().billingFormID, (/** @type {billingForm} */billingForm) => {
                                    billingForm.setError(response.errorMessage);
                                });
                            }
                        } else if (response.errorMessage) {
                            this.ref('summaryError').show().setText(response.errorMessage);
                        }
                    } else if (response && response.continueUrl) {
                        this.summarySubmitted();
                        window.location.assign(
                            appendParamsToUrl(response.continueUrl, {
                                ID: response.orderID,
                                token: response.orderToken
                            })
                        );
                    }
                });
            });
        }

        summarySubmitted() {
        }
    }

    return CheckoutMgr;
}
