// TODO: JSDoc for all methods
import { submitFormJson } from 'widgets/toolbox/ajax';

/**
 * @typedef {ReturnType<typeof import('widgets/checkout/AddressCheckoutForm').default>} AddressCheckoutForm
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/BasicInput').default>>} basicInput
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputCheckbox').default>>} inputCheckbox
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/RadioSelector').default>>} radioSelector
 * @typedef {InstanceType<ReturnType<typeof import('widgets/checkout/PaymentAccordion').default>>} paymentAccordion
 */

/**
 * @description Base BillingForm implementation
 * @param {AddressCheckoutForm} AddressCheckoutForm Base widget for extending
 * @returns {typeof BillingForm} Billing Form
 */
export default function (AddressCheckoutForm) {
    /**
     * @class BillingForm
     * @augments AddressCheckoutForm
     */
    class BillingForm extends AddressCheckoutForm {
        isChildsValid() {
            return super.isChildsValid()
                && (this.getById('payment', (/** @type {paymentAccordion} */payment) => payment.validate()) || false);
        }

        handleSubmit() {
            if (this.isChildsValid() && !this.submitting) {
                this.submitting = true;

                this.showProgressBar();
                this.ref(this.prefs().submitButton).disable();
                this.ref(this.prefs().errorMessageLabel).hide();

                this.getById('payment', (/** @type {paymentAccordion} */payment) => {
                    payment.process().then(data => {
                        return submitFormJson(
                            this.getFormUrl(),
                            { ...this.getFormFields(), ...data },
                            this.ref('self').attr('method') === 'GET' ? 'GET' : 'POST'
                        );
                    }).then(this.onSubmitted.bind(this)).finally(this.afterSubmission.bind(this));
                });
            }

            return false;
        }

        cleanupSensitiveData() {
            this.getById('payment', (/** @type {paymentAccordion} */payment) => {
                payment.data('cleanupSensitiveData');
            });
        }

        onSubmitted(data) {
            if (data.error && data.serverErrors && data.serverErrors.length) {
                this.setError(data.serverErrors.join(''), undefined, true);
            } else if (data.error && data.fieldErrors && data.fieldErrors.length) {
                data.fieldErrors.forEach((entries) => {
                    Object.entries(entries).forEach(([name, errorMsg]) => {
                        this.getById(name, (/** @type {basicInput} */address) => {
                            if (address.id === name) {
                                address.setError(errorMsg);
                            } else {
                                this.getById('payment', (/** @type {paymentAccordion} */payment) => {
                                    payment.handleError(name, errorMsg);
                                });
                            }
                        });
                    });
                });
            } else {
                super.onSubmitted(data);
                this.cleanupSensitiveData();
            }
        }

        activate({ order, customer }) {
            this.cleanupSensitiveData();
            super.activate({ order, customer });
        }

        onAddressSelectorActivated() {
            this.getById('useShipping', this.toggleUseShipping);
        }

        /**
         * @param {inputCheckbox} useShippingCheckbox
         */
        toggleUseShipping(useShippingCheckbox) {
            const checked = Boolean(useShippingCheckbox.getValue());

            this.getById(this.prefs().addressSelector, (/** @type {radioSelector} */addressSelector) => {
                addressSelector.toggle(!checked);

                this.hideAddressFields();
                this.fulfillAddress();
            });
        }
    }

    return BillingForm;
}
