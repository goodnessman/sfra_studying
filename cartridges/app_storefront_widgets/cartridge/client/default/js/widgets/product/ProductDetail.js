// TODO: JSDoc for all methods
// TODO: freeze loaded sections during update with aria-busy
// TODO: investigate proper implementation of aria-live region for updated sections
// TODO: keep track that focus stays on the same place during PLP update

import { submitFormJson } from 'widgets/toolbox/ajax';
import { showErrorLayout } from 'widgets/toolbox/util';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputSelect').default>>} inputSelect
 */

/**
 * @description Base ProductDetail implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof ProductDetail} Product Detail class
 */
export default function (Widget) {
    /**
     * @class ProductDetail
     * @augments Widget
     * @property {boolean} data-ready-to-order - ready to order flag
     * @property {string} data-text-select-options - select option text
     * @property {string} data-text-stock-limit - stock limit text
     * @property {string} data-out-of-stock-label - out of stock label
     * @property {number} data-selected-quantity - selected quantity
     */
    class ProductDetail extends Widget {
        prefs() {
            return {
                addToCartMsg: 'addToCartMsg',
                disableHistory: false,
                descriptions: 'descriptions',
                readyToOrder: false,
                selectedQuantity: 0,
                textSelectOptions: '',
                textStockLimit: '',
                outOfStockLabel: '',
                updateLabel: '',
                update: false,
                accessibilityAlerts: {},
                ...super.prefs()
            };
        }

        updateProduct(updateBtn) {
            submitFormJson(
                updateBtn.ref('self').attr('data-update-url'),
                {
                    pid: this.currentProductID || updateBtn.prefs().pid,
                    quantity: this.selectedQuantity || updateBtn.ref('self').attr('data-selected-quantity') || 1,
                    uuid: updateBtn.ref('self').attr('data-uuid')
                }
            ).then(this.afterUpdateProduct.bind(this))
                .finally(() => {
                    this.emit('updated');
                });
        }

        afterUpdateProduct(response) {
            this.eventBus().emit('product.updated', response);
        }

        /**
         * @description Update Product View
         * @param {any} product productModel
         */
        updateProductView(product) {
            this.currentProductID = product.id;
            this.currentGtmInfo = product.gtmInfo;
            this.selectedQuantity = product.selectedQuantity;
            this.uuid = product.uuid;
            this.readyToOrder = product.readyToOrder;
            this.stock = product.availability && product.availability.stock ? product.availability.stock : null;

            this.setGtmInfo(product);

            this.getById(this.prefs().addToCartMsg, (addToCartMsg) => {
                addToCartMsg.hide();
            });
            this.renderVariationAttributes(product);
            this.renderQuantities(product);
            this.renderPrice(product);
            this.renderAvailability(product);
            this.renderImages(product);
            this.renderName(product);
            this.renderPromotions(product);
            this.renderProductDescriptions(product);
            this.updateViewFullProductURL(product);
        }

        updateViewFullProductURL(product) {
            if (product.selectedProductUrl) {
                this.has('viewFullProductURL', (el) => {
                    el.attr('href', product.selectedProductUrl);
                });
            }
        }

        setGtmInfo(product) {
            this.currentGtmInfo = product.gtmInfo;

            if (this.currentGtmInfo && product.selectedQuantity) {
                this.currentGtmInfo.quantity = product.selectedQuantity;
            }
        }

        renderVariationAttributes(product) {
            if (product.variationAttributes && product.variationAttributes.length) {
                product.variationAttributes.forEach(variationAttribute => {
                    variationAttribute.values = variationAttribute.values.map(value => {
                        return {
                            ...value,
                            selected: value.selected,
                            disabled: !value.selectable,
                            selectable: value.selectable ? 'selectable' : ''
                        };
                    });
                    this.getById('attr-' + variationAttribute.attributeId, (attribute) => {
                        attribute.render('template', {
                            attr: variationAttribute
                        });
                    });
                });
            }
        }

        renderQuantities(product) {
            if (product.quantities) {
                ['null', 1, 2, 3, 3].forEach(idx => {
                    this.getById('quantity-' + idx, (quantity) => {
                        quantity.render('template', {
                            attr: {
                                value: idx,
                                values: product.quantities.map(qty => {
                                    return {
                                        selected: qty.selected ? 'selected' : '',
                                        url: qty.url,
                                        value: qty.value
                                    };
                                })
                            },
                            isOutOfStock: product.availability && product.availability.isOutOfStock
                        });
                    });
                });
            }
        }

        renderName(product) {
            if (product.productName) {
                this.ref('name').setText(product.productName);
            }
        }

        renderPromotions(product) {
            this.getById('promotions', (promotions) => {
                promotions.render('template', {
                    promotions: product.promotions || []
                }, promotions.ref('container'));
            });
        }

        renderPrice(product) {
            if (product.price && product.price.html) {
                this.getById('priceBlock', (priceBlock) => {
                    priceBlock.render('template', [], undefined, product.price.html);
                });
            }
        }

        renderAvailability(product) {
            let message = '';
            let availabilityClass = '';

            if (product.availability && product.availability.messages && product.readyToOrder) {
                message = product.availability.messages.join('');
                availabilityClass = product.availability.class;
            }

            if (product.availability.isReachedLimit) {
                message = product.availability.inStockMsg.join('');
            }

            this.getById('availability', (availabilityLabel) => {
                availabilityLabel.render('template', {
                    message: message,
                    class: availabilityClass
                }, availabilityLabel.ref('container'));
            });
        }

        renderImages(product) {
            this.getById('productImages', (productImages) => {
                productImages.renderImages(product);
            });
        }

        renderProductDescriptions(product) {
            this.getById(this.prefs().descriptions, element => {
                element.render('template', product, element.ref('container')).then(() => element.init());
            });
        }

        /**
         * @description widget triggered event
         * Saves `selectWidget.id` into a property for further global notifications etc.
         * Later, when triggering event, `selectWidget.id` will be analised in order to send with event correct data.
         * @param {inputSelect} selectWidget widget
         */
        changeAttribute(selectWidget) {
            this.changeAttributeID = selectWidget.id;
            const selected = selectWidget.getSelectedOptions();

            if (!selected || selected.data('attrIsSelected')) {
                return;
            }

            if (selected) {
                submitFormJson(selected.data('attrUrl'), undefined, 'GET', true)
                    .then(this.afterChangeAttribute.bind(this))
                    .catch(e => {
                        showErrorLayout(e);
                    });
            }
        }

        afterChangeAttribute(response) {
            if (response && response.product) {
                if (!this.prefs().disableHistory) {
                    this.updateHistoryState(response.product);
                }

                this.updateProductView(response.product);
                this.triggerChangeAttributeEvent();
            }
        }

        /**
         * @description Triggers global event after `afterChangeAttribute` method executed.
         * Puts localised global alert event description
         */
        triggerChangeAttributeEvent() {
            if (!this.changeAttributeID) {
                return;
            }

            let accessibilityAlert = '';
            if (this.changeAttributeID.indexOf('quantity-') === 0) {
                accessibilityAlert = this.prefs().accessibilityAlerts.quantitychanged;
            } else if (this.changeAttributeID.indexOf('attr-') === 0) {
                accessibilityAlert = this.prefs().accessibilityAlerts.variationselected;
            }
            this.eventBus().emit('alert.show', {
                accessibilityAlert
            });
            this.changeAttributeID = undefined;
        }

        /**
         * @description Update History State
         * @param {object} product product object
         */
        updateHistoryState(product) {
            window.history.replaceState(undefined, '', product.selectedProductUrl);
        }
    }

    return ProductDetail;
}
