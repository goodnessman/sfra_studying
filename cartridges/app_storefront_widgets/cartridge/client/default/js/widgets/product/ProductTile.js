// TODO: JSDoc for all methods
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base ProductTile implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof ProductTile} Product Tile class
 */
export default function (Widget) {
    /**
     * @class ProductTile
     * @augments Widget
     */
    class ProductTile extends Widget {
        init() {
            super.init();

            this.eventBus().emit('product.tile.init', this);
        }

        onProductTileClick() {
            this.eventBus().emit('product.tile.product.click', this);
        }

        onModalShow() {
            this.eventBus().emit('product.tile.qv.open', {
                gtmInfo: this.data('analytics')
            });
        }

        showQuickView(button) {
            this.eventBus().emit('dialog.show', {
                url: button.data('url'),
                wrapperClasses: 'm-quick_view'
            });

            this.onModalShow();
        }

        /**
         * @description Focus
         */
        focus() {
            this.ref('tileImageLink').focus();
        }
    }

    return ProductTile;
}
