// TODO: freeze loaded sections during update with aria-busy
// TODO: investigate proper implementation of aria-live region for updated sections
// TODO: keep track that focus stays on the same place during PLP update

import { submitFormJson } from 'widgets/toolbox/ajax';
/**
 * @typedef {ReturnType<typeof import('widgets/product/ProductDetail').default>} ProductDetail
 */

/**
 * @description Base AddToCartMixin implementation
 * <br>This class is not intended to have a separate DOM representation.
 * @param {ProductDetail} ProductDetail Base widget for extending
 * @returns {typeof AddToCartMixin} Add to Cart mixin class
 */
export default function (ProductDetail) {
    /**
     * @class AddToCartMixin
     * @augments ProductDetail
     * @property {string} data-text-network-error - network error text
     * @property {string} data-add-to-cart-label - add to cart label
     * @property {string} data-pid - product id
     * @property {string} data-show-minicart-on-product-add - Show minicart on product add
     * @property {string} data-show-message-on-product-add - Show message on poroduct add
     */
    class AddToCartMixin extends ProductDetail {
        prefs() {
            return {
                pid: '',
                textNetworkError: 'Network Error',
                addToCartBtn: 'addToCart',
                classesBusy: 'm-busy',
                showMinicartOnProductAdd: false,
                showMessageOnProductAdd: true,
                zeroWidthSpaceSymbol: '\u200B',
                ...super.prefs()
            };
        }

        init() {
            super.init();
            this.readyToOrder = this.prefs().readyToOrder;
            this.has(this.prefs().addToCartBtn, (element) => {
                const stock = element.data('stock');
                if (stock) {
                    this.stock = stock;
                }
            });
            this.selectedQuantity = this.prefs().selectedQuantity;
            this.spacesLength = 0;
        }

        /**
         * @description Show message
         * @param {string} msg Message
         * @param {boolean} error Error flag
         */
        showCartMessage(msg, error = false) {
            this.getById(this.prefs().addToCartMsg, (addToCartMsg) => {
                if (typeof this.spacesLength === 'undefined') {
                    this.spacesLength = 0;
                }
                // It's needed to force render previously added success message. If we try to add the same product
                // several times, success message alert will not be announced by a screen reader.
                msg = msg.concat(Array(++this.spacesLength).join(this.prefs().zeroWidthSpaceSymbol));

                addToCartMsg.render('template', { msg, error }, addToCartMsg.ref('container')).then(() => addToCartMsg.show());
            });
        }

        /**
         * @description handle response from server after product added
         * @param {any} response Add Product response
         */
        postAddProduct(response) {
            if (!response.error && response.cart) {
                this.emit('productadded');
                const cartModel = response.cart;
                cartModel.showMinicart = this.prefs().showMinicartOnProductAdd;
                this.eventBus().emit('product.added.to.cart', cartModel, this);

                const accessibilityAlert = this.prefs().accessibilityAlerts.addedtocart;
                this.eventBus().emit('alert.show', {
                    accessibilityAlert
                });
            }

            if (response.error || this.prefs().showMessageOnProductAdd) {
                this.eventBus().emit('product.addedTocart.with.error', response);
                this.showCartMessage(response.message, response.error);
            }
        }

        addToCart(button) {
            const addToCartBtnPrefs = button.prefs();

            if (addToCartBtnPrefs.readyToOrder) {
                this.readyToOrder = addToCartBtnPrefs.readyToOrder;
            }

            if (!this.readyToOrder) {
                this.showCartMessage(this.prefs().textSelectOptions, true);

                return;
            }

            if (this.stock && this.stock < this.selectedQuantity) {
                this.showCartMessage(this.prefs().textStockLimit, true);

                return;
            }

            button.startProcess();

            this.showProgressBar();

            submitFormJson(addToCartBtnPrefs.addToCartUrl, {
                pid: this.currentProductID || addToCartBtnPrefs.pid,
                quantity: this.selectedQuantity || 1
            }).then(response => this.postAddProduct(response))
                .finally(() => {
                    button.stopProcess();

                    this.hideProgressBar();
                }).catch(() => {
                    this.showCartMessage(this.prefs().textNetworkError, true);
                });
        }

        showProgressBar() {
            this.ref('self').addClass(this.prefs().classesBusy);
        }

        hideProgressBar() {
            this.ref('self').removeClass(this.prefs().classesBusy);
        }

        /**
         * @description Update Product View
         * @param {any} product productModel
         */
        updateProductView(product) {
            super.updateProductView(product);
            this.renderAddToCart(product);
        }

        renderAddToCart(product) {
            this.getById('addToCart', (addToCartBtn) => {
                if (!product.available && product.availability.isOutOfStock) {
                    addToCartBtn.setText(this.prefs().outOfStockLabel);
                    addToCartBtn.disable();
                } else if (product.availability.isReachedLimit) {
                    this.getById(this.prefs().addToCartMsg, (addToCartMsg) => {
                        let msg = product.availability.messages[0];
                        let error = true;
                        addToCartMsg.render(
                            'template',
                            { msg, error },
                            addToCartMsg.ref('container')
                        ).then(() => addToCartMsg.show());
                    });
                    addToCartBtn.setText(this.prefs()[this.prefs().update ? 'updateLabel' : 'addToCartLabel']);
                    addToCartBtn.disable();
                } else {
                    addToCartBtn.setText(this.prefs()[this.prefs().update ? 'updateLabel' : 'addToCartLabel']);
                    addToCartBtn.enable();
                }
            });
        }
    }

    return AddToCartMixin;
}
