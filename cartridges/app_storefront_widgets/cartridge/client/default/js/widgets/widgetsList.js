/* eslint-disable indent */
const [, pdp, , plp, wishlist, cart, checkout, , account, content] = [
    'home',
    'pdp',
    'clp',
    'plp',
    'wishlist',
    'cart',
    'checkout',
    'orderconfirmation',
    'account',
    'static'
]
// @ts-ignore
.map(ctx => window.contexts.includes(ctx));

import widgetsMgr from 'widgets/widgetsMgr';
import Accordion from 'widgets/global/Accordion';
import AccordionItem from 'widgets/global/AccordionItem';
import Button from 'widgets/global/Button';
import ListAccessibility from 'widgets/global/ListAccessibility';
import MegaMenu from 'widgets/global/MegaMenu';
import SwipeToClose from 'widgets/global/SwipeToClose';
import MenuPanel from 'widgets/global/MenuPanel';
import MenuPanelToggle from 'widgets/global/MenuPanelToggle';
import MenuBarItem from 'widgets/global/MenuBarItem';
import HamburgerMenuItem from 'widgets/global/HamburgerMenuItem';
import DismissBlock from 'widgets/global/DismissBlock';
import Form from 'widgets/forms/Form';
import BasicForm from 'widgets/forms/BasicForm';
import CountrySelector from 'widgets/global/CountrySelector';
import AjaxForm from 'widgets/forms/AjaxForm';
import Token from 'widgets/global/Token';
import BasicInput from 'widgets/forms/BasicInput';
import InputEmail from 'widgets/forms/InputEmail';
import InputPassword from 'widgets/forms/InputPassword';
import InputCheckbox from 'widgets/forms/InputCheckbox';
import InputText from 'widgets/forms/InputText';
import InputTextarea from 'widgets/forms/InputTextarea';
import InputHidden from 'widgets/forms/InputHidden';
import InputTel from 'widgets/forms/InputTel';
import MaskableInput from 'widgets/forms/MaskableInput';
import Tabs from 'widgets/global/Tabs';
import TabPanel from 'widgets/global/TabPanel';
import InputSelect from 'widgets/forms/InputSelect';
import Label from 'widgets/global/Label';
import EmailSubscribe from 'widgets/forms/ajax/EmailSubscribe';
import ConfirmDialog from 'widgets/global/ConfirmDialog';
import Carousel from 'widgets/global/Carousel';
import BackToTop from 'widgets/global/BackToTop';
import Modal from 'widgets/global/Modal';
import InputRadio from 'widgets/forms/InputRadio';
import ConsentTracking from 'widgets/global/ConsentTracking';
import DropDown from 'widgets/global/DropDown';
import ModalLink from 'widgets/global/ModalLink';
import EmitBusEvent from 'widgets/global/EmitBusEvent';
import AjaxFormResponseContainer from 'widgets/account/AjaxFormResponseContainer';
import Combobox from 'widgets/forms/Combobox';
import SearchBox from 'widgets/search/SearchBox';
import FocusHighlighter from 'widgets/global/FocusHighlighter';
import SearchBoxToggle from 'widgets/search/SearchBoxToggle';
import StickyHeader from 'widgets/global/StickyHeader';
import ButtonWithIcon from 'widgets/global/ButtonWithIcon';
import AccessibilityFocusTrapMixin from 'widgets/global/AccessibilityFocusTrapMixin';
import SoftKeyboardToggleMixin from 'widgets/global/SoftKeyboardToggleMixin';
import ProductTile from 'widgets/product/ProductTile';
import AddToWishlistMixin from 'widgets/wishlist/AddToWishlistMixin';
import Disclosure from 'widgets/global/Disclosure';
import GlobalAlerts from 'widgets/global/GlobalAlerts';
import GlobalModal from 'widgets/global/GlobalModal';

widgetsMgr.addWidgetsList('global.widgets', () => [
    ['token', Token],
    ['label', Label],
    ['carousel', Carousel],
    ['button', Button],
        ['buttonWithIcon', ButtonWithIcon, 'button'],
    ['listAccessibility', ListAccessibility],
        ['refinementsPanel', Modal, 'listAccessibility'],
        ['refinementsPanel', SwipeToClose, 'refinementsPanel'],
        ['megaMenu', MegaMenu, 'listAccessibility'],
        ['menuBarItem', MenuBarItem, 'listAccessibility'],
        ['hamburgerMenuItem', HamburgerMenuItem, 'listAccessibility'],
        ['accordion', Accordion, 'listAccessibility'],
    ['accordionItem', AccordionItem],
    ['swipeToClose', SwipeToClose],
        ['menuPanel', MenuPanel, 'swipeToClose'],
    ['menuPanelToggle', MenuPanelToggle],
    ['dropDown', DropDown],
    ['dismissBlock', DismissBlock],
    ['countrySelector', CountrySelector],
    ['backtotop', BackToTop],
    ['consentTracking', ConsentTracking],
    ['emitBusEvent', EmitBusEvent],
    ['focusHighlighter', FocusHighlighter],
    ['searchBoxToggle', SearchBoxToggle],
    ['stickyHeader', StickyHeader],
    ['basicForm', BasicForm],
        ['form', Form, 'basicForm'],
            ['ajaxform', AjaxForm, 'basicForm'],
                ['emailSubscribe', EmailSubscribe, 'ajaxform'],
    ['combobox', Combobox],
        ['searchBox', SearchBox, 'combobox'],
            ['searchBox', SoftKeyboardToggleMixin, 'searchBox'],
            ['searchBox', AccessibilityFocusTrapMixin, 'searchBox'],
    ['basicInput', BasicInput],
        ['inputPassword', InputPassword, 'basicInput'],
        ['inputCheckbox', InputCheckbox, 'basicInput'],
        ['inputSelect', InputSelect, 'basicInput'],
        ['inputText', InputText, 'basicInput'],
            ['inputEmail', InputEmail, 'inputText'],
            ['inputTextarea', InputTextarea, 'inputText'],
            ['inputTel', InputTel, 'inputText'],
                ['inputTel', MaskableInput, 'inputTel'],
        ['inputRadio', InputRadio, 'basicInput'],
        ['inputHidden', InputHidden, 'basicInput'],
    ['modal', Modal],
        ['modal', AccessibilityFocusTrapMixin, 'modal'],
        ['globalModal', GlobalModal, 'modal'],
        ['confirmDialog', ConfirmDialog, 'modal'],
        ['modalLink', ModalLink, 'modal'],
        ['searchBoxToggle', SearchBoxToggle, 'modal'],
    ['tabs', Tabs],
    ['tabPanel', TabPanel],
    ['productTile', ProductTile],
        ['productTile', AddToWishlistMixin, 'productTile'],
    ['ajaxFormResponseContainer', AjaxFormResponseContainer],
    ['disclosure', Disclosure],
    ['globalAlerts', GlobalAlerts]
]);

if (content) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'content.widgets' */ 'widgets/content/ContactUs')
    ]).then(deps => {
        const [ContactUs] = deps.map(dep => dep.default);

        return {
            listId: 'content.widgets',
            widgetsDefinition: () => [
                ['contactUs', ContactUs]
            ]
        };
    }));
}

if (!checkout) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'minicart.widgets' */ 'widgets/cart/Minicart'),
        import(/* webpackChunkName: 'minicart.widgets' */ 'widgets/cart/MinicartDialog')
    ]).then(deps => {
        const [
            Minicart,
            MinicartDialog
        ] = deps.map(dep => dep.default);

        return {
            listId: 'minicart.widgets',
            widgetsDefinition: () => [
                ['minicart', Minicart],
                // modal
                ['minicartDialog', MinicartDialog, 'modal']
            ]
        };
    }));
}

if (wishlist) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'wishlist.widgets' */ 'widgets/wishlist/WishlistMgr'),
        import(/* webpackChunkName: 'wishlist.widgets' */ 'widgets/product/AddToCartMixin')
    ]).then(deps => {
        const [WishlistMgr, AddToCartMixin] = deps.map(dep => dep.default);

        return {
            listId: 'wishlist.widgets',
            widgetsDefinition: () => [
                ['wishlistMgr', WishlistMgr],
                    ['wishlistMgr', AddToCartMixin, 'wishlistMgr']
            ]
        };
    }));
}

if (pdp || plp || cart || wishlist) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'product.widgets' */ 'widgets/product/ProductDetail'),
        import(/* webpackChunkName: 'product.widgets' */ 'widgets/product/ProductImages'),
        import(/* webpackChunkName: 'product.widgets' */ 'widgets/product/ColorSwatch'),
        import(/* webpackChunkName: 'product.widgets' */ 'widgets/product/AddToCartMixin'),
        import(/* webpackChunkName: 'product.widgets' */ 'widgets/global/ProcessButton')
    ]).then(deps => {
        const [
            ProductDetail,
            ProductImages,
            ColorSwatch,
            AddToCartMixin,
            ProcessButton
        ] = deps.map(dep => dep.default);

        return {
            listId: 'product.widgets',
            widgetsDefinition: () => [
                ['productDetail', ProductDetail],
                    ['productDetail', AddToCartMixin, 'productDetail'],
                    ['productDetail', AddToWishlistMixin, 'productDetail'],
                ['productImages', ProductImages],
                ['colorSwatch', ColorSwatch],
                // button
                    ['processButton', ProcessButton, 'button']
            ]
        };
    }));
}

if (plp) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'search.widgets' */ 'widgets/search/ProductListingMgr'),
        import(/* webpackChunkName: 'search.widgets' */ 'widgets/search/RefinementsPanel'),
        import(/* webpackChunkName: 'search.widgets' */ 'widgets/search/RefinementsPanelToggle'),
        import(/* webpackChunkName: 'search.widgets' */ 'widgets/search/RefinementMenuItem'),
        import(/* webpackChunkName: 'search.widgets' */ 'widgets/global/Refinement'),
        import(/* webpackChunkName: 'search.widgets' */ 'widgets/search/NoSearchResultsForm')
    ]).then(plpDeps => {
        let [
            ProductListingMgr,
            RefinementsPanel,
            RefinementsPanelToggle,
            RefinementMenuItem,
            Refinement,
            NoSearchResultsForm
        ] = plpDeps.map(dep => dep.default);

        return {
            listId: 'search.widgets',
            widgetsDefinition: () => [
                // listAccessibility
                    ['refinementsPanel', RefinementsPanel, 'refinementsPanel'],
                    ['refinementMenuItem', RefinementMenuItem, 'listAccessibility'],
                // tabs
                    ['productListingMgr', ProductListingMgr, 'tabs'],
                ['refinementsPanelToggle', RefinementsPanelToggle],
                ['refinement', Refinement],
                // basicForm
                    ['noSearchResultsForm', NoSearchResultsForm, 'basicForm']
            ]
        };
    }));
}

if (account) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/account/PaymentMgr'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/account/AddressMgr'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/account/OrderHistoryMgr'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/forms/AddressBookForm'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/account/PasswordResetModal'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/account/PasswordResetForm'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/forms/AddressForm'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/forms/ajax/LoginForm'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/forms/GoogleAutocompleteField'),
        import(/* webpackChunkName: 'account.widgets' */ 'widgets/global/SessionExpiration')
    ]).then(deps => {
        const [
            PaymentMgr,
            AddressMgr,
            OrderHistoryMgr,
            AddressBookForm,
            PasswordResetModal,
            PasswordResetForm,
            AddressForm,
            LoginForm,
            GoogleAutocompleteField,
            SessionExpiration
        ] = deps.map(dep => dep.default);

        return {
            listId: 'account.widgets',
            widgetsDefinition: () => [
                ['addressMgr', AddressMgr],
                ['paymentMgr', PaymentMgr],
                ['orderHistoryMgr', OrderHistoryMgr],
                // modal
                    ['passwordResetModal', PasswordResetModal, 'modal'],
                    ['sessionExpiration', SessionExpiration, 'modal'],
                // basicForm / ajaxform
                    ['addressForm', AddressForm, 'ajaxform'],
                    ['addressBookForm', AddressBookForm, 'addressForm'],
                    ['loginForm', LoginForm, 'ajaxform'],
                    ['passwordResetForm', PasswordResetForm, 'ajaxform'],
                // inputText
                ['googleAutocompleteField', GoogleAutocompleteField, 'inputText']
            ]
        };
    }));
}

if (cart) {
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'cart.widgets' */ 'widgets/cart/CartMgr'),
        import(/* webpackChunkName: 'cart.widgets' */ 'widgets/forms/CouponForm'),
        import(/* webpackChunkName: 'cart.widgets' */ 'widgets/cart/PromoCodeDisclosure'),
        import(/* webpackChunkName: 'cart.widgets' */ 'widgets/global/SessionExpiration')
    ]).then(deps => {
        const [
            CartMgr,
            CouponForm,
            PromoCodeDisclosure,
            SessionExpiration
        ] = deps.map(dep => dep.default);

        return {
            listId: 'cart.widgets',
            widgetsDefinition: () => [
                ['cartMgr', CartMgr],
                // modal
                    ['sessionExpiration', SessionExpiration, 'modal'],
                // basicForm / form / ajaxForm
                    ['couponform', CouponForm, 'ajaxform'],
                // disclosure
                    ['promoCodeDisclosure', PromoCodeDisclosure, 'disclosure']
            ]
        };
    }));
}

if (checkout) {
    // @ts-ignore
    widgetsMgr.asyncListsPromises.push(Promise.all([
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/forms/ajax/LoginForm'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/account/PasswordResetModal'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/CheckoutMgr'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/CheckoutStepHeader'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/AddressCheckoutForm'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/ShippingForm'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/BillingForm'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/PaymentAccordion'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/PaymentAccordionItem'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/PaymentAccordionItemCREDIT_CARD'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/PaymentAccordionItemZERO_PAYMENT'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/PaymentAccordionItemPAYPAL_EXPRESS'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/InputCreditCardNumber'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/checkout/RadioSelector'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/forms/GoogleAutocompleteField'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/forms/AddressForm'),
        import(/* webpackChunkName: 'checkout.widgets' */ 'widgets/global/SessionExpiration')
    ]).then(deps => {
        let [
            LoginForm,
            PasswordResetModal,
            CheckoutMgr,
            CheckoutStepHeader,
            AddressCheckoutForm,
            ShippingFrom,
            BillingForm,
            PaymentAccordion,
            PaymentAccordionItem,
            PaymentAccordionItemCREDITCARD,
            PaymentAccordionItemZEROPAYMENT,
            PaymentAccordionItemPAYPALEXPRESS,
            InputCreditCardNumber,
            RadioSelector,
            GoogleAutocompleteField,
            AddressForm,
            SessionExpiration
        ] = deps.map(dep => dep.default);

        return {
            listId: 'checkout.widgets',
            widgetsDefinition: () => [
                ['checkoutMgr', CheckoutMgr],
                // modal
                    ['sessionExpiration', SessionExpiration, 'modal'],
                    ['passwordResetModal', PasswordResetModal, 'modal'],
                // basicForm / form / ajaxform
                    ['loginForm', LoginForm, 'ajaxform'],
                    ['addressForm', AddressForm, 'ajaxform'],
                        ['addressCheckoutForm', AddressCheckoutForm, 'addressForm'],
                            ['shippingForm', ShippingFrom, 'addressCheckoutForm'],
                            ['billingForm', BillingForm, 'addressCheckoutForm'],
                // basicInput
                    ['inputCreditCardNumber', InputCreditCardNumber, 'basicInput'],
                    ['googleAutocompleteField', GoogleAutocompleteField, 'inputText'],
                    // inputRadio
                        ['radioSelector', RadioSelector, 'inputRadio'],
                // accordion
                    ['paymentAccordion', PaymentAccordion, 'accordion'],
                // accordionItems
                    ['paymentAccordionItem', PaymentAccordionItem, 'accordionItem'],
                        ['paymentAccordionItemCREDIT_CARD', PaymentAccordionItemCREDITCARD, 'paymentAccordionItem'],
                        ['paymentAccordionItemPAYPAL_EXPRESS', PaymentAccordionItemPAYPALEXPRESS, 'paymentAccordionItem'],
                        ['paymentAccordionItemZERO_PAYMENT', PaymentAccordionItemZEROPAYMENT, 'paymentAccordionItem'],
                ['checkoutStepHeader', CheckoutStepHeader, 'label']
            ]
        };
    }));
}
