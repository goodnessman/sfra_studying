import { getJSONByUrl, getContentByUrl, submitFormJson } from 'widgets/toolbox/ajax';
import { scrollWindowTo } from 'widgets/toolbox/scroll';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/ConfirmDialog').default>>} confirmDialog
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/Modal').default>>} modal
 */

/**
 * @description Base WishlistMgr implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof WishlistMgr} Wishlist Manager class
 */

export default function (Widget) {
    /**
     * @class WishlistMgr
     * @augments Widget
     * @classdesc CartMgr component on Cart page. Renders the whole cart template on each update e.g. quantity/coupon/edit etc.
     * @property {string} data-widget - Widget name `wishlistMgr`
     * @property {boolean} data-show-more - Load More button toggle(on/off)
     * @property {string} data-page-number - Page number
     * @property {object} data-wishlist-model - Wishlist model
     * @property {string} data-share-confirmation-dialog-title - Confirmation Dialog Share title
     * @property {string} data-share-confirmation-dialog-message - Confirmation Dialog Share message
     * @property {string} data-share-confirmation-dialog-confirm - Confirmation Dialog Share confirm button
     * @property {string} data-share-confirmation-dialog-cancel - Confirmation Dialog Share cancel button
     * @property {string} data-delete-confirmation-dialog-title - Confirmation Dialog Delete Product title
     * @property {string} data-delete-confirmation-dialog-message - Confirmation Dialog Delete Product message
     * @property {string} data-delete-confirmation-dialog-confirm - Confirmation Dialog Delete Product confirm button
     * @property {string} data-delete-confirmation-dialog-cancel - Confirmation Dialog Delete Product cancel button
     * @property {string} data-add-to-cart-error-message - Add to Cart error message
     * @property {boolean} data-show-minicart-on-product-add - Show minicart on product add to Cart
     * @property {object} data-accessibility-alerts - Accessibility alerts messages for different user actions
     * Possible values are: `linkCopied`
     * @example
     * // use this code to display widget
     *  <div
     *      class="col-sm-8 col-md-6"
     *      data-widget="wishlistMgr"
     *      data-page-number="1"
     *      data-wishlist-model="${JSON.stringify(pdict.wishlist)}"
     *      data-share-confirmation-dialog-title="${Resource.msg('wishlist.shareLinkConfirmation.title','wishlist',null)}"
     *      data-share-confirmation-dialog-message="${Resource.msg('wishlist.shareLinkConfirmation.message','wishlist',null)}"
     *      data-share-confirmation-dialog-confirm="${Resource.msg('wishlist.shareLinkConfirmation.confirm','wishlist',null)}"
     *      data-share-confirmation-dialog-cancel="${Resource.msg('common.cancel','common',null)}"
     *      data-delete-confirmation-dialog-title="${Resource.msg('wishlist.removeItemConfirmation.title','wishlist',null)}"
     *      data-delete-confirmation-dialog-message="${Resource.msg('wishlist.removeItemConfirmation.message','wishlist',null)}"
     *      data-delete-confirmation-dialog-confirm="${Resource.msg('wishlist.removeItemConfirmation.confirm','wishlist',null)}"
     *      data-delete-confirmation-dialog-cancel="${Resource.msg('wishlist.removeItemConfirmation.cancel','wishlist',null)}"
     *      data-add-to-cart-error-message="${Resource.msg('alert.addToCart.error', 'wishlist', null)}"
     *      data-accessibility-alerts='{
     *          "linkCopied": "${Resource.msg('alert.linkCopied', 'wishlist', null)}"
     *      }'
     *      data-show-minicart-on-product-add="true"
     *  >
     *      <div data-ref="wishlistCountContainer"></div>
     *      <script data-ref="wishlistCountTemplate" type="template/mustache">
     *          <div data-ref="wishlistCountContainer">
     *              <h2 class="wishlist-owner">{{count}}</h2>
     *          </div>
     *      </script>
     *      <div>
     *          <isset name="shareLinks" value="${pdict.socialLinks}" scope="page"/>
     *          <isinclude template="wishlist/components/socialsharing" />
     *      </div>
     *      <div data-ref="wishlistGrid">
     *          <isinclude template="wishlist/components/list" />
     *      </div>
     *      <isinclude template="cart/cartEditProductModal" />
     *  </div>
     */
    class WishlistMgr extends Widget {
        prefs() {
            return {
                accessibilityAlerts: {},
                wishlistModel: {},
                wishlistId: '',
                publicView: '',
                urlToSetPublic: '',
                shareConfirmationDialogTitle: '',
                shareConfirmationDialogMessage: '',
                shareConfirmationDialogConfirm: '',
                shareConfirmationDialogCancel: '',
                deleteConfirmationDialogTitle: '',
                deleteConfirmationDialogMessage: '',
                deleteConfirmationDialogConfirm: '',
                deleteConfirmationDialogCancel: '',
                addToCartMsg: 'addToCartMsg',
                wishlistGrid: 'wishlistGrid',
                addToCartErrorMessage: '',
                ...super.prefs()
            };
        }

        /**
         * @description Widget initialisation
         */
        init() {
            super.init();
            this.eventBus().on('product.updated', 'closeModalAfterEditProduct');
            this.eventBus().on('product.addedTocart.with.error', 'productAddedToCartWishError');
            this.wishlistModel = this.prefs().wishlistModel;
            this.reRenderWishlist(this.wishlistModel);
        }

        /**
         * Render to template wishlist
         *
         * @param {object} wishlistModel Wishlist Model object to render
         */

        reRenderWishlist(wishlistModel) {
            this.calculateLoadMorePercentage(wishlistModel);
            this.renderWishlist(wishlistModel);
            this.renderWishlistCount(wishlistModel);
            this.renderWishlistProgressIndicator(wishlistModel);
        }

        /**
         * Calculate Progress Indicator percentage
         *
         * @param {object} wishlistModel Wishlist Model object to render
         */

        calculateLoadMorePercentage(wishlistModel) {
            let loadMorePercentage;
            loadMorePercentage = Math.round((wishlistModel.viewedItemsCount * 100) / wishlistModel.totalCount);
            wishlistModel.loadMorePercentage = loadMorePercentage;
        }

        /**
         * Share click handler
         *
         * @param {RefElement} link Link to share
         */
        shareLink(link) {
            this.linkToShare = link;
            this.confirmationHandler = this.confirmedShareLink;
            this.getById('confirmDialog', (/** @type {confirmDialog} */confirmDialog) => {
                confirmDialog.showModal({
                    title: this.prefs().shareConfirmationDialogTitle,
                    message: this.prefs().shareConfirmationDialogMessage,
                    confirm: this.prefs().shareConfirmationDialogConfirm,
                    cancel: this.prefs().shareConfirmationDialogCancel
                });
            });
        }

        /**
         * Remove product handler
         *
         * @param {RefElement} product Product to remove
         */
        removeProductFromWishlist(product) {
            this.productToRemove = product;
            this.confirmationHandler = this.confirmedRemoveProductFromWishlist;
            const productName = product.data('productName');
            const message = this.prefs().deleteConfirmationDialogMessage.replace('{0}', productName);
            this.getById('confirmDialog', (/** @type {confirmDialog} */confirmDialog) => {
                confirmDialog.showModal({
                    title: this.prefs().deleteConfirmationDialogTitle,
                    message: message,
                    confirm: this.prefs().deleteConfirmationDialogConfirm,
                    cancel: this.prefs().deleteConfirmationDialogCancel
                });
            });
        }

        confirmDialog() {
            let confirmationHandler = this.confirmationHandler;
            if (confirmationHandler) {
                confirmationHandler.call(this);
            }
        }

        confirmedShareLink() {
            const linkToShare = this.linkToShare;
            if (linkToShare) {
                const link = linkToShare.data('href');
                const shareMethod = linkToShare.data('share');
                const url = this.prefs().urlToSetPublic;
                const wishlistID = this.prefs().wishlistId;
                const publicView = this.prefs().publicView;
                submitFormJson(url, { id: wishlistID, publicView })
                    .then(response => {
                        if (shareMethod === 'clipboard') {
                            if (response.success) {
                                this.copyToClipboard(link);
                                linkToShare.focus();
                                const accessibilityAlert = this.prefs().accessibilityAlerts.linkCopied;
                                this.eventBus().emit('alert.show', {
                                    accessibilityAlert
                                });
                            }
                            return;
                        }
                        window.open(link, '_blank');
                    });
            }
        }

        /**
         * @description Handler for "Edit" product button in a Cart
         * @param {RefElement} editBtn - Target "Edit" button
         */
        editProduct(editBtn) {
            const uuid = editBtn.data('uuid');
            const pid = editBtn.data('pid');
            getContentByUrl(editBtn.data('href'), { uuid, pid })
                .then((response) => {
                    this.getById('editProductModal', (/** @type {modal} */ editProductModal) => editProductModal
                        .showModal({ body: response }));
                });
        }

        /**
         * Render Wishlist
         *
         * @param {object} wishlistModel Wishlist Model object to render
         * @returns {Promise} Return new Promise
         */
        renderWishlist(wishlistModel) {
            return this.render('wishlistTemplate', wishlistModel, this.ref('wishlistContainer'));
        }

        renderWishlistProgressIndicator(wishlistModel) {
            return this.render('wishlistProgressTemplate', wishlistModel, this.ref('wishlistProgressContainer'));
        }

        /**
         * Error handler on product added to cart
         *
         * @param {object} response Response from server
         */

        productAddedToCartWishError(response) {
            if (response.error) {
                const addToCartErrorMessage = this.prefs().addToCartErrorMessage;
                const ref = this.prefs().addToCartMsg + '-' + response.pid;
                this.ref(ref)
                    .setText(addToCartErrorMessage)
                    .show();
                this.eventBus().emit('alert.show', {
                    addToCartErrorMessage
                });
            }
        }

        /**
         * Load More Button handler
         *
         * @param {RefElement} button Load More Button
         */

        loadMore(button) {
            const href = button.data('href');
            const id = button.data('uuid');
            const pageNumber = String(parseInt(button.data('pageNumber'), 10) + 1);
            const publicView = button.data('publicView');
            let url = '';

            if (typeof href === 'string') {
                url = href;
            }

            getJSONByUrl(url, {
                publicView,
                pageNumber,
                id
            }).then(response => {
                if (this.wishlistModel) {
                    this.wishlistModel = {
                        ...response.wishlist,
                        items: this.wishlistModel.items.concat(response.wishlist.items)
                    };
                }
                this.reRenderWishlist(this.wishlistModel);
            });
        }

        /**
         * Render Wishlist total count
         *
         * @param {object} wishlistModel Wishlist Model object to render
         */

        renderWishlistCount(wishlistModel) {
            return this.render(
                'wishlistCountTemplate',
                { count: wishlistModel.viewedItemsCount },
                this.ref('wishlistCountContainer')
            );
        }

        /**
         * Get response from server after editing wishlist items. Remove old items and add new to wishslist.
         * Rerender wishlist with updated items
         *
         * @param {object} response object response from server
         */
        closeModalAfterEditProduct(response) {
            if (response.success
                && (response.newLineItem || response.removedUUIDs)
                && this.wishlistModel) {
                const wishlistGrid = this.ref(this.prefs().wishlistGrid).get();
                this.getById('editProductModal', (/** @type {modal} */ editProductModal) => editProductModal
                    .closeModal());
                this.wishlistModel.items = this.wishlistModel.items.filter(item => {
                    return !response.removedUUIDs.includes(item.UUID);
                });
                this.wishlistModel.items.unshift(response.newLineItem);
                this.reRenderWishlist(this.wishlistModel);
                scrollWindowTo(wishlistGrid, true);
            }
        }

        confirmedRemoveProductFromWishlist() {
            const productToRemove = this.productToRemove;
            if (productToRemove) {
                const url = productToRemove.data('url');
                const pid = productToRemove.data('productId');
                getJSONByUrl(url, { pid }).then((response) => {
                    if (response.success && this.wishlistModel) {
                        let wishlistModel = this.wishlistModel;
                        const productUuid = productToRemove.data('productUuid');
                        const itemIndexToRemove = wishlistModel.items.findIndex(item => {
                            return item.UUID === productUuid;
                        });
                        wishlistModel.items.splice(itemIndexToRemove, 1);
                        this.wishlistModel.viewedItemsCount--;
                        this.wishlistModel.totalCount--;
                        this.wishlistModel.loadMoreCount--;
                        if (this.wishlistModel.loadMoreCount <= 0) {
                            this.wishlistModel.loadMoreCount = 0;
                            this.wishlistModel.showMore = false;
                        }
                        delete this.productToRemove;
                        this.reRenderWishlist(wishlistModel);
                    }
                });
            }
        }

        /**
         * Copy link to clipboard
         *
         * @param {string} str Link that copied to clipboard
         */

        copyToClipboard(str) {
            const el = document.createElement('textarea');
            el.value = str;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        }
    }

    return WishlistMgr;
}
