// TODO: JSDoc for all methods
import { submitFormJson } from 'widgets/toolbox/ajax';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/Label').default>>} label
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/Button').default>>} button
 */

/**
 * @description Base AddToWishlistMixin implementation.
 * <br>This class is not intended to have a separate DOM representation.
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof AddToWishlistMixin} Add to Wishlist Mixin class
 */
export default function (Widget) {
    /**
     * @class AddToWishlistMixin
     * @augments Widget
     * @property data-widget - Widget name "addToWishlist"
     * @property data-pid - product id
     * @property data-text-network-error - Network error message
     * @property data-text-added-to-wishlist - Added to wishlist message
     * @property data-class-added-to-wishlist - Added to wishlist class
     */
    class AddToWishlistMixin extends Widget {
        prefs() {
            return {
                pid: '',
                textNetworkError: 'Network Error',
                addToWishlistMsg: 'addToWishlistMsg',
                addToWishlistBtn: 'addToWishlist',
                addToWishlistHideTexts: false,
                ...super.prefs()
            };
        }

        init() {
            super.init();
            this.pid = this.prefs().pid;
            this.addToWishlistHideTexts = this.prefs().addToWishlistHideTexts;
        }

        /**
         * @description Show message
         * @param {string} msg Message
         * @param {boolean} error Error flag
         */
        showWishlistMessage(msg, error = false) {
            const addToWishlistMsg = this.getById(this.prefs().addToWishlistMsg, (/** @type {label} */ label) => label);
            if (!addToWishlistMsg) {
                return;
            }

            if (error) {
                addToWishlistMsg.setError(msg).show();
                addToWishlistMsg.deactivate();
            } else {
                addToWishlistMsg.setText(msg).show();
                addToWishlistMsg.activate();
            }
        }

        addToWishlist(button) {
            const addTWishlistBtnPrefs = button.prefs();

            button.busy();

            this.getById(this.prefs().addToWishlistMsg, (/** @type {label} */ label) => label.hide());

            submitFormJson(addTWishlistBtnPrefs.addToWishlistUrl, {
                pid: this.pid
            }).then(response => this.handleSuccess(response)).finally(() => {
                button.unbusy();
            }).catch(() => {
                this.showWishlistMessage(this.prefs().textNetworkError, true);
            });
        }

        /**
         * @description Handle Success
         * @param {object} response response
         */
        handleSuccess(response) {
            const addToWishlist = this.getById(this.prefs().addToWishlistBtn, (/** @type {button} */ element) => element);

            if (!addToWishlist) {
                return;
            }

            if (response.success) {
                addToWishlist.activate();
                if (!this.addToWishlistHideTexts) {
                    addToWishlist.setText(this.prefs().textAddedToWishlist);
                }

                const accessibilityAlert = this.prefs().accessibilityAlerts.addedtowishlist;
                this.eventBus().emit('alert.show', {
                    accessibilityAlert
                });
            }

            if (!this.addToWishlistHideTexts) {
                this.showWishlistMessage(response.msg, response.error);
            }
        }
    }

    return AddToWishlistMixin;
}
