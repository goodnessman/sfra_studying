import { scrollToTop } from '../toolbox/scroll';
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base Contact Us implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof ContactUs} Contact Us class
 */
export default function (Widget) {
    /**
     * @class ContactUs
     * @augments Widget
     */
    class ContactUs extends Widget {
        /**
         *
         * @param {typeof import('widgets/forms/AjaxForm').default} form - ajaxForm Widget
         * @param {object} data - response data
         */
        onSuccessSubmit(form, data) {
            if (data.success) {
                this.render(undefined, undefined, undefined, data.msg).then(()=> {
                    scrollToTop();
                });
            }
        }
    }

    return ContactUs;
}
