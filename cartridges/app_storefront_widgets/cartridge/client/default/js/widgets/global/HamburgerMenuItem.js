const keyCode = Object.freeze({
    ESC: 27,
    PAGEUP: 33,
    PAGEDOWN: 34,
    END: 35,
    HOME: 36,
    UP: 38,
    DOWN: 40,
    RIGHT: 39,
    LEFT: 37
});

/**
 * @description Base HamburgerMenuItem implementation
 * @param {ReturnType<typeof import('widgets/global/ListAccessibility').default>} listAccessibility Base widget for extending
 * @returns {typeof HamburgerMenuItem} HamburgerMenuItem widget
 */
export default function (listAccessibility) {
    /**
     * @class HamburgerMenuItem
     * @augments listAccessibility
     * @classdesc Represents Menu Item with specific logic for submenus, handling viewtype changes, keyboard navigation.
     * Hamburger menu item may contain another hamburger menu item.
     * @property {string} data-widget - Widget name `menuItem`
     * @property {string} data-event-keydown - Event listener for `handleKeydown` method
     * @property {boolean} data-prevent-show-submenu - prevent showing submenu
     * @example
     * // use this code to display widget
     * <li role="none"
     *    class="b-menu_bar-item"
     *    data-widget="hamburgerMenuItem"
     *    data-prevent-show-submenu="false"
     *    data-event-keydown="handleKeydown"
     *>
     *    <a
     *        role="menuitem"
     *        class="b-menu_bar-link m-has-submenu"
     *        aria-label="${category.name}"
     *        aria-haspopup="true"
     *        aria-expanded="false"
     *        itemprop="url name"
     *        tabindex="${status.first ? 0 : -1}"
     *        href="<isprint value="${category.url}" encoding="htmldoublequote"/>"
     *        data-ref="itemLink"
     *    >
     *        ${category.name}
     *    </a>
     *    <isset name="menuItem" value="${category}" scope="page" />
     *    <div
     *        role="menu"
     *        class="b-menu_bar-flyout"
     *        aria-label="${category.name}"
     *        aria-hidden="true"
     *        data-ref="submenu"
     *    >
     *        <div role="none" class="b-menu_bar-flyout_inner">
     *            <isinclude template="components/header/menuItem" />
     *
     *            <button
     *                role="menuitem"
     *                class="b-menu_bar-flyout_close"
     *                aria-label="${Resource.msg('common.close', 'common', null)}"
     *                title="${Resource.msg('common.close', 'common', null)}"
     *                type="button"
     *                tabindex="-1"
     *            >
    *                <isinclude template="/common/svg/close" />
     *            </button>
     *        </div>
     *    </div>
     *</li>
     */

    class HamburgerMenuItem extends listAccessibility {
        /**
         * @description calls super constructor, adds initial states etc.
         *
         * @param {HTMLElement} el DOM element
         * @param {{[x: string]: object|string|number|boolean|null|undefined}} config widget config
         */
        constructor(el, config = {}) {
            super(el, config);
            this.isSubmenuOpen = false;
        }

        prefs() {
            return {
                preventShowSubmenu: false,
                submenu: 'submenu',
                ...super.prefs()
            };
        }

        /**
         * @description Initialize widget logic
         */
        init() {
            this.defineItems();
        }

        /**
         * @description Mark Submenu Opened
         */
        markSubmenuOpened() {
            this.isSubmenuOpen = true;
        }

        /**
         * @description Has submenu
         * @returns {boolean} Return true if submenu exist
         */
        hasSubmenu() {
            return this.has(this.prefs().submenu) && !this.prefs().preventShowSubmenu;
        }

        /**
         * @description Has opened submenu
         * @returns {boolean} Return true if submenu opened
         */
        hasOpenedSubmenu() {
            return this.isSubmenuOpen;
        }

        /**
         * @description Open next menu level if exist
         */
        openMenu() {
            if (this.hasSubmenu()) {
                let submenuHTML;
                let itemLinkRefEl;

                this.has(this.prefs().itemLink, (itemLink) => {
                    itemLinkRefEl = itemLink;
                });
                this.has(this.prefs().submenu, (submenu) => {
                    const submenuElement = submenu.get();
                    if (submenuElement) {
                        submenuHTML = submenuElement.innerHTML;
                    }
                });
                this.eventBus().emit('nav.panel.next.level', {
                    htmlMarkup: submenuHTML,
                    parentMenuItem: itemLinkRefEl
                });
            }
        }

        /**
         * @description Clear submenu HTML
         */
        clearSubmenuHTML() {
            this.has(this.prefs().submenu, (submenu) => {
                let submenuElement = submenu.get();
                if (submenuElement) {
                    submenuElement.innerHTML = '';
                }
            });
        }

        /**
         * @description Set Submenu HTML
         * @param {string} submenuHTML Submenu HTML
         */
        setSubmenuHTML(submenuHTML) {
            this.has(this.prefs().submenu, (submenu) => {
                let submenuElement = submenu.get();
                if (submenuElement) {
                    submenuElement.innerHTML = submenuHTML;
                }
            });
        }

        /**
         * @description Click Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {Event} event Event object
         */
        handleClick(_, event) {
            if (this.hasSubmenu()) {
                event.preventDefault();
                this.openMenu();
            }
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            if (!this.hasOpenedSubmenu()) {
                return;
            }

            switch (event.keyCode) {
                case keyCode.LEFT:
                    this.eventBus().emit('nav.panel.previos.level');
                    preventEventActions = true;

                    break;

                case keyCode.RIGHT:
                    this.currentItem.openMenu();
                    preventEventActions = true;

                    break;

                case keyCode.PAGEUP:
                case keyCode.HOME:
                    this.setFocusToFirstItem();
                    preventEventActions = true;

                    break;

                case keyCode.PAGEDOWN:
                case keyCode.END:
                    this.setFocusToLastItem();
                    preventEventActions = true;

                    break;

                case keyCode.UP:
                    this.setFocusToPreviousItem();
                    preventEventActions = true;

                    break;

                case keyCode.DOWN:
                    this.setFocusToNextItem();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return HamburgerMenuItem;
}
