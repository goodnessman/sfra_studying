const keyCode = Object.freeze({
    RETURN: 13,
    SPACE: 32
});

/**
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base AccordionItem implementation
 * @typedef {InstanceType <typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @param {typeof import('widgets/Widget').default} Widget Base widget for extending
 * @returns {typeof AccordionItem} AccordionItem widget
 */
export default function (Widget) {
    /**
     * @class AccordionItem
     * @augments Widget
     * @classdesc Represents AccordionItem with specific logic for accordion item, handling viewtype changes, keyboard navigation
     * @property {string} data-widget - Widget name `accordionItem`
     * @property {string} data-event-keydown - Event listener for `handleKeydown` method
     * @property {string} data-event-click - Event listener for `mouseEnter` method
     * @property {boolean} data-expanded - Expanded flag
     * @example
     * // use this code to display widget
     *   <section
     *      data-widget="accordionItem"
     *      data-widget-event-closeallitems="closeItems"
     *  >
     *      <h2
     *          data-ref="accordionItemBtn"
     *          data-event-click="togglePanel"
     *          data-event-keydown="handleKeydown"
     *      >
     *          <button
     *              type="button"
     *          >
     *              Account
     *          </button>
     *      </h2>
     *      <div data-ref="accordionItemPanel">
     *          <div data-ref="accordionItemPanelInner">
     *              Panel content here
     *          </div>
     *      </div>
     *  </section>
     */

    class AccordionItem extends Widget {
        prefs() {
            return {
                accordionItemBtn: 'accordionItemBtn',
                accordionItemPanel: 'accordionItemPanel',
                accordionItemPanelInner: 'accordionItemPanelInner',
                expanded: false,
                animateToggle: true,
                ...super.prefs()
            };
        }

        /**
         * @description Initialize widget logic
         * @returns {void}
         */
        init() {
            this.isPanelOpen = this.prefs().expanded;
            this.isToggleAllowed = false;
            this.isMultipleSections = false;
            this.defineAttributes(this.prefs().expanded);
            this.onDestroy(this.clearAttributes.bind(this));
            this.onDestroy(this.cleanPanelHeight.bind(this));

            if (!this.prefs().expanded) {
                this.closePanel();
            }
        }

        /**
         * @description Define attributes
         * @param {boolean} [isOpen] Panel open flag
         * @returns {void}
         */
        defineAttributes(isOpen) {
            const accordionItemBtn = this.ref('accordionItemBtn');
            const accordionItemPanel = this.ref('accordionItemPanel');
            const accordionItemBtnID = accordionItemBtn.attr('id');
            const accordionItemPanelID = accordionItemPanel.attr('id');

            accordionItemBtn.attr('role', 'button');
            accordionItemBtn.attr('aria-controls', accordionItemPanelID);
            accordionItemBtn.attr('aria-expanded', 'false');
            accordionItemBtn.attr('tabindex', '0');
            accordionItemPanel.attr('role', 'region');
            accordionItemPanel.attr('aria-labelledby', accordionItemBtnID);

            if (isOpen) {
                accordionItemPanel.attr('aria-hidden', 'false');
                accordionItemBtn.attr('aria-expanded', 'true');
            } else {
                accordionItemPanel.attr('aria-hidden', 'true');
                accordionItemBtn.attr('aria-expanded', 'false');
            }
        }

        /**
         * @description Clear Attributes
         * @returns {void}
         */
        clearAttributes() {
            this.has(this.prefs().accordionItemBtn, (accordionItemBtn) => {
                accordionItemBtn.attr('role', false);
                accordionItemBtn.attr('aria-controls', false);
                accordionItemBtn.attr('aria-expanded', false);
                accordionItemBtn.attr('aria-disabled', false);
                accordionItemBtn.attr('tabindex', false);
            });
            this.has(this.prefs().accordionItemPanel, (accordionItemPanel) => {
                accordionItemPanel.attr('role', false);
                accordionItemPanel.attr('aria-labelledby', false);
                accordionItemPanel.attr('aria-hidden', false);
            });
        }

        /**
         * @description Set Focus
         * @returns {void}
         */
        focus() {
            this.has(this.prefs().accordionItemBtn, (accordionItemBtn) => {
                accordionItemBtn.focus();
            });
        }

        /**
         * @description On Refresh Handler
         * @returns {void}
         */
        onRefresh() {
            super.onRefresh();

            this.defineAttributes(this.isPanelOpen);

            if (!this.isPanelOpen) {
                this.closePanel();
            }
        }

        /**
         * @description Open panel
         * @returns {void}
         */
        openPanel() {
            if (!this.isMultipleSections) {
                this.emit('closeallitems');
            }

            this.has(this.prefs().accordionItemBtn, (accordionItemBtn) => {
                accordionItemBtn.attr('aria-expanded', 'true');

                if (!this.isToggleAllowed) {
                    accordionItemBtn.attr('aria-disabled', 'true');
                }
            });
            this.has(this.prefs().accordionItemPanel,
                accordionItemPanel => this.togglePanelHeight(true, accordionItemPanel));

            this.isPanelOpen = true;
        }

        /**
         * @description Close panel
         * @returns {void}
         */
        closePanel() {
            this.has(this.prefs().accordionItemBtn, (accordionItemBtn) => {
                accordionItemBtn.attr('aria-expanded', 'false');

                if (!this.isToggleAllowed) {
                    accordionItemBtn.attr('aria-disabled', false);
                }
            });
            this.has(this.prefs().accordionItemPanel,
                accordionItemPanel => this.togglePanelHeight(false, accordionItemPanel));

            this.isPanelOpen = false;
        }

        /**
         * @description Toggle panel
         * @returns {void}
         */
        togglePanel() {
            if (!this.isPanelOpen) {
                this.openPanel();
            } else if (this.isToggleAllowed) {
                this.closePanel();
            }
        }

        /**
         * @description Toggle panel height, so it could be properly animated.
         * It is required inner element to has content height properly.
         * @param {boolean} isOpen Panel Open flag
         * @param {refElement} panel IsOpen flag
         * @returns {void}
         */
        togglePanelHeight(isOpen, panel) {
            panel.attr('aria-hidden', isOpen ? 'false' : 'true');

            this.has(this.prefs().accordionItemPanelInner, inner => {
                const panelElement = panel.get();
                const innerElement = inner.get();
                if (innerElement && panelElement) {
                    panelElement.style.height = `${isOpen ? innerElement.offsetHeight : 0}px`;
                }
            });
        }

        /**
         * @description Remove hardcoded panel height in case of widget destroyed
         * @returns {void}
         */
        cleanPanelHeight() {
            this.has(this.prefs().accordionItemPanel, accordionItemPanel => {
                const element = accordionItemPanel.get();

                if (element && (parseInt(element.style.height, 10) > 0)) {
                    element.style.height = 'auto';
                }
            });
        }

        /**
         * @description Update accordion item state, recalculate item height
         * TODO: Describe approach for accordion height in case of content change
         */
        updateState() {
            this.has(this.prefs().accordionItemPanel,
                accordionItemPanel => this.togglePanelHeight(Boolean(this.isPanelOpen), accordionItemPanel));
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         * @returns {void}
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.RETURN:
                case keyCode.SPACE:
                    this.togglePanel();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return AccordionItem;
}
