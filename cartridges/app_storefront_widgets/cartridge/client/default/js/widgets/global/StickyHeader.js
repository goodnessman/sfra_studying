export default function (Widget) {
    /**
     * @class StickyHeader
     * @augments Widget
     * @classdesc sticky header helper. Should be on element before sticky header to observe intersection
     * Could be reimplemented with getBoundClientRect.top and scroll listener
     */
    class StickyHeader extends Widget {
        prefs() {
            return {
                classesShow: 'b-header_stuck',
                ...super.prefs()
            };
        }

        init() {
            this.observer = new IntersectionObserver(([entry]) => this.toggleStickyState(entry), { threshold: 1 });
            this.observer.observe(this.ref('self').get());
        }

        toggleStickyState(entry) {
            document.body.classList.toggle(this.prefs().classesShow, entry.intersectionRatio < 1);
        }

        destroy() {
            this.observer.unobserve(this.ref('self').get());
            this.observer.disconnect();
        }
    }

    return StickyHeader;
}
