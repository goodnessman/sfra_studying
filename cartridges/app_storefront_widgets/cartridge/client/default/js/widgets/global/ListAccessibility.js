/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base ListAccessibility implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof ListAccessibility} List Accessibility class
 */
export default function (Widget) {
    /**
     * @class ListAccessibility
     * @augments Widget
     * @classdesc ListAccessibility widget, its parent widget for accessibility widgets.
     */
    class ListAccessibility extends Widget {
        prefs() {
            return {
                itemLink: 'itemLink',
                ...super.prefs()
            };
        }

        /**
         * @description Define Items
         * @param {InstanceType<Widget>} [currentItem] Current Item
         * @returns {void}
         */
        defineItems(currentItem) {
            if (this.items) {
                this.focusableItems = [];
                this.focusableItems = this.items.filter((item) => {
                    return item.isShown() && 'focus' in item;
                });


                if (this.focusableItems.includes(currentItem)) {
                    this.currentItem = currentItem;
                } else {
                    this.currentItem = this.focusableItems[0];
                }

                this.firstItem = this.focusableItems[0];
                this.lastItem = this.focusableItems[this.focusableItems.length - 1];
            }
        }

        /**
         * @description Set focus to item
         * @param {HTMLElement|undefined} item item
         */
        setFocusToItem(item) {
            if (item) {
                item.focus();
                this.currentItem = item;
            }
        }

        /**
         * @description Set focus to current item
         */
        setFocusToCurrentItem() {
            this.setFocusToItem(this.currentItem);
        }

        /**
         * @description Set focus to first item
         */
        setFocusToFirstItem() {
            this.setFocusToItem(this.firstItem);
        }

        /**
         * @description Set focus to last item
         */
        setFocusToLastItem() {
            this.setFocusToItem(this.lastItem);
        }

        /**
         * @description Get item index
         * @param {InstanceType<Widget>} item item widget
         * @returns {number} Item index
         */
        getItemIndex(item) {
            return this.focusableItems ? this.focusableItems.indexOf(item) : -1;
        }

        /**
         * @description Set focus to next item
         */
        setFocusToNextItem() {
            if (this.focusableItems) {
                let newItem = this.currentItem === this.lastItem
                    ? this.firstItem
                    : this.focusableItems[this.getItemIndex(this.currentItem) + 1];

                this.setFocusToItem(newItem);
            }
        }

        /**
         * @description Set focus to previous item
         */
        setFocusToPreviousItem() {
            if (this.focusableItems) {
                let newItem = this.currentItem === this.firstItem
                    ? this.lastItem
                    : this.focusableItems[this.getItemIndex(this.currentItem) - 1];

                this.setFocusToItem(newItem);
            }
        }

        /**
         * @description Set Focus
         */
        focus() {
            this.has(this.prefs().itemLink, (itemLink) => {
                itemLink.focus();
            });
        }
    }

    return ListAccessibility;
}
