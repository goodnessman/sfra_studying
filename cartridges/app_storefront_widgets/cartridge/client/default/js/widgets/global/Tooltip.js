// NOT USED WIRGET, JUST FOR REFERENCE
// TODO: should be implemented as form of https://www.w3.org/TR/wai-aria-practices/#disclosure
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base Tooltip implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof Tooltip} Tooltip class
 */
export default function (Widget) {
    /**
     * @class Tooltip
     * @augments Widget
     * @classdesc Represents a Tooltip, which can be shown / hided. Widget markup contains original container
     * <br>and referenced tooltip content. Hovering over original container Tooltip should be exposed
     * @property {string} data-widget - Widget name `tooltip`
     * @property {string} data-event-focus - Event listener for `focus` event. Usualy used to show tooltip content.
     * @property {string} data-event-blur - Event listener for `blur` event. Usualy used to hide tooltip content.
     * @example
     * // use this code to display minicart widget
     * <button type="button" class="info-icon"
     *     data-widget="tooltip"
     *     data-event-focus="showTip"
     *     data-event-blur="hideTip"
     * >
     *     <span class="icon" aria-hidden="true">i</span>
     *     <span class="sr-only">${Resource.msg('button.help.assistive.text','common',null)}</span>
     *     <span class="tooltip" data-ref="content" hidden>
     *         ${Resource.msg('tooltip.phone.number','creditCard',null)}
     *     </span>
     * </button>
     */
    class Tooltip extends Widget {
        prefs() {
            return {
                content: 'content',
                ...super.prefs()
            };
        }

        showTip() {
            this.ref(this.prefs().content).show();
        }

        hideTip() {
            this.ref(this.prefs().content).hide();
        }
    }

    return Tooltip;
}
