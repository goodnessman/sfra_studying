// TODO: implement lazy loading promo tiles for mega menu

const keyCode = Object.freeze({
    TAB: 9,
    RETURN: 13,
    SPACE: 32,
    PAGEUP: 33,
    PAGEDOWN: 34,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40
});
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/MenuBarItem').default>>} menuBarItem
 */

/**
 * @description Base MegaMenu item implementation.
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof MegaMenu} MegaMenu widget
 */
export default function (Widget) {
    /**
     * @class MegaMenu
     * @augments Widget
     * @classdesc Menu bar widget for Top Navigation menu. Could be opened/closed
     * <br>For additional information you can check
     * <br>`cartridges/app_storefront_widgets/cartridge/templates/default/components/header/menu.isml`
     * @property {string} data-widget - Widget name `MegaMenu`
     * @property {string} data-event-keydown - Event listener for `handleKeydown` method
     * @example
     * use this code to display widget
     * <ul
     *     role="menubar"
     *     class="b-menu_bar-inner"
     *     aria-label="Main categories"
     *     data-widget="megaMenu"
     *     data-event-keydown="handleKeydown"
     * >
     */
    class MegaMenu extends Widget {
        /**
         * @description Initialize widget logic.
         * Define current, first and last menu item.
         * Set the role of menu.
         */
        init() {
            this.defineItems();
            this.eventBus().on('mega.menu.close.items', 'closeItems');
            this.ref('self').attr('role', 'menubar');
            this.onDestroy(() => {
                this.ref('self').attr('role', false);
            });
        }


        /**
         * @description Set focus to item
         * @param {MenuItem} menuItem menuItem Widget
         */
        setFocusToItem(menuItem) {
            let hasOpenedMenu = false;

            this.eachChild((child) => {
                if (child.hasOpenedSubmenu()) {
                    hasOpenedMenu = true;
                }

                child.removeTabIndex();
                child.closeMenu();
            });

            menuItem.addTabIndex();
            menuItem.focus();

            if (hasOpenedMenu) {
                menuItem.openMenu();
            }

            this.currentItem = menuItem;
        }

        /**
         * @description Close all Items
         */
        closeItems() {
            this.eachChild((child) => {
                if (typeof child.closeMenu === 'function') {
                    child.closeMenu();
                }
            });
        }

        /**
         * @description Handle Enter key event
         * @param {Event} event Event Object
         */
        handleEnterKey(event) {
            if (event.target && event.target.href) {
                window.location.assign(event.target.href);
            }
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {Event} event Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.PAGEUP:
                case keyCode.HOME:
                    this.setFocusToFirstItem();
                    preventEventActions = true;

                    break;

                case keyCode.PAGEDOWN:
                case keyCode.END:
                    this.setFocusToLastItem();
                    preventEventActions = true;

                    break;

                case keyCode.RIGHT:
                    this.setFocusToNextItem();
                    preventEventActions = true;

                    break;

                case keyCode.LEFT:
                    this.setFocusToPreviousItem();
                    preventEventActions = true;

                    break;

                case keyCode.UP:
                    this.currentItem.openMenu(false, true);
                    preventEventActions = true;

                    break;

                case keyCode.DOWN:
                    this.currentItem.openMenu(true, false);
                    preventEventActions = true;

                    break;

                case keyCode.TAB:
                    this.closeItems();

                    break;

                case keyCode.RETURN:
                case keyCode.SPACE:
                    this.handleEnterKey(event);
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return MegaMenu;
}
