const keyCode = Object.freeze({
    RETURN: 13,
    SPACE: 32
});

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base BaseButton implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof Button} Button class
 */
export default function (Widget) {
    /**
     * @class BaseButton
     * @augments Widget
     * @classdesc Button widget, which could be used in different contextes (html-markup, inside widgets etc).
     * <br>Could be also used to trigger parent's widget method
     * @property {string} data-widget - Widget name `button`
     * @property {string} data-widget-event-click - Event listener to call Parent's widget method
     * @property {string} data-event-click - Event listener method `handleClick` for click event on widget
     * @property {boolean} data-prevent-action - Prevent action flag
     * @example
     * // use this code to display widget
     * <button type="submit" class="btn btn-block btn-primary"
     *     data-widget="button"
     *     data-widget-event-click="handleSubmit"
     *     data-event-click.prevent="handleClick"
     *     data-id="submitButton"
     * >
     *     ${Resource.msg('button.text.loginform', 'login', null)}
     * </button>
     */
    class Button extends Widget {
        prefs() {
            return {
                classesError: 'm-error',
                classesActive: 'm-active',
                classesBusy: 'm-busy',
                preventAction: false,
                ...super.prefs()
            };
        }

        init() {
            super.init();
            if (!this.id) {
                this.id = String(this.config.id);
            }

            this.disabled = this.ref('self').attr('disabled') === 'disabled';
        }

        handleClick() {
            if (!this.prefs().preventAction) {
                this.emit('click', this);
            }
        }

        getValue() {
            return this.ref('self').val();
        }

        /**
         * @param {(string|number|boolean|undefined)} val - Value to set into widget's val attribute
         */
        setValue(val = '') {
            this.setError();
            return this.ref('self').val(val);
        }

        getText() {
            return this.ref('self').getText();
        }

        /**
         * @param {(string|undefined)} val - Value to set as a text of the Button
         */
        setText(val = '') {
            return this.ref('self').setText(val);
        }

        /**
         * @param {(string|undefined)} err - Sets `err` as a Button text and if `err` is not empty - adds error class to Button
         */
        setError(err = '') {
            this.setText(err);
            if (err) {
                this.ref('self').addClass(this.prefs().classesError);
            }
        }

        activate() {
            this.ref('self').addClass(this.prefs().classesActive);
        }

        deactivate() {
            this.ref('self').removeClass(this.prefs().classesActive);
        }

        /**
         * @param {(boolean|undefined)} state true to show/false to hide/undefined to auto
         */
        toggleActive(state) {
            this.ref('self').toggleClass(this.prefs().classesActive, state);
        }

        disable() {
            this.disabled = true;
            this.ref('self').disable();
        }

        enable() {
            this.disabled = false;
            this.ref('self').enable();
        }

        busy() {
            this.isBusy = true;
            this.ref('self').addClass(this.prefs().classesBusy);
        }

        unbusy() {
            this.isBusy = false;
            this.ref('self').removeClass(this.prefs().classesBusy);
        }

        select() {
            this.selected = true;
            this.ref('self').attr('aria-selected', true);
        }

        unselect() {
            this.selected = false;
            this.ref('self').attr('aria-selected', false);
        }

        getName() {
            return this.ref('self').attr('name');
        }

        /**
         * @description Focus to element
         * @returns {Button} - return instance for chaining
         */
        focus() {
            this.ref('self').focus();
            return this;
        }

        /**
         * @description Sets button to be accountable with `tab` button navigation
         * @returns {Button} - return instance for chaining
         */
        setTabindex() {
            this.ref('self').attr('tabindex', '0');
            return this;
        }

        /**
         * @description Sets button to be not accountable with `tab` button navigation
         * @returns {Button} - return instance for chaining
         */
        unsetTabindex() {
            this.ref('self').attr('tabindex', '-1');
            return this;
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         * @returns {void}
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.RETURN:
                case keyCode.SPACE:
                    this.handleClick();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return Button;
}
