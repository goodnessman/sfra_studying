import { timeout } from 'widgets/toolbox/util';
import { dialogMgr } from 'widgets/toolbox/dialogMgr';
import { clickOutside } from 'widgets/toolbox/util';

const ESCAPE_CODE = 27;

/**
 * @description Modal implementation
 * @param {ReturnType<typeof import('widgets/global/AccessibilityFocusTrapMixin').default>} AccessibilityFocusTrapMixin
 * @returns {typeof Modal} Modal class
 */
export default function (AccessibilityFocusTrapMixin) {
    /**
     * @class Modal
     * @augments AccessibilityFocusTrapMixin
     * @classdesc Generic modal popups implementation.<br/>
     * <p>
     * If Modal need to be inited only on some viewport can be used modificators (sm,md,lg,xl). For example data-widget.sm.md="modal"</br>
     * Note that in this case need also add same modificator to sub events in html elements. This can be easily done via "dialogViewtypes" variable in ISML template (see example below).<br/>
     * Don't forget clear this variable after dialog closing tag.
     * </p>
     * @property {string} data-widget - Widget name `modal`
     * @property {boolean} data-disable-rendering - Disable rendering flag
     * @property {string} data-classes-extra - Extra classes
     * @example
     * // use this code to display widget
     * <div data-widget="modal">
     *     // dialogViewtypes need only in case when modal should be inited on some viewports
     *     <isset name="dialogViewtypes" value="sm.md" scope="page" />
     *
     *     <div data-ref="container" data-event-click.self="closeModal"></div>
     *     <script type="template/mustache" data-ref="template">
     *         ... modal popup contents
     *     </script>
     * </div>
     */
    class Modal extends AccessibilityFocusTrapMixin {
        prefs() {
            return {
                classesGlobalDialog: 'm-has_dialog',
                classesShow: 'm-opened',
                classesTopDialog: 'm-top_dialog',
                classesActive: 'm-active',
                refContainer: 'container',
                refDialog: 'dialog',
                classesExtra: '',
                clickOutSide: true,
                disableRendering: false,
                closeByEscape: true,
                modalTimeout: 400,
                ...super.prefs()
            };
        }

        init() {
            super.init();
            this.onDestroy(() => {
                const refDialog = this.ref(this.prefs().refDialog);
                refDialog.attr('role', false);
                refDialog.attr('aria-modal', false);
            });
        }

        /**
         * @description Show Modal
         * @param {object} [templateData] data to be rendered in template
         * @returns {void}
         */
        showModal(templateData) {
            const container = this.ref(this.prefs().refContainer);
            const classes = [this.prefs().classesShow];

            if (this.prefs().classesExtra) {
                classes.push(this.prefs().classesExtra);
            }

            this.backFocusElement = /** @type {HTMLElement} */(document.activeElement);

            if (templateData && !this.prefs().disableRendering) {
                this.render(undefined, templateData, container)
                    .then(() => {
                        dialogMgr.openDialog(this);
                        container.addClass(classes.join(' '));

                        this.show();

                        this.onDestroy(timeout(() => this.focusFirstElement(), this.prefs().modalTimeout));
                    });
            } else {
                dialogMgr.openDialog(this);
                container.addClass(classes.join(' '));
                this.show();

                this.onDestroy(timeout(() => this.focusFirstElement(), this.prefs().modalTimeout));
            }
        }

        /**
         * @description Open
         * @returns {void}
         */
        open() {
            const dialog = this.ref(this.prefs().refDialog);
            dialog.attr('role', 'dialog');
            dialog.attr('aria-modal', 'true');
            this.addGlobalDialogClass();
            this.addListeners();
            dialog.addClass(this.prefs().classesActive);
            dialog.addClass(this.prefs().classesTopDialog);
        }

        /**
         * @description Move Behind
         * @returns {void}
         */
        moveBehind() {
            this.cleanUpListeners();
            this.ref(this.prefs().refDialog).removeClass(this.prefs().classesTopDialog);
        }

        /**
         * @description Move To Top
         * @returns {void}
         */
        moveToTop() {
            this.addListeners();
            this.ref(this.prefs().refDialog).addClass(this.prefs().classesTopDialog);
        }

        /**
         * @description Close
         * @returns {void}
         */
        close() {
            this.cleanUpListeners();
            this.ref(this.prefs().refDialog).removeClass([this.prefs().classesTopDialog, this.prefs().classesActive].join(' '));
        }

        /**
         * @description Add Global Dialog Class
         * @returns {void}
         */
        addGlobalDialogClass() {
            const html = this.ref('html');

            if (!html.hasClass(this.prefs().classesGlobalDialog)) {
                html.addClass(this.prefs().classesGlobalDialog);
            }
        }

        /**
         * @description Remove Global Dialog Class
         * @returns {void}
         */
        removeGlobalDialogClass() {
            this.ref('html').removeClass(this.prefs().classesGlobalDialog);
        }

        /**
         * @description Close Modal
         * @returns {void}
         */
        closeModal() {
            const classes = [this.prefs().classesShow];

            if (this.prefs().classesExtra) {
                classes.push(this.prefs().classesExtra);
            }

            this.ref(this.prefs().refContainer).removeClass(classes.join(' '));
            this.hide();

            dialogMgr.closeDialog();

            if (this.backFocusElement) {
                this.backFocusElement.focus();
                this.backFocusElement = null;
            }

            if (!this.prefs().disableRendering) {
                this.ref(this.prefs().refContainer).empty();
            }
        }

        /**
         * @description Clean Up Listeners
         * @returns {void}
         */
        cleanUpListeners() {
            if (this.escHandler) {
                this.escHandler();
                this.escHandler = null;
            }

            if (this.listener) {
                this.listener();
                this.listener = null;
            }
        }

        /**
         * @description Cancel Handler
         * @returns {void}
         */
        cancel() {
            this.closeModal();
            this.emit('cancel');
        }

        /**
         * @description Add Click Outside Listener
         * @returns {void}
         */
        addListeners() {
            if (this.prefs().clickOutSide) {
                this.listener = clickOutside(this.ref(this.prefs().refDialog), () => {
                    this.cancel();
                    return false;
                }, true);
                // @ts-ignore
                this.onDestroy(this.listener);
            }

            if (this.prefs().closeByEscape) {
                this.escHandler = this.ev('keyup', (_, event) => {
                    const kdbEvent = /** @type {KeyboardEvent} */(event);
                    if (kdbEvent.keyCode === ESCAPE_CODE) {
                        this.cancel();
                    }
                }, window).pop();
            }
        }

        /**
         * @description Show
         * @returns {this}
         */
        show() {
            this.ref(this.prefs().refContainer).show();
            return this;
        }

        /**
         * @description Hide
         * @returns {this}
         */
        hide() {
            this.ref(this.prefs().refContainer).hide();
            return this;
        }
    }

    return Modal;
}
