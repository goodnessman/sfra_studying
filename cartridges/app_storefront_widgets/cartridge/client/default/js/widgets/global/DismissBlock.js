// TO BE REVIEWED
// TODO: issues bouncing during loading should be addressed in this implementation
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Dismiss block implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof DismissBlock} Dismiss Block class
 */
export default function (Widget) {
    /**
     * @class DismissBlock
     * @augments Widget
     * @classdesc Promo header widget, which could contain dynamic markup. Could be dismissed.
     * @property {string} data-widget - Widget name `dismissBlock`
     * @property {string} data-key - Key for window.sessionStorage to store header banner display status.
     * If value exists - header will not be shown.
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="dismissBlock"
     *     data-key="hide_header_banner"
     * >
     *     ... widget markup
     *     <button data-event-click.once.prevent="dismiss">close</button>
     * </div>
     */
    class DismissBlock extends Widget {
        prefs() {
            return {
                classesEnabled: 'm-visible',
                ...super.prefs()
            };
        }

        init() {
            super.init();

            const dismissBlockStatus = window.sessionStorage.getItem(this.prefs().key);
            if (!dismissBlockStatus || dismissBlockStatus < 0) {
                this.ref('self').addClass(this.prefs().classesEnabled);
            }
        }

        dismiss() {
            this.ref('self').removeClass(this.prefs().classesEnabled);
            window.sessionStorage.setItem(this.prefs().key, '1');
        }
    }

    return DismissBlock;
}
