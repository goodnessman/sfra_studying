import { timeout } from 'widgets/toolbox/util';
import { isDesktopView } from 'widgets/toolbox/viewtype';

const keyCode = Object.freeze({
    RETURN: 13,
    SPACE: 32,
    TAB: 9,
    ESC: 27,
    DOWN: 40,
    UP: 38,
    RIGHT: 39,
    PAGEUP: 33,
    PAGEDOWN: 34,
    END: 35,
    HOME: 36
});

/**
 * @description Base MenuPanel implementation
 * @typedef {InstanceType <typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @param {ReturnType<typeof import('widgets/global/SwipeToClose').default>} SwipeToClose Base widget for extending
 * @returns {typeof MenuPanel} MenuPanel class
 */
export default function (SwipeToClose) {
    /**
     * @class MenuPanel
     * @augments SwipeToClose
     * @classdesc MenuPanel widget, that contains menu for touch/mobile devices.
     * @property {string} data-widget - Widget name `menuPanel`
     * @property {string} data-event-click - Event listener handler for click events
     * @property {string} data-event-keydown - Event listener handler for keydown events
     * @property {number} data-panel-items-count - count of items
     * @property {number} data-panel-count - count of panels
     * @property {string} data-panel-container - reference id of child element, value of its data-ref attribute
     * @property {string} data-classes-backdrop-active - class added/removed on 'self' element on openPanel/closePanel
     * @property {string} data-classes-dialog-open - class added/removed on ref element 'dialog' on openPanel/closePanel
     * @property {string} data-classes-has-dialog - class added/removed on 'html' element on openPanel/closePanel
     * @property {string} data-classes-active-level - prefix for class that is added/removed from panelContainer when navigating between panels
     * @property {string} data-panel-items-prefix - prefix of ID of panel items
     * @property {string} data-main-manu - id of menu element
     * @property {string} data-panel - prefix of ID of panels
     * @property {string} data-item-switch-timeout - timeout (in ms) between panels focus changes
     * @example
     * // use this code to display widget
     * <div
     *    id="main-navigation"
     *    class="b-menu_panel"
     *    aria-labelledby="main-navigation-toggle"
     *    data-widget="menuPanel"
     *    data-event-click="closeOnBackdrop"
     *    data-event-keydown.sm.md="handleKeydown"
     * >
     *     <div class="b-menu_panel-head">
     *         <button data-event-click.prevent="closePanel">close</button>
     *     </div>
     *     ... widget content
     * </div>
     */
    class MenuPanel extends SwipeToClose {
        /**
         * @description calls super constructor, adds initial states etc.
         *
         * @param {HTMLElement} el DOM element
         * @param {{[x: string]: object|string|number|boolean|null|undefined}} config widget config
         */
        constructor(el, config = {}) {
            super(el, config);
            this.parentMenuItems = [];
            this.panelItems = [];
            this.currentLevel = 1;
        }

        /**
         * @description Preferences
         * @returns {object} Preferences object
         */
        prefs() {
            return {
                classesBackdropActive: 'm-active',
                classesDialogOpen: 'm-opened',
                classesHasDialog: 'm-has_dialog',
                classesActiveLevel: 'm-active_level_',
                panelItemsCount: 0,
                panelCount: 3,
                panelItemsPrefix: 'panelItem-',
                mainManu: 'main-menu',
                panelTitle: 'panelTitleLevel',
                panel: 'panel-level-',
                panelContainer: 'panelContainer',
                itemSwitchTimeout: 500,
                ...super.prefs()
            };
        }

        /**
         * @description Initialize widget logic
         */
        init() {
            super.init();
            this.definePanelItems();
            this.checkAriaAttributes();
            this.eventBus().on('viewtype.change', 'checkAriaAttributes');
            this.eventBus().on('viewtype.change', 'closePanel');
            this.eventBus().on('nav.panel.open', 'openPanel');
            this.eventBus().on('nav.panel.next.level', 'nextPanel');
            this.eventBus().on('nav.panel.previos.level', 'previosPanel');
            this.ref('self').attr('role', 'menu');
            this.onDestroy(() => {
                this.ref('self').attr('role', false);
            });
        }

        /**
         * @description Define Panel Items
         */
        definePanelItems() {
            this.panelItems = this.getPanelItems();
            this.currentPanelItem = this.panelItems[0];
            this.firstPanelItem = this.panelItems[0];
            this.lastPanelItem = this.panelItems[this.panelItems.length - 1];
        }

        /**
         * @description Check Aria attributes and updated it for custom menu items depend on viewport.
         * <br> Custom Menu has sub item on Large and XLarge viewports but doesn't have on Small and Medium, as per FSD.
         * To correctly support accessibility requirements aria attributes dynamically updated.
         */
        checkAriaAttributes() {
            this.panelItems
                .map(menuItem => menuItem.ref('itemLink'))
                .filter(itemLink => itemLink.data('isCustomMenuHtml'))
                .forEach(itemLink => {
                    itemLink.attr('aria-expanded', isDesktopView());
                    itemLink.attr('aria-haspopup', isDesktopView());
                });
        }

        /**
         * @description Open next panel level
         * @param {object} data data
         */
        nextPanel(data) {
            data.parentMenuItem.attr('aria-expanded', 'true');
            this.parentMenuItems.push(data.parentMenuItem);

            this.handlePanel(this.currentLevel + 1, true, data.parentMenuItem.getText(), data.htmlMarkup);
        }

        /**
         * @description Open previos panel level
         */
        previosPanel() {
            if (this.parentMenuItems.length) {
                let last = this.parentMenuItems.pop();
                last.attr('aria-expanded', 'false');
            }

            this.handlePanel(this.currentLevel - 1, true);
        }

        /**
         * @description Handle level
         * @param {number} level Level
         * @param {boolean} focusPanelItem Focus panel item flag
         * @param {string} [panelName] Panel Name
         * @param {string} [htmlMarkup] HTML Markup
         */
        handlePanel(level, focusPanelItem, panelName, htmlMarkup) {
            if (panelName) {
                this.has(this.prefs().panelTitle + level, (panelTitle) => {
                    panelTitle.setText(panelName);
                });
            }

            if (htmlMarkup) {
                this.getById(this.prefs().panel + level, (panel) => {
                    panel.ref('self').attr('aria-busy', 'true');
                    panel.setSubmenuHTML(htmlMarkup);
                });
            }

            if (level === 1) {
                this.ref('self').attr('aria-busy', 'true');
            }

            this.has(this.prefs().panelContainer, (panelContainer) => {
                panelContainer.removeClass(this.prefs().classesActiveLevel + this.currentLevel);
                panelContainer.addClass(this.prefs().classesActiveLevel + level);
            });

            if (focusPanelItem) {
                this.setFocusToPanelItem(level, level > this.currentLevel);
            }

            this.currentLevel = level;
        }

        /**
         * @description Set focus to panel item
         * @param {number} level Level
         * @param {boolean} updateItems Update item flag
         */
        setFocusToPanelItem(level, updateItems) {
            this.onDestroy(timeout(() => {
                if (level === 1) {
                    this.setFocusToItem(this.currentPanelItem);
                    this.ref('self').attr('aria-busy', 'false');
                } else {
                    this.getById(this.prefs().panel + level, (panel) => {
                        panel.markSubmenuOpened();

                        if (updateItems) {
                            panel.defineItems();
                        }

                        panel.setFocusToCurrentItem();
                        panel.ref('self').attr('aria-busy', 'false');
                    });
                }
            }, this.prefs().itemSwitchTimeout));
        }

        /**
         * @description Set focus to first item
         */
        setFocusToFirstItem() {
            this.ref('self').attr('aria-busy', 'true');

            this.onDestroy(timeout(() => {
                this.setFocusToItem(this.firstPanelItem);
                this.ref('self').attr('aria-busy', 'false');
            }, this.prefs().itemSwitchTimeout));
        }

        /**
         * @description Set focus to last item
         */
        setFocusToLastItem() {
            this.ref('self').attr('aria-busy', 'true');

            this.onDestroy(timeout(() => {
                this.setFocusToItem(this.lastPanelItem);
                this.ref('self').attr('aria-busy', 'false');
            }, this.prefs().itemSwitchTimeout));
        }

        /**
         * @description Set focus to next item
         */
        setFocusToNextItem() {
            if (this.panelItems === undefined) return;

            if (this.panelItems.length) {
                let newItem = this.currentPanelItem === this.lastPanelItem
                    ? this.firstPanelItem
                    : this.panelItems[this.getItemIndex(this.currentPanelItem) + 1];

                this.setFocusToItem(newItem);
            }
        }

        /**
         * @description Set focus to previos item
         */
        setFocusToPreviosItem() {
            if (this.panelItems === undefined) return;

            if (this.panelItems.length) {
                let newItem = this.currentPanelItem === this.firstPanelItem
                    ? this.lastPanelItem
                    : this.panelItems[this.getItemIndex(this.currentPanelItem) - 1];

                this.setFocusToItem(newItem);
            }
        }

        /**
         * @description Set focus to item
         * @param {object} panelItem Panel item
         */
        setFocusToItem(panelItem) {
            panelItem.focus();
            this.currentPanelItem = panelItem;
        }

        /**
         * @description Get item index
         * @param {object} panelItem panel item
         * @returns {number} Item index
         */
        getItemIndex(panelItem) {
            return this.panelItems ? this.panelItems.indexOf(panelItem) : -1;
        }

        /**
         * @description Get Panel Items
         * @returns {Array} Panel items
         */
        getPanelItems() {
            let panelItems = [];

            this.getById(this.prefs().mainManu, (item) => {
                panelItems.push(...item.items);
            });

            for (let i = 0; i < this.prefs().panelItemsCount; i++) {
                this.getById(this.prefs().panelItemsPrefix + i, (item) => {
                    panelItems.push(item);
                });
            }

            return panelItems;
        }

        /**
         * @description Open Menu Panel
         * @param {object} data data
         */
        openPanel(data) {
            this.ref('html').addClass(this.prefs().classesHasDialog);
            this.ref('self').addClass(this.prefs().classesBackdropActive);
            this.ref('dialog').addClass(this.prefs().classesDialogOpen);
            this.definePanelItems();
            if (data.focusToLastItem) {
                this.setFocusToLastItem();
            } else {
                this.setFocusToFirstItem();
            }
            this.isOpened = true;
        }

        /**
         * @description Close Menu Panel
         */
        closePanel() {
            if (this.parentMenuItems.length) {
                this.parentMenuItems = [];
            }

            if (this.isOpened) {
                this.ref('html').removeClass(this.prefs().classesHasDialog);
                this.ref('self').removeClass(this.prefs().classesBackdropActive);
                this.ref('dialog').removeClass(this.prefs().classesDialogOpen);
                this.eventBus().emit('nav.panel.close');
                this.eventBus().emit('nav.panel.toggle.focus');
                this.handlePanel(1, false);
                this.onDestroy(timeout(() => {
                    for (let i = 1; i <= this.prefs().panelCount; i++) {
                        this.getById(this.prefs().panel + i, (panel) => {
                            panel.clearSubmenuHTML();
                        });
                    }
                }, this.prefs().itemSwitchTimeout));
                this.isOpened = false;
            }
        }

        /**
         * @description Close On Backdrop handler
         * @param {RefElement} ref - interacted element
         * @param {Event} event - event object
         */
        closeOnBackdrop(ref, event) {
            if (event.target === this.ref('self').get()) {
                this.closePanel();
            }
        }

        /**
         * @description Handle Enter key event
         * @param {Event} event Event Object
         */
        handleEnterKey(event) {
            const domNode = /** @type {HTMLAnchorElement} */(event.target);
            if (domNode && domNode.href) {
                window.location.assign(domNode.href);
            }
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.PAGEUP:
                case keyCode.HOME:
                    this.setFocusToFirstItem();
                    preventEventActions = true;

                    break;

                case keyCode.PAGEDOWN:
                case keyCode.END:
                    this.setFocusToLastItem();
                    preventEventActions = true;

                    break;

                case keyCode.TAB:
                    this.closePanel();

                    break;

                case keyCode.ESC:
                    this.closePanel();
                    preventEventActions = true;
                    break;

                case keyCode.RETURN:
                case keyCode.SPACE:
                    this.handleEnterKey(event);
                    preventEventActions = true;
                    break;

                case keyCode.DOWN:
                    this.setFocusToNextItem();
                    preventEventActions = true;

                    break;

                case keyCode.UP:
                    this.setFocusToPreviosItem();
                    preventEventActions = true;

                    break;

                case keyCode.RIGHT:
                    this.currentPanelItem.openMenu();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return MenuPanel;
}
