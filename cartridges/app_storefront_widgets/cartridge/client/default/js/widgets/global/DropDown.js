// TO BE REVIEWED
// TODO: should be imlemented according to https://www.w3.org/TR/wai-aria-practices/#menu specs
import { clickOutside } from 'widgets/toolbox/util';
const ariaExpanded = 'aria-expanded';
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base DropDown item implementation.
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof DropDown} DropDown class
 */

export default function (Widget) {
    /**
     * @class DropDown
     * @augments Widget
     * @classdesc Widget to serve different drop down blocks
     * @property {string} data-widget - Widget name `dropDown`
     * @property {string} data-event-mouseenter - Event listener for opening dropdown
     * @property {string} data-event-mouseleave - Event listener for closing dropdown
     * @property {string} data-event-focus - Event listener for opening dropdown
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="dropDown"
     *     data-event-mouseenter="openByMouseOver"
     *     data-event-mouseleave="close"
     *     data-event-focus="openByMouseOver"
     *     class="header-account" tabindex="0"
     *     aria-label="${Resource.msg('myaccount.link.title', 'accessibility', null)}"
     * >
     *     <button data-ref="dropDownButton" class="o-link--block header-account__user u-p2"
     *         data-event-click.prevent="toggle"
     *         aria-haspopup="true" aria-expanded="false" tabindex="-1"
     *     >${pdict.monogram}</button>
     *
     *     <div data-ref="dropDownBox" class="header-account-dropdown is-hidden" aria-hidden="true">
     *         <div class="header-account__title">
     *             <span>${Resource.msgf('label.header.salutation','account', null, pdict.name)}</span>
     *         </div>
     *         <iscontentasset aid="account-component-dropdown" />
     *     </div>
     * </div>
     */
    class DropDown extends Widget {
        prefs() {
            return {
                classesOpen: 'is--open',
                classesOptionSelected: 'selected',
                closeOnOutsideClick: true,
                outsideClickPreventDefault: true,
                ...super.prefs()
            };
        }

        init() {
            super.init();

            this.isOpened = false;
            // For ignore duplicate method calls
            this.toIgnoreDuplicateEvent = false;

            this.ref('dropDownBox').hide();
            this.ref('dropDownButton').attr(ariaExpanded, 'false');
        }

        onRefresh() {
            super.onRefresh();

            this.isOpened = false;
        }

        open() {
            if (this.isOpen()) {
                return false;
            }

            this.ref('self').addClass(this.prefs().classesOpen);
            this.ref('dropDownBox').show();
            this.ref('dropDownButton').attr(ariaExpanded, 'true');

            this.isOpened = true;

            if (this.prefs().closeOnOutsideClick) {
                if (this.outSideListener) {
                    this.outSideListener();
                } else {
                    this.outSideListener = clickOutside(this.ref('self'), () => {
                        this.close();
                    }, this.prefs().outsideClickPreventDefault);

                    this.onDestroy(this.outSideListener);
                }
            }

            this.emit('open');
            return true;
        }

        close() {
            if (this.outSideListener) {
                this.outSideListener();
                this.outSideListener = null;
            }

            if (!this.isOpen()) {
                return false;
            }

            this.ref('self').removeClass(this.prefs().classesOpen);
            this.ref('dropDownBox').hide();
            this.ref('dropDownButton').attr(ariaExpanded, 'false');

            this.isOpened = false;
            this.emit('close');
            return true;
        }

        toggle() {
            if (this.toIgnoreDuplicateEvent) {
                this.toIgnoreDuplicateEvent = false;
                return false;
            }

            if (this.isOpen()) {
                this.close();
            } else {
                this.open();
            }
            return true;
        }

        isOpen() {
            return this.isOpened;
        }

        openByMouseOver() {
            this.toIgnoreDuplicateEvent = true;
            this.open();
        }

        getOptions() {
            const options = [];

            Object.keys(this.refs).forEach(k => {
                if (k.indexOf('option-') !== -1) {
                    options.push(this.refs[k]);
                }
            });

            return options;
        }

        selectOption(optionInitial) {
            let option = optionInitial;
            if (typeof option === 'number') {
                if (!this.ref('option-' + option)) {
                    return false;
                }

                option = this.ref('option-' + option);
            }

            this.getOptions().forEach(o => {
                o.attr('data-selected', 'false')
                    .removeClass(this.prefs().classesOptionSelected);
            });

            option
                .attr('data-selected', 'true')
                .addClass(this.prefs().classesOptionSelected);

            this.ref('dropDownButton')
                .setText(option.attr('data-name'));

            this.close();
            return true;
        }

        handleClick() {
            this.emit('click');
        }
    }

    return DropDown;
}
