// TO BE REVIEWED
// CLARIFY: to be discussed not fit in widget approach very well
import { timeout } from 'widgets/toolbox/util';
import { debounce } from 'widgets/toolbox/debounce';

const KEY_TAB = 9;
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 */

/**
 * @description Focus highlighter
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof FocusHighlighter} Focus Highlighter class
 */
export default function (Widget) {
    /**
     * @class FocusHighlighter
     * @augments Widget
     * @classdesc This class intended to address Accessibility issue with focus highlighting on links element.
     * <br>Usually used on `<body>` tag to observe focus move events
     * <br>In case if page was focused,
     * next subsequent `TAB` press will leads to next available link/input element focus highlighting
     * @property {string} data-widget - Widget name `focusHighlighter`
     * @property {string} data-event-keyup - Event handler for `handleKeyup` method
     * @property {string} data-event-click - Event handler for `handleClick` method
     * @example
     * // use this code to display widget
     * <body
     *   class="b-page"
     *   data-widget="focusHighlighter"
     *   data-event-keyup="handleKeyup"
     *   data-event-click="handleClick"
     * >
     *     ... page content here
     * </body>
     */
    class FocusHighlighter extends Widget {
        prefs() {
            return {
                classesInited: 'b-highlighter_inited',
                classesEnabled: 'm-visible',
                classesHurry: 'm-hurry',
                DELAY_TIMEOUT: 200, // Delay for animation

                ...super.prefs()
            };
        }

        init() {
            this.keyboardModality = false;
            this.isHighlighterVisible = false;
            this.lastFocusedElement = null;
            this.highlighter = null;

            this.ref('self').addClass(this.prefs().classesInited);
            this.ev('resize', debounce(this.handleResize.bind(this), 200), window);
        }

        /**
         * @param {refElement} ref - key up event referenced element
         * @param {Event} event - key up event
         */
        handleKeyup(ref, event) {
            if (event.keyCode === KEY_TAB) {
                this.enableHighlighter();
            }
        }

        /**
         * @param {refElement} ref - click event referenced element
         * @param {Event} event - click event
         */
        handleClick(ref, event) {
            // Space keyup on button DomNode generates synthetic click
            // so we need to ensure that it is not the one
            if (this.isHighlighterVisible && !(event.x === 0 && event.y === 0)) {
                this.disableHighlighter();
            }
        }

        handleResize() {
            if (this.isHighlighterVisible) {
                this.moveTo(this.lastFocusedElement);
            }
        }

        handleFocus() {
            const focusedElement = document.activeElement;

            if (
                !this.isValidTarget(focusedElement)
                || (this.isTextInput(focusedElement) && !this.keyboardModality)
            ) {
                return;
            }

            this.detectHurryNavigation();
            this.moveTo(focusedElement);

            this.lastFocusedElement = focusedElement;
        }

        enableHighlighter() {
            if (this.keyboardModality) {
                return;
            }
            this.keyboardModality = true;

            this.ref('highlighter').addClass(this.prefs().classesEnabled);
            this.isHighlighterVisible = true;
            this.onDestroy(() => this.ref('highlighter').removeClass(this.prefs().classesEnabled));

            this.handleFocus();

            this.focusinHandlers = this.ev('focusin', this.handleFocus, document);
            // all other events are handled by focusin event
        }

        disableHighlighter() {
            if (!this.keyboardModality) {
                return;
            }
            this.keyboardModality = false;

            this.ref('highlighter').removeClass(this.prefs().classesEnabled);
            this.isHighlighterVisible = false;
            this.hideTimeOut = timeout(this.hide.bind(this), this.prefs().DELAY_TIMEOUT);

            this.focusinHandlers.forEach(fn => fn());
            this.lastFocusedElement = null;
        }

        /**
         * @param {HTMLElement} focusedElement - element, which gets focus
         */
        moveTo(focusedElement) {
            const highlighterStyle = this.ref('highlighter').get().style;
            highlighterStyle.top = `${focusedElement.getBoundingClientRect().top + window.scrollY - 1}px`;
            highlighterStyle.left = `${focusedElement.getBoundingClientRect().left + window.scrollX - 1}px`;
            highlighterStyle.width = `${focusedElement.offsetWidth + 1}px`;
            highlighterStyle.height = `${focusedElement.offsetHeight + 2}px`;
        }

        hide() {
            const highlighterStyle = this.ref('highlighter').get().style;
            highlighterStyle.width = '0';
            highlighterStyle.height = '0';

            this.hideTimeOut();
        }

        detectHurryNavigation() {
            const currentTime = Date.now();
            const isHurryNavigation = (currentTime - this.lastKeyTime) < 190;

            this.ref('highlighter').toggleClass(this.prefs().classesHurry, isHurryNavigation);
            this.isHurryNavigation = isHurryNavigation;

            this.lastKeyTime = currentTime;
        }

        /**
         * @param {HTMLElement} domNode - focused element
         */
        isValidTarget(domNode) {
            return !!(domNode
                && domNode !== this.lastFocusedElement
                && domNode !== document
                && domNode.nodeName !== 'HTML'
                && domNode.nodeName !== 'BODY'
            );
        }

        /**
         * @param {HTMLElement} domNode - focused element
         */
        isTextInput(domNode) {
            return !!((domNode.tagName === 'TEXTAREA' && !domNode.readOnly)
                || (domNode.tagName === 'INPUT' && !domNode.readOnly)
                || domNode.getAttribute('contenteditable'));
        }
    }

    return FocusHighlighter;
}
