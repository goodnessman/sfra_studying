// CLARIFY: not fit well into widget approach. Trigger and panel separate components
import { getContentByUrl, getJSONByUrl } from 'widgets/toolbox/ajax';
/**
 * @typedef {ReturnType <typeof import('widgets/global/Modal').default>} Modal
 */

/**
 * @description Base ModalLink implementation
 * @param {Modal} Modal Base widget for extending
 * @returns {typeof ModalLink} Modal Link class
 */
export default function (Modal) {
    /**
     * @class ModalLink
     * @augments Modal
     * @classdesc Modal popup, which should be exposed clicking on a related link.
     * <br>Content will be dynamically loaded based on input properties.
     * @property {string} data-widget - Widget name `modalLink`
     * @property {string} data-url - URL to load dynamic popup content
     * @property {string} data-content-type - should be specified
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="modalLink"
     *     data-url="${pdict.urls.quickView}"
     *     data-content-type="json"
     * >
     *     <button data-event-click="showModal" type="button">
     *         ${Resource.msg('tile.quickview', 'product', null)}
     *     </button>
     *     <div data-ref="container" hidden="hidden"></div>
     *         <script type="template/mustache" data-ref="template">
     *             <div>{{& renderedTemplate}}</div>
     *         </script>
     *     </div>
     * </div>
     */
    class ModalLink extends Modal {
        prefs() {
            return {
                url: '',
                contentType: 'html', // html | json
                ...super.prefs()
            };
        }

        showModal() {
            if (!this.prefs().url.length) {
                return false;
            }

            if (this.prefs().contentType === 'json') {
                getJSONByUrl(this.prefs().url, undefined, true).then(res => {
                    if (Object.keys(res).length) {
                        super.showModal(res);
                    }
                });
            } else {
                getContentByUrl(this.prefs().url).then(res => {
                    super.showModal({ content: res });
                });
            }

            this.emit('show');

            return true;
        }

        /**
         * @description Overrides parent method to not to hide content out of the container (exact modal link)
         */
        hide() {

        }
    }

    return ModalLink;
}
