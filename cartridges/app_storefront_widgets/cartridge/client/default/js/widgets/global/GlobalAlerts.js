import { PausableTimeout } from 'widgets/toolbox/util';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base GlobalAlerts implementation.
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof GlobalAlerts} GlobalAlerts class
 */
export default function (Widget) {
    /**
     * @class GlobalAlerts
     * @augments Widget
     * @classdesc Global Alerts with accessibility alert role possibility.
     * Acts as a popup with delayed disappearing. Popup will render user action description.
     * @property {boolean} data-widget - Widget name `globalAlerts`
     * @property {number} data-timeout - Value of hide timeout in milliseconds
     * @property {object} data-global-alerts - Global alerts localisation messages
     * Possible values are: `error500`
     * @example
     * // use this code to display GlobalAlerts widget
     * <div
     *     data-widget="globalAlerts"
     *     data-timeout="4000"
     *     data-global-alerts='{
     *         "error500": "${Resource.msg('alert.error500', 'global', null)}""
     *     }'
     *
     * >
     *     <div data-ref="item" hidden="hidden" role="alert">
     *         ... alert message will be shown here
     *     </div>
     * </div>
     */
    class GlobalAlerts extends Widget {
        prefs() {
            return {
                timeout: 4000,
                globalAlerts: {},
                ...super.prefs()
            };
        }

        /**
         * @description Widget intialisation, onDestroy hook to clear pausable timeout.
         * Listening `show.alert` event to display a relevant Global Alert
         */
        init() {
            super.init();
            this.onDestroy(() => this.disposePausableTimeout());
            this.eventBus().on('alert.show', 'showAlert');
            this.eventBus().on('alert.error', 'showError');
        }

        /**
         * @description Shows global errors like 500 response
         * @param {{errorCode: number}} data - Error data
         */
        showError(data) {
            if (data.errorCode) {
                const accessibilityAlert = this.prefs().globalAlerts[`error${data.errorCode}`];
                this.showAlert({
                    accessibilityAlert
                });
            }
        }

        /**
         * @description Dispose pausable timeout and clear it resources
         */
        disposePausableTimeout() {
            if (this.pausableTimeout) {
                this.pausableTimeout.destroy();
                this.pausableTimeout = undefined;
            }
        }

        /**
         * @description Show Global Alert by starting pausable timeout
         * @param {{accessibilityAlert: string}} data - Message to be shown and pronounced
         */
        showAlert(data) {
            if (data && data.accessibilityAlert) {
                if (this.shown) {
                    this.hideAlert();
                }

                this.ref('item')
                    .setText(data.accessibilityAlert)
                    .show();
                this.shown = true;

                this.pausableTimeout = new PausableTimeout(() => {
                    this.hideAlert();
                }, this.prefs().timeout);
            }
        }

        /**
         * @description Hide Global Alert and clear timeout
         */
        hideAlert() {
            this.ref('item')
                .setText('')
                .hide();
            this.shown = false;
            this.disposePausableTimeout();
        }

        /**
         * @description Pauses Global Alert displaying.
         * Pauses hide alert with pausable timeout.
         */
        pauseAlert() {
            if (this.pausableTimeout) {
                this.pausableTimeout.pause();
            }
        }

        /**
         * @description Resumes Global Alert displaying.
         * Resumes hide alert with pausable timeout.
         */
        resumeAlert() {
            if (this.pausableTimeout) {
                this.pausableTimeout.resume();
            }
        }
    }

    return GlobalAlerts;
}
