import { scrollToTop } from '../toolbox/scroll';
import { debounce } from './../toolbox/debounce';
import { isSmallView } from './../toolbox/viewtype';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base BackToTop implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof BackToTop} BackToTop class
 */
export default function (Widget) {
    /**
     * @class BackToTop
     * @augments Widget
     * @classdesc Back to top component. Allows scrolling window to top, pressing on a corresponding element
     * @property {string} data-widget - Widget name `backtotop`
     * @property {string} data-event-click - Event listener method `backToTop` for click event on widget
     * @example
     * // use this code to display widget
     * <div
     *     class=""
     *     data-widget="backtotop"
     *     data-event-click="backToTop"
     * >
     *     .... back to top element
     * </div>
     */
    class BackToTop extends Widget {
        prefs() {
            return {
                pageSize: 1.5,
                ...super.prefs()
            };
        }

        init() {
            this.doInit();
            this.eventBus().on('viewtype.change', 'doInit');
        }

        attachScrollListener() {
            this.scrollDisposable = this.ev('scroll', debounce(() => {
                this.toggleBackToTopButton();
            }, 50), window, true);
        }

        removeScrollListener() {
            if (this.scrollDisposable) {
                this.scrollDisposable.forEach(disposable => disposable());
                this.scrollDisposable = null;
            }
        }

        doInit() {
            if (isSmallView()) {
                this.removeScrollListener();
                this.show();
            } else {
                this.attachScrollListener();
                this.toggleBackToTopButton();
            }
        }

        toggleBackToTopButton() {
            const scrolled = document.body.scrollTop || document.documentElement.scrollTop;
            const windowHeight = window.innerHeight;

            this.toggle(scrolled > (windowHeight * this.prefs().pageSize));
        }

        backToTop() {
            scrollToTop();
        }
    }

    return BackToTop;
}
