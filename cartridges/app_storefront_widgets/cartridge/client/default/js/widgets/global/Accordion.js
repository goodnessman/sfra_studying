const keyCode = Object.freeze({
    END: 35,
    HOME: 36,
    UP: 38,
    DOWN: 40
});

/**
 * @typedef {ReturnType<typeof import('widgets/global/ListAccessibility').default>} ListAccessibility
 */

/**
 * @description Base Accordion implementation
 * @param {ListAccessibility} ListAccessibility Base widget for extending
 * @returns {typeof Accordion} Accordion widget
 */
export default function (ListAccessibility) {
    /**
     * @class Accordion
     * @augments ListAccessibility
     * @classdesc Represents Menu Item with specific logic for submenus, handling viewtype changes, keyboard navigation
     * @property {string} data-widget - Widget name `accordion`
     * @property {string} data-event-keydown - Event listener for `handleKeydown` method
     * @property {boolean} data-allow-toggle - Flag that allow or dissallow toggle
     * @property {boolean} data-open-first - Flag that open first item
     * @property {boolean} data-allow-multiple - Flag that allow or dissallow multiple open items
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="accordion"
     *     data-event-keydown="handleKeydown"
     *     data-allow-toggle="false"
     *     data-open-first="true"
     *     data-allow-multiple="false"
     *     class="b-footer_nav">
     *     <iscontentasset aid="footer-navigation" />
     * </div>
     */

    class Accordion extends ListAccessibility {
        prefs() {
            return {
                allowToggle: false,
                allowMultiple: false,
                openFirst: false,
                ...super.prefs()
            };
        }

        /**
         * @description Initialize widget logic
         * @returns {void}
         */
        init() {
            this.defineItems();
            this.eachChild((item) => {
                item.isToggleAllowed = this.prefs().allowToggle;
                item.isMultipleSections = this.prefs().allowMultiple;
            });

            if (this.prefs().openFirst) {
                this.firstItem.openPanel();
            }
        }

        /**
         * @description Close items
         * @returns {void}
         */
        closeItems() {
            this.eachChild((accordionItem) => {
                accordionItem.closePanel();
            });
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         * @returns {void}
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.HOME:
                    this.setFocusToFirstItem();
                    preventEventActions = true;

                    break;

                case keyCode.END:
                    this.setFocusToLastItem();
                    preventEventActions = true;

                    break;

                case keyCode.UP:
                    this.setFocusToPreviousItem();
                    preventEventActions = true;

                    break;

                case keyCode.DOWN:
                    this.setFocusToNextItem();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return Accordion;
}
