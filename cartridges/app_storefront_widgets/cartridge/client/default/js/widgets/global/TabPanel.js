// TODO: should be implemented according to https://www.w3.org/TR/wai-aria-practices/#tabpanel
// CLARIFY: functionality, select not/follow focus, fetch content on open
// CLARIFY: not fit well into widget approach. Trigger and panel separate components. Hard to understand and support.
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base TabPanel implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof TabPanel} Tab Panel class
 */
export default function (Widget) {
    /**
     * @class TabPanel
     * @augments Widget
     * @classdesc Represents a Tab in a Tabs widget
     * @property {string} data-widget - Widget name `tabPanel`
     * @example
     * // use this code to display minicart widget
     * <div data-widget="tabs">
     *     <ul class="nav nav-tabs nav-fill">
     *         <li class="nav-item">
     *             <a
     *                 data-widget="button"
     *                 data-panel-name="login"
     *                 data-widget-event-click="handleTabClick"
     *                 data-id="button-login"
     *                 data-event-click.prevent="handleClick"
     *                 href="#login" role="tab"
     *             >
     *                 ${Resource.msg('link.header.login.module', 'login', null)}
     *             </a>
     *         </li>
     *         <li class="nav-item">
     *             <a
     *                 data-widget="button"
     *                 data-panel-name="register"
     *                 data-widget-event-click="handleTabClick"
     *                 data-id="button-register"
     *                 data-event-click.prevent="handleClick"
     *                 href="#register" role="tab"
     *             >
     *                 ${Resource.msg('link.header.register.module', 'login', null)}
     *             </a>
     *         </li>
     *     </ul>
     *     <div class="tab-content">
     *         <div id="login" role="tabpanel" data-widget="tabPanel">
     *             ... tab content
     *             <isinclude template="account/components/loginForm" />
     *             <isinclude template="account/components/oauth" />
     *         </div>
     *         <div id="register" role="tabanel" data-widget="tabPanel">
     *             ... tab content
     *             <isinclude template="account/components/registerForm" />
     *         </div>
     *     </div>
     * </div>
     */
    class TabPanel extends Widget {
        prefs() {
            return {
                classesActive: 'm-active',
                ...super.prefs()
            };
        }

        onRefresh() {
            this.toggleActive(this.isActive);
        }

        activate() {
            this.toggleActive(true);
        }

        deactivate() {
            this.toggleActive(false);
        }

        /**
         * @param {(boolean|undefined)} state true to show/false to hide/undefined to auto
         */
        toggleActive(state) {
            this.isActive = this.ref('self')
                .toggleClass(this.prefs().classesActive, state)
                .hasClass(this.prefs().classesActive);
        }

        process() {
            return Promise.resolve({});
        }

        validate() {
            return this.isChildsValid();
        }

        isChildsValid() {
            return true;
        }
    }

    return TabPanel;
}
