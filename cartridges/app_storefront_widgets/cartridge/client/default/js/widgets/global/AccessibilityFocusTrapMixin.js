/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base AccessibilityFocusTrapMixin implementation.
 * Used to trap focus inside widget according to usability requirements
 * Can be either looped or not
 * <br>This class is not intended to have a separate DOM representation.
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof AccessibilityFocusTrapMixin} AccessibilityFocusTrapMixin class
 */
export default function (Widget) {
    /**
     * @class AccessibilityFocusTrapMixin
     * @augments Widget
     * @property data-ref-last-focus-element - Reference to last possible focusable element
     * @property data-ref-first-focus-element - Reference to first possible focusable element
     * @property data-focus-loop - Do we need to loop focus inside widget?
     * @example
     * // use this code to display minicart widget
     * <div
     *     data-widget="searchBox"
     *     data-url="${URLUtils.url('SearchServices-GetSuggestions')}"
     *     data-ref-last-focus-element="closeSearch"
     *     data-ref-first-focus-element="input"
     *     data-focus-loop="true"
     *     data-close-on-tab="false"
     * >
     *     <form>
     *         <isinclude template="components/modal/focusTrapFirst">
     *         ... focusable elements list, including refs `closeSearch` and `input`
     *         <isinclude template="components/modal/focusTrapLast">
     *     </form>
     *     ...
     * </div>
     */
    class AccessibilityFocusTrapMixin extends Widget {
        prefs() {
            return {
                refFirstFocusElement: 'firstFocusElement',
                refLastFocusElement: 'lastFocusElement',
                focusLoop: false,
                ...super.prefs()
            };
        }

        /**
         * @description Handle Focus Trap Last
         */
        handleFocusTrapLast() {
            if (this.prefs().focusLoop) {
                this.focusFirstElement();
                return;
            }

            let hasLastElement = this.focusLastElement();

            if (!hasLastElement) {
                this.focusFirstElement();
            }
        }

        /**
         * @description Handle Focus Trap First
         */
        handleFocusTrapFirst() {
            if (this.prefs().focusLoop) {
                this.focusLastElement();
                return;
            }

            this.focusFirstElement();
        }

        /**
         * @description Focus first element
         */
        focusFirstElement() {
            return this.has(this.prefs().refFirstFocusElement, (element) => {
                const domNode = element.get();
                if (domNode) domNode.focus();
            });
        }

        /**
         * @description Focus last element
         */
        focusLastElement() {
            return this.has(this.prefs().refLastFocusElement, (element) => {
                const domNode = element.get();
                if (domNode) domNode.focus();
            });
        }
    }

    return AccessibilityFocusTrapMixin;
}
