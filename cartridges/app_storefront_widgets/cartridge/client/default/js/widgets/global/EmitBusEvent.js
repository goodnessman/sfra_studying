// TO BE REVIEWED
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Emit event for eventBus implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof EmitBusEvent} Emit Bus Event class
 */
export default function (Widget) {
    /**
     * @class EmitBusEvent
     * @augments Widget
     * @classdesc Could be used to trigger any global event using interaction element
     * <br>Could be useful to emit global events like `page.show.globalmodal` to notify
     * <br>concerned widgets, so they could change page layout class etc.
     * <br>Event emission could happens mostly on UI elements like links, buttons etc.
     * @property {string} data-widget - Widget name `emitBusEvent`
     * @property {string} data-bus-event-type - Global event type, which should be triggered
     * @property {string} data-event-click - Event listener to trigger `emitBusEvent` method
     * @property {string} data-url - any kind of additional properties
     * @example
     * // use this code to display widget
     * <a
     *     data-widget="emitBusEvent"
     *     data-bus-event-type="page.show.globalmodal"
     *     data-event-click.prevent="emitBusEvent"
     *     data-url="${URLUtils.url('Login-ShowPasswordResetModal', 'loginmodal', 1, 'rurl', pdict.rurl)}"
     * >
     *     ${Resource.msg('link.login.forgotpassword', 'login', null)}
     * </a>
     */
    class EmitBusEvent extends Widget {
        prefs() {
            return {
                busEventType: '',
                ...super.prefs()
            };
        }

        emitBusEvent() {
            if (this.prefs().busEventType) {
                this.eventBus().emit(this.prefs().busEventType, this);
            }
        }
    }

    return EmitBusEvent;
}
