const keyCode = Object.freeze({
    END: 35,
    HOME: 36,
    LEFT: 37,
    RIGHT: 39
});

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {ReturnType<typeof import('widgets/global/Button').default>} Button
 * @typedef {InstanceType<Button>} button
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/TabPanel').default>>} tabPanel
 */

/**
 * @description Base Tabs implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof Tabs} Tabs class
 */
export default function (Widget) {
    /**
     * @class Tabs
     * @augments Widget
     * @classdesc Represents tabbed contents displaying and navigation
     * @property {string} data-widget - Widget name `tabs`
     * @property {boolean} data-active-first - activate first tab and first tab panel
     * @property {string} data-active-panel - activate tab and tab panel by provided panel id
     * @property {boolean} data-auto-activation - if tabs list should follow accessibility `Tabs with Automatic Activation` feature
     * @example
     * // use this code to display minicart widget
     * <div data-widget="tabs">
     *     <ul class="nav nav-tabs nav-fill">
     *         <li class="nav-item">
     *             <a
     *                 data-widget="button"
     *                 data-panel-name="login"
     *                 data-widget-event-click="handleTabClick"
     *                 data-id="button-login"
     *                 data-event-click.prevent="handleClick"
     *                 href="#login" role="tab"
     *             >
     *                 ${Resource.msg('link.header.login.module', 'login', null)}
     *             </a>
     *         </li>
     *         <li class="nav-item">
     *             <a
     *                 data-widget="button"
     *                 data-panel-name="register"
     *                 data-widget-event-click="handleTabClick"
     *                 data-id="button-register"
     *                 data-event-click.prevent="handleClick"
     *                 href="#register" role="tab"
     *             >
     *                 ${Resource.msg('link.header.register.module', 'login', null)}
     *             </a>
     *         </li>
     *     </ul>
     *     <div class="tab-content">
     *         <div id="login" role="tabpanel" data-widget="tabPanel">
     *             ... tab content
     *             <isinclude template="account/components/loginForm" />
     *             <isinclude template="account/components/oauth" />
     *         </div>
     *         <div id="register" role="tabanel" data-widget="tabPanel">
     *             ... tab content
     *             <isinclude template="account/components/registerForm" />
     *         </div>
     *     </div>
     * </div>
     */
    class Tabs extends Widget {
        prefs() {
            return {
                classesActive: 'm-active',
                activePanel: '',
                activeFirst: false,
                autoActivation: false,
                ...super.prefs()
            };
        }

        onRefresh() {
            super.onRefresh();
            this.fulFillPanelNames();
        }

        init() {
            super.init();
            this.fulFillPanelNames();

            if (this.prefs().activePanel) {
                this.activatePanel(this.prefs().activePanel);
            } else if (this.prefs().activeFirst && this.panelNames && this.panelNames.length) {
                this.activatePanel(this.panelNames[0]);
            }
        }

        fulFillPanelNames() {
            /**
            * @type {string[]}
            */
            this.panelNames = [];
            const Button = /** @type {Button} */(this.getConstructor('button'));

            this.eachChild(child => {
                if (child instanceof Button && this.panelNames) {
                    this.panelNames.push(String(child.config.panelName));
                }
            });
        }

        /**
         * @param {InstanceType<Widget>} clickedButton - Widget, representing customer's tab clicked element
         */
        handleTabClick(clickedButton) {
            this.activatePanel(String(clickedButton.config.panelName));
        }

        /**
         * @param {string} panelName name of panel to activate
         */
        activatePanel(panelName) {
            if (this.panelNames) {
                this.panelNames.forEach(id => {
                    const isActive = id === panelName;

                    if (isActive) {
                        this.activePanel = panelName;
                        this.focusedTab = panelName;
                    }

                    const currentTabPanel = this.getById(id, (/** @type {tabPanel} */tabPanel) => {
                        tabPanel[isActive ? 'activate' : 'deactivate']();

                        return tabPanel;
                    });

                    if (!currentTabPanel && this.ref(id)) {
                        this.toggleActiveRefPanel(this.ref(id), isActive);
                    }

                    this.getById(`button-${id}`, (/** @type {button} */ button) => isActive
                        ? button.activate()
                        : button.deactivate());
                });
            }
        }

        toggleActiveRefPanel(panel, state) {
            panel.toggleClass(this.prefs().classesActive, state);
        }

        /**
         * @description Gets last focused panel name
         * <br>(focused panel is not necessarily active panel)
         * @returns {string} Focused panel name if founded. Empty string otherwise
         */
        getLastFocusedTab() {
            return this.focusedTab
                ? this.focusedTab
                : this.activePanel || (this.panelNames && this.panelNames[0]) || '';
        }

        /**
         * @description Sets focus to panel with given name
         * <br>Uses `roving focus` accessibility feature
         * <br>https://www.w3.org/TR/wai-aria-practices/#kbd_roving_tabindex
         * @param {string} tab Tab name to set focus to
         */
        setFocusToTab(tab) {
            if (!tab) {
                return;
            }

            this.focusedTab = tab;

            if (this.panelNames) {
                this.panelNames.forEach(id => {
                    const isTargetTab = id === tab;
                    this.getById(`button-${id}`, (/** @type {button} */ button) => {
                        if (isTargetTab) {
                            button.focus().setTabindex();
                        } else {
                            button.unsetTabindex();
                        }
                    });
                });
            }

            if (this.prefs().autoActivation) {
                this.activatePanel(tab);
            }
        }

        /**
         * @description Sets focus to the very first panel
         */
        setFocusToFirstTab() {
            const firstTab = (this.panelNames
                && this.panelNames.length
                && this.panelNames[0])
                || '';
            this.setFocusToTab(firstTab);
        }

        /**
         * @description Sets focus to the very last panel
         */
        setFocusToLastTab() {
            const lastTab = (this.panelNames
                && this.panelNames.length
                && this.panelNames[this.panelNames.length - 1])
                || '';
            this.setFocusToTab(lastTab);
        }

        /**
         * @description Sets focus to previous panel. Loops focus, if first panel reached
         */
        setFocusToPreviousTab() {
            if (this.panelNames && this.panelNames.length) {
                const currentFocusedTab = this.getLastFocusedTab();
                const currentFocusedTabIndex = this.panelNames.indexOf(currentFocusedTab);
                const previousTab = currentFocusedTabIndex === 0
                    ? this.panelNames[this.panelNames.length - 1]
                    : this.panelNames[currentFocusedTabIndex - 1];
                this.setFocusToTab(previousTab);
            }
        }

        /**
         * @description Sets focus to next panel. Loops focus, if last panel reached
         */
        setFocusToNextTab() {
            if (this.panelNames && this.panelNames.length) {
                const currentFocusedTab = this.getLastFocusedTab();
                const currentFocusedTabIndex = this.panelNames.indexOf(currentFocusedTab);
                const nextTab = currentFocusedTabIndex === this.panelNames.length - 1
                    ? this.panelNames[0]
                    : this.panelNames[currentFocusedTabIndex + 1];
                this.setFocusToTab(nextTab);
            }
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.HOME:
                    this.setFocusToFirstTab();
                    preventEventActions = true;
                    break;

                case keyCode.END:
                    this.setFocusToLastTab();
                    preventEventActions = true;
                    break;

                case keyCode.LEFT:
                    this.setFocusToPreviousTab();
                    preventEventActions = true;
                    break;

                case keyCode.RIGHT:
                    this.setFocusToNextTab();
                    preventEventActions = true;
                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return Tabs;
}
