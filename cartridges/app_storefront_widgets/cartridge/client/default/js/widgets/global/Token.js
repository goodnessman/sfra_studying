// TO BE REVIEWED
import { setCurrentToken } from '../toolbox/ajax';
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base Token implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof Token} Token class
 */
export default function (Widget) {
    /**
     * @class Token
     * @augments Widget
     * @classdesc If available on page - adds CSRF data in context of ajax requests. Widget container is usually empty and hidden.
     * @property {string} data-widget - Widget name `token`
     * @property {string} data-token-name - Name of CSRF token
     * @property {string} data-token-value - Value of CSRF token
     * @example
     * // use this code to display minicart widget
     * <div
     *     data-widget="token"
     *     data-token-name="${require('dw/web/CSRFProtection').getTokenName()}"
     *     data-token-value="${require('dw/web/CSRFProtection').generateToken()}"
     *     hidden="hidden"
     * ></div>
     */
    class Token extends Widget {
        init() {
            setCurrentToken(this.config.tokenName, this.config.tokenValue);
        }
    }

    return Token;
}
