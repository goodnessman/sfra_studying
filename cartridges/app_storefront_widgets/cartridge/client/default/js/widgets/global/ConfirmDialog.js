/**
 * @typedef {ReturnType <typeof import('widgets/global/Modal').default>} Modal
 */

/**
 * @description Base ConfirmDialog implementation
 * @param {Modal} Modal Base Modal widget for extending
 * @returns {typeof ConfirmDialog} Confirmation Dialog class
 */
export default function (Modal) {
    /**
     * @class ConfirmDialog
     * @augments Modal
     * @classdesc Confirm dialog, which could be used to confirm an action in different contexts, like
     * - Remove coupon from the cart
     * - Remove product from the cart
     * Can have different markup, but serves 2 actions: `confirm` and `cancel`
     * @property {string} data-widget - Widget name `confirmDialog`
     * @property {string} data-widget-event-confirm - method to be triggered on parent's widget, once customer confirms dialog
     * @property {string} data-widget-event-alternativeConfirm - method to be triggered on parent's widget, once customer confirms dialog
     * @property {string} data-widget-event-cancel - method to be triggered on parent's widget, once customer cancels dialog
     * @example
     * // use this code to display widget
     * <div hidden="hidden" data-id="confirmDialog"
     *     data-widget="confirmDialog"
     *     data-widget-event-confirm="confirmedRemoveProduct"
     *     data-widget-event-alternativeConfirm="cancelledRemoveProduct"
     *     data-widget-event-cancel="confirmedRemoveProduct"
     * >
     *     ... delete product confirm dialog contents
     *     <button type="button" data-event-click.prevent="cancel" data-dismiss="modal">
     *         ${Resource.msg('button.cancel','cart',null)}
     *     </button>
     *     <button type="button" data-event-click.prevent="confirm" data-dismiss="modal">
     *         ${Resource.msg('button.confirm','cart',null)}
     *     </button>
     * </div>
     */
    class ConfirmDialog extends Modal {
        /**
         * @description Confirm Handler
         * @returns {void}
         */
        confirm() {
            this.closeModal();
            this.emit('confirm');
        }

        /**
         * @description alternativeConfirm Handler
         * @returns {void}
         */
        alternativeConfirm() {
            this.closeModal();
            this.emit('alternativeconfirm');
        }
    }

    return ConfirmDialog;
}
