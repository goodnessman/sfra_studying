import { getJSONByUrl } from '../toolbox/ajax';

const keyCode = Object.freeze({
    RETURN: 13,
    SPACE: 32
});

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} refElement
 */

/**
 * @description Country selector switcher
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof CountrySelector} Country Selector class
 */

export default function (Widget) {
    /**
     * @class CountrySelector
     * @augments Widget
     * @classdesc Base `CountrySelector` classdesc implementation
     * Can be used in context of different markup. I.e. different for sm/md and lg/xl viewports.
     * @property {string} data-widget - Widget name `countrySelector`
     * @property {string} data-url-locale - Set chosen localization info URL for current site locale
     * @property {string} data-url-site - Set chosen site info URL for target site locale
     * @example
     * // use this code to display minicart widget
     * <div
     *     data-widget="countrySelector"
     *     data-url-locale="${URLUtils.url('Page-SetLocale')}"
     *     data-url-site="${URLUtils.url('Page-SetSite')}"
     * >
     *     ...
     *     <isloop items="${pdict.localeModel.locale.localLinks}" var="localeLink">
     *         <button
     *              data-event-click="switchLocale"
     *              data-locale="${localeLink.localID}"
     *              data-currencyCode="${localeLink.currencyCode}"
     *         >${localeLink.language.toUpperCase()}</button>
     *     </isloop>
     * </div>
     */
    class CountrySelector extends Widget {
        prefs() {
            return {
                urlLocale: '',
                urlSite: '',
                ...super.prefs()
            };
        }

        /**
         * @param {refElement} button An element, which was chosen by customer and contains all info about selected localization
         * @param {Event} event Target event instance
         */
        switchLocale(button, event) {
            if (event && event instanceof KeyboardEvent) {
                if (![keyCode.RETURN, keyCode.SPACE].includes(event.keyCode)) {
                    return;
                }
            }

            const localeConfig = button.data('localeConfig');
            const queryString = this.ref('html').data('querystring');
            const data = {
                code: localeConfig.localeID,
                CurrencyCode: localeConfig.currencyCode,
                queryString: queryString || '',
                action: this.ref('html').data('action'),
                siteID: localeConfig.siteID
            };

            const url = localeConfig.isOtherSite ? this.prefs().urlSite : this.prefs().urlLocale;
            getJSONByUrl(url, data).then(
                response => {
                    if (response && response.redirectUrl) {
                        window.location.assign(response.redirectUrl);
                    }
                }
            );
        }
    }

    return CountrySelector;
}
