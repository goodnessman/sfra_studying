// CLARIFY: decomposition and functionality
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base Label implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof Label} Label class
 */
export default function (Widget) {
    /**
     * @class Label
     * @augments Widget
     * @classdesc Widget could be used in different context, where we need to display dynamic content
     * <br>depending on different global events. Could also be enabled/disabled.
     * <br>In addition: there is a possibility to manage text/value/error messages on referenced element
     * @property {string} data-widget - Widget name `label`
     * @property {string} data-listen-events - Events used to update template
     * Listen to global bus events, separated by comma and updates content based on event data
     * @property {string} data-widget-event-click - Triggers parent's method on widget's elements click
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="label"
     *     data-listen-events="checkout.step.updated"
     *     data-widget-event-click="submitCurrentStep"
     * >
     *     <script data-ref="template" type="template/mustache">
     *         <div
     *             data-widget="label"
     *             data-listen-events="checkout.step.updated"
     *             data-widget-event-click="submitCurrentStep"
     *         >
     *             {{${'#'}shipping}}
     *                 <button data-event-click="handleClick" type="submit" name="submit" value="submit-shipping">
     *                     ${Resource.msg('button.next.payment', 'checkout', null)}
     *                 </button>
     *             {{/shipping}}
     *             {{${'#'}billing}}
     *                 <button data-event-click="handleClick" type="submit" name="submit" value="submit-payment">
     *                     ${Resource.msg('button.next.place.order', 'checkout', null)}
     *                 </button>
     *             {{/billing}}
     *             {{${'#'}summary}}
     *                 <button data-event-click="handleClick" type="submit" name="submit" value="place-order">
     *                     ${Resource.msg('button.next.place.order', 'checkout', null)}
     *                 </button>
     *             {{/summary}}
     *         </div>
     *     </script>
     * </div>
     * @example
     * // use this code to display widget
     * <div data-widget="label" data-listen-events="order.updated">
     *     <script data-ref="template" type="template/mustache">
     *         <div data-widget="label" data-listen-events="order.update">
     *             <div class="col-12">
     *                 <span class="grand-total-label">
     *                     {{numItems}} ${Resource.msgf('label.number.items.in.cart','cart', null,'')}
     *                 </span>
     *                 <span class="grand-total-price">{{totals.subTotal}}</span>
     *             </div>
     *         </div>
     *     </script>
     * </div>
     */
    class Label extends Widget {
        prefs() {
            return {
                listenEvents: '',
                classesError: 'm-error',
                classesActive: 'active',
                target: 'self',
                ...super.prefs()
            };
        }

        init() {
            super.init();
            if (!this.id) {
                this.id = String(this.config.id);
            }
            this.shown = true;

            if (this.config.hidden || this.ref('self').hasAttr('hidden')) {
                this.hide();
            } else {
                this.show();
            }
            this.disabled = this.ref('self').attr('disabled') === 'disabled';

            const listenEvents = this.config.listenEvents;
            if (typeof listenEvents === 'string' && listenEvents) {
                listenEvents.split(',').forEach(eventName => {
                    this.eventBus().on(eventName.trim(), 'updateTemplate');
                });
            }
        }

        /**
         * @param {object} data data to render, usually executes with the data, streamed down with an event
         */
        updateTemplate(data = {}) {
            this.render(undefined, data);
        }

        getValue() {
            return this.ref(this.prefs().target).val();
        }

        /**
         * @param {(string|number|boolean|undefined)} val - Value to set into widget's target referenced attribute
         */
        setValue(val = '') {
            this.setError();
            this.ref(this.prefs().target).val(val);
        }

        getText() {
            return this.ref(this.prefs().target).getText();
        }

        /**
         * @param {(string|undefined)} val - Value to set as a text of the widget's target referenced attribute
         * @returns {InstanceType<Label>} result
         */
        setText(val = '') {
            const currentText = this.getText();
            this.ref(this.prefs().target).removeClass(this.prefs().classesError);
            if (currentText !== val) {
                this.ref(this.prefs().target).setText(val);
            }
            return this;
        }

        /**
         * @param {(string|undefined)} err - Sets `err` as a widget's target referenced attribute text
         * and if `err` is not empty - adds error class to widget's target referenced attribute
         * @returns {InstanceType<Label>} result
         */
        setError(err = '') {
            this.setText(err);
            if (err) {
                this.ref(this.prefs().target).addClass(this.prefs().classesError);
            }
            return this;
        }

        activate() {
            this.ref('self').addClass(this.prefs().classesActive);
        }

        deactivate() {
            this.ref('self').removeClass(this.prefs().classesActive);
        }

        disable() {
            this.disabled = true;
            this.ref('self').disable();
        }

        enable() {
            this.disabled = false;
            this.ref('self').enable();
        }

        handleClick() {
            this.emit('click');
        }
    }

    return Label;
}
