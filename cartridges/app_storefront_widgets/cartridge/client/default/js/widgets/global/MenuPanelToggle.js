const keyCode = Object.freeze({
    RETURN: 13,
    SPACE: 32,
    UP: 38,
    DOWN: 40
});
/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base MenuPanelToggle implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof MenuPanelToggle} Menu Panel Toggle class
 */
export default function (Widget) {
    /**
     * @class MenuPanelToggle
     * @augments Widget
     * @classdesc Widget, which triggers touch/mobile navigation menu panel to open/close
     * @property {string} data-widget - Widget name `menuPanelToggle`
     * @property {string} data-event-click - Event handler method for click event
     * @example
     * // use this code to display widget
     * <button id="main-navigation-toggle" type="button"
     *     data-widget="menuPanelToggle"
     *     data-event-click.prevent="showPanel"
     *     data-widget="menuPanelToggle"
     * >
     *     ... toggle nav menu
     * </button>
     */
    class MenuPanelToggle extends Widget {
        prefs() {
            return {
                classesOpenedNav: 'm-active',
                ...super.prefs()
            };
        }

        init() {
            this.eventBus().on('nav.panel.close', 'closePanel');
            this.eventBus().on('nav.panel.open', 'openPanel');
            this.eventBus().on('nav.panel.toggle.focus', 'focus');
        }

        focus() {
            this.ref('self').focus();
        }

        /**
         * @description Show panel
         * @param {boolean} [focusToLastItem] Focus to last item flag
         */
        showPanel(focusToLastItem) {
            this.openPanel();
            this.eventBus().emit('nav.panel.open', {
                focusToLastItem: focusToLastItem
            });
        }

        onClick() {
            this.showPanel();
        }

        openPanel() {
            this.ref('self')
                .addClass(this.prefs().classesOpenedNav)
                .attr('aria-expanded', 'true');
            this.isOpenDialog = true;
        }

        closePanel() {
            this.ref('self')
                .removeClass(this.prefs().classesOpenedNav)
                .attr('aria-expanded', 'false');

            if (this.isOpenDialog) {
                this.focus();
                this.isOpenDialog = false;
            }
        }

        /**
         * @description Keydown Event handler
         * @param {HTMLElement} _ Source of keydown event
         * @param {KeyboardEvent} event  Event object
         */
        handleKeydown(_, event) {
            let preventEventActions = false;

            switch (event.keyCode) {
                case keyCode.UP:
                    this.showPanel(true);
                    preventEventActions = true;

                    break;

                case keyCode.RETURN:
                case keyCode.SPACE:
                case keyCode.DOWN:
                    this.showPanel();
                    preventEventActions = true;

                    break;

                default:
                    break;
            }

            if (preventEventActions) {
                event.preventDefault();
                event.stopPropagation();
            }
        }
    }

    return MenuPanelToggle;
}
