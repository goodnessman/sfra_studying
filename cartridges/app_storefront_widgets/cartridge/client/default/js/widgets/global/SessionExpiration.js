import { submitFormJson } from 'widgets/toolbox/ajax';
import { timeout } from 'widgets/toolbox/util';
import { setCurrentToken } from '../toolbox/ajax';

/**
 * @typedef {ReturnType <typeof import('widgets/global/Modal').default>} Modal
 */

/**
 * @description Base SessionExpiration implementation
 * @param {Modal} Modal Base widget for extending
 * @returns {typeof SessionExpiration} SessionExpiration class
 */
export default function (Modal) {
    /**
     * @class SessionExpiration
     * @augments Modal
     * @classdesc Session expiration component. Used to notify customer via popup, that his/her session is about to expire.
     * At page loads starts a timeout. After timeout, popup with session expiration alert will be shown.
     * Popup also contains countdown before session will be expired.
     * Any customer interaction with popup will lead to backend ping and CSRF token re-newal, which prolongs customer session.
     * If popup was not closed - redirect to session expiration page happens.
     * @property {string} data-widget - Widget name `sessionExpiration`
     * @property {string} data-disable-rendering - Can disable modal dynamic content rendering.
     * Should be set to `true` in order countdown to work correctly!
     * @property {string} data-session-timeout-url - URL to redirect customer once session timeout
     * @property {string} data-session-continue-url - URL to get updated token and prolong customer's session
     * @property {number} data-initial-timeout - Initial timeout in minutes prior to expiration popup
     * @property {number} data-countdown-timeout - Countdown time in minutes up to session expires
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="sessionExpiration"
     *     data-disable-rendering="true"
     *     data-session-timeout-url="${URLUtils.url('CSRF-Fail')}"
     *     data-session-continue-url="${URLUtils.url('CSRF-Generate')}"
     *     data-initial-timeout="24"
     *     data-countdown-timeout="5"
     * >
     *     <div
     *         class="b-dialog"
     *         aria-label=""
     *         data-ref="container"
     *         hidden
     *     >
     *         <div
     *             class="b-dialog-window"
     *             role="dialog"
     *             data-ref="dialog"
     *             aria-modal="true"
     *         >
     *             <isinclude template="components/modal/focusTrapFirst">
     *             <div class="b-dialog-header">
     *                 <isinclude template="components/modal/closeButton">
     *             </div>
     *             <div class="b-dialog-body b-user_content">
     *                 <isprint value="${Resource.msg('sessionExpiration.abouttoexpire', 'global', null)}" />
     *                 <div data-ref="countdown"></div>
     *                 <button
     *                     type="button"
     *                     class="b-button"
     *                     data-event-click.prevent="continueSession"
     *                     data-dismiss="modal"
     *                     data-ref="lastFocusElement"
     *                 >
     *                     <isprint value="${Resource.msg('sessionExpiration.continuesession', 'global', null)}" />
     *                 </button>
     *             </div>
     *             <isinclude template="components/modal/focusTrapLast">
     *         </div>
     *     </div>
     * </div>
     */
    class SessionExpiration extends Modal {
        prefs() {
            return {
                initialTimeout: 24,
                countdownTimeout: 5,
                countdown: 'countdown',
                countdownCircleAnimated: 'countdownCircleAnimated',
                countdownIcon: 'countdownIcon',
                countdownCircleBg: 'countdownCircleBg',
                sessionTimeoutUrl: '',
                sessionContinueUrl: '',
                ...super.prefs()
            };
        }

        init() {
            super.init();
            this.onDestroy(() => this.clearDisposableResources());
            this.eventBus().on('response.ok', 'resetInitialTimeout');
            this.resetInitialTimeout();
        }

        /**
         * @description Resets initial timeout and starts new timeout. Usually is called once any ajax request finishes
         */
        resetInitialTimeout() {
            this.clearDisposableResources();
            this.initialTimeout = timeout(() => {
                this.startCountdown();
            }, this.prefs().initialTimeout * 60 * 1000);
        }

        /**
         * @description Shows "Session expires in..." modal and starts countdown.
         * Once countdown finishes, trigger session expiration logic
         */
        startCountdown() {
            this.showModal();
            this.ref('countdown').setText(`${this.prefs().countdownTimeout}:00`);
            const countdownCircle = this.ref('countdownCircle').get();
            const countdownCircleBg = this.ref('countdownCircleBg').get();
            const countdownCircleAnimated = this.ref('countdownCircleAnimated').get();
            if (countdownCircle && countdownCircleAnimated && countdownCircleBg) {
                const TAU = Math.PI * 2;
                const radius = 92;
                countdownCircle.setAttribute('style',
                    `--circle-length: ${String(TAU * radius)};
                    --animation-duration: ${String(this.prefs().countdownTimeout * 60)}s`);
                countdownCircleBg.setAttributeNS(null, 'r', String(radius));
                countdownCircleAnimated.setAttributeNS(null, 'r', String(radius));
            }

            import(/* webpackChunkName: 'simplecountdown' */'widgets/toolbox/SimpleCountdown').then((SimpleCountdown) => {
                // eslint-disable-next-line new-cap
                this.countdown = new SimpleCountdown.default(progress => {
                    this.ref('countdown').setText(`${progress.minutes}:${progress.seconds}`);
                }, () => this.sessionExpired(), {
                    time: this.prefs().countdownTimeout * 60
                });
            });
        }

        /**
         * @description Session expired logic. Redirects customer to given URL.
         */
        sessionExpired() {
            this.clearDisposableResources();
            window.location.assign(this.prefs().sessionTimeoutUrl);
        }

        /**
         * @description Continue session logic. Sends request to update csrf-token.
         */
        continueSession() {
            return submitFormJson(this.prefs().sessionContinueUrl, {}).then((response) => {
                setCurrentToken(response.csrf.tokenName, response.csrf.token);
                this.closeModal();
            });
        }

        /**
         * @description Method, which is called once customer closes modal
         * (i.e. press close button or clicked outside modal)
         */
        cancel() {
            this.continueSession().finally(() => {
                super.cancel();
            });
        }

        /**
         * @description Clear disposable resources
         */
        clearDisposableResources() {
            if (this.initialTimeout) {
                this.initialTimeout();
                this.initialTimeout = undefined;
            }
            if (this.countdown) {
                this.countdown.destroy();
                this.countdown = undefined;
            }
        }
    }

    return SessionExpiration;
}
