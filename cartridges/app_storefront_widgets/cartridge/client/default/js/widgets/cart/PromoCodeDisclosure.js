/**
 * @typedef {ReturnType<typeof import('widgets/global/Disclosure').default>} Disclosure
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/CouponForm').default>>} couponForm
 */

/**
 * @description Base PromoCodeDisclosure implementation
 * @param {Disclosure} Disclosure Base widget for extending
 * @returns {typeof PromoCodeDisclosure} BackToTop class
 */
export default function (Disclosure) {
    /**
     * @class PromoCodeDisclosure
     * @augments Disclosure
     * @classdesc PromoCodeDisclosure component. https://www.w3.org/TR/wai-aria-practices/#disclosure
     * @property {string} data-widget - Widget name `promoCodeDisclosure`
     * @property {string} data-show-on-init - If we need to show disclosure content on init
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="promoCodeDisclosure"
     *     data-show-on-init="true"
     * >
     *     <button
     *         type="button"
     *         data-event-click="toggleContent"
     *         aria-expanded="false"
     *         aria-controls="coupon-form"
     *         data-ref="disclosureButton"
     *     >Open me</button>
     *     <div
     *         data-ref="disclosureContent"
     *         id="coupon-form"
     *         hidden="hidden"
     *         class="m-hide"
     *     >
     *         .... content
     *     </div>
     * </div>
     */
    class PromoCodeDisclosure extends Disclosure {
        /**
         * @description Toggles disclosure content and update related attributes
         */
        toggleContent() {
            super.toggleContent();
            if (this.shown) {
                this.getById('couponForm', (/** @type {couponForm} */ couponForm) => couponForm.focusInput());
            }
        }
    }

    return PromoCodeDisclosure;
}
