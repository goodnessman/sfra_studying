import { range, timeout } from 'widgets/toolbox/util';
import { submitFormJson, getContentByUrl } from 'widgets/toolbox/ajax';
import { scrollWindowTo } from 'widgets/toolbox/scroll';

/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputSelect').default>>} inputSelect
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/ConfirmDialog').default>>} confirmDialog
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/Modal').default>>} modal
 * @typedef {{items: Array<object>, shipments: Array<object>, valid: object, error: string, redirectUrl: string, couponAction: string}} Cart
 */

/**
 * @description Base CartMgr implementation
 * @param {Widget} Widget Base widget for extending
 * @returns {typeof CartMgr} Cart Manager class
 */
export default function (Widget) {
    /**
     * @class CartMgr
     * @augments Widget
     * @classdesc CartMgr component on Cart page. Renders the whole cart template on each update e.g. quantity/coupon/edit etc.
     * @property {string} data-widget - Widget name `cartMgr`
     * @property {object} data-items - Cart items list
     * @property {object} data-approaching-discounts - Cart approaching discounts list
     * @property {object} data-action-urls - Cart action URLs
     * @property {object} data-totals - Cart totals
     * @property {object} data-shipments - Cart shipments list
     * @property {boolean} data-valid - Is Cart valid
     * @property {number} data-num-items - Number of items in Cart
     * @property {string} data-cart-get-url - An URL to get updated Cart object from server
     * @property {object} data-accessibility-alerts - Accessibility alerts messages for different user actions
     * Possible values are: `quantitychanged`, `productupdated`, `productremoved`,
     * `promocodeapplied`, `promocoderemoved`, `addedtowishlist`
     * @example
     * // use this code to display widget
     * <div
     *     data-widget="cartMgr"
     *     data-items="${JSON.stringify(pdict.items.toArray())}"
     *     data-approaching-discounts="${JSON.stringify(pdict.approachingDiscounts.toArray())}"
     *     data-action-urls="${JSON.stringify(pdict.actionUrls)}"
     *     data-cart-get-url="${URLUtils.url('Cart-Get')}"
     *     data-totals="${JSON.stringify(pdict.totals)}"
     *     data-shipments="${JSON.stringify(pdict.shipments.toArray())}"
     *     data-valid="${JSON.stringify(pdict.valid)}"
     *     data-num-items="${JSON.stringify(pdict.numItems)}"
     *     data-accessibility-alerts='{
     *         "quantitychanged": "${Resource.msg('alert.quantitychanged', 'cart', null)}",
     *         "productupdated": "${Resource.msg('alert.productupdated', 'cart', null)}",
     *         "productremoved": "${Resource.msg('alert.productremoved', 'cart', null)}",
     *         "promocodeapplied": "${Resource.msg('alert.promocodeapplied', 'cart', null)}",
     *         "promocoderemoved": "${Resource.msg('alert.promocoderemoved', 'cart', null)}",
     *         "addedtowishlist": "${Resource.msg('alert.addedtowishlist', 'cart', null)}"
     *     }'
     * >
     *     <div data-ref="cartContainer"></div>
     *     <script data-ref="cartTemplate" type="template/mustache">
     *         <div data-ref="cartContainer">
     *              ... cart global errors
     *              ... cart approaching discounts
     *              ... cart items
     *              ... cart summary
     *         </div>
     *     </script>
     * </div>
     */
    class CartMgr extends Widget {
        prefs() {
            return {
                cartGetUrl: '',
                accessibilityAlerts: {},
                ...super.prefs()
            };
        }

        /**
         * @description Widget initialisation
         */
        init() {
            this.cart = this.prefs();
            // render in next tick so will not block initialization
            this.onDestroy(timeout(() => {
                this.renderCart(this.cart);
            }, 0));
            // CLARIFY: minicart.updated more convenient when actual DOM in minicart updated
            this.eventBus().on('minicart.updated', 'renderCart');
            this.eventBus().on('product.updated', 'onProductUpdated');
            this.eventBus().on('coupon.updated', 'onCouponUpdated');

            this.eventBus().emit('cart.page.init', this);
        }

        /**
         * @description Executes on product updated in Quick View
         * @param {object} response Response object from server
         */
        onProductUpdated(response) {
            const cart = response.cartModel;
            if (cart) {
                this.renderCart(cart)
                    .then(() => this.accessibilityAlert(this.prefs().accessibilityAlerts.productupdated));
            }
        }

        /**
         * @description Executes on product updated in Quick View
         * @param {Cart} cart Cart object
         */
        onCouponUpdated(cart) {
            this.renderCart(cart)
                .then(() => {
                    const accessibilityAlert = this.prefs().accessibilityAlerts[`promocode${cart.couponAction}`];
                    this.accessibilityAlert(accessibilityAlert);
                });
        }

        /**
         * Renders Cart object and saves it as a current cart state
         * Adds some information into Cart object proior to rendering, to make the whole object mustache template friendly
         * @param {Cart} cart - Cart object to reflect in template
         */
        renderCart(cart) {
            cart.items = cart.items.map(item => {
                return {
                    ...item,
                    quantityOptionsList: range(
                        item.quantityOptions.minOrderQuantity,
                        item.quantityOptions.maxOrderQuantity + 1
                    ).map(qty => ({
                        index: qty,
                        selected: qty === item.quantity ? 'selected' : ''
                    }))
                };
            });

            cart.shipments = (cart.shipments || []).map(shipment => {
                shipment.shippingMethods = (shipment.shippingMethods || []).map(shippingMethod => ({
                    ...shippingMethod,
                    selected: shippingMethod.ID === shipment.selectedShippingMethod ? 'selected' : ''
                }));
                shipment.selectedShippingMethodObject = (shipment.shippingMethods || [])
                    .find(shippingMethod => shippingMethod.ID === shipment.selectedShippingMethod);
                return shipment;
            });

            this.cart = cart;

            this.eventBus().emit('cart.updated', cart);

            return this.render('cartTemplate', cart, this.ref('cartContainer'));
        }

        /**
         * @description Updates quantity of choosen product in Cart page
         * @param {inputSelect} inputSelect - Select quantity input
         */
        updateQty(inputSelect) {
            this.showProgressBar();
            const uuid = inputSelect.data('uuid');
            submitFormJson(inputSelect.data('action'), {
                pid: inputSelect.data('pid'),
                uuid: uuid,
                quantity: inputSelect.getValue()
            }, 'GET')
                .then((resp) => {
                    this.renderCartResponse(resp);
                    this.accessibilityAlert(this.prefs().accessibilityAlerts.quantitychanged);
                })
                .catch((error) => {
                    if (error && error.error && error.errorMessage) {
                        this.render('errorTemplate', { message: error.errorMessage }, this.ref('errorMsgs'));
                    } else {
                        this.renderCartWithItemLevelActionError(uuid, error.message);
                    }
                })
                .finally(() => {
                    this.hideProgressBar();
                });
        }

        /**
         * @description Fires `alert.show` event to be handled with Global Alerting System
         * @param {string} accessibilityAlert - alert string
         */
        accessibilityAlert(accessibilityAlert) {
            this.eventBus().emit('alert.show', {
                accessibilityAlert
            });
        }

        /**
         * @description Renders cart response from server.
         * Follows redirect URL in case if cart was not found on server
         * (request to update product/quantity etc)
         * @param {Cart} resp - server response
         */
        renderCartResponse(resp) {
            if (resp.error && resp.redirectUrl) {
                window.location.assign(resp.redirectUrl);
            } else {
                this.renderCart(resp);
            }
        }

        /**
         * @description Handler for "Edit" product button in a Cart
         * @param {RefElement} editBtn - Target "Edit" button
         */
        editProduct(editBtn) {
            this.showProgressBar();
            const uuid = editBtn.data('uuid');
            getContentByUrl(editBtn.data('href'), { uuid })
                .then((response) => {
                    this.getById('editProductModal', (/** @type {modal} */ editProductModal) => editProductModal
                        .showModal({ body: response }));
                })
                .catch(error => {
                    this.renderCartWithItemLevelActionError(uuid, error.message);
                })
                .finally(() => {
                    this.hideProgressBar();
                });
        }

        /**
         * @description Handler for "Remove" product button in a Cart
         * Stores clicked button instance to property because of "Remove Popup" displaying and further interaction
         * @param {RefElement} removeButton - Clicked "Remove" product button
         */
        removeProduct(removeButton) {
            this.removeButton = removeButton;
            this.getById('confirmDialog', (/** @type {confirmDialog} */ confirmDialog) => confirmDialog
                .showModal({ productName: removeButton.data('name') }));
        }

        /**
         * @description Renders Cart with error message on Cart Item level.
         * Usually it may happens when in parallel tab user removed product from cart.
         * As far as we should display error on product, which was removed, thus we don't need
         * to update this exact item in a stored earlier cart object
         * @param {string} itemUUID - Product item identifier
         * @param {string} error - an error response from the server
         */
        renderCartWithItemLevelActionError(itemUUID, error) {
            this.cart.items = this.cart.items.map(item => {
                const existingOldItem = this.cart.items.find((cartItem) => cartItem.UUID === item.UUID);
                const returnItem = !existingOldItem ? item : existingOldItem;
                return {
                    ...returnItem,
                    actionErrorMessage: (itemUUID === item.UUID) ? error : ''
                };
            });
            this.renderCart(this.cart);
        }

        /**
         * @description A callback, which is executed, when customer agreed to remove product in confirmation popup
         * @param {RefElement} button - Button, pressed in confirmation modal
         * @param {boolean} [movedToWishlist] - Indicates if product was moved to wishlist
         */
        confirmedRemoveProduct(button, movedToWishlist = false) {
            if (this.removeButton) {
                const removeButtonUUID = this.removeButton.data('uuid');
                this.eventBus().emit('cart.remove.product', this);
                this.showProgressBar();
                submitFormJson(this.removeButton.data('removeAction') || this.removeButton.data('action'), {
                    pid: this.removeButton.data('pid'),
                    uuid: removeButtonUUID
                }, 'GET')
                    .then((response) => {
                        this.confirmationRemoveCallback(response.basket, movedToWishlist);
                    })
                    .catch(error => {
                        this.renderCartWithItemLevelActionError(removeButtonUUID, error.message);
                    })
                    .finally(() => {
                        this.hideProgressBar();
                    });
                this.removeButton = undefined;
            }
        }

        /**
         * @description Move product from cart to wishlist.
         * In fact this functionality implemented in 2 steps:
         * add product to wishlist -> remove product from cart
         * This is done due to possible backend code duplication in one universal endpoint
         */
        moveProductToWishlist() {
            if (this.removeButton) {
                const removeButtonUUID = this.removeButton.data('uuid');
                this.showProgressBar();
                submitFormJson(this.prefs().actionUrls.moveProductToWishlist, {
                    pid: this.removeButton.data('pid')
                }).then(() => {
                    this.confirmedRemoveProduct(undefined, true);
                }).catch(error => {
                    this.renderCartWithItemLevelActionError(removeButtonUUID, error.message);
                }).finally(() => {
                    this.hideProgressBar();
                });
            }
        }

        /**
         * @description A callback, which is executed once `remove` product server call was performed
         * @param {Cart} cart - server response object
         * @param {boolean} [movedToWishlist] - Indicates if product was moved to wishlist
         */
        confirmationRemoveCallback(cart, movedToWishlist = false) {
            this.renderCart(cart)
                .then(() => this.accessibilityAlert(
                    movedToWishlist
                        ? this.prefs().accessibilityAlerts.movedtowishlist
                        : this.prefs().accessibilityAlerts.productremoved
                ));
        }

        /**
         * @description Shows progress bar
         */
        showProgressBar() {
            this.ref('cartContainer').attr('aria-busy', 'true');
        }

        /**
         * @description Hides progress bar
         */
        hideProgressBar() {
            this.ref('cartContainer').attr('aria-busy', 'false');
        }

        /**
         * @description Handles `proceed to checkout` button click.
         * <br>In case of error - to not allow Customer to enter checkout flow and scroll to cart error message
         * @param {RefElement} btn - clicked button
         * @param {Event} event - target event
         */
        proceedToCheckout(btn, event) {
            if (btn.data('error')) {
                event.preventDefault();
                this.scrollToError();
            }
        }

        /**
         * @description Scrolls cart error messages into viewport
         */
        scrollToError() {
            const cartErrorMessage = this.ref('errorMsgs').get();
            if (cartErrorMessage) {
                scrollWindowTo(cartErrorMessage);
            }
        }
    }

    return CartMgr;
}
