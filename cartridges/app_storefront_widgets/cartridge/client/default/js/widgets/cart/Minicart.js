/**
 * @typedef {typeof import('widgets/Widget').default} Widget
 */

/**
 * @description Base Minicart implementation
 * @param {Widget} Widget Modal widget for extending
 * @returns {typeof Minicart} Minicart class
 */
export default function (Widget) {
    /**
     * @class Minicart
     * @augments Widget
     * @classdesc Minicart widget, that contains all needed logic to display/update minicart component.
     * @property {string} data-widget - Widget name `minicart`
     * URL to be used for updating minicart widget (for ex. in case of removing products from minicart)
     * @property {string} data-ref Known reference to be available in widget as a `RefElement` object.
     * Known elements for Minicart are: `minicartContent`, `template`
     * @property {object} data-accessibility-alerts - Accessibility alerts per minicart action
     * Possible values are: `quantitychanged`, `productremoved`
     * @example
     * // use this code to display widget
     * <div
     *     class="b-minicart_popup"
     *     data-widget="minicart"
     *     data-action-url="${URLUtils.url('Cart-MiniCartShow')}"
     * >
     *     <div
     *         data-ref="minicartContent"
     *         data-count="${pdict.quantityTotal}"
     *     >
     *         .... minicart content
     *     </div>
     * </div>
     */
    class Minicart extends Widget {
        /**
         * @description Returns Widget configuration object
         * @returns {object} Widget configuration object
         */
        prefs() {
            return {
                refMinicartContent: 'minicartContent',
                accessibilityAlerts: {},
                ...super.prefs()
            };
        }

        /**
         * @description Widget initialization logic
         * @returns {void}
         */
        init() {
            this.eventBus().on('product.added.to.cart', 'reRenderMinicartWrapper');
            this.eventBus().on('cart.updated', 'reRenderMinicartWrapper');
            this.eventBus().on('product.updated', 'reRenderMinicartWrapper');
            this.eventBus().on('minicart.updated', 'reRenderMinicartWrapper');
        }

        normalizeCartModel(response) {
            return response.cartModel ? response.cartModel : response;
        }

        /**
         * @description Called when needs to re-render minicart (for ex. when cart / product in a cart was updated)
         * @param {object} response response from server
         */
        reRenderMinicartWrapper(response) {
            const cart = this.normalizeCartModel(response);
            // Return if response have no Cart Model
            if (!cart.totals) {
                return;
            }
            const isShownQtyTotal = parseInt(cart.numItems, 10);


            this.render('template', {
                quantityTotal: cart.numItems,
                isShownQtyTotal: isShownQtyTotal > 0,
                isOpen: cart.isOpen
            }, this.ref(
                this.prefs().refMinicartContent
            )).then(() => {
                if (cart.showMinicart) {
                    this.showMinicart();
                }
            });
        }

        /**
         * @description A method, called when event `click` happens on minicart icon
         *     Check cart total if empty redirect to cart page
         *     Shows minicart popover
         * @returns {void}
         */
        showMinicart() {
            const cartQtyTotal = parseInt(this.ref(this.prefs().refMinicartContent).data('count'), 10);

            if (cartQtyTotal === 0) {
                window.location.assign(this.prefs().cartUrl);
                return;
            }

            this.eventBus().emit('minicart.show');
        }
    }

    return Minicart;
}
