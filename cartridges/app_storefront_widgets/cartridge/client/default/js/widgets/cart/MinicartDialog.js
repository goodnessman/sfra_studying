import { getContentByUrl, submitFormJson } from 'widgets/toolbox/ajax';

/**
 * @typedef @typedef {ReturnType <typeof import('widgets/global/Modal').default>} Modal
 * @typedef {InstanceType<ReturnType<typeof import('widgets/global/ConfirmDialog').default>>} confirmDialog
 * @typedef {InstanceType<typeof import('widgets/toolbox/RefElement').RefElement>} RefElement
 * @typedef {InstanceType<ReturnType<typeof import('widgets/forms/InputSelect').default>>} inputSelect
 */

/**
 * @description Base MinicartDialog implementation
 * @param {Modal} Modal Modal widget for extending
 * @returns {typeof MinicartDialog} MinicartDialog class
 */
export default function (Modal) {
    /**
     * @class MinicartDialog
     * @augments Modal
     * @classdesc MinicartDialog widget, that contains all needed logic to display/update minicart component.
     * @property {string} data-widget - Widget name `minicartDialog`
     * @property {string} data-action-url
     * URL to be used for updating minicart widget (for ex. in case of removing products from minicart)
     * @property {object} data-accessibility-alerts - Accessibility alerts per minicart action
     * Possible values are: `quantitychanged`, `productremoved`
     */
    class MinicartDialog extends Modal {
        /**
         * @description Returns Widget configuration object
         * @returns {object} Widget configuration object
         */
        prefs() {
            return {
                actionUrl: '',
                accessibilityAlerts: {},
                ...super.prefs()
            };
        }

        /**
         * @description Widget initialization logic
         * @returns {void}
         */
        init() {
            this.needUpdate = true;
            this.eventBus().on('minicart.show', 'showMinicart');
            this.eventBus().on('product.added.to.cart', 'setUpdateFlag');
            this.eventBus().on('cart.updated', 'setUpdateFlag');
            this.eventBus().on('product.updated', 'setUpdateFlag');
        }

        /**
         * @description Set Update Flag
         * @param {object} response Response object from server
         */
        setUpdateFlag(response) {
            if (response.cartModel) {
                this.needUpdate = true;
            }
        }

        /**
         * @description A method, called when event `click` happens on minicart icon
         *     Check cart total if empty redirect to cart page
         *     Shows minicart popover
         * @returns {void}
         */
        showMinicart() {
            this.ref(this.prefs().refContainer).attr('aria-busy', true);

            if (this.needUpdate) {
                getContentByUrl(this.prefs().actionUrl).then((response) => {
                    this.needUpdate = false;

                    this.render(
                        undefined,
                        {},
                        this.ref(this.prefs().refDialog),
                        response
                    ).then(() => {
                        this.showModal();
                    });
                }).finally(() => {
                    this.ref(this.prefs().refContainer).attr('aria-busy', false);
                });
            } else {
                this.showModal();
            }
        }

        /**
         * @description Updates quantity for concrete product in minicart widget
         * @param {inputSelect} inputSelect - Target select input in minicart widget, which triggers quantity update
         */
        updateQty(inputSelect) {
            this.showSpinner();
            submitFormJson(inputSelect.ref('self').data('action'), {
                pid: inputSelect.ref('self').data('pid'),
                uuid: inputSelect.ref('self').data('uuid'),
                quantity: inputSelect.getValue()
            }, 'GET')
                .then((response) => {
                    this.needUpdate = true;
                    this.eventBus().emit('minicart.updated', response);
                    this.accessibilityAlert(this.prefs().accessibilityAlerts.quantitychanged);
                    this.showMinicart();
                })
                .finally(() => {
                    this.hideSpinner();
                });
        }

        /**
         * @description Called when Customer confirms product deletion in a modal
         * <br>In fact sends request to server to delete product line item from basket
         * <br>and updates minicart widget based on server response
         * @param {RefElement|null} button - confirmation popup button, triggered action
         * @param {boolean} [movedToWishlist] - Optional, if we need to move item to wishlist
         */
        confirmedRemoveProduct(button, movedToWishlist = false) {
            if (this.removeProductLink) {
                this.showSpinner();
                this.eventBus().emit('minicart.remove.product', this);
                submitFormJson(this.removeProductLink.data('removeAction') || this.removeProductLink.data('action'), {
                    pid: this.removeProductLink.data('pid'),
                    uuid: this.removeProductLink.data('uuid')
                }, 'GET')
                    .then((response) => {
                        this.needUpdate = true;
                        this.eventBus().emit('minicart.updated', response.basket);
                        this.accessibilityAlert(
                            movedToWishlist
                                ? this.prefs().accessibilityAlerts.movedtowishlist
                                : this.prefs().accessibilityAlerts.productremoved
                        );
                        this.showMinicart();
                    })
                    .finally(() => {
                        this.hideSpinner();
                    });
                this.removeProductLink = null;
            }
        }

        /**
         * @description Shows `remove product` dialog for Customer
         * @param {*} link link which is clicked by user
         */
        removeProduct(link) {
            this.removeProductLink = link;
            this.getById('confirmDialog', (/** @type {confirmDialog} */ dialog) => dialog
                .showModal({ productName: link.data('name') }));
        }

        /**
         * @description Shows spinner bar in widget once any update operations (server calls) are pending
         */
        showSpinner() {
            this.ref(this.prefs().refDialog)
                .addClass(this.prefs().classesLoading);
        }

        /**
         * @description Hides spinner in widget once any update operations (server calls) were finished
         */
        hideSpinner() {
            this.ref(this.prefs().refDialog)
                .removeClass(this.prefs().classesLoading);
        }

        /**
         * @description Fires `alert.show` event to be handled with Global Alerting System
         * @param {string} accessibilityAlert - alert string
         */
        accessibilityAlert(accessibilityAlert) {
            this.eventBus().emit('alert.show', {
                accessibilityAlert
            });
        }

        /**
         * @description Move product from minicart to wishlist.
         * In fact this functionality implemented in 2 steps:
         * add product to wishlist -> remove product from minicart
         * This is done due to possible backend code duplication in one universal endpoint
         */
        moveProductToWishlist() {
            if (this.removeProductLink) {
                submitFormJson(this.prefs().actionMovetowishlist, {
                    pid: this.removeProductLink.data('pid')
                }).then(() => {
                    this.confirmedRemoveProduct(null, true);
                });
            }
        }
    }

    return MinicartDialog;
}
