var currentYear = new Date().getFullYear();
var creditCardExpirationYears = [];

for (var i = 0; i <= 10; i++) {
    var targetYear = String(currentYear + i);
    creditCardExpirationYears.push({
        value: targetYear,
        label: targetYear
    });
}

module.exports = creditCardExpirationYears;
