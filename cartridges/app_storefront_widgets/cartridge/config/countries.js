/**
 * @description normalizes countries as array to be used by native SFRA code
 * @returns {array} normalized countries array in format, shown below
 * `[{
 *      "id": "en_GB",
 *       "currencyCode": "GBP",
 *       "alternativeCurrencyCodes": ["EUR"]
 *   },
 *   {
 *      "id": "en_CA",
 *      "currencyCode": "CAD"
 *   }
 *   ...
 * ]`
 */
function getCountries() {
    var localesConfig = require('util/sitepref').getJSONValue('LocalesConfig');
    var countriesOutput = [];
    if (localesConfig) {
        localesConfig.forEach(function (locale) {
            var alreadyExistingCountry = countriesOutput.filter(function (country) {
                return country.id === locale.locale;
            });
            if (alreadyExistingCountry && alreadyExistingCountry.length) {
                var alternativeCurrencyCodes = alreadyExistingCountry[0].alternativeCurrencyCodes || [];
                alternativeCurrencyCodes.push(locale.currencyCode);
                alreadyExistingCountry[0].alternativeCurrencyCodes = alternativeCurrencyCodes;
            } else {
                countriesOutput.push({
                    id: locale.locale,
                    currencyCode: locale.currencyCode
                });
            }
        });
    }
    return countriesOutput;
}

var CacheMgr = require('dw/system/CacheMgr');
var cache = CacheMgr.getCache('configData');
var countries = cache.get('countries', getCountries);

module.exports = countries;
