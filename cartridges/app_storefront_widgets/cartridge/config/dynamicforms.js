module.exports = {
    forms: {
        profile: {
            customer: {
                firstname: {
                    extends: 'fields.generic.firstName'
                },
                lastname: {
                    extends: 'fields.generic.lastName'
                },
                phone: {
                    extends: 'fields.generic.phone'
                },
                email: {
                    extends: 'fields.generic.email'
                },
                emailconfirm: {
                    extends: 'fields.generic.email',
                    'label.text': 'form.profile.emailConfirm',
                    validation: {
                        compareWith: 'profile.customer.email',
                        'errors.compareWith': 'validation.errors.comparewith.emailconfirm'
                    }
                },
                agreeToPrivacy: {
                    extends: 'fields.generic.agreeToPrivacy'
                }
            },
            login: {
                password: {
                    extends: 'fields.generic.password',
                    element: {
                        attributes: {
                            autocomplete: 'new-password'
                        }
                    }
                },
                passwordconfirm: {
                    extends: 'fields.generic.password',
                    element: {
                        attributes: {
                            autocomplete: 'new-password'
                        }
                    },
                    validation: {
                        compareWith: 'profile.login.password',
                        'errors.compareWith': 'error.message.mismatch.password'
                    }
                },
                currentpassword: {
                    extends: 'fields.generic.password',
                    'caption.text': 'form.profile.currentpassword.caption',
                    'label.text': 'form.profile.currentpassword',
                    element: {
                        attributes: {
                            autocomplete: 'current-password'
                        }
                    },
                    validation: {
                        'errors.passwordFormatError': 'validation.errors.parse'
                    }
                },
                newpasswords: {
                    newpassword: {
                        extends: 'fields.generic.password',
                        element: {
                            attributes: {
                                autocomplete: 'new-password'
                            }
                        }
                    },
                    newpasswordconfirm: {
                        extends: 'fields.generic.password',
                        'label.text': 'form.profile.passwordconfirm',
                        element: {
                            attributes: {
                                autocomplete: 'new-password'
                            }
                        },
                        validation: {
                            compareWith: 'profile.login.newpasswords.newpassword',
                            'errors.compareWith': 'error.message.mismatch.password'
                        }
                    }
                }
            },
            resetPassword: {
                email: {
                    extends: 'fields.generic.email'
                }
            }
        },
        address: {
            firstName: {
                extends: 'fields.generic.firstName'
            },
            lastName: {
                extends: 'fields.generic.lastName'
            },
            address1: {
                extends: 'fields.generic.address1'
            },
            address2: {
                extends: 'fields.generic.address2'
            },
            country: {
                extends: 'fields.generic.country'
            },
            quickFinder: {
                extends: 'fields.generic.quickFinder'
            },
            setAsDefault: {
                extends: 'fields.generic.setAsDefault'
            },
            'states.stateCode': {
                extends: 'fields.generic.state'
            },
            city: {
                extends: 'fields.generic.city',
                widget: {
                    classes: 'm-small'
                }
            },
            postalCode: {
                extends: 'fields.generic.postalCode'
            },
            phone: {
                extends: 'fields.generic.phone'
            },
            saveAddress: {
                extends: 'fields.generic.saveAddress'
            },
            useShipping: {
                extends: 'fields.generic.useShipping'
            }
        },
        shipping: {
            shippingAddress: {
                addressFields: {
                    firstName: {
                        extends: 'fields.generic.firstName'
                    },
                    lastName: {
                        extends: 'fields.generic.lastName'
                    },
                    address1: {
                        extends: 'fields.generic.address1'
                    },
                    address2: {
                        extends: 'fields.generic.address2'
                    },
                    country: {
                        extends: 'fields.generic.country'
                    },
                    quickFinder: {
                        extends: 'fields.generic.quickFinder'
                    },
                    'states.stateCode': {
                        extends: 'fields.generic.state',
                        widget: {
                            attributes: {
                                'data-widget-event-change': 'updateShippingState'
                            }
                        },
                        element: {
                            attributes: {
                                'data-event-change': 'onChange'
                            }
                        }
                    },
                    city: {
                        extends: 'fields.generic.city',
                        widget: {
                            classes: 'm-small'
                        }
                    },
                    postalCode: {
                        extends: 'fields.generic.postalCode'
                    },
                    phone: {
                        extends: 'fields.generic.phone'
                    },
                    saveAddress: {
                        extends: 'fields.generic.saveAddress'
                    }
                },
                isGift: {
                    widget: {
                        attributes: {
                            'data-widget-event-change': 'toggleGiftWrap'
                        }
                    },
                    extends: 'fields.input.checkbox',
                    'label.class': 'b-checkbox-label',
                    'label.text': 'form.gift.check',
                    element: {
                        attributes: {
                            'data-event-change': 'handleChange'
                        }
                    }
                },
                giftMessage: {
                    widget: {
                        attributes: {
                            'data-id': 'giftMessage',
                            hidden: 'hidden'
                        },
                        classes: 'gift-message'
                    },
                    extends: 'fields.input.textarea',
                    'label.text': 'form.gift.message',
                    element: {
                        maxcounter: true,
                        attributes: {
                            'data-event-input': 'onInput'
                        },
                        placeholder: 'form.gift.placeholder'
                    }
                }
            }
        },
        billing: {
            addressFields: {
                firstName: {
                    extends: 'fields.generic.firstName'
                },
                lastName: {
                    extends: 'fields.generic.lastName'
                },
                address1: {
                    extends: 'fields.generic.address1'
                },
                address2: {
                    extends: 'fields.generic.address2'
                },
                country: {
                    extends: 'fields.generic.country'
                },
                quickFinder: {
                    extends: 'fields.generic.quickFinder'
                },
                'states.stateCode': {
                    extends: 'fields.generic.state'
                },
                city: {
                    extends: 'fields.generic.city',
                    widget: {
                        classes: 'm-small'
                    }
                },
                postalCode: {
                    extends: 'fields.generic.postalCode'
                },
                useShipping: {
                    extends: 'fields.generic.useShipping'
                }
            },
            contactInfoFields: {
                email: {
                    extends: 'fields.generic.email',
                    'caption.show': true,
                    'caption.text': 'form.contact.email.caption'
                },
                phone: {
                    extends: 'fields.generic.phone'
                }
            },
            creditCardFields: {
                cardType: {
                    extends: 'fields.generic.cardType'
                },
                cardOwner: {
                    extends: 'fields.generic.cardOwner',
                    widget: {
                        attributes: {
                            'data-widget-event-inputstatechanged': 'updateState'
                        }
                    }
                },
                cardNumber: {
                    extends: 'fields.generic.cardNumber',
                    widget: {
                        attributes: {
                            'data-widget-event-inputstatechanged': 'updateState'
                        }
                    }
                },
                expirationMonth: {
                    extends: 'fields.generic.expirationMonth',
                    widget: {
                        attributes: {
                            'data-widget-event-inputstatechanged': 'updateState'
                        }
                    }
                },
                expirationYear: {
                    extends: 'fields.generic.expirationYear',
                    widget: {
                        attributes: {
                            'data-widget-event-inputstatechanged': 'updateState'
                        }
                    }
                },
                securityCode: {
                    extends: 'fields.generic.securityCode',
                    widget: {
                        attributes: {
                            'data-widget-event-inputstatechanged': 'updateState'
                        }
                    }
                },
                savedCardSecurityCode: {
                    extends: 'fields.generic.securityCode',
                    widget: {
                        attributes: {
                            'data-widget-event-inputstatechanged': 'updateState'
                        }
                    },
                    element: {
                        attributes: {
                            name: 'securityCode'
                        }
                    }
                },
                saveCard: {
                    extends: 'fields.input.checkbox',
                    'label.text': 'label.save.card'
                }
            }
        },
        emailsubscribe: {
            email: {
                extends: 'fields.generic.email',
                widget: {
                    attributes: {
                        'data-widget-event-input': 'onEmailInput'
                    }
                },
                'label.show': false,
                'label.text': 'form.emailsubscribe.email',
                element: {
                    maxLength: 50,
                    required: true,
                    classes: 'b-newsletters-input',
                    placeholder: 'form.emailsubscribe.email.placeholder',
                    skipValidationOnBlur: true,
                    attributes: {
                        'data-event-input': 'onInput'
                    }
                }
            },
            agreeToPrivacy: {
                extends: 'fields.generic.agreeToPrivacy',
                'label.text': 'form.emailsubscribe.agreeToPrivacy',
                validation: {
                    'errors.required': 'form.emailsubscribe.agreeToPrivacy.required'
                },
                element: {
                    attributes: {
                        required: true,
                        'data-event-change': 'validate'
                    }
                }
            }
        },
        login: {
            email: {
                extends: 'fields.generic.email',
                'label.show': true,
                'label.text': 'form.loginPage.email',
                element: {
                    attributes: {
                        'data-event-input': 'onInput'
                    }
                }
            },
            password: {
                extends: 'fields.generic.password',
                'label.text': 'form.loginPage.password',
                'caption.show': false,
                element: {
                    attributes: {
                        autocomplete: 'current-password'
                    }
                }
            },
            rememberMe: {
                extends: 'fields.input.checkbox',
                'label.text': 'form.loginPage.rememberMe'
            }
        },
        changePassword: {
            currentpassword: {
                extends: 'fields.generic.password',
                'caption.text': 'form.changePassword.currentpassword.caption',
                'label.text': 'form.changePassword.currentpassword',
                element: {
                    attributes: {
                        autocomplete: 'current-password'
                    },
                    mask: true
                }
            },
            newpassword: {
                extends: 'fields.generic.password',
                'label.text': 'form.changePassword.newpassword',
                element: {
                    attributes: {
                        autocomplete: 'new-password'
                    },
                    mask: true
                }
            },
            newpasswordconfirm: {
                extends: 'fields.generic.password',
                'label.text': 'form.profile.passwordconfirm',
                element: {
                    attributes: {
                        autocomplete: 'new-password'
                    },
                    mask: true
                },
                validation: {
                    compareWith: 'changePassword.newpassword',
                    'errors.compareWith': 'error.message.mismatch.password',
                    'errors.passwordFormatError': 'validation.errors.parse'
                }
            }
        },
        coupon: {
            couponCode: {
                extends: 'fields.input.text',
                'label.show': false,
                'label.class': 'b-form_section-label',
                'caption.show': true,
                'caption.text': 'form.promoCode.code.caption',
                validation: {
                    errors: {
                        required: 'form.promoCode.required'
                    }
                },
                element: {
                    attributes: {
                        'data-id': 'couponCode',
                        'data-ref': 'field'
                    },
                    placeholder: 'form.promoCode.code.placeholder',
                    skipValidationOnBlur: true
                }
            }
        },
        creditCard: {
            cardType: {
                extends: 'fields.generic.cardType'
            },
            cardOwner: {
                extends: 'fields.generic.cardOwner'
            },
            cardNumber: {
                extends: 'fields.generic.cardNumber'
            },
            expirationMonth: {
                extends: 'fields.generic.expirationMonth'
            },
            expirationYear: {
                extends: 'fields.generic.expirationYear'
            },
            securityCode: {
                extends: 'fields.generic.securityCode'
            },
            addressFields: {
                firstName: {
                    extends: 'fields.generic.firstName'
                },
                lastName: {
                    extends: 'fields.generic.lastName'
                },
                address1: {
                    extends: 'fields.generic.address1'
                },
                address2: {
                    extends: 'fields.generic.address2'
                },
                country: {
                    extends: 'fields.generic.country'
                },
                'states.stateCode': {
                    extends: 'fields.generic.state'
                },
                city: {
                    extends: 'fields.generic.city',
                    widget: {
                        classes: 'm-small'
                    }
                },
                postalCode: {
                    extends: 'fields.generic.postalCode'
                }
            },
            email: {
                extends: 'fields.generic.email'
            }
        },
        trackOrder: {
            orderNumber: {
                extends: 'fields.generic.orderNumber'
            },
            orderEmail: {
                extends: 'fields.generic.email',
                'label.show': true,
                'label.text': 'form.loginPage.email'
            },
            billingZipCode: {
                extends: 'fields.input.text',
                'label.show': true,
                'label.text': 'form.trackorder.zip',
                'label.class': 'b-form_section-label',
                'caption.show': true,
                'caption.text': 'form.trackorder.zip.caption',
                element: {
                    required: true,
                    minLength: 1,
                    maxLength: 255
                },
                validation: {
                    'patterns.parse': 'validation.patterns.parse.zipCode',
                    'errors.parse': 'validation.errors.billingZipCode'
                }
            }
        },
        contactUs: {
            name: {
                extends: 'fields.generic.firstName',
                'label.text': 'form.contact.name'
            },
            email: {
                extends: 'fields.generic.email',
                'label.text': 'form.contact.email'
            },
            phone: {
                extends: 'fields.generic.phone',
                'label.showOptionalMarker': true,
                element: {
                    required: false
                }
            },
            orderNumber: {
                extends: 'fields.generic.orderNumber',
                'label.text': 'form.contact.orderNo',
                'label.showOptionalMarker': true,
                'caption.show': false,
                element: {
                    required: false
                }
            },
            questionType: {
                extends: 'fields.select',
                'label.show': true,
                'label.text': 'form.contact.questionType',
                element: {
                    required: true
                }
            },
            message: {
                extends: 'fields.input.textarea',
                'label.text': 'form.contact.message',
                element: {
                    required: true,
                    maxcounter: true,
                    maxLength: 5000,
                    attributes: {
                        'data-event-input': 'onInput'
                    },
                    placeholder: 'form.contact.message.placeholder'
                }
            }
        },
        newPasswords: {
            newpassword: {
                extends: 'fields.generic.password',
                element: {
                    attributes: {
                        autocomplete: 'new-password'
                    }
                }
            },
            newpasswordconfirm: {
                extends: 'fields.generic.password',
                'label.text': 'form.profile.passwordconfirm',
                element: {
                    attributes: {
                        autocomplete: 'new-password'
                    }
                },
                validation: {
                    compareWith: 'profile.login.newpasswords.newpassword',
                    'errors.compareWith': 'error.message.mismatch.password'
                }
            },
            agreeToPrivacy: {
                extends: 'fields.generic.agreeToPrivacy'
            }
        }
    },
    fields: {
        input: {
            base: {
                validation: {
                    errors: {
                        required: 'validation.errors.required',
                        minLength: 'validation.errors.minlength',
                        maxLength: 'validation.errors.maxlength'
                    }
                }
            },

            text: {
                extends: 'fields.input.base',
                validation: {
                    'patterns.security': 'validation.patterns.security',
                    'errors.security': 'validation.errors.security'
                },
                element: {
                    minLength: 2,
                    maxLength: 256,
                    type: 'text'
                }
            },

            textarea: {
                extends: 'fields.input.base',
                validation: {
                    'patterns.security': 'validation.patterns.security',
                    'errors.security': 'validation.errors.security'
                },
                element: {
                    minLength: 1,
                    maxLength: 256,
                    type: 'textarea'
                }
            },

            password: {
                extends: 'fields.input.base',
                widget: {
                    attributes: {
                        'data-numbers-amount': '1',
                        'data-upper-case-amount': '1',
                        'data-special-chars-amount': '1'
                    }
                },
                element: {
                    type: 'password',
                    required: true,
                    minLength: 8,
                    maxLength: 255,
                    attributes: {
                        'data-event-input': 'onPasswordInput'
                    }
                },
                validation: {
                    'errors.passwordFormatError': 'validation.errors.password.notmatchesformat'
                }
            },

            checkbox: {
                'label.after': true,
                'element.type': 'checkbox'
            },

            tel: {
                extends: 'fields.input.base',
                validation: {
                    'patterns.parse': 'validation.patterns.parse.phone',
                    'errors.parse': 'validation.errors.phone'
                },
                'label.text': 'form.address.phone',
                'caption.show': true,
                element: {
                    type: 'tel',
                    minLength: 5,
                    maxLength: 20,
                    attributes: {
                        autocomplete: 'tel',
                        'data-event-input': 'onInput',
                        'data-event-keypress': 'onKeypress',
                        'data-event-change': 'onChange',
                        'data-event-paste.prevent': 'onPaste'
                    }
                },
                localization: {
                    us: {
                        widget: {
                            attributes: {
                                'data-mask': '9-999-999-9999'
                            }
                        },
                        caption: {
                            text: 'form.address.phone.caption.us'
                        }
                    },
                    ca: {
                        widget: {
                            attributes: {
                                'data-mask': '9-999-999-9999'
                            }
                        },
                        caption: {
                            text: 'form.address.phone.caption.ca'
                        }
                    },
                    gb: {
                        widget: {
                            attributes: {
                                'data-mask': '+99 (9)99 9999 9999'
                            }
                        },
                        caption: {
                            text: 'form.address.phone.caption.gb'
                        }
                    }
                }
            },

            hidden: {
                'element.type': 'hidden'
            }
        },
        select: {
            'element.type': 'select',
            validation: {
                'patterns.security': 'validation.patterns.security',
                errors: {
                    required: 'validation.errors.required',
                    security: 'field.errors.security'
                }
            }
        },
        generic: {
            firstName: {
                extends: 'fields.input.text',
                'label.text': 'form.address.firstName',
                element: {
                    maxLength: 50,
                    required: true,
                    attributes: {
                        autocomplete: 'given-name'
                    }
                }
            },
            lastName: {
                extends: 'fields.input.text',
                'label.text': 'form.address.lastName',
                element: {
                    maxLength: 50,
                    required: true,
                    attributes: {
                        autocomplete: 'family-name'
                    }
                }
            },
            address1: {
                extends: 'fields.input.text',
                'label.text': 'form.address.line1',
                element: {
                    maxLength: 50,
                    required: true,
                    attributes: {
                        autocomplete: 'address-line1'
                    }
                }
            },
            address2: {
                extends: 'fields.input.text',
                'label.text': 'form.address.line2',
                element: {
                    minLength: 2,
                    maxLength: 50,
                    attributes: {
                        autocomplete: 'address-line2'
                    }
                }
            },
            city: {
                extends: 'fields.input.text',
                'label.text': 'form.address.city',
                element: {
                    minLength: 2,
                    maxLength: 50,
                    required: true,
                    attributes: {
                        autocomplete: 'address-level2'
                    }
                },
                validation: {
                    'patterns.parse': 'validation.patterns.parse.city',
                    'errors.parse': 'validation.errors.city'
                },
                localization: {
                    gb: {
                        extends: 'fields.input.text',
                        'label.text': 'form.address.city.gb'
                    }
                }
            },
            postalCode: {
                widget: {
                    classes: 'm-small'
                },
                extends: 'fields.input.text',
                'caption.show': true,
                'caption.text': 'form.address.zip.caption',
                'label.text': 'form.address.zip',
                validation: {
                    'patterns.parse': 'validation.patterns.parse.postalcode',
                    'errors.parse': 'validation.errors.postalcode'
                },
                element: {
                    minLength: 5,
                    maxLength: 10,
                    required: true
                },
                localization: {
                    us: {
                        extends: 'fields.input.text',
                        'caption.text': 'form.address.zip.caption.us',
                        validation: {
                            'patterns.parse': 'validation.patterns.parse.postalcode.us'
                        }
                    },
                    ca: {
                        extends: 'fields.input.text',
                        'caption.text': 'form.address.zip.caption.ca',
                        'label.text': 'form.address.zip.ca',
                        validation: {
                            'patterns.parse': 'validation.patterns.parse.postalcode.ca'
                        }
                    },
                    gb: {
                        extends: 'fields.input.text',
                        'caption.text': 'form.address.zip.caption.gb',
                        'label.text': 'form.address.zip.gb',
                        validation: {
                            'patterns.parse': 'validation.patterns.parse.postalcode.gb'
                        }
                    }
                }
            },
            country: {
                widget: {
                    attributes: {
                        'data-widget-event-change': 'onCountryChange'
                    },
                    classes: 'm-small'
                },
                extends: 'fields.select',
                'label.text': 'form.address.country',
                element: {
                    required: true,
                    attributes: {
                        'data-event-change': 'onChange'
                    }
                }
            },
            quickFinder: {
                widget: {
                    classes: 'm-highlight',
                    type: 'googleAutocompleteField',
                    attributes: {
                        'data-widget-event-placechanged': 'handlePlaceChange',
                        'data-id': 'googleAutocomplete'
                    }
                },
                extends: 'fields.input.text',
                'label.text': 'form.address.quickFinder',
                validation: {
                    'patterns.security': 'validation.patterns.parse.addressfinder',
                    'errors.security': 'validation.errors.invalid'
                },
                element: {
                    placeholder: 'form.address.quickFinder.placeholder',
                    attributes: {
                        'data-event-focus': 'handleFocus'
                    }
                }
            },
            state: {
                widget: {
                    classes: 'm-small'
                },
                extends: 'fields.input.text',
                'label.text': 'form.address.state',
                element: {
                    required: true
                },
                localization: {
                    us: {
                        extends: 'fields.select',
                        'label.text': 'form.address.state',
                        element: {
                            'options.binding': '*/cartridge/config/states/us_states.json',
                            required: true
                        }
                    },
                    ca: {
                        extends: 'fields.select',
                        'label.text': 'form.address.states.ca',
                        element: {
                            'options.binding': '*/cartridge/config/states/ca_states.json',
                            required: true
                        }
                    },
                    gb: {
                        extends: 'fields.input.text',
                        'label.text': 'form.address.states.gb',
                        element: {
                            required: false
                        }
                    }
                }
            },
            phone: {
                extends: 'fields.input.tel',
                element: {
                    required: true
                }
            },
            setAsDefault: {
                extends: 'fields.input.checkbox',
                'label.text': 'form.address.setDefault'
            },
            saveAddress: {
                extends: 'fields.input.checkbox',
                'label.text': 'form.address.saveaddress'
            },
            useShipping: {
                widget: {
                    attributes: {
                        'data-widget-event-change': 'toggleUseShipping',
                        'data-id': 'useShipping'
                    }
                },
                extends: 'fields.input.checkbox',
                'label.class': 'b-checkbox-label',
                'label.text': 'form.address.useshipping',
                element: {
                    attributes: {
                        'data-event-change': 'handleChange'
                    }
                }
            },
            cardType: {
                extends: 'fields.input.text',
                'label.text': 'form.payment.card.type',
                element: {
                    type: 'hidden',
                    maxLength: 20,
                    required: true
                }
            },
            cardOwner: {
                extends: 'fields.input.text',
                'label.text': 'form.payment.card.owner',
                element: {
                    maxLength: 20,
                    required: true
                }
            },
            cardNumber: {
                widget: {
                    type: 'inputCreditCardNumber'
                },
                extends: 'fields.input.text',
                'label.text': 'form.payment.card.number',
                'caption.show': true,
                'caption.text': 'form.payment.card.number.caption',
                validation: {
                    'patterns.parse': 'validation.patterns.parse.cardnumber',
                    'errors.parse': 'validation.errors.parse'
                },
                element: {
                    minLength: 13,
                    maxLength: 19,
                    required: true,
                    attributes: {
                        autocomplete: 'cc-number',
                        inputmode: 'numeric'
                    }
                }
            },
            expirationMonth: {
                extends: 'fields.select',
                'label.text': 'form.payment.card.month',
                element: {
                    required: true,
                    attributes: {
                        autocomplete: 'cc-exp-month'
                    }
                }
            },
            expirationYear: {
                extends: 'fields.select',
                'label.text': 'form.payment.card.year',
                element: {
                    emptyoption: 'form.payment.card.year.emptyoption',
                    required: true,
                    'options.script': '*/cartridge/config/exp_years',
                    attributes: {
                        autocomplete: 'cc-exp-year'
                    }
                }
            },
            securityCode: {
                extends: 'fields.input.text',
                'label.text': 'form.payment.card.cvv',
                'caption.show': true,
                'caption.text': 'form.payment.card.cvv.caption',
                validation: {
                    'patterns.parse': 'validation.patterns.parse.securitycode',
                    'errors.parse': 'validation.errors.parse'
                },
                element: {
                    type: 'password',
                    maxLength: 4,
                    minLength: 3,
                    required: true,
                    classes: 'm-width_intrinsic',
                    attributes: {
                        autocomplete: 'cc-csc',
                        inputmode: 'numeric'
                    }
                }
            },
            email: {
                extends: 'fields.input.base',
                element: {
                    maxLength: 50,
                    type: 'email',
                    required: true
                },
                validation: {
                    'patterns.parse': 'validation.patterns.parse.email',
                    'errors.parse': 'validation.errors.email'
                },
                'label.text': 'form.profile.email'
            },
            agreeToPrivacy: {
                extends: 'fields.input.checkbox',
                widget: {
                    attributes: {
                        'data-id': 'agreeToPrivacy'
                    }
                },
                'label.text': 'form.profile.agreeToPrivacy',
                validation: {
                    'errors.required': 'form.profile.agreeToPrivacy.required'
                },
                element: {
                    attributes: {
                        required: true,
                        'data-event-change': 'validate'
                    }
                }
            },
            password: {
                extends: 'fields.input.password',
                'caption.show': true,
                'caption.text': 'form.profile.password.caption',
                'label.text': 'form.profile.password'
            },
            orderNumber: {
                extends: 'fields.input.text',
                'label.show': true,
                'label.text': 'form.trackorder.number',
                'label.class': 'b-form_section-label',
                'caption.show': true,
                'caption.text': 'form.trackorder.number.caption',
                element: {
                    required: true,
                    minLength: 1,
                    maxLength: 24
                },
                validation: {
                    'patterns.parse': 'validation.patterns.parse.ordernumber',
                    'errors.parse': 'validation.errors.ordernumber'
                }
            }
        }
    }
};
