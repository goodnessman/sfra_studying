var Resource = require('dw/web/Resource');

var FILTER_YEAR_PARAM = 'orderYear';
var FILTER_MONTHS_PARAM = 'orderTime';

/**
 * @module orderHelpers
 * @description Helper to work with orders
 */

/**
* Returns paging model for customer orders
*
* @param {dw.util.ArrayList<dw.order.Order>} orders - list of prepared orders
* @param {object} querystring - querystring properties
* @returns {dw.web.PagingModel} paging model with customer orders
* */
function getPaging(orders, querystring) {
    var PagingModel = require('dw/web/PagingModel');

    var pageSize = querystring[PagingModel.PAGING_SIZE_PARAMETER]
        ? parseInt(querystring[PagingModel.PAGING_SIZE_PARAMETER], 10)
        : require('util/sitepref').getValue('orderHistoryPageSize');
    var pageStart = querystring[PagingModel.PAGING_START_PARAMETER]
        ? parseInt(querystring[PagingModel.PAGING_START_PARAMETER], 10)
        : 0;

    var pagingModel = new PagingModel(orders);

    pagingModel.setStart(pageStart);
    pagingModel.setPageSize(pageSize);

    return pagingModel;
}

/**
 * @typedef {object} ShowMore
 * @property {number} total - Count of all orders in the paging model
 * @property {dw.web.URL} url - next page URL
 * @property {number} count - load more count
 * @property {number} viewed - count viewed
 * @property {number} viewedPercents - percents viewed
 */


/**
* Returns show more link data
*
* @see {@link module:orderHelpers~ShowMore}
* @param {dw.web.PagingModel} paging - paging model with customer orders
* @returns {ShowMore} - show more link data
* */
function getShowMore(paging) {
    var URLUtils = require('dw/web/URLUtils');
    var ACTION_ENDPOINT_AJAX = 'Order-LoadItems';
    var nextStart = paging.end + 1;
    var result;

    if (paging.pageSize > paging.count || nextStart >= paging.count) {
        result = {
            total: paging.count,
            viewed: paging.count,
            viewedPercents: 100
        };
    } else {
        var countLeft = paging.count - nextStart;
        var currentPagingStart = paging.start;

        paging.setStart(nextStart);
        result = {
            total: paging.count,
            url: paging.appendPaging(URLUtils.url(ACTION_ENDPOINT_AJAX)),
            count: countLeft > paging.pageSize ? paging.pageSize : countLeft,
            viewed: nextStart,
            viewedPercents: (nextStart / paging.count) * 100
        };
        paging.setStart(currentPagingStart);
    }

    return result;
}

/**
* Returns a list of orders created with model
*
* @param {dw.util.Iterator} orders - chunk of orders
* @param {object} config - querystring properties
* @returns {Array} list of orders created with model
* */
function createOrderObjects(orders, config) {
    var OrderModel = require('*/cartridge/models/order');
    var orderObjects = [];

    while (orders.hasNext()) {
        var order = orders.next();

        orderObjects.push(new OrderModel(order, {
            config: config
        }));
    }

    return orderObjects;
}

/**
* Returns filter options
*
* @param {Array<number>} yearFilters - possible year values for filter
* @param {object} querystring - querystring properties
* @returns {Array} filters
* */
function createFilters(yearFilters, querystring) {
    var URLUtils = require('dw/web/URLUtils');
    var filtersConfig = require('*/cartridge/config/order_history_filters');
    var ACTION_ENDPOINT_FILTER = 'Order-Filtered';

    var filterValues = filtersConfig.map(function (filter) {
        return {
            displayValue: Resource.msg(filter.label, 'history', null),
            optionValue: URLUtils.url(ACTION_ENDPOINT_FILTER, FILTER_MONTHS_PARAM, filter.value),
            selected: querystring[FILTER_MONTHS_PARAM] === String(filter.value)
        };
    });

    yearFilters.forEach(function (year) {
        filterValues.push({
            displayValue: year,
            optionValue: URLUtils.url(ACTION_ENDPOINT_FILTER, FILTER_YEAR_PARAM, year),
            selected: querystring[FILTER_YEAR_PARAM] === String(year)
        });
    });

    return filterValues;
}

/**
 * @typedef {object} FilteringResult
 * @property {dw.util.ArrayList<dw.order.Order>} orders - filtered orders
 * @property {Array<number>} yearFilters - possible year values for filter
 */

/**
* Returns a list of filtered orders and possible orders years for filter
* Year options are being created here for performance purposes
*
* @see {@link module:orderHelpers~FilteringResult}
* @param {dw.util.SeekableIterator} customerOrders - orders search result for customer
* @param {object} querystring - querystring properties
* @returns {FilteringResult} filtering result
* */
function processFiltering(customerOrders, querystring) {
    var ArrayList = require('dw/util/ArrayList');
    var Site = require('dw/system/Site');
    var Calendar = require('dw/util/Calendar');
    var filtersConfig = require('*/cartridge/config/order_history_filters');
    var defaultTimeAgo = !empty(filtersConfig) ? filtersConfig[0].value : 6;
    var yearFilter = querystring[FILTER_YEAR_PARAM];
    var yearFilterValues = [];
    var orders = new ArrayList();
    var timeAgoFilter;

    if (!yearFilter) {
        var timeAgoCalendar = Site.getCalendar();

        if (querystring[FILTER_MONTHS_PARAM]) {
            timeAgoCalendar.add(Calendar.MONTH, -querystring[FILTER_MONTHS_PARAM]);
        } else {
            timeAgoCalendar.add(Calendar.MONTH, -defaultTimeAgo);
        }
        timeAgoFilter = timeAgoCalendar.getTime();
    }

    while (customerOrders.hasNext()) {
        var customerOrder = customerOrders.next();
        var orderYear = customerOrder.getCreationDate().getFullYear().toString();

        if (yearFilterValues.indexOf(orderYear) === -1) {
            yearFilterValues.push(orderYear);
        }

        if (yearFilter) {
            if (orderYear === yearFilter) {
                orders.push(customerOrder);
            }
        } else if (timeAgoFilter && customerOrder.getCreationDate().getTime() > timeAgoFilter) {
            orders.push(customerOrder);
        }
    }

    return {
        orders: orders,
        yearFilters: yearFilterValues
    };
}

/**
* Returns a list of orders for the current customer
*
* @param {object} currentCustomer - object with customer properties
* @param {object} querystring - querystring properties
* @returns {object} orderModel of the current dw order object
* */
function getOrders(currentCustomer, querystring) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Order = require('dw/order/Order');
    var customerNo = currentCustomer.profile.customerNo;
    var customerOrders = OrderMgr.searchOrders(
        'customerNo={0} AND status!={1}',
        'creationDate desc',
        customerNo,
        Order.ORDER_STATUS_REPLACED
    );
    var orderModelConfig = {
        numberOfLineItems: 'single'
    };

    var { orders, yearFilters } = this.processFiltering(customerOrders, querystring);
    var paging = this.getPaging(orders, querystring);

    return {
        orders: this.createOrderObjects(paging.pageElements, orderModelConfig),
        ordersCount: paging.count,
        filterValues: this.createFilters(yearFilters, querystring),
        showMore: this.getShowMore(paging)
    };
}

module.exports = module.superModule;
module.exports.getPaging = getPaging;
module.exports.getShowMore = getShowMore;
module.exports.createOrderObjects = createOrderObjects;
module.exports.createFilters = createFilters;
module.exports.processFiltering = processFiltering;
module.exports.getOrders = getOrders;
