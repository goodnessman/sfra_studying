/**
 * @description Middleware generating a List Schema for breadcrumbs
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function generateListSchema(req, res, next) {
    this.on('route:BeforeComplete', function (request, response) {
        var structuredDataHelper = require('*/cartridge/scripts/helpers/structuredDataHelper');

        var viewData = response.getViewData();
        var breadcrumbListSchema = structuredDataHelper.getBreadcrumbListSchema(viewData.breadcrumbs);

        response.setViewData({
            breadcrumbListSchema: breadcrumbListSchema
        });
    });

    next();
}

/**
 * @description Middleware generating a List Schema for breadcrumbs
 * @param {object} res - Response object
 * @param {string} propertyName - Property name
 * @param {string} propertiesFile - Properties file name
 * @returns {void}
 */
function addCurrentPageToBreadcrumbs(propertyName, propertiesFile) {
    return function (req, res, next) {
        var Resource = require('dw/web/Resource');
        var URLUtils = require('dw/web/URLUtils');
        var viewData = res.getViewData();
        if (viewData.breadcrumbs) {
            viewData.breadcrumbs.push({
                htmlValue: Resource.msg(propertyName, propertiesFile, null),
                url: URLUtils.url(res.viewData.action).toString()
            });
        }
        next();
    };
}

/**
 * @description Middleware used to generate breadcrumbs for product landing page
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function generatePLPBreadcrumbs(req, res, next) {
    this.on('route:BeforeComplete', function (_req, _res) {
        var ObjectUtils = require('util/object');
        var category = ObjectUtils.get(_res.getViewData(), 'productSearch.productSearch.category');

        if (category) {
            var Resource = require('dw/web/Resource');
            var URLUtils = require('dw/web/URLUtils');
            var pageBreadcrumbs = [];

            while (category.parent) {
                pageBreadcrumbs.push({
                    htmlValue: category.displayName,
                    url: URLUtils.url('Search-Show', 'cgid', category.ID).toString()
                });
                category = category.parent;
            }

            pageBreadcrumbs.push({
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            });

            _res.setViewData({
                breadcrumbs: pageBreadcrumbs.reverse()
            });
        }
    });

    next();
}


/**
 * @description Middleware used to generate breadcrumbs for content page
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function generateContentBreadcrumbs(req, res, next) {
    this.on('route:BeforeComplete', function (_req, _res) {
        var viewData = _res.getViewData();
        var content = viewData.content;

        if (content) {
            var Resource = require('dw/web/Resource');
            var URLUtils = require('dw/web/URLUtils');
            var pageBreadcrumbs = [];
            var folder;

            if (content.isFolder) {
                folder = content.raw;
            } else {
                pageBreadcrumbs.push({
                    htmlValue: content.name,
                    url: URLUtils.url('Page-Show', 'cid', content.ID).toString()
                });
                folder = content.raw.getClassificationFolder();
                if (!folder && content.raw.folders.length) {
                    folder = content.raw.folders[0];
                }
            }

            while (folder.parent) {
                pageBreadcrumbs.push({
                    htmlValue: folder.displayName,
                    url: URLUtils.url('Search-ShowContent', 'fdid', folder.ID).toString()
                });
                folder = folder.parent;
            }

            pageBreadcrumbs.push({
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            });
            viewData.breadcrumbs = pageBreadcrumbs.reverse();
        }
    });

    next();
}

module.exports = {
    generateListSchema: generateListSchema,
    addCurrentPageToBreadcrumbs: addCurrentPageToBreadcrumbs,
    generatePLPBreadcrumbs: generatePLPBreadcrumbs,
    generateContentBreadcrumbs: generateContentBreadcrumbs
};
