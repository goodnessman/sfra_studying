/**
 * @description Middleware executing content search
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function contentSearch(req, res, next) {
    // eslint-disable-next-line no-unused-vars
    this.on('route:BeforeComplete', function (requestObj, responseObj) {
        if (require('util/object').get(responseObj.getViewData(), 'productSearch.isCategorySearch')) {
            return;
        }

        var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
        var contentSearchResults = searchHelper.setupContentSearch(req.querystring);
        responseObj.setViewData({
            contentSearch: contentSearchResults
        });
    });

    next();
}

/**
 * @description Middleware adds count information into search
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function searchCount(req, res, next) {
    // eslint-disable-next-line no-unused-vars
    this.on('route:BeforeComplete', function (requestObj, responseObj) {
        var objectUtils = require('util/object');
        var viewData = responseObj.getViewData();

        responseObj.setViewData({
            searchHasProducts: objectUtils.get(viewData, 'productSearch.productIds.length') > 0,
            searchHasContent: objectUtils.get(viewData, 'contentSearch.contentCount') > 0,
            searchTotalCount: (objectUtils.get(viewData, 'productSearch.count') || 0)
                + (objectUtils.get(viewData, 'contentSearch.contentCount') || 0)
        });
    });

    next();
}

/**
 * @description Checks if search needs to be redirected, if only one entity (product, content) found.
 * If so - redirects to proper entity page.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function redirect(req, res, next) {
    this.on('route:BeforeComplete', function (_req, _res) {
        var objectUtils = require('util/object');
        var URLUtils = require('dw/web/URLUtils');
        var viewData = _res.getViewData();
        var productSearchResults = objectUtils.get(viewData, 'productSearch');
        var contentSearchResults = objectUtils.get(viewData, 'contentSearch');
        var productsCount = (productSearchResults && productSearchResults.count) || 0;
        var contentsCount = (contentSearchResults && contentSearchResults.contentCount) || 0;

        if (productSearchResults && empty(productSearchResults.searchKeywords)) {
            return;
        }

        if ((productsCount + contentsCount) > 1) {
            return;
        }

        if (productsCount === 1) {
            var productID = objectUtils.get(productSearchResults, 'productIds.0.productID');
            if (productID) {
                _res.redirect(URLUtils.url('Product-Show', 'pid', productID));
            }
        }

        if (contentsCount === 1) {
            var contentURL = objectUtils.get(contentSearchResults, 'contents.0.url');
            if (contentURL) {
                _res.redirect(contentURL);
            }
        }
    });

    next();
}

module.exports = {
    contentSearch: contentSearch,
    searchCount: searchCount,
    redirect: redirect
};
