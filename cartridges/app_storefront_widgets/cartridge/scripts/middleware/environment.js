var System = require('dw/system/System');

/**
 * @description Middleware ensures environment is not Production.
 * In case if Production - flow ends up with error 'Permission denied'.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function ensureNotProduction(req, res, next) {
    if (System.getInstanceType() === System.PRODUCTION_SYSTEM) {
        next(new Error('Permission denied'));
    } else {
        next();
    }
}

module.exports = {
    ensureNotProduction: ensureNotProduction
};
