function generateNavigationMenuItems(req, res, next) {
    var navigationItems = require('*/cartridge/scripts/account/navigationItems');
    var menuItems = navigationItems();
    for (var i = 0; i < menuItems.length; i++) {
        menuItems[i].selected = menuItems[i].actions.indexOf(res.viewData.action) !== -1;
    }
    res.setViewData({
        menuItems: menuItems
    });

    next();
}

module.exports = {
    generateNavigationMenuItems: generateNavigationMenuItems
};
