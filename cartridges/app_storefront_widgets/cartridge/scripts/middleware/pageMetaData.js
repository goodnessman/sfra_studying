/**
 * @module pageMetaData
 * @description Middleware to apply SEO meta data/tags
 * @see [pageMetaData documentation]{@link https://confluence.ontrq.com/display/SFRA/SFRA+-+TSD+-+SEO+for+Custom+Pages}
 */

var ContentMgr = require('dw/content/ContentMgr');

var SEO_CONTENT_ASSET_PREFIX = 'SEO-';
var SEO_CUSTOM_TAG_PREFIX = 'custom:';

/**
 * @description Parse meta tag content using Velocity templates
 * @param {string} metaTagContent - A string that should be parsed
 * @param {Object} pdict - Entire pdict to be used as a source for Velocity
 */
function parseMetaTagContent(metaTagContent, pdict) {
    var velocityHelper = require('*/cartridge/scripts/helpers/velocityHelper');
    return velocityHelper.render(metaTagContent, pdict);
}

/**
 * @description Function to get current action pipeline in format `Home-Start`
 * <br>Uses known fix for some domain configuration cases
 * @param {Object} req Current request
 */
function getPageAction(req) {
    var pathParts = req.path.split('/');
    var action;
    /*
    * req.path is in format /on/demandware.store/Sites-site_id-Site/en_GB/Search-Show
    * But when configured domain only home page URL on production
    * it became without last two parts /on/demandware.store/Sites-site_id-Site
    * As a result logic not correctly detect action name
    * This hack with checking of magic number "6"(amount of parts in correct req.path)
    * used to fix this issue
    */
    if (pathParts.length < 6) {
        try {
            action = session.clickStream.last.pipelineName;
        } catch (e) {
            action = 'Home-Show';
        }
    } else {
        action = pathParts[pathParts.length - 1];
    }

    return action;
}

/**
 * @description Find SEO content asset based on defined rules
 * @param {string} pageAction - current page action in format `Home-Show`
 */
function getSEOContentAsset(pageAction) {
    var seoContentAsset;

    if (pageAction) {
        seoContentAsset = ContentMgr.getContent(SEO_CONTENT_ASSET_PREFIX + pageAction);

        if (!seoContentAsset) {
            seoContentAsset = ContentMgr.getContent(SEO_CONTENT_ASSET_PREFIX + pageAction.split('-').shift());
        }
    }

    return seoContentAsset;
}

/**
 * @description Get custom prefixed meta tags from given content asset
 * <br>Applies velocity rendering for meta tag content.
 * @param {dw.content.Content} seoContentAsset - Founded SEO content asset container
 * @param {Object} res - Response object
 */
function getCustomPageMetaTags(seoContentAsset, res) {
    var pdict = res.getViewData();
    var customMetaTags = seoContentAsset.pageMetaTags.filter(function (metaTag) {
        return metaTag.ID.indexOf(SEO_CUSTOM_TAG_PREFIX) === 0;
    });

    customMetaTags = customMetaTags.map(function (metaTag) {
        return {
            ID: metaTag.ID.replace(SEO_CUSTOM_TAG_PREFIX, ''),
            content: parseMetaTagContent(metaTag.content, pdict),
            name: metaTag.name,
            property: metaTag.property,
            title: metaTag.title
        };
    });

    return customMetaTags;
}

/**
 * @description Get native meta tags (skipping custom-prefixed)
 * @param {array} requestPageMetaTags - Page meta tags from native request
 */
function getNativePageMetaTags(requestPageMetaTags) {
    return requestPageMetaTags.filter(function (metaTag) {
        return metaTag.ID.indexOf(SEO_CUSTOM_TAG_PREFIX) === -1;
    });
}

/**
 * @description Middleware to compute request pageMetaData object
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function computedPageMetaData(req, res, next) {
    var seoContentAsset = getSEOContentAsset(getPageAction(req));

    if (seoContentAsset && seoContentAsset.online) {
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
        pageMetaHelper.setPageMetaData(req.pageMetaData, seoContentAsset);
    }

    var computedMetaData = {
        title: req.pageMetaData.title,
        description: req.pageMetaData.description,
        keywords: req.pageMetaData.keywords,
        pageMetaTags: []
    };

    var pageMetaTags = seoContentAsset && seoContentAsset.online
        ? getCustomPageMetaTags(seoContentAsset, res)
        : getNativePageMetaTags(req.pageMetaData.pageMetaTags);

    pageMetaTags.forEach(function (item) {
        if (item.title) {
            computedMetaData.title = item.content;
        } else if (item.name && item.ID === 'description') {
            computedMetaData.description = item.content;
        } else if (item.name && item.ID === 'keywords') {
            computedMetaData.keywords = item.content;
        } else {
            computedMetaData.pageMetaTags.push(item);
        }
    });

    res.setViewData({
        CurrentPageMetaData: computedMetaData
    });

    next();
}

/**
 * @description Middleware adds pagination information into search
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function paginationMetadata(req, res, next) {
    this.on('route:BeforeComplete', function (_req, _res) {
        var ObjectUtils = require('util/object');

        var paginationMeta = [];
        var showMore = ObjectUtils.get(_res.getViewData(), 'productSearch.showMore');
        var showPrev = ObjectUtils.get(_res.getViewData(), 'productSearch.showPrev');

        if (showPrev && showPrev.prevCanonicalPageURL) {
            paginationMeta.push({
                rel: 'prev',
                href: showPrev.prevCanonicalPageURL.toString()
            });
        }
        if (showMore && showMore.nextCanonicalPageURL) {
            paginationMeta.push({
                rel: 'next',
                href: showMore.nextCanonicalPageURL.toString()
            });
        }

        _res.setViewData({
            paginationMeta: paginationMeta
        });
    });

    next();
}

/**
 * @description Middleware corrects canonical information during search
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function canonicalUrl(req, res, next) {
    this.on('route:BeforeComplete', function (_req, _res) {
        var URLUtils = require('dw/web/URLUtils');
        if (_req.querystring.cgid) {
            _res.setViewData({
                canonicalUrl: URLUtils.abs('Search-Show', 'cgid', _req.querystring.cgid)
            });
        } else if (_req.querystring.q) {
            _res.setViewData({
                canonicalUrl: ''
            });
        }
    });
    next();
}

/**
 * @description Middleware adds hreflang tags into page
 * Href Lang tags are enabled for all locales of the same site by default,
 * it can be vary only for external sites.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function hrefLang(req, res, next) {
    this.on('route:BeforeComplete', function (_req, _res) {
        var localesConfig = require('util/sitepref').getJSONValue('LocalesConfig');
        if (localesConfig && localesConfig.length) {
            var action = _res.getViewData().action;
            var queryString = _res.getViewData().queryString;

            var server = require('server');
            var QueryString = server.querystring;
            var queryStringObj = new QueryString(queryString || '');
            if (Object.hasOwnProperty.call(queryStringObj, 'lang')) {
                delete queryStringObj.lang;
            }

            var Site = require('dw/system/Site');
            var URLAction = require('dw/web/URLAction');
            var URLUtils = require('dw/web/URLUtils');

            var currentSiteID = Site.getCurrent().getID();
            var hrefLangTags = [];

            localesConfig.forEach(function (locale) {
                var isCurrentSite = currentSiteID === locale.siteID;
                if (!isCurrentSite && !locale.enableHrefLang) {
                    return;
                }

                var href = URLAction(action, locale.siteID, locale.locale);
                href = URLUtils.abs(href);

                var requestParams = Object.keys(queryStringObj);
                if (requestParams && requestParams.length) {
                    requestParams.forEach(function (requestParamName) {
                        href.append(requestParamName, queryStringObj[requestParamName]);
                    });
                }

                var hrefLangLocale = locale.locale === 'default'
                    ? 'x-default'
                    : (locale.locale || '').replace('_', '-').toLowerCase();

                hrefLangTags.push({
                    href: href.toString(),
                    hrefLang: hrefLangLocale
                });
            });

            _res.setViewData({
                hrefLangTags: hrefLangTags
            });
        }
    });
    next();
}

module.exports = {
    computedPageMetaData: computedPageMetaData,
    paginationMetadata: paginationMetadata,
    canonicalUrl: canonicalUrl,
    hrefLang: hrefLang
};
