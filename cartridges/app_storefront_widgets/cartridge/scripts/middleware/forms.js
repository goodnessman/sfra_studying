var server = require('server');
var ObjectUtils = require('util/object');

var definitions = {
    shipping: function () {
        return {
            form: server.forms.getForm('shipping').shippingAddress.addressFields,
            template: 'checkout/shipping/shippingAddressInner',
            formOptions: 'shippingFormOptions',
            formPath: 'forms.shippingForm.shippingAddress.addressFields'
        };
    },
    billing: function () {
        return {
            form: server.forms.getForm('billing').addressFields,
            template: 'checkout/billing/billingAddressInner',
            formOptions: 'billingFormOptions',
            formPath: 'forms.billingForm.addressFields'
        };
    },
    address: function () {
        return {
            form: server.forms.getForm('address'),
            template: 'account/components/addressFormInner',
            formOptions: 'addressFormOptions',
            formPath: 'addressForm'
        };
    }
};

var options = {
    shipping: function () {
        return {
            countryOptions: 'forms.shippingForm.shippingAddress.addressFields.country.options',
            formOptions: 'shippingFormOptions'
        };
    },
    billing: function () {
        return {
            countryOptions: 'forms.billingForm.addressFields.country.options',
            formOptions: 'billingFormOptions'
        };
    },
    address: function () {
        return {
            countryOptions: 'addressForm.country.options',
            formOptions: 'addressFormOptions'
        };
    }
};

/**
 * @description Applies country select filtering in My Account and Checkout areas based on preference
 * @param {object} req Current request
 * @param {array} countryOptions An array of unfiltered countries
 * @param {object} viewData View Data object to set filtered options to
 * @param {object} optionsContext Context object (see `options` above)
 * @param {string} form Form name i.e. `address`, `shipping`, `billing`
 * @returns {array} Filtered countries list
 */
function applyCountryFilter(req, countryOptions, viewData, optionsContext, form) {
    var Locale = require('dw/util/Locale');
    var locale = Locale.getLocale(req.locale.id);
    if (locale) {
        var context = '';
        if (form === 'shipping,billing') {
            context = 'checkout';
        } else if (form === 'address') {
            context = 'account';
        }

        var allowedCountries = require('util/pref')
            .get('address.' + context + '.allowed.' + (locale.country || '').toLowerCase(), '');
        if (!empty(allowedCountries)) {
            countryOptions = countryOptions.filter(function (country) {
                return allowedCountries.split(',').indexOf(country.id) > -1;
            });
        }

        var optionsHasSelectedCountry = countryOptions.some(function (country) {
            return country.selected;
        });

        // Preselect country as per current locale, if there is no preselected country yet
        if (!optionsHasSelectedCountry) {
            countryOptions.some(function (country) {
                if (country.id === locale.country) {
                    country.selected = true;
                    return true;
                }
                return false;
            });
        }
    }

    if (viewData) {
        ObjectUtils.set(viewData, optionsContext.countryOptions, countryOptions);
    }
    return countryOptions;
}

/**
 * Middleware to render updated form definition
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function updateFormDefinition(req, res, next) {
    var countryCode = !empty(req.querystring.countryCode)
        ? req.querystring.countryCode
        : require('dw/util/Locale').getLocale(request.locale).getCountry();

    var form = !empty(req.querystring.form)
        ? req.querystring.form
        : '';

    if (!empty(form)) {
        var definitionContext = definitions[form] && definitions[form]();
        if (definitionContext) {
            var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
            var renderContext = {};
            var formHtml = '';

            var countryOptions = definitionContext.form.country.options;
            definitionContext.form.country.options = applyCountryFilter(req, countryOptions, null, null, form);

            ObjectUtils.set(renderContext, definitionContext.formPath, definitionContext.form);
            ObjectUtils.set(renderContext, definitionContext.formOptions, { countryCode: countryCode });
            formHtml = renderTemplateHelper.getRenderedHtml(renderContext, definitionContext.template);

            res.json({
                formDefinition: formHtml
            });
        }
    }

    next();
}

/**
 * Middleware to update (inject into view) form options
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function updateFormOptions(req, res, next) {
    var viewData = res.getViewData();
    var form = viewData.formDefinitionContext || '';

    if (!empty(form)) {
        form.split(',').forEach(function (option) {
            var optionsContext = options[option] && options[option]();
            if (optionsContext.countryOptions) {
                var countryOptions = ObjectUtils.get(viewData, optionsContext.countryOptions);
                countryOptions = applyCountryFilter(req, countryOptions, viewData, optionsContext, form);

                if (countryOptions && countryOptions.length) {
                    countryOptions.some(function (countryOption) {
                        if (countryOption.selected) {
                            ObjectUtils.set(viewData, optionsContext.formOptions, {
                                countryCode: countryOption.value
                            });
                            return true;
                        }
                        return false;
                    });
                }
            }
        });
    }

    next();
}

module.exports = {
    updateFormDefinition: updateFormDefinition,
    updateFormOptions: updateFormOptions
};
