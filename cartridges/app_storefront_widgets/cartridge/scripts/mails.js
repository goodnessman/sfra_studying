var System = require('dw/system/System');
var isProduction = System.getInstanceType() === System.PRODUCTION_SYSTEM;
var useLocalEnabled = !isProduction && (request.httpParameters.containsKey('useLocal') || request.httpCookies.useLocal);

/**
 * @description Return styles from properties bundle by class name.
 * Could return classes for development needs or actual css string. Used in mails development.
 * @param {string} classList - list of class names, space separated
 * @example
 *      style="${mailstyle.getStyle('someClass')}"
 *      style="${mailstyle.getStyle('someClass someOtherClass')}"
 * @returns {string} css properties associated with style class or style class
 */
function getStyle(classList) {
    var Resource = require('dw/web/Resource');
    var classes = classList.trim().replace(/\s{2,}/g, ' ').split(' ');
    var resultedString = '';

    for (let i = 0; i < classes.length; i++) {
        if (useLocalEnabled) {
            resultedString += classes[i] + ' ';
        } else {
            resultedString += Resource.msg(classes[i], '_mailStyles', isProduction ? '' : 'outline:2px dashed red;');
        }
    }

    return resultedString;
}

module.exports = {
    getStyle: getStyle,
    isUseLocal: useLocalEnabled
};
