/**
 * @description Returns Contact Us Question Type config from site preference for provided locale
 * @param {string} locale - requested config locale
 * @returns {array} Contact Us Question Type config
 */
function getContactUsQuestionTypes(locale) {
    var Locale = require('dw/util/Locale');
    var DEFAULT_FALLBACK_LOCALE = 'default';

    var currentLocale = Locale.getLocale(locale);
    var currentLanguage = currentLocale.language;
    var contactUsQuestionTypesObj = require('util/sitepref').getJSONValue('contactUsQuestionTypes');
    var contactUsQuestionTypes = [];

    if (currentLocale.ID in contactUsQuestionTypesObj) {
        contactUsQuestionTypes = contactUsQuestionTypesObj[currentLocale.ID];
    } else if (currentLanguage in contactUsQuestionTypesObj) {
        contactUsQuestionTypes = contactUsQuestionTypesObj[currentLanguage];
    } else if (DEFAULT_FALLBACK_LOCALE in contactUsQuestionTypesObj) {
        contactUsQuestionTypes = contactUsQuestionTypesObj[DEFAULT_FALLBACK_LOCALE];
    }

    return contactUsQuestionTypes;
}

/**
 * @description Creates options for Question Type field in Contact Us form
 * @param {string} locale - requested options locale
 * @returns {dw.util.ArrayList} question type options
 */
function getContactUsQuestionTypeOptions(locale) {
    var ArrayList = require('dw/util/ArrayList');
    var Resource = require('dw/web/Resource');

    var contactUsQuestionTypeOptions = new ArrayList({
        id: '',
        value: '',
        label: Resource.msg('form.resource.emptyoption', 'forms', null)
    });
    var contactUsQuestionTypes = getContactUsQuestionTypes(locale);

    return contactUsQuestionTypes.reduce(function (questionOptions, questionOption) {
        questionOptions.push({
            id: questionOption.id,
            value: questionOption.id,
            label: questionOption.label
        });

        return questionOptions;
    }, contactUsQuestionTypeOptions);
}


module.exports = {
    getContactUsQuestionTypeOptions: getContactUsQuestionTypeOptions,
    getContactUsQuestionTypes: getContactUsQuestionTypes
};
