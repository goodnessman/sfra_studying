var collections = require('*/cartridge/scripts/util/collections');

/**
 * @description Retrieves attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition Refinement
 *     definition for which we wish to retrieve refinement values for
 * @returns {object} Attribute refinement value model module
 */
function getAttributeRefinementValueModel(refinementDefinition) {
    var pref = require('util/pref');

    if (refinementDefinition.priceRefinement) {
        return require('*/cartridge/models/search/attributeRefinementValue/price');
    } else if (refinementDefinition.attributeID === pref.get('refinements.color.attr.id')) {
        return require('*/cartridge/models/search/attributeRefinementValue/color');
    } else if (refinementDefinition.attributeID === pref.get('refinements.size.attr.id')) {
        return require('*/cartridge/models/search/attributeRefinementValue/size');
    } else if (refinementDefinition.categoryRefinement) {
        return require('*/cartridge/models/search/attributeRefinementValue/category');
    }

    return require('*/cartridge/models/search/attributeRefinementValue/boolean');
}

/**
 * @description Creates an array of category refinements for category search
 * @param {dw.catalog.ProductSearchModel} productSearch Product search object
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition Refinement
 *     definition for which we wish to retrieve refinement values for
 * @param {dw.util.Collection.<dw.catalog.ProductSearchRefinementValue>} refinementValues
 *     Collection of refinement values
 * @param {CategoryAttributeValue} Model model of the category class
 * @returns {Array} List of categories
 */
function createCategorySearchRefinement(productSearch, refinementDefinition, refinementValues, Model) {
    var childCategory = null;
    var currentCategory = productSearch.category;
    var topCategory = null;
    var insertPoint = null;

    if (currentCategory.root || currentCategory.parent.root) {
        topCategory = new Model(productSearch, refinementDefinition, currentCategory, true, refinementValues);
        insertPoint = topCategory.subCategories;
    } else {
        topCategory = new Model(productSearch, refinementDefinition, currentCategory.parent, false);
        childCategory = new Model(productSearch, refinementDefinition, currentCategory, true, refinementValues);
        topCategory.subCategories.push(childCategory);
        insertPoint = topCategory.subCategories[0].subCategories;
    }

    collections.forEach(currentCategory.subCategories, function (category) {
        if (category.online) {
            insertPoint.push(new Model(productSearch, refinementDefinition, category, false, refinementValues));
        }
    });

    return [topCategory];
}

/**
 * @description Creates an array of category refinements for category search
 * @param {dw.catalog.ProductSearchModel} productSearch Product search object
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition Refinement
 *     definition for which we wish to retrieve refinement values for
 * @param {dw.util.Collection.<dw.catalog.ProductSearchRefinementValue>} refinementValues
 *     Collection of refinement values
 * @param {CategoryAttributeValue} Model model of the category class
 * @returns {Array} List of categories
 */
function createProductSearchRefinement(productSearch, refinementDefinition, refinementValues, Model) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var Resource = require('dw/web/Resource');

    var root = CatalogMgr.getSiteCatalog().getRoot();
    var currentCategory = productSearch.category;
    var tree = [];
    var currentCategoryModel = null;
    var topCategoryModel = null;

    if (currentCategory && !currentCategory.root) {
        tree.push({
            displayValue: Resource.msg('refinements.allresults', 'search', null),
            title: Resource.msg('refinements.allresults', 'search', null),
            url: productSearch.urlRefineCategory('Search-ShowAjax', 'root').relative().toString(),
            isBackButton: true,
            selected: false,
            selectable: true,
            subCategories: []
        });
    }

    if (currentCategory && currentCategory.parent && currentCategory.parent.root) {
        currentCategoryModel = new Model(
            productSearch,
            refinementDefinition,
            currentCategory,
            true,
            refinementValues
        );
        currentCategoryModel.subCategories = collections.map(currentCategory.subCategories, function (subCategory) {
            return new Model(
                productSearch,
                refinementDefinition,
                subCategory,
                productSearch.categoryID === subCategory.ID,
                refinementValues
            );
        });
        tree.push(currentCategoryModel);
    }

    if (currentCategory && currentCategory.parent && !currentCategory.parent.root) {
        topCategoryModel = new Model(productSearch, refinementDefinition, currentCategory.parent, false);
        currentCategoryModel = new Model(productSearch, refinementDefinition, currentCategory, true, refinementValues);
        currentCategoryModel.subCategories = collections.map(currentCategory.subCategories, function (subCategory) {
            return new Model(
                productSearch,
                refinementDefinition,
                subCategory,
                productSearch.categoryID === subCategory.ID,
                refinementValues
            );
        });
        topCategoryModel.subCategories.push(currentCategoryModel);
        tree.push(topCategoryModel);
    }

    if (!currentCategory || currentCategory.root) {
        collections.forEach(root.subCategories, function (category) {
            var categoryModel = new Model(
                productSearch,
                refinementDefinition,
                category,
                false,
                refinementValues
            );

            tree.push(categoryModel);
        });
    }

    return tree;
}

/**
 * @description Retrieve refinement values based on refinement type
 *
 * @param {dw.catalog.ProductSearchModel} productSearch Product search object
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition Refinement
 *     definition for which we wish to retrieve refinement values for
 * @param {dw.util.Collection.<dw.catalog.ProductSearchRefinementValue>} refinementValues -
 *     Collection of refinement values
 * @returns {Array} List of refinement values
 */
function get(productSearch, refinementDefinition, refinementValues) {
    var Model = getAttributeRefinementValueModel(refinementDefinition);

    if (refinementDefinition.categoryRefinement) {
        if (productSearch.categorySearch) {
            // return only current category, direct children and direct parent
            return createCategorySearchRefinement(
                productSearch,
                refinementDefinition,
                refinementValues,
                Model
            );
        }

        return createProductSearchRefinement(
            productSearch,
            refinementDefinition,
            refinementValues,
            Model
        );
    }

    return collections.map(refinementValues, function (value) {
        return new Model(productSearch, refinementDefinition, value);
    });
}

module.exports = {
    get: get,
    getAttributeRefinementValueModel: getAttributeRefinementValueModel,
    createProductSearchRefinement: createProductSearchRefinement,
    createCategorySearchRefinement: createCategorySearchRefinement
};
