var base = module.superModule;

/**
 * @description Creates the breadcrumbs object
 * @param {dw.catalog.Category} category - category
 * @param {object} product product model
 * @param {Array} breadcrumbs array of breadcrumbs object
 * @returns {Array} an array of breadcrumb objects
 */
function getAllBreadcrumbs(category, product, breadcrumbs) {
    var URLUtils = require('dw/web/URLUtils');
    var Resource = require('dw/web/Resource');

    var apiProduct;
    var apiCategory;

    if (product && product.raw) {
        apiProduct = product.raw;
        apiCategory = apiProduct.variant
            ? apiProduct.masterProduct.primaryCategory
            : apiProduct.primaryCategory;

        breadcrumbs.push({
            htmlValue: apiProduct.name,
            url: URLUtils.https('Product-Show', 'pid', apiProduct.ID)
        });
    } else if (category) {
        apiCategory = category;
    }

    if (apiCategory) {
        breadcrumbs.push({
            htmlValue: apiCategory.displayName,
            url: URLUtils.https('Search-Show', 'cgid', apiCategory.ID)
        });

        if (apiCategory.parent && apiCategory.parent.ID !== 'root') {
            return getAllBreadcrumbs(apiCategory.parent, null, breadcrumbs);
        }
    }

    breadcrumbs.push({
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.https('Home-Show')
    });

    return breadcrumbs;
}

/**
 * @description Renders the Product Details Page
 * @param {object} querystring query string parameters
 * @param {object} reqPageMetaData request pageMetaData object
 * @returns {object} contain information needed to render the product page
 */
function showProductPage(querystring, reqPageMetaData) {
    var URLUtils = require('dw/web/URLUtils');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

    var params = querystring;
    var product = ProductFactory.get(params);
    var addToCartUrl = URLUtils.url('Cart-AddProduct');
    var canonicalUrl = URLUtils.url('Product-Show', 'pid', product.id);
    var breadcrumbs = getAllBreadcrumbs(null, product, []).reverse();
    var template = 'product/productDetails';

    if (product.productType === 'bundle' && !product.template) {
        template = 'product/bundleDetails';
    } else if (product.productType === 'set' && !product.template) {
        template = 'product/setDetails';
    } else if (product.template) {
        template = product.template;
    }

    pageMetaHelper.setPageMetaData(reqPageMetaData, product);
    pageMetaHelper.setPageMetaTags(reqPageMetaData, product);
    var schemaData = require('*/cartridge/scripts/helpers/structuredDataHelper').getProductSchema(product);

    return {
        template: template,
        product: product,
        addToCartUrl: addToCartUrl,
        resources: base.getResources(),
        breadcrumbs: breadcrumbs,
        canonicalUrl: canonicalUrl,
        schemaData: schemaData
    };
}

/**
 * @description Renders the Product Availability
 * @param {object} querystring query string parameters
 * @returns {object} contain information needed to render the product page
 */
function showProductAvailability(querystring) {
    var URLUtils = require('dw/web/URLUtils');
    var ProductMgr = require('dw/catalog/ProductMgr');

    var productAvailability = require('*/cartridge/models/product/productAvailability');

    var apiProduct = ProductMgr.getProduct(querystring.pid);
    var options = {
        quantity: querystring.quantity,
        productType: base.getProductType(apiProduct)
    };
    var product = productAvailability({}, apiProduct, options);


    return {
        template: 'product/components/availabilityInclude',
        product: product,
        addToCartUrl: URLUtils.url('Cart-AddProduct')
    };
}

/**
 * @description Get Color Code
 * @param {string} colorID Color ID
 * @returns {string|null} Color code
 */
function getColorCode(colorID) {
    var CacheMgr = require('dw/system/CacheMgr');

    var cache = CacheMgr.getCache('colorSwatchMapping');
    var colorConfig = cache.get('colorMapping', function () {
        var mappingJSON = require('util/sitepref').getValue('colorSwatchMapJSON');

        return require('util/json').parse(mappingJSON, {});
    });

    return colorConfig[colorID.toLowerCase()] || null;
}

/**
 * @description get product variant attributes by productID
 * @param {string} productId ID of variant product
 * @returns {Array} array of attributes objects with name and value, empty if no attributes available or product is not variant
 */
function getVariationAttributes(productId) {
    var product = require('dw/catalog/ProductMgr').getProduct(productId);

    if (!product || !product.isVariant()) {
        return [];
    }

    var variationModel = product.getVariationModel();
    var variationAttributes = variationModel.getProductVariationAttributes();

    return variationAttributes
        .toArray()
        .map(function (va) {
            var selectedValue = variationModel.getSelectedValue(va);

            return {
                name: va.getDisplayName(),
                value: selectedValue && selectedValue.getDisplayValue()
            };
        }).filter(function (vaAttr) {
            return vaAttr.value;
        });
}

module.exports = base;
module.exports.getAllBreadcrumbs = getAllBreadcrumbs;
module.exports.showProductPage = showProductPage;
module.exports.showProductAvailability = showProductAvailability;
module.exports.getColorCode = getColorCode;
module.exports.getVariationAttributes = getVariationAttributes;
