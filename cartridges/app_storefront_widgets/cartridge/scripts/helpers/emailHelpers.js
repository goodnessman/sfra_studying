var baseEmailHelper = module.superModule;

baseEmailHelper.emailTypesInternal = {
    registration: 'registration',
    passwordReset: 'passwordReset',
    passwordChanged: 'passwordChanged',
    orderConfirmation: 'orderConfirmation',
    accountLocked: 'accountLocked',
    accountEdited: 'accountEdited'
};

/**
 * @description Meta of the mail page
 * @param {string} type - type of the mail
 * @param {object} args - string for insertions in message
 * @returns {{preheader: string, theme: string}}
 */
baseEmailHelper.getMailMeta = function (type, args) {
    var Resource = require('dw/web/Resource');

    return {
        theme: Resource.msg(type + '.theme', 'mails', ''),
        preheader: Resource.msgf(type + '.preheader', 'mails', '', args)
    };
};

/**
 * @description Get email type based on `emailTypes` variable.
 * @param {number} type - SFRA original email type
 */
baseEmailHelper.getEmailType = function (type) {
    var currentEmailType = '';
    Object.keys(baseEmailHelper.emailTypes).some(function (emailType) {
        if (baseEmailHelper.emailTypes[emailType] === type) {
            currentEmailType = baseEmailHelper.emailTypesInternal[emailType];
            return true;
        }
        return false;
    });
    return currentEmailType;
};

/**
 * @description Get User email metadata for templates
 * @param {object} context - Email data container
 * @returns {string} - User first name
 */
baseEmailHelper.getUserEmailMeta = function (context) {
    return context.firstName || '';
};

/**
 * @description Get Order email metadata for templates
 * @param {object} context - Email data container
 * @returns {dw.util.ArrayList} - Container with order data
 */
baseEmailHelper.getOrderEmailMeta = function (context) {
    if (!context.order) {
        return null;
    }

    var ArrayList = require('dw/util/ArrayList');
    var metaArgs = new ArrayList();
    metaArgs.push(context.order.orderNumber || '');

    var Calendar = require('dw/util/Calendar');
    var StringUtils = require('dw/util/StringUtils');
    var orderCreationDate = new Calendar(context.order.creationDate);
    metaArgs.push(StringUtils.formatCalendar(orderCreationDate) || '');

    return metaArgs;
};

/**
 * @description Get User related email types
 * @returns {array} - User email types
 */
baseEmailHelper.getUserEmails = function () {
    return [
        baseEmailHelper.emailTypes.registration,
        baseEmailHelper.emailTypes.passwordReset,
        baseEmailHelper.emailTypes.passwordChanged,
        baseEmailHelper.emailTypes.accountLocked,
        baseEmailHelper.emailTypes.accountEdited
    ];
};

/**
 * @description Get Order related email types
 * @returns {array} - Order email types
 */
baseEmailHelper.getOrderEmails = function () {
    return [baseEmailHelper.emailTypes.orderConfirmation];
};

/**
 * @description Get meta additional arguments for rendering context
 * @param {number} type - SFRA original email type
 */
baseEmailHelper.getMetaArgs = function (type, context) {
    var metaArgs = null;
    var userEmails = baseEmailHelper.getUserEmails();
    var orderEmails = baseEmailHelper.getOrderEmails();

    if (userEmails.indexOf(type) > -1) {
        metaArgs = baseEmailHelper.getUserEmailMeta(context);
    } else if (orderEmails.indexOf(type) > -1) {
        metaArgs = baseEmailHelper.getOrderEmailMeta(context);
    }

    return metaArgs;
};

/**
 * @description Helper that sends an email to a customer. This will only get called if hook handler is not registered
 * SFRA functionality was extended with additional email meta information (with subject etc)
 * @param {obj} emailObj - An object that contains information about email that will be sent
 * @param {string} emailObj.to - Email address to send the message to (required)
 * @param {string} emailObj.subject - Subject of the message to be sent (required)
 * @param {string} emailObj.from - Email address to be used as a "from" address in the email (required)
 * @param {int} emailObj.type - Integer that specifies the type of the email being sent out. See export from emailHelpers for values.
 * @param {string} template - Location of the ISML template to be rendered in the email.
 * @param {obj} context - Object with context to be passed as pdict into ISML template.
 */
function send(emailObj, template, context) {
    var Mail = require('dw/net/Mail');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    var metaArgs = baseEmailHelper.getMetaArgs(emailObj.type, context);
    var emailType = baseEmailHelper.getEmailType(emailObj.type);
    var emailMeta = baseEmailHelper.getMailMeta(emailType, metaArgs);
    context.meta = emailMeta;

    var email = new Mail();
    email.addTo(emailObj.to);
    email.setSubject(emailMeta.theme);
    email.setFrom(emailObj.from);
    email.setContent(renderTemplateHelper.getRenderedHtml(context, template), 'text/html', 'UTF-8');
    email.send();
}

module.exports = baseEmailHelper;
module.exports.send = send;
module.exports.sendEmail = function (emailObj, template, context) {
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    return hooksHelper('app.customer.email', 'sendEmail', [emailObj, template, context], send);
};
