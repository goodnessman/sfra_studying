var URLUtils = require('dw/web/URLUtils');

var structuredDataBase = module.superModule;

var currentSite = require('dw/system/Site').getCurrent();

/**
 * @description Get general WebSite Schema
 */
function getWebSiteSchema() {
    var schema = {
        '@context': 'http://schema.org/',
        '@type': 'WebSite',
        name: currentSite.name,
        url: URLUtils.home().toString()
    };

    return schema;
}

/**
 * @description Get Organization Schema
 */
function getOrganization() {
    var schema = {
        '@context': 'http://schema.org/',
        '@type': 'Organization',
        name: currentSite.name,
        url: URLUtils.home().toString()
    };

    return schema;
}

/**
 * @description Get Search Schema
 */
function getSearch() {
    var schema = {
        '@context': 'http://schema.org/',
        '@type': 'SearchAction',
        'query-input': 'required name=search_term',
        target: URLUtils.abs('Search-Show').toString() + '?q={search_term}'
    };

    return schema;
}

/**
 * @description Get Breadcrumb List Schema
 * @param {dw.util.List} breadcrumbs Breadcrumbs object
 * @returns {Object} JSON LD Object
 */
function getBreadcrumbListSchema(breadcrumbs) {
    var List = require('dw/util/List');
    var breadcrumbList = breadcrumbs instanceof List
        ? breadcrumbs.toArray()
        : breadcrumbs;

    var schema = {
        '@context': 'http://schema.org/',
        '@type': 'BreadcrumbList',
        itemListElement: (breadcrumbList || []).map(function (breadcrumb, index) {
            return {
                '@type': 'ListItem',
                item: breadcrumb.url.toString(),
                name: breadcrumb.htmlValue || '',
                position: index + 1
            };
        })
    };

    return schema;
}


module.exports = structuredDataBase;

module.exports.getWebSiteSchema = getWebSiteSchema;
module.exports.getSearch = getSearch;
module.exports.getOrganization = getOrganization;
module.exports.getBreadcrumbListSchema = getBreadcrumbListSchema;
