var base = module.superModule;

var originalValidateProducts = base.validateProducts;

/**
 * Added check if cart has not enough inventory, than mark result as error.
 * This will prevent for ex. customer to move to checkout, without full cart inventory.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {Object} validation result object
 */
function validateProducts(basket) {
    var result = originalValidateProducts(basket);
    if (!result.hasInventory) {
        result.error = true;
    }

    if (require('util/test').forceNegativeCase('basket_invalid')) {
        result.error = true;
    }

    return result;
}

module.exports = base;
module.exports.validateProducts = validateProducts;
