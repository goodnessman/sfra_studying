var base = module.superModule;

var originalUpdateAddressFields = base.updateAddressFields;

/**
 * Additionally sets state into empty value (for GB state(county) is not mandatory)
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {*} address - Address to copy from
 */
function updateAddressFields(newAddress, address) {
    originalUpdateAddressFields(newAddress, address);
    newAddress.setStateCode(require('util/object').get(address, 'states.stateCode') || '');
}

module.exports = base;
module.exports.updateAddressFields = updateAddressFields;
