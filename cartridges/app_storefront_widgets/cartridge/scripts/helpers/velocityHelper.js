/* eslint-disable max-len */
var Velocity = require('dw/template/Velocity');

/**
 * @module velocityHelper
 * @description Helper to work with Velocity template
 * @see [Velocity documentation]{@link https://documentation.b2c.commercecloud.salesforce.com/DOC1/topic/com.demandware.dochelp/SiteDevelopment/RenderingVelocityTemplates.html}
 */

/**
 * @description render velocity template
 * @param {string} markup - velocity markup to be rendered
 * @param {Object} [contextObject] optional context object that
 * will be available in velocity template via $object
 * @returns {string} rendered velocity template
 */
function render(markup, contextObject) {
    var StringWriter = require('dw/io/StringWriter');
    var writer = new StringWriter();
    var velocityScope = this.getScope(contextObject);

    Velocity.render(markup, velocityScope, writer);
    return writer.toString();
}


/**
 * @typedef {Object} ScopeObject
 * @property {dw.template.Velocity} velocity - add velocity object to scope.
 * Adds ability execute remote includes.
 * See [documentation]{@link https://documentation.b2c.commercecloud.salesforce.com/DOC1/topic/com.demandware.dochelp/DWAPI/scriptapi/html/api/class_dw_template_Velocity.html}
 * @property {Object} tools - Object with tools methods. See {@link module:velocityHelper~ToolsObject}
 * @property {Object} object - Context Object that available in Velocity template via $object.
 * Used to pass some dynamic data to velocity template
 */

/**
 * @description Get scope for velocity context
 * @param {Object} [contextObject] optional context object that
 * will be available in velocity template via $object
 * @returns {module:velocityHelper~ScopeObject} velocity scope object
 */
function getScope(contextObject) {
    var velocityScope = {
        velocity: Velocity,
        tools: this.getTools(contextObject),
        content: this.getContentUtils(contextObject),
        object: contextObject || null
    };

    return velocityScope;
}

/**
 * @typedef {Object} ToolsObject
 * @property {function} empty - check if passed data is empty
 * @property {function} includeAsset - local asset include
 * @property {function} includeIsml - render some ISML
 * @property {function} getCurrentYear - return current year as four digits. Used in copyright asset
 */

/**
 * @description Get velocity tools.<br>
 * Velocity tools it is set of methods that provide additional functionality inside velocity template
 * @see {@link module:velocityHelper~ToolsObject}
 * @param {Object} [contextObject] optional context object can be used as context for includeAsset and includeIsml
 * @returns {module:velocityHelper~ToolsObject} velocity tools object
 */
function getTools(contextObject) {
    var tools = {
        empty: function (data) {
            return empty(data);
        },
        includeAsset: function (assetId) {
            var ContentModel = require('*/cartridge/models/content');
            var content = new ContentModel(assetId);
            return content.getMarkup(contextObject);
        },
        includeIsml: function (templatePath) {
            var renderHelper = require('*/cartridge/scripts/renderTemplateHelper');
            return renderHelper.getRenderedHtml(contextObject, templatePath);
        },
        getCurrentYear: function () {
            var Site = require('dw/system/Site');
            var StringUtils = require('dw/util/StringUtils');
            var calendar = Site.getCalendar();

            return StringUtils.formatCalendar(calendar, 'YYYY');
        },
        bindStyles: function () {
            var mails = require('*/cartridge/scripts/mails');
            var output = mails.getStyle.apply(null, arguments);
            return mails.isUseLocal
                ? 'class="' + output + '"'
                : 'style="' + output + '"';
        }
    };

    return tools;
}

/**
 * @typedef {Object} ContentUtilsObject
 * @property {function} renderSubFolders - renders subfolders for current folder
 */

/**
 * @description Get content utils.<br>
 * Velocity content utils it is set of methods that provide additional functionality for content page velocity template
 * @see {@link module:velocityHelper~ContentUtilsObject}
 * @param {Object} [contextObject] object created with content model
 * @returns {module:velocityHelper~ContentUtilsObject} velocity content utils
 */

function getContentUtils(contextObject) {
    return {
        renderSubFolders: function (params) {
            var collections = require('*/cartridge/scripts/util/collections');

            if (!contextObject || !contextObject.isFolder) {
                return '';
            }

            var folder = contextObject.raw;
            var includeSecondLevel = params
                ? !!params.includeSecondLevel
                : false;
            var subFolders = folder.onlineSubFolders;
            var subFoldersContent;
            if (empty(subFolders)) {
                subFoldersContent = collections.map(folder.onlineContent, function (subFolder) {
                    return contextObject.getContentNavElement(subFolder, []);
                });
            } else {
                subFoldersContent = collections.map(subFolders, function (subFolder) {
                    return contextObject.getFolderNavElement(subFolder, null, includeSecondLevel);
                });
            }

            return require('*/cartridge/scripts/renderTemplateHelper').getRenderedHtml(
                {
                    subFolders: subFoldersContent
                },
                'components/content/subFolders'
            );
        }

    };
}

module.exports = {
    render: render,
    getScope: getScope,
    getTools: getTools,
    getContentUtils: getContentUtils
};
