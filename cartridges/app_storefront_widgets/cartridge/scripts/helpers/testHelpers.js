/**
 * @description Checks if current instance is production
 * @returns {boolean} result
 */
function isProduction() {
    var System = require('dw/system/System');

    return System.getInstanceType() === System.PRODUCTION_SYSTEM;
}

/**
 * @description Returns result of negative case force
 * @param {string} negativeCase - negative case type
 * @returns {boolean} result
 */
function forceNegativeCase(negativeCase) {
    if (isProduction()) {
        return false;
    }

    var forcedCase;
    var httpCookies = request.getHttpCookies();

    switch (negativeCase) {
        case 'no_shipping_address':
            forcedCase = httpCookies.force_no_shipping_address;
            break;
        case 'wrong_shipping_address':
            forcedCase = httpCookies.force_wrong_shipping_address;
            break;
        case 'gift_message_not_saved':
            forcedCase = httpCookies.force_gift_message_not_saved;
            break;
        case 'no_billing_address':
            forcedCase = httpCookies.force_no_billing_address;
            break;
        case 'selected_payment_unsupported':
            forcedCase = httpCookies.force_selected_payment_unsupported;
            break;
        case 'selected_payment_unapplicable':
            forcedCase = httpCookies.force_selected_payment_unapplicable;
            break;
        case 'payment_processor_error':
            forcedCase = httpCookies.force_payment_processor_error;
            break;
        case 'payment_reject':
            forcedCase = httpCookies.force_payment_reject;
            break;
        case 'general_error':
            forcedCase = httpCookies.force_general_error;
            break;
        case 'unable_place_order':
            forcedCase = httpCookies.force_unable_place_order;
            break;
        case 'unable_create_order':
            forcedCase = httpCookies.force_unable_create_order;
            break;
        case 'unable_calculate_totals':
            forcedCase = httpCookies.force_unable_calculate_totals;
            break;
        case 'basket_invalid':
            forcedCase = httpCookies.force_basket_invalid;
            break;
        default:
            break;
    }

    return Boolean(forcedCase);
}

module.exports = {
    forceNegativeCase: forceNegativeCase
};
