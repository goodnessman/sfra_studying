var Resource = require('dw/web/Resource');

exports.subscribe = function (email) {
    var subscriptionResult = {
        error: false
    };

    if (email.indexOf('error') > -1) {
        subscriptionResult = {
            error: true,
            msg: Resource.msg('form.emailsubscribe.email.error', 'forms', null)
        };
    }

    return subscriptionResult;
};
