/**
 * @description Verifies the required information for billing form is provided.
 * @param {Object} req - The request object
 * @param {Object} paymentForm - the payment form
 * @param {Object} viewFormData - object contains billing form data
 * @returns {Object} an object that has error information or payment information
 */
function processForm(req, paymentForm, viewFormData) {
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var viewData = viewFormData;
    var creditCardErrors = {};

    if (!paymentForm.creditCardFields.selectedCardID.value) {
        // verify credit card form data
        creditCardErrors = COHelpers.validateCreditCard(paymentForm);
    } else {
        viewData.storedPaymentUUID = paymentForm.creditCardFields.selectedCardID.value;
    }

    if (Object.keys(creditCardErrors).length) {
        return {
            fieldErrors: creditCardErrors,
            error: true
        };
    }

    viewData.paymentMethod = {
        value: paymentForm.paymentMethod.value,
        htmlName: paymentForm.paymentMethod.value
    };

    viewData.paymentInformation = {
        cardType: {
            value: paymentForm.creditCardFields.cardType.value,
            htmlName: paymentForm.creditCardFields.cardType.htmlName
        },
        cardNumber: {
            value: paymentForm.creditCardFields.cardNumber.value,
            htmlName: paymentForm.creditCardFields.cardNumber.htmlName
        },
        securityCode: {
            value: paymentForm.creditCardFields.securityCode.value,
            htmlName: paymentForm.creditCardFields.securityCode.htmlName
        },
        expirationMonth: {
            value: parseInt(
                paymentForm.creditCardFields.expirationMonth.selectedOption,
                10
            ),
            htmlName: paymentForm.creditCardFields.expirationMonth.htmlName
        },
        expirationYear: {
            value: parseInt(paymentForm.creditCardFields.expirationYear.value, 10),
            htmlName: paymentForm.creditCardFields.expirationYear.htmlName
        }
    };

    viewData.saveCard = paymentForm.creditCardFields.saveCard.checked;

    return {
        error: false,
        viewData: viewData
    };
}

function createPaymentToken() {
    return {
        subscriptionID: '',
        error: false
    };
}

exports.processForm = processForm;
exports.CreatePaymentToken = createPaymentToken;
