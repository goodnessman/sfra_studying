/**
 * @description Verifies the required information for billing form is provided.
 * @param {object} req - The request object
 * @param {object} paymentForm - the payment form
 * @param {object} viewFormData - object contains billing form data
 * @returns {object} an object that has error information or payment information
 */
function processForm(req, paymentForm, viewFormData) {
    var viewData = viewFormData;

    viewData.paymentMethod = {
        value: paymentForm.paymentMethod.value,
        htmlName: paymentForm.paymentMethod.value
    };

    return {
        error: false,
        viewData: viewData
    };
}

exports.processForm = processForm;
