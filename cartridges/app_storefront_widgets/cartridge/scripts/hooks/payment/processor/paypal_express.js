var collections = require('*/cartridge/scripts/util/collections');

/**
 * @description Verifies that selected payment is valid and then payment instrument creates for the basket
 * @param {dw.order.Basket} basket Current users's basket
 * @returns {object} returns an error object
 */
module.exports.Handle = function (basket) {
    var Transaction = require('dw/system/Transaction');
    var currentBasket = basket;
    var serverErrors = [];

    Transaction.wrap(function () {
        var paymentInstruments = currentBasket.getPaymentInstruments();

        collections.forEach(paymentInstruments, function (item) {
            currentBasket.removePaymentInstrument(item);
        });

        currentBasket.createPaymentInstrument(
            'PAYPAL_EXPRESS', currentBasket.totalGrossPrice
        );
    });

    return {
        fieldErrors: [],
        serverErrors: serverErrors,
        error: false
    };
};


/**
 * @description Authorizes a payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @returns {object} returns an error object
 */
module.exports.Authorize = function (orderNumber, paymentInstrument, paymentProcessor) {
    var Logger = require('dw/system/Logger');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var serverErrors = [];
    var fieldErrors = {};
    var error = false;

    if (require('util/test').forceNegativeCase('payment_processor_error')) {
        return {
            fieldErrors: fieldErrors,
            serverErrors: [Resource.msg('error.payment', 'checkout', null)],
            error: error
        };
    }

    if (require('util/test').forceNegativeCase('payment_reject')) {
        return {
            fieldErrors: fieldErrors,
            serverErrors: [Resource.msg('error.payment.rejected', 'checkout', null)],
            error: error
        };
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });
    } catch (err) {
        Logger.error('Unable to Authorize zero payment: ' + err.message);

        error = true;
        serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }

    return {
        fieldErrors: fieldErrors,
        serverErrors: serverErrors,
        error: error
    };
};
