var base = module.superModule;

function createPaymentToken() {
    return {
        error: false,
        subscriptionID: base.createToken()
    };
}

/**
 * @description Authorizes a payment using a credit card.
 * Customizations may use other processors and custom logic to authorize credit card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @returns {object} returns an error object
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor) {
    var Logger = require('dw/system/Logger');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var serverErrors = [];
    var fieldErrors = {};
    var error = false;

    if (require('util/test').forceNegativeCase('payment_processor_error')) {
        return {
            fieldErrors: fieldErrors,
            serverErrors: [Resource.msg('error.payment', 'checkout', null)],
            error: error
        };
    }

    if (require('util/test').forceNegativeCase('payment_reject')) {
        return {
            fieldErrors: fieldErrors,
            serverErrors: [Resource.msg('error.payment.rejected', 'checkout', null)],
            error: error
        };
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });
    } catch (err) {
        Logger.error('Unable to Authorize basic payment: ' + err.message);

        error = true;
        serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }

    return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
}

module.exports = base;
module.exports.CreatePaymentToken = createPaymentToken;
module.exports.Authorize = Authorize;
