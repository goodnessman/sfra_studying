// TO BE REVIEWED
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');
var System = require('dw/system/System');
var Resource = require('dw/web/Resource');
var customPrefs = Site.current.preferences.custom;
var isProduction = System.getInstanceType() === System.PRODUCTION_SYSTEM;
var styles = [];
var scripts = [];
var prefetch = [];
var preload = [];
var integrity = {};
var useLocalEnabled = !isProduction && (request.httpParameters.containsKey('useLocal') || request.httpCookies.useLocal);
var localHost;
var isSRIEnabled = 'enableSubresourceIntegrity' in customPrefs && !!customPrefs.enableSubresourceIntegrity;
var httpProtocolRegex = /((http(s)?:)?\/\/)/;
var cssRegex = /(\S*)\.css/;

if (useLocalEnabled) {
    var devServerConfig = require('../../devServerConfig');

    localHost = 'https://' + [devServerConfig.host, devServerConfig.port].join(':');
}

var assets = {
    contexts: [],
    getContexts: function () {
        return this.contexts;
    },
    staticURL: function (src) {
        return localHost ? localHost + src : URLUtils.staticURL(src).toString();
    },
    add: function (src, sources) {
        if (!src) {
            return;
        }

        var source;
        var isCss = cssRegex.test(src);
        var sourceArray = sources || (isCss ? styles : scripts);

        if (httpProtocolRegex.test(src) && sourceArray.lastIndexOf(src) < 0) {
            source = src;
        } else if (sourceArray.lastIndexOf(URLUtils.staticURL(src).toString()) < 0) {
            source = this.staticURL(src);

            if (useLocalEnabled) {
                if (isCss) {
                    scripts.push(source.replace('.css', '.js')); // for webpack HMR
                }
            } else {
                this.addIntegrity(src, source);
            }
        }

        sourceArray.push(source);
    },
    addCss: function (src) { // for SFRA compatibility usage
        this.add(src);
    },
    addJs: function (src) { // for SFRA compatibility usage
        this.add(src);
    },
    addContext: function (contextId) {
        var self = this;

        Resource.msg(contextId, '_assets', '').split(',').forEach(function (el) {
            self.add(el);
        }); // add scripts/styles
        Resource.msg(contextId + '.prefetch', '_assets', '').split(',').forEach(function (el) {
            self.add(el, prefetch);
        }); // add prefetching
        Resource.msg(contextId + '.preload', '_assets', '').split(',').forEach(function (el) {
            self.add(el, preload);
        }); // add preloads

        this.contexts.push(contextId);
    },
    addIntegrity: function (src, url) {
        if (!isSRIEnabled) {
            return;
        }

        var fileNameMatch = src.match(/\/?(.*?)$/);
        if (fileNameMatch && fileNameMatch[1]) {
            var fileName = fileNameMatch[1];
            var integrityHash = Resource.msg('file.' + fileName.replace('~', '.'), 'integrity', '');

            if (integrityHash) {
                integrity[url] = integrityHash;
            }
        }
    },
    getIntegrity: function (src) {
        return integrity[src] || false;
    },
    getPrefetches: function () {
        return prefetch.map(function (src) {
            var isCss = cssRegex.test(src);

            return {
                src: src,
                type: isCss ? 'style' : 'script'
            };
        });
    },
    getPreloads: function () {
        return preload.map(function (src) {
            var isCss = cssRegex.test(src);

            return {
                src: src,
                type: isCss ? 'style' : 'script'
            };
        });
    },
    scripts: scripts,
    styles: styles
};

assets.addContext('global');

module.exports = assets;
