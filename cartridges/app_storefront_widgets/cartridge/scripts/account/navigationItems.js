/**
 * Generate array with object that contains property for rendering MA Navigation
 *
 * @returns {object} items
 */
function getNavigationItems() {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');
    var isWishListEnable = require('util/pref').is('wishlist.enable');

    var items = [
        {
            url: URLUtils.https('Account-Show').toString(),
            title: Resource.msg('navigation.title', 'account', null),
            actions: ['Account-Show']
        },
        {
            url: URLUtils.https('Account-EditProfile').toString(),
            title: Resource.msg('navigation.profiledetails', 'account', null),
            actions: ['Account-EditProfile']
        },
        {
            url: URLUtils.https('Account-EditPassword').toString(),
            title: Resource.msg('navigation.changepassword', 'account', null),
            actions: ['Account-EditPassword']
        },
        {
            url: URLUtils.https('Order-History').toString(),
            title: Resource.msg('navigation.orderhistory', 'account', null),
            actions: ['Order-History', 'Order-Details']
        },
        {
            url: URLUtils.https('Address-List').toString(),
            title: Resource.msg('navigation.addressbook', 'account', null),
            actions: ['Address-List', 'Address-AddAddress', 'Address-EditAddress']
        }
    ];

    if (isWishListEnable) {
        items.push({
            url: URLUtils.https('Wishlist-Show').toString(),
            title: Resource.msg('navigation.wishlist', 'account', null),
            actions: ['Wishlist-Show']
        });
    }
    return items;
}

module.exports = getNavigationItems;
