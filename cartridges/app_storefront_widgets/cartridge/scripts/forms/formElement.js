/* eslint-disable no-underscore-dangle */
/* eslint-disable dot-notation */

const ObjectUtils = require('util/object');
const TemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
const secureEncoder = require('dw/util/SecureEncoder');

/**
 * @constructor
 * @param {Object} field
 * @param {Object} options
 */
function FormElement(field, options) {
    if (!field) {
        throw new Error('FormElement: field is required parameter');
    }

    const formInfo = this._parseFormStr(field.htmlName);
    const formName = options.formName || formInfo.formName;
    const formConfig = {};
    const dynamic = require('*/cartridge/scripts/forms/dynamic/forms').getFormFieldConfig(field, options.countryCode);

    this.field = field;
    this.formName = formName;
    this.config = ObjectUtils.deepAssign({}, formConfig[formInfo.fieldPath] || {}, dynamic || {}, options);
    this._setupTemplateContext(options, dynamic);

    if (FormElement.possibleElements.indexOf(this.templateContext.element.type) === -1) {
        throw new Error('FormElement: Element type must be one of ' + FormElement.possibleElements);
    }

    this.templateName = this._getTemplateName(this.templateContext.element.type);
}

FormElement.Type = {
    COLOR: 'color',
    CHECKBOX: 'checkbox',
    DATE: 'date',
    DATETIME: 'datetime',
    DATETIMELOCAL: 'datetime-local',
    EMAIL: 'email',
    HIDDEN: 'hidden',
    MONTH: 'month',
    NUMBER: 'number',
    PASSWORD: 'password',
    RADIO: 'radio',
    RANGE: 'range',
    SEARCH: 'search',
    TEL: 'tel',
    PHONE: 'phone',
    TEXT: 'text',
    TIME: 'time',
    URL: 'url',
    WEEK: 'week',
    SELECT: 'select',
    TEXTAREA: 'textarea'
};
FormElement.possibleElements = ObjectUtils.values(FormElement.Type);

/**
 * Returns correct widget instance name
 * @param {string} elementType - element type
 */
function getWidgetType(elementType) {
    return 'input' + (elementType.charAt(0).toUpperCase() + elementType.slice(1));
}

/**
 * Restores attributes object from field for further deep assign
 * @param {FormField} field
 */
function getFieldAttributesObject(field) {
    var attributesObject = {};
    attributesObject['name'] = field.htmlName;

    if (field.mandatory) {
        attributesObject['required'] = '';
    }
    if (field.options && field.options.optionsCount > 0) {
        return attributesObject;
    }

    if (field.type === field.FIELD_TYPE_BOOLEAN && field.checked) {
        attributesObject['checked'] = field.checked;
    }

    var value = field.htmlValue == null ? '' : field.htmlValue;
    attributesObject['value'] = secureEncoder.forHtmlInDoubleQuoteAttribute(value);

    if (field.maxValue) {
        attributesObject['max'] = field.maxValue;
    }
    if (field.minValue) {
        attributesObject['min'] = field.minValue;
    }
    if (field.maxLength) {
        attributesObject['maxLength'] = field.maxLength;
    }
    if (field.minLength) {
        attributesObject['minLength'] = field.minLength;
    }
    if (field.regEx) {
        attributesObject['pattern'] = field.regEx;
    }

    return attributesObject;
}

FormElement.prototype = {
    constructor: FormElement,

    /**
     * @param {string} key
     * @returns {FormElement}
     */
    getData: function (key) {
        return ObjectUtils.get(this.templateContext, key);
    },

    /**
     * @param {string} key
     * @param {*} value
     * @returns {FormElement}
     */
    setData: function (key, value) {
        ObjectUtils.set(this.templateContext, key, value);

        return this;
    },

    /**
     * @returns {string}
     */
    render: function () {
        return TemplateHelper.getRenderedHtml(
            this._getContextForRender(),
            this.templateName
        );
    },

    /**
     * @param {Object} attributesObject
     * @param {Object} dynamic
     */
    _refillAttributesFromDynamicConfig: function (attributesObject, dynamic) {
        const dynamicElement = ObjectUtils.get(dynamic, 'element');
        if (!dynamicElement) {
            return attributesObject;
        }

        if (dynamicElement.required && !dynamicElement.optional) {
            attributesObject['required'] = '';
            attributesObject['aria-required'] = 'true';
        }

        if (dynamicElement.maxLength) {
            attributesObject['maxLength'] = dynamicElement.maxLength;
        }

        if (dynamicElement.minLength) {
            attributesObject['minLength'] = dynamicElement.minLength;
        }

        return attributesObject;
    },

    _setupTemplateContext: function (options, dynamic) {
        const field = this.field;
        const config = this.config;
        const fieldAttributes = getFieldAttributesObject(field);
        const refilledAttributes = this._refillAttributesFromDynamicConfig(
            fieldAttributes, dynamic
        );

        let fieldType = 'text';
        if (field.options && field.options.length) {
            fieldType = 'select';
        }

        this.templateContext = ObjectUtils.deepAssign({
            field: field,
            widget: {
                classes: ''
            },
            validate: true,
            wrapper: {
                classes: '',
                attributes: {}
            },
            label: {
                show: true,
                text: field.label || ''
            },
            labelDescription: {
                show: false,
                text: field.labelDescription || ''
            },
            element: {
                type: fieldType,
                classes: '',
                required: field.mandatory,
                disabled: false,
                optional: false,
                optionalText: '',
                attributes: refilledAttributes,
                readonly: false,
                placeholder: ''
            },
            caption: {
                show: false,
                content: '',
                text: ''
            },
            error: {
                show: true,
                text: ''
            },
            localization: {
                countryCode: null
            }
        }, config);

        var widgetType = this.templateContext.widget.type;
        widgetType = widgetType || getWidgetType(this.templateContext.element.type);

        this.templateContext.widget.type = widgetType;
    },

    /**
     * @param {string} str
     * @returns {Object}
     */
    _parseFormStr: function (str) {
        const formPrefix = 'dwfrm';
        const parts = str.split('_');

        if (parts[0] !== formPrefix) {
            throw new TypeError('FormElement: Can not detect form name from "field.htmlName"');
        }

        return {
            formName: parts[1],
            fieldPath: parts.slice(2).join('.')
        };
    },

    /**
     * @param {Object} obj
     * @returns {string}
     */
    _getAttributesStr: function (obj) {
        const SecureEncoder = require('dw/util/SecureEncoder');
        let result = [];

        Object.keys(obj || {}).forEach(function (key) {
            const value = obj[key];
            const delimiter = '\'';
            const attribute = value
                ? key + '=' + delimiter + SecureEncoder.forHtmlInSingleQuoteAttribute(value) + delimiter
                : key;

            result.push(attribute);
        });

        return result.join(' ');
    },

    /**
     * @return {Object}
     */
    _getContextForRender: function () {
        const context = ObjectUtils.assign({}, this.templateContext);

        context.wrapper.attributes = this._getAttributesStr(context.wrapper.attributes);
        context.element.attributes = this._getAttributesStr(context.element.attributes);
        context.widget.attributes = this._getAttributesStr(context.widget.attributes);

        return context;
    },

    /**
     * @param {string} type
     * @return {string}
     */
    _getTemplateName: function (type) {
        switch (type) {
            default:
                return 'forms/formElement';
        }
    }
};

/**
 * @param {Object} field
 * @param {Object} [options]
 * @returns {FormElement}
 */
module.exports = function (field, options) {
    if (!field) {
        throw new TypeError('FormElement: Field is mandatory parameter');
    }

    return new FormElement(field, options || {});
};
