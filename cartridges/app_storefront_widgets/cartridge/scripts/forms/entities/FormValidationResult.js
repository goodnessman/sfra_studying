/**
 *
 * @param {boolean} valid
 * @param {Object} fieldErrors
 */
function FormValidationResult(valid, fieldErrors) {
    this.valid = !!valid;
    this.fieldErrors = fieldErrors || {};
}

FormValidationResult.prototype.add = function (field, fieldValiadtionResult) {
    if (!fieldValiadtionResult.valid) {
        this.fieldErrors[field.htmlName] = fieldValiadtionResult.error;

        if (this.valid) {
            this.valid = false;
        }
    }
};

FormValidationResult.prototype.addGroup = function (formGroupValidationResult) {
    if (!formGroupValidationResult.valid) {
        var objectUtils = require('util/object');

        objectUtils.deepAssign(this.fieldErrors, formGroupValidationResult.fieldErrors);

        if (this.valid) {
            this.valid = false;
        }
    }
};

module.exports = FormValidationResult;
