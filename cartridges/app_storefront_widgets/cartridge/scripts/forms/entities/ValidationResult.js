/**
 * @param {boolean} valid
 * @param {string} error
 */
function ValidationResult(valid, error) {
    this.valid = !!valid;
    this.error = error || null;
}

module.exports = ValidationResult;
