/* eslint-disable no-undef */

/**
 *
 * @param {*} valueToValidate
 * @param {*} validationSchema
 * @returns {Object}
 */
function validate(valueToValidate, validationSchema) {
    var FieldValidationResult = require('*/cartridge/scripts/forms/entities/ValidationResult');
    var value = (valueToValidate || '').toString().trim();
    var isEmptyValue = value === '';
    var {
        mandatory,
        minLength,
        maxLength,
        errors,
        patterns,
        compareWith
    } = validationSchema;
    var error = null;

    if (isEmptyValue) {
        if (mandatory) {
            error = errors.required;
        } else {
            return new FieldValidationResult(true);
        }
    } else if (minLength && minLength > value.length) {
        error = errors.minLength;
    } else if (maxLength && value.length > maxLength) {
        error = errors.maxLength;
    } else if (patterns) {
        var sortedPatternKeys = Object.keys(patterns).sort(function (pattern) {
            return pattern === 'security' ? -1 : 0;
        });

        for (let i = 0; i < sortedPatternKeys.length; i++) {
            var regExp = new RegExp(patterns[sortedPatternKeys[i]]);

            if (errors[sortedPatternKeys[i]] && regExp.test(value) === false) {
                error = errors[sortedPatternKeys[i]];
                break;
            }
        }
    }

    if (compareWith && !empty(compareWith)) {
        var httpParameterMap = request.httpParameterMap;
        var targetFieldValue = (httpParameterMap[compareWith] || '').toString().trim();
        if (targetFieldValue && !empty(targetFieldValue)) {
            if (targetFieldValue !== value) {
                error = errors.compareWith;
            }
        }
    }

    return new FieldValidationResult(
        error === null,
        error
    );
}

exports.validate = function (formField) {
    var objectUtils = require('util/object');
    var dynamicForms = require('*/cartridge/scripts/forms/dynamic/forms');

    var httpParameterMap = request.httpParameterMap;
    var rootForm = formField.parent;
    var countryField = objectUtils.get(rootForm, 'country.htmlName');

    if (empty(countryField)) {
        while (rootForm.parent) {
            rootForm = rootForm.parent;
            countryField = objectUtils.get(rootForm, 'country.htmlName');
            if (!empty(countryField)) {
                break;
            }
        }
    }

    var countryCode = '';
    if (!empty(countryField)) {
        countryCode = (httpParameterMap[countryField] && httpParameterMap[countryField].value) || '';
    }

    var validationSchema = empty(countryCode)
        ? dynamicForms.getFormFieldValidationSchema(formField)
        : dynamicForms.getFormFieldValidationSchema(formField, countryCode);
    var validationResult = validate(formField.value, validationSchema);

    var FormElementValidationResult = require('dw/web/FormElementValidationResult');
    var formElementValidationResult = new FormElementValidationResult(true);

    if (!validationResult.valid) {
        formElementValidationResult.setValid(false);
        formField.invalidateFormElement();
        formElementValidationResult.setMessage(validationResult.error);
    }

    return formElementValidationResult;
};


/**
 * Checks if the email value entered is correct format
 *
 * @param {string} email - email string to check if valid
 * @returns {boolean} Whether email is valid
 */
exports.validateEmail = function (email) {
    var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    return regex.test(email);
};
