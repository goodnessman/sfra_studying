var objectUtils = require('util/object');
/**
 * @description To extend on brand level need to create /cartridge/config/dynamicforms.js on brand level with next code:
 * `
 * var baseDynamicForms = module.superModule;
 * module.exports = require('util/object').deepAssign(baseDynamicForms, {
 *    // Overriden denamic forms configuration
 * });
 * `
 */
var formsJSON = require('*/cartridge/config/dynamicforms');


/**
 *
 * Extends target with multiple source objects
 * in case when first parameter (target) is null, in this case logic create empty object or array,
 * depend of type of first source parameter, and set it to target
 *
 * @param {Object} target
 * @returns {Object}
 */
function extend(target) {
    var source = null;

    if (!target) {
        target = (arguments[1] instanceof Array) ? [] : {};
    }

    for (var i = 1; i < arguments.length; i++) {
        source = arguments[i];
        for (var prop in source) {
            // recurse for non-API objects
            if (source[prop]
                && typeof source[prop] === 'object'
                && !(source instanceof Array)
                && !source[prop].class
            ) {
                target[prop] = this.extend(target[prop], source[prop]);
            } else {
                target[prop] = source[prop];
            }
        }
    }

    return target;
}

/**
 *
 * @param {Object} source
 * @param {Array} fieldID
 * @returns {Object}
 */
function getRawFormFieldConfig(source, fieldID) {
    var pathToProcess = fieldID.split('.').slice(0);
    var nextPathToProcess = [];
    var sourceToProcess = source;

    while (pathToProcess.length > 0) {
        var configPath = pathToProcess.join('.');

        if (sourceToProcess && sourceToProcess[configPath]) {
            sourceToProcess = sourceToProcess[configPath];
            pathToProcess = nextPathToProcess;
            nextPathToProcess = [];
        } else {
            nextPathToProcess.unshift(
                pathToProcess.pop()
            );

            if (pathToProcess.length === 0) {
                sourceToProcess = null;
            }
        }
    }

    return sourceToProcess;
}

/**
 *
 * @param {*} formFieldConfigRaw
 * @returns {Object} formFieldConfigRaw
 */
function getRawBaseFormFieldConfig(formFieldConfigRaw) {
    return empty(formFieldConfigRaw.extends)
        ? null
        : getRawFormFieldConfig(formsJSON, formFieldConfigRaw.extends);
}

/**
 *
 * @param {*} formFieldConfigRaw
 * @param {*} countryCode
 * @returns {Object} formFieldConfigRaw
 */
function getRawLocalizedFormFieldConfig(formFieldConfigRaw, countryCode) {
    return !empty(formFieldConfigRaw)
        ? objectUtils.get(formFieldConfigRaw, 'localization.' + countryCode) || null
        : null;
}

/**
 * Parse passed formfield json config to build dynamic formfield config
 * Supports inheritance and localization features
 *
 * @param {*} formFieldConfigRaw
 * @param {*} countryCode
 * @returns {Object} formFieldConfig
 */
function parseFormFieldConfig(formFieldConfigRaw, countryCode) {
    if (empty(formFieldConfigRaw)) {
        return {};
    }

    var formFieldConfig = Object.keys(formFieldConfigRaw).reduce(function (result, key) {
        if (['extends', 'localization'].indexOf(key) < 0) {
            var propertyValue = formFieldConfigRaw[key];

            if (typeof (formFieldConfigRaw[key]) === 'object') {
                propertyValue = objectUtils.keys(formFieldConfigRaw[key]).reduce(function (childConfig, childKey) {
                    objectUtils.set(childConfig, childKey, formFieldConfigRaw[key][childKey]);

                    return childConfig;
                }, {});
            }

            objectUtils.set(result, key, propertyValue);
        }

        return result;
    }, {});


    var baseFormFieldConfigRaw = getRawBaseFormFieldConfig(formFieldConfigRaw);
    var localizedFormFieldConfigRaw = getRawLocalizedFormFieldConfig(formFieldConfigRaw, countryCode);
    var baseFormFieldConfig = parseFormFieldConfig(baseFormFieldConfigRaw, countryCode);
    var localizedFormFieldConfig = parseFormFieldConfig(localizedFormFieldConfigRaw, countryCode);

    return extend(
        {},
        baseFormFieldConfig,
        formFieldConfig,
        localizedFormFieldConfig
    );
}

/**
 *
 * @param {Object} formField
 * @param {string} countryCode
 * @returns {Object} dynamicFormFieldConfig
 */
exports.getFormFieldConfig = function (formField, countryCode) {
    var formFieldPath = formField.htmlName.split('_');
    var FormFieldConfigResult = require('./entities').FormFieldConfigResult;

    formFieldPath.shift();

    var formFieldConfigRaw = getRawFormFieldConfig(formsJSON.forms, formFieldPath.join('.'));

    if (empty(formFieldConfigRaw)) {
        return null;
    }

    var formFieldConfigParsed = parseFormFieldConfig(formFieldConfigRaw, countryCode);

    return new FormFieldConfigResult(formField, formFieldConfigParsed);
};

/**
 * @param {Object} field
 * @param {string} countryCode
 * @returns {Object} dynamicFormFieldConfig
 */
exports.getFieldConfig = function (field, countryCode) {
    var FormFieldConfigResult = require('./entities').FormFieldConfigResult;
    var fieldConfigRaw = getRawFormFieldConfig(formsJSON.fields, field.id);

    if (empty(fieldConfigRaw)) {
        return null;
    }

    var fieldConfigParsed = parseFormFieldConfig(fieldConfigRaw, countryCode);

    return new FormFieldConfigResult(field, fieldConfigParsed);
};
