/**
 * @returns {string}
 */
function getCustomerCountryCode() {
    return require('dw/util/Locale').getLocale(request.locale).getCountry();
}

/**
 *
 * @param {Object} formField
 * @param {string} countryCode
 * @returns {Object} dynamicFieldConfig
 */
function getFormFieldConfig(formField, countryCode) {
    var formsConfigurationParser = require('./formsConfigurationParser');
    var countryCodeToUse = (countryCode || getCustomerCountryCode()).toLowerCase();

    return formsConfigurationParser.getFormFieldConfig(formField, countryCodeToUse);
}

/**
 *
 * @param {Object} field
 * @param {string} countryCode
 * @returns {Object} dynamicFieldConfig
 */
function getFieldConfig(field, countryCode) {
    var formsConfigurationParser = require('./formsConfigurationParser');
    var countryCodeToUse = (countryCode || getCustomerCountryCode()).toLowerCase();

    return formsConfigurationParser.getFieldConfig(field, countryCodeToUse);
}

/**
 *
 * @param {string} formFieldID
 * @param {string} countryCode
 * @returns {Object} dynamicFieldConfig
 */
function getFormFieldValidationSchema(formFieldID, countryCode) {
    var formFieldConfig = getFormFieldConfig(formFieldID, countryCode);

    return formFieldConfig.validation;
}

/**
 *
 * @param {Object} field
 * @param {string} countryCode
 * @returns {Object} dynamicFieldConfig
 */
function getFieldValidationSchema(field, countryCode) {
    var formFieldConfig = getFieldConfig(field, countryCode);

    return formFieldConfig.validation;
}

exports.getFormFieldConfig = getFormFieldConfig;
exports.getFormFieldValidationSchema = getFormFieldValidationSchema;
exports.getFieldValidationSchema = getFieldValidationSchema;
