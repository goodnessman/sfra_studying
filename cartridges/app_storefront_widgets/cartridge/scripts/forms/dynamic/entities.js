var objectUtils = require('util/object');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

/**
 * @param {Array} formFieldConfig
 * @returns {Object}
 */
function ValidationErrors(formFieldConfig) {
    var minLength = objectUtils.get(formFieldConfig, 'element.minLength');
    var maxLength = objectUtils.get(formFieldConfig, 'element.maxLength');
    var formFieldErrors = formFieldConfig.validation.errors || {};
    var validationErrors = objectUtils.keys(formFieldErrors).reduce(function (accumulator, key) {
        accumulator[key] = Resource.msg(formFieldErrors[key], 'forms', '')
            .replace('{fieldname}', formFieldConfig.label.text);

        return accumulator;
    }, {});

    if (minLength && validationErrors.minLength) {
        validationErrors.minLength = validationErrors.minLength.replace('{minlength}', minLength);
    }

    if (maxLength && validationErrors.maxLength) {
        validationErrors.maxLength = validationErrors.maxLength.replace('{maxlength}', maxLength);
    }

    return validationErrors;
}

/**
 * @param {Array} formFieldConfig
 * @returns {Object}
 */
function ValidationPatterns(formFieldConfig) {
    var patterns = formFieldConfig.validation.patterns || {};

    return objectUtils.keys(patterns).reduce(function (result, key) {
        result[key] = Resource.msg(patterns[key], 'forms', null);

        return result;
    }, {});
}

/**
 * @param {string} fieldName
 * @returns {string}
 */
function getDynamicFieldName(fieldName) {
    return objectUtils.get(session, 'forms.' + fieldName + '.htmlName');
}

/**
 * @param {Object} formFieldConfig
 */
function FormFieldValidationSchema(formFieldConfig) {
    this.patterns = new ValidationPatterns(formFieldConfig);
    this.errors = new ValidationErrors(formFieldConfig);
    var compareWith = formFieldConfig.validation.compareWith;

    if (compareWith) {
        this.compareWith = getDynamicFieldName(compareWith) || '';
    }

    this.mandatory = formFieldConfig.element.required;

    if (formFieldConfig.element.minLength) {
        this.minLength = formFieldConfig.element.minLength;
    }

    if (formFieldConfig.element.maxLength) {
        this.maxLength = formFieldConfig.element.maxLength;
    }
}

/**
 * @description Injects some predefined dynamic data into resource string
 * <br>Known formats are: `<a href="{pageLink:privacy-policy}" target="_blank">`
 * <br>Usefull for rendering links in input labels etc.
 * @param {string} text - parsed resource string
 * @returns {string} returnValue
 */
function injectDynamicValues(text) {
    return text.replace(/\{(.*?)(?::(.*?))?\}/g, function (match, action, params) {
        var returnValue = match;
        switch (action) {
            case 'pageLink':
                returnValue = URLUtils.url('Page-Show', 'cid', params).toString();
                break;
            default:
                break;
        }
        return returnValue;
    });
}

/**
 * @param {Object} formField
 * @param {Object} formFieldConfig
 * @returns {Object}
 */
function FormFieldConfigResult(formField, formFieldConfig) {
    var config = objectUtils.deepAssign({
        element: {
            required: !!formField.mandatory
        },
        validation: {
            patterns: {},
            errors: {},
            compareWith: ''
        },
        label: {
            text: ''
        },
        caption: {
            text: ''
        }
    }, formFieldConfig || {});

    if (config.label.text) {
        config.label.text = injectDynamicValues(Resource.msg(config.label.text, 'forms', ''));
    } else {
        config.label.text = formField.label || '';
    }

    if (config.label.showOptionalMarker) {
        config.label.text += ' ' + Resource.msg('form.resource.optional', 'forms', null);
    }

    if (config.caption.text) {
        config.caption.text = Resource.msg(config.caption.text, 'forms', '');
    }

    if (config.element.placeholder) {
        config.element.placeholder = Resource.msg(config.element.placeholder, 'forms', '');
    }

    if (config.element.options) {
        // eslint-disable-next-line no-nested-ternary
        var options = !empty(config.element.options.binding)
            ? require(config.element.options.binding)
            : !empty(config.element.options.script)
                ? require(config.element.options.script)
                : config.element.options;

        if (config.element.emptyoption) {
            options.unshift({
                value: '',
                label: Resource.msg(config.element.emptyoption, 'forms', null)
            });
        } else {
            options.unshift({
                value: '',
                label: Resource.msg('form.resource.emptyoption', 'forms', null)
            });
        }

        config.element.options = options.map(function (option, index) {
            return {
                htmlValue: option.value,
                label: Resource.msg(option.label, 'forms', null) || option.value || '',
                id: option.id || index,
                selected: option.value === formField.value,
                value: option.value
            };
        });
    }


    if (empty(config.element.optional)) {
        config.element.optional = !config.element.required;
    }

    config.validation = new FormFieldValidationSchema(config);

    return config;
}

exports.FormFieldValidationSchema = FormFieldValidationSchema;
exports.FormFieldConfigResult = FormFieldConfigResult;
