var base = module.superModule;

var HashMap = require('dw/util/HashMap');
var Template = require('dw/util/Template');

/**
 * @description Gets the render html for the given isml template
 * Trimming string added
 * @param {object} templateContext - object that will fill template placeholders
 * @param {string} templateName - the name of the isml template to render.
 * @returns {string} the rendered isml.
 */
function getRenderedHtml(templateContext, templateName) {
    var context = new HashMap();

    Object.keys(templateContext).forEach(function (key) {
        context.put(key, templateContext[key]);
    });

    var template = new Template(templateName);
    return template.render(context).text.trim();
}

module.exports = base;
module.exports.getRenderedHtml = getRenderedHtml;
