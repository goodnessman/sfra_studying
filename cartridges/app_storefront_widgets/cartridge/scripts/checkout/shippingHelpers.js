var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');

/**
 * @override Comparing to SFRA, fixed displaying of the correct price
 * for basket\order instances in the Shipping and ShippingMethod Models
 */

/**
 * @description Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * CHANGE: Comparing to SFRA, added containerView and lineItemContainer parameters for the ShippingModel
 * @param {dw.order.Basket | dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {object} customer - the associated Customer Model object
 * @param {string} containerView - view of the shipping models (order or basket)
 * @returns {dw.util.ArrayList} an array of ShippingModels
 */
function getShippingModels(lineItemContainer, customer, containerView) {
    var ShippingModel = require('*/cartridge/models/shipping');
    var shipments = lineItemContainer ? lineItemContainer.getShipments() : null;

    if (!shipments) return [];

    return collections.map(shipments, function (shipment) {
        return new ShippingModel(shipment, null, customer, containerView, lineItemContainer);
    });
}

/**
 * @description Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * CHANGE: Comparing to SFRA, added containerView and lineItemContainer parameters for the ShippingMethodModel
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {object} [address] - optional address object
 * @param {string} containerView - the view of the product line items (order or basket)
 * @param {dw.order.Basket | dw.order.lineItemContainer} lineItemContainer Current users's basket/order
 * @returns {dw.util.Collection} an array of ShippingModels
 */
function getApplicableShippingMethods(shipment, address, containerView, lineItemContainer) {
    if (!shipment) {
        return null;
    }

    var ShippingMgr = require('dw/order/ShippingMgr');
    var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');
    var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
    var shippingMethods;
    var filteredMethods = []; // Filter out whatever the method associated with in store pickup

    if (address) {
        shippingMethods = shipmentShippingModel.getApplicableShippingMethods(address);
    } else {
        shippingMethods = shipmentShippingModel.getApplicableShippingMethods();
    }

    collections.forEach(shippingMethods, function (shippingMethod) {
        if (!shippingMethod.custom.storePickupEnabled) {
            filteredMethods.push(new ShippingMethodModel(shippingMethod, shipment, containerView, lineItemContainer));
        }
    });

    return filteredMethods;
}

module.exports = base;
module.exports.getShippingModels = getShippingModels;
module.exports.getApplicableShippingMethods = getApplicableShippingMethods;
