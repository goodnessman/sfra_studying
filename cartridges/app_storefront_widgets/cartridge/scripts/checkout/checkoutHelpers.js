var checkoutHelpersBase = module.superModule;

var Logger = require('dw/system/Logger');

/**
 * @description Returns object with address data
 * @param {object} form - Form object
 * @returns {object} result
 */
function getBillingAddressObject(form) {
    var address = {
        firstName: {
            value: form.addressFields.firstName.value
        },
        lastName: {
            value: form.addressFields.lastName.value
        },
        address1: {
            value: form.addressFields.address1.value
        },
        address2: {
            value: form.addressFields.address2.value
        },
        city: {
            value: form.addressFields.city.value
        },
        postalCode: {
            value: form.addressFields.postalCode.value
        },
        countryCode: {
            value: form.addressFields.country.value
        }
    };

    if (Object.prototype.hasOwnProperty.call(form.addressFields, 'states')) {
        address.stateCode = {
            value: form.addressFields.states.stateCode.value
        };
    }

    return address;
}

/**
 * @description Process payment form hook
 * @param {dw.order.PaymentProcessor} paymentProcessor - Payment Processor
 * @param {object} req - Request object
 * @param {object} paymentForm - Form object
 * @param {object} viewData - view data
 * @returns {object} result
 */
function processPaymentForm(paymentProcessor, req, paymentForm, viewData) {
    var HookMgr = require('dw/system/HookMgr');
    var ppID = paymentProcessor.ID.toLowerCase();

    if (HookMgr.hasHook('app.payment.form.processor.' + ppID)) {
        return HookMgr.callHook(
            'app.payment.form.processor.' + ppID,
            'processForm',
            req,
            paymentForm,
            viewData
        );
    }

    return HookMgr.callHook(
        'app.payment.form.processor.default_form_processor',
        'processForm'
    );
}

/**
 * @description Set basket billing address
 * @param {object} req - request object
 * @param {dw.order.Basket} currentBasket - basket object
 * @param {object} billingData - billing data object
 * @returns {object} result
 */
function handleBillingAddress(req, currentBasket, billingData) {
    var Transaction = require('dw/system/Transaction');
    var billingAddress = currentBasket.billingAddress;

    try {
        Transaction.wrap(function () {
            if (!billingAddress) {
                billingAddress = currentBasket.createBillingAddress();
            }

            if (require('util/test').forceNegativeCase('no_billing_address')) {
                throw new Error();
            }

            billingAddress.setFirstName(billingData.address.firstName.value);
            billingAddress.setLastName(billingData.address.lastName.value);
            billingAddress.setAddress1(billingData.address.address1.value);
            billingAddress.setAddress2(billingData.address.address2.value);
            billingAddress.setCity(billingData.address.city.value);
            billingAddress.setPostalCode(billingData.address.postalCode.value);

            if (Object.prototype.hasOwnProperty.call(billingData.address, 'stateCode')) {
                billingAddress.setStateCode(billingData.address.stateCode.value);
            }

            billingAddress.setCountryCode(billingData.address.countryCode.value);

            if (billingData.storedPaymentUUID) {
                billingAddress.setPhone(req.currentCustomer.profile.phone);
                currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
            } else {
                billingAddress.setPhone(billingData.phone.value);
                currentBasket.setCustomerEmail(billingData.email.value);
            }
        });
    } catch (err) {
        Logger.error('Unable to handle billing address: ' + err.message);

        return {
            error: true
        };
    }

    return {
        error: false
    };
}

/**
 * @description Clear credit card form data
 * @param {object} billingForm - billing form object
 */
function clearBillingFormSensetiveData(billingForm) {
    billingForm.creditCardFields.cardNumber.htmlValue = ''; // eslint-disable-line no-param-reassign
    billingForm.creditCardFields.securityCode.htmlValue = ''; // eslint-disable-line no-param-reassign
}

/**
 * @description Handle payment processor hook
 * @param {dw.order.PaymentProcessor} paymentProcessor - Payment Processor
 * @param {dw.order.Basket} currentBasket - basket object
 * @param {object} paymentInformation - payment information object
 * @returns {object} result
 */
function handlePaymentProcessor(paymentProcessor, currentBasket, paymentInformation) {
    var HookMgr = require('dw/system/HookMgr');
    var ppID = paymentProcessor.ID.toLowerCase();

    if (require('util/test').forceNegativeCase('selected_payment_unsupported')) {
        return HookMgr.callHook('app.payment.processor.default', 'Handle');
    }

    if (HookMgr.hasHook('app.payment.processor.' + ppID)) {
        return HookMgr.callHook(
            'app.payment.processor.' + ppID,
            'Handle',
            currentBasket,
            paymentInformation
        );
    }

    return HookMgr.callHook('app.payment.processor.default', 'Handle');
}

/**
 * @description handles the payment authorization for each payment instrument
 * CHANGE: Added possibility to display server errors
 * @param {dw.order.Order} order - the order object
 * @param {string} orderNumber - The order number for the order
 * @returns {object} an error object
 */
function handlePayments(order, orderNumber) {
    var Transaction = require('dw/system/Transaction');
    var OrderMgr = require('dw/order/OrderMgr');
    var HookMgr = require('dw/system/HookMgr');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var result = {};

    if (order.totalNetPrice !== 0.00) {
        var paymentInstruments = order.paymentInstruments;

        if (paymentInstruments.length === 0) {
            Transaction.wrap(function () { OrderMgr.failOrder(order, true); });
            result.error = true;
        }

        if (!result.error) {
            for (var i = 0; i < paymentInstruments.length; i++) {
                var paymentInstrument = paymentInstruments[i];
                var paymentProcessor = PaymentMgr
                    .getPaymentMethod(paymentInstrument.paymentMethod)
                    .paymentProcessor;
                var authorizationResult;
                if (paymentProcessor === null) {
                    Transaction.begin();
                    paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
                    Transaction.commit();
                } else {
                    if (HookMgr.hasHook('app.payment.processor.' + paymentProcessor.ID.toLowerCase())) {
                        authorizationResult = HookMgr.callHook(
                            'app.payment.processor.' + paymentProcessor.ID.toLowerCase(),
                            'Authorize',
                            orderNumber,
                            paymentInstrument,
                            paymentProcessor
                        );
                    } else {
                        authorizationResult = HookMgr.callHook(
                            'app.payment.processor.default',
                            'Authorize'
                        );
                    }

                    if (authorizationResult.error) {
                        Transaction.wrap(function () { OrderMgr.failOrder(order, true); });
                        result.error = true;

                        if (authorizationResult.serverErrors) {
                            result.errorMessage = authorizationResult.serverErrors.join(', ');
                        }

                        break;
                    }
                }
            }
        }
    }

    return result;
}

/**
 * @description Save payment information hook
 * @param {object} req - request object
 * @param {dw.order.PaymentProcessor} paymentProcessor - Payment Processor
 * @param {dw.order.Basket} currentBasket - basket object
 * @param {object} billingData billing data object
 * @returns {object} result
 */
function savePaymentInformation(req, paymentProcessor, currentBasket, billingData) {
    var HookMgr = require('dw/system/HookMgr');
    var ppID = paymentProcessor.ID.toLowerCase();

    if (HookMgr.hasHook('app.payment.form.processor.' + ppID)) {
        return HookMgr.callHook(
            'app.payment.form.processor.' + ppID,
            'savePaymentInformation',
            req,
            currentBasket,
            billingData
        );
    }

    return HookMgr.callHook('app.payment.form.processor.default', 'savePaymentInformation');
}

/**
 * @description Subscribe email handler hook
 * @param {object} paymentForm - payment form object
 */
function handleEmailSubscription(paymentForm) {
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

    hooksHelper(
        'app.customer.subscription',
        'subscribeTo',
        [
            paymentForm.subscribe.checked,
            paymentForm.contactInfoFields.email.htmlValue
        ],
        function () {}
    );
}

/**
 * @description restore payment info from customer saved payment
 * @param {object} req - request object
 * @param {object} billingData - billing data object
 * @returns {undefined} result
 */
function updatePaymentInformation(req, billingData) {
    var currentCustomer = req.currentCustomer.raw;
    var isLoggedInCustomer = currentCustomer.authenticated && currentCustomer.registered;
    var storedPayment = billingData.storedPaymentUUID;

    if (!isLoggedInCustomer || !storedPayment) {
        return;
    }

    var array = require('*/cartridge/scripts/util/array');
    var paymentInformation = billingData.paymentInformation;
    var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
    var paymentInstrument = array.find(paymentInstruments, function (item) {
        return storedPayment === item.UUID;
    });

    paymentInformation.cardNumber.value = paymentInstrument.creditCardNumber;
    paymentInformation.cardType.value = paymentInstrument.creditCardType;
    paymentInformation.securityCode.value = req.form.securityCode;
    paymentInformation.expirationMonth.value = paymentInstrument.creditCardExpirationMonth;
    paymentInformation.expirationYear.value = paymentInstrument.creditCardExpirationYear;
    paymentInformation.creditCardToken = paymentInstrument.raw.creditCardToken;
}

/**
 * @description Update multishipping flag in session based on basket shipments
 * @param {object} req - request object
 * @param {dw.order.Basket} currentBasket - current basket
 * @returns {boolean} result
 */
function handleMultishippingUse(req, currentBasket) {
    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');

    if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
        req.session.privacyCache.set('usingMultiShipping', false);
        usingMultiShipping = false;
    }

    return usingMultiShipping;
}

/**
 * @description Prefills customer email field on the billing step
 * @param {object} viewData response view data
 * @param {object} currentCustomer current customer
 * @param {dw.order.Basket} currentBasket - current basket
 */
function prefillCustomerEmail(viewData, currentCustomer, currentBasket) {
    if (!viewData || !currentCustomer || !currentBasket) {
        return;
    }

    var contactInfoFields = viewData.forms.billingForm.contactInfoFields;

    if (viewData.forms && currentBasket.customerEmail) {
        contactInfoFields.email.value = currentBasket.customerEmail;
    }

    if (!currentBasket.customerEmail && currentCustomer.profile) {
        contactInfoFields.email.value = currentCustomer.profile.email;
    }
}

/**
 * @description Validates payment
 * @param {object} req - The local instance of the request object
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {object} an object that has error information
 */
function validatePayment(req, currentBasket) {
    var PaymentMgr = require('dw/order/PaymentMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var applicablePaymentCards;
    var applicablePaymentMethods;
    var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
    var paymentAmount = currentBasket.totalGrossPrice.value;
    var countryCode = req.geolocation.countryCode;
    var currentCustomer = req.currentCustomer.raw;
    var paymentInstruments = currentBasket.paymentInstruments;
    var result = {};

    applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(
        currentCustomer,
        countryCode,
        paymentAmount
    );
    applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(
        currentCustomer,
        countryCode,
        paymentAmount
    );

    var invalid = true;

    for (var i = 0; i < paymentInstruments.length; i++) {
        var paymentInstrument = paymentInstruments[i];

        if (PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstrument.paymentMethod)) {
            invalid = false;
        }

        var paymentMethod = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod());

        if (paymentMethod && applicablePaymentMethods.contains(paymentMethod)) {
            if (PaymentInstrument.METHOD_CREDIT_CARD.equals(paymentInstrument.paymentMethod)) {
                var card = PaymentMgr.getPaymentCard(paymentInstrument.creditCardType);

                // Checks whether payment card is still applicable.
                if (card && applicablePaymentCards.contains(card)) {
                    invalid = false;
                }
            } else {
                invalid = false;
            }
        }

        if (invalid) {
            break; // there is an invalid payment instrument
        }
    }

    result.error = invalid;

    if (require('util/test').forceNegativeCase('selected_payment_unapplicable')) {
        result.error = true;
    }

    return result;
}

function saveOrderAddressForCustomer(order, currentCustomer) {
    if (!currentCustomer || !currentCustomer.addressBook) {
        return;
    }

    var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
    // save all used shipping addresses to address book of the logged in customer
    var allAddresses = addressHelpers.gatherShippingAddresses(order);

    allAddresses.forEach(function (address) {
        if (!addressHelpers.checkIfAddressStored(address, currentCustomer.addressBook.addresses)) {
            addressHelpers.saveAddress(address, currentCustomer, addressHelpers.generateAddressName(address));
        }
    });
}

/**
 * @description sets the gift message on a shipment
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {boolean} isGift - is the shipment a gift
 * @param {string} giftMessage - The gift message the user wants to attach to the shipment
 * @returns {object} object containing error information
 */
function setGift(shipment, isGift, giftMessage) {
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var result = {
        error: false,
        errorMessage: null
    };

    try {
        Transaction.wrap(function () {
            if (require('util/test').forceNegativeCase('gift_message_not_saved')) {
                throw new Error();
            }

            shipment.setGift(isGift);

            if (isGift && giftMessage) {
                shipment.setGiftMessage(giftMessage);
            } else {
                shipment.setGiftMessage(null);
            }
        });
    } catch (err) {
        Logger.error('Unable to save gift message: ' + err.message);

        result.error = true;
        result.errorMessage = Resource.msg('error.message.could.not.be.attached', 'checkout', null);
    }

    return result;
}

/**
 * @description Attempts to create an order from the current basket
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.order.Order} The order object created from the current basket
 */
function createOrder(currentBasket) {
    var Transaction = require('dw/system/Transaction');
    var OrderMgr = require('dw/order/OrderMgr');
    var order;

    try {
        if (require('util/test').forceNegativeCase('unable_create_order')) {
            throw new Error();
        }

        order = Transaction.wrap(function () {
            return OrderMgr.createOrder(currentBasket);
        });
    } catch (err) {
        Logger.error('Unable to create order: ' + err.message);

        return null;
    }
    return order;
}

/**
 * @description Sets the payment transaction amount
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {object} an error object
 */
function calculatePaymentTransaction(currentBasket) {
    var Transaction = require('dw/system/Transaction');
    var result = { error: false };

    try {
        // TODO: This function will need to account for gift certificates at a later date
        Transaction.wrap(function () {
            if (require('util/test').forceNegativeCase('unable_calculate_totals')) {
                throw new Error();
            }

            var paymentInstruments = currentBasket.paymentInstruments;

            if (!paymentInstruments.length) {
                return;
            }

            // Assuming that there is only one payment instrument used for the total order amount.
            // TODO: Will have to rewrite this logic once we start supporting multiple payment instruments for same order
            var orderTotal = currentBasket.totalGrossPrice;
            var paymentInstrument = paymentInstruments[0];

            paymentInstrument.paymentTransaction.setAmount(orderTotal);
        });
    } catch (err) {
        Logger.error('Unable to calculate payment transaction: ' + err.message);

        result.error = true;
    }

    return result;
}

/**
 * @description Attempts to place the order
 * @param {dw.order.Order} order - The order object to be placed
 * @param {object} fraudDetectionStatus - an Object returned by the fraud detection hook
 * @returns {object} an error object
 */
function placeOrder(order, fraudDetectionStatus) {
    var Order = require('dw/order/Order');
    var Status = require('dw/system/Status');
    var OrderMgr = require('dw/order/OrderMgr');
    var Transaction = require('dw/system/Transaction');
    var result = { error: false };

    try {
        Transaction.begin();
        if (require('util/test').forceNegativeCase('unable_place_order')) {
            throw new Error();
        }

        var placeOrderStatus = OrderMgr.placeOrder(order);

        if (placeOrderStatus === Status.ERROR) {
            throw new Error();
        }

        if (fraudDetectionStatus.status === 'flag') {
            order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
        } else {
            order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
        }

        order.setExportStatus(Order.EXPORT_STATUS_READY);
        Transaction.commit();
    } catch (err) {
        Logger.error('Unable to place order: ' + err.message);

        Transaction.wrap(function () { OrderMgr.failOrder(order, true); });
        result.error = true;
    }

    return result;
}

/**
 * @description Copies information from the shipping form to the associated shipping address
 * @param {object} shippingData - the shipping data
 * @param {dw.order.Shipment} [shipmentOrNull] - the target Shipment
 * @returns {object} result
 */
function copyShippingAddressToShipment(shippingData, shipmentOrNull) {
    var Resource = require('dw/web/Resource');
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;

    var shippingAddress = shipment.shippingAddress;

    try {
        Transaction.wrap(function () {
            if (shippingAddress === null) {
                shippingAddress = shipment.createShippingAddress();
            }

            if (require('util/test').forceNegativeCase('no_shipping_address')) {
                throw new Error();
            }

            shippingAddress.setFirstName(shippingData.address.firstName);
            shippingAddress.setLastName(shippingData.address.lastName);
            shippingAddress.setAddress1(shippingData.address.address1);
            shippingAddress.setAddress2(shippingData.address.address2);
            shippingAddress.setCity(shippingData.address.city);
            shippingAddress.setPostalCode(shippingData.address.postalCode);
            shippingAddress.setStateCode(shippingData.address.stateCode);

            var countryCode = shippingData.address.countryCode.value
                ? shippingData.address.countryCode.value
                : shippingData.address.countryCode;

            shippingAddress.setCountryCode(countryCode);
            shippingAddress.setPhone(shippingData.address.phone);

            ShippingHelper.selectShippingMethod(shipment, shippingData.shippingMethod);
        });
    } catch (err) {
        Logger.error('Unable to set shipping address. ' + err.message);

        return {
            error: true,
            errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
        };
    }

    return {
        error: false
    };
}

/**
 * @description Returns the first non-default shipment with more than one product line item
 * @param {dw.order.Basket} currentBasket - The current Basket
 * @returns {dw.order.Shipment} - the shipment
 */
function getFirstNonDefaultShipmentWithProductLineItems(currentBasket) {
    var shipment;
    var match;

    for (var i = 0, ii = currentBasket.shipments.length; i < ii; i++) {
        shipment = currentBasket.shipments[i];
        if (!shipment.default && shipment.productLineItems.length > 0) {
            match = shipment;
            break;
        }
    }

    return match;
}

/**
 * @description Ensures that no shipment exists with 0 product line items
 * @param {object} req - the request object needed to access session.privacyCache
 */
function ensureNoEmptyShipments(req) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var AddressModel = require('*/cartridge/models/address');
    var collections = require('*/cartridge/scripts/util/collections');

    Transaction.wrap(function () {
        var currentBasket = BasketMgr.getCurrentBasket();

        var iter = currentBasket.shipments.iterator();
        var shipment;
        var shipmentsToDelete = [];

        while (iter.hasNext()) {
            shipment = iter.next();
            if (shipment.productLineItems.length < 1 && shipmentsToDelete.indexOf(shipment) < 0) {
                if (shipment.default) {
                    // Cant delete the defaultShipment
                    // Copy all line items from 2nd to first
                    var altShipment = getFirstNonDefaultShipmentWithProductLineItems(currentBasket);
                    if (!altShipment) return;

                    // Move the valid marker with the shipment
                    var altValid = req.session.privacyCache.get(altShipment.UUID);
                    req.session.privacyCache.set(currentBasket.defaultShipment.UUID, altValid);

                    collections.forEach(altShipment.productLineItems,
                        function (lineItem) {
                            lineItem.setShipment(currentBasket.defaultShipment);
                        });

                    if (altShipment.shippingAddress) {
                        // Copy from other address
                        var addressModel = new AddressModel(altShipment.shippingAddress);
                        copyShippingAddressToShipment(addressModel, currentBasket.defaultShipment);
                    } else {
                        // Or clear it out
                        currentBasket.defaultShipment.createShippingAddress();
                    }

                    if (altShipment.custom && altShipment.custom.fromStoreId && altShipment.custom.shipmentType) {
                        currentBasket.defaultShipment.custom.fromStoreId = altShipment.custom.fromStoreId;
                        currentBasket.defaultShipment.custom.shipmentType = altShipment.custom.shipmentType;
                    }

                    currentBasket.defaultShipment.setShippingMethod(altShipment.shippingMethod);
                    // then delete 2nd one
                    shipmentsToDelete.push(altShipment);
                } else {
                    shipmentsToDelete.push(shipment);
                }
            }
        }

        for (var j = 0, jj = shipmentsToDelete.length; j < jj; j++) {
            currentBasket.removeShipment(shipmentsToDelete[j]);
        }
    });
}

/**
 * @description Validate billing form
 * @param {object} form - the form object with pre-validated form fields
 * @returns {object} the names of the invalid form fields
 */
function validateFields(form) {
    var formErrors = require('*/cartridge/scripts/formErrors');

    return formErrors.getFormErrors(form);
}

/**
 * @description Validate shipping form fields
 * @param {object} form - the form object with pre-validated form fields
 * @returns {object} the names of the invalid form fields
 */
function validateShippingForm(form) {
    if (require('util/test').forceNegativeCase('wrong_shipping_address')) {
        // invalidate at least one field on the shipping form
        if (form && form.firstName) {
            form.firstName.value = '';
        }
    }

    return validateFields(form);
}

module.exports = checkoutHelpersBase;
module.exports.getBillingAddressObject = getBillingAddressObject;
module.exports.processPaymentForm = processPaymentForm;
module.exports.handleBillingAddress = handleBillingAddress;
module.exports.clearBillingFormSensetiveData = clearBillingFormSensetiveData;
module.exports.handlePaymentProcessor = handlePaymentProcessor;
module.exports.savePaymentInformation = savePaymentInformation;
module.exports.handleEmailSubscription = handleEmailSubscription;
module.exports.updatePaymentInformation = updatePaymentInformation;
module.exports.handleMultishippingUse = handleMultishippingUse;
module.exports.prefillCustomerEmail = prefillCustomerEmail;
module.exports.setGift = setGift;
module.exports.createOrder = createOrder;
module.exports.placeOrder = placeOrder;
module.exports.calculatePaymentTransaction = calculatePaymentTransaction;
module.exports.validateFields = validateFields;
module.exports.handlePayments = handlePayments;
module.exports.validateShippingForm = validateShippingForm;
module.exports.copyShippingAddressToShipment = copyShippingAddressToShipment;
module.exports.ensureNoEmptyShipments = ensureNoEmptyShipments;
module.exports.validatePayment = validatePayment;
module.exports.saveOrderAddressForCustomer = saveOrderAddressForCustomer;
module.exports.getFirstNonDefaultShipmentWithProductLineItems = getFirstNonDefaultShipmentWithProductLineItems;
