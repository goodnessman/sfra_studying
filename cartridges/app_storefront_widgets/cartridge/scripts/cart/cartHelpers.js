var ProductMgr = require('dw/catalog/ProductMgr');
var Resource = require('dw/web/Resource');

var productHelper = require('*/cartridge/scripts/helpers/productHelpers');

var base = module.superModule;

/**
 * @description Adds a product to the cart. If the product is already in the cart it increases the quantity of
 * that product.
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string} productId - the productId of the product being added to the cart
 * @param {number} quantity - the number of products to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @returns {object} returns an error object
 */
function addProductToCart(currentBasket, productId, quantity, childProducts, options) {
    var product = ProductMgr.getProduct(productId);

    if (!product) {
        return {
            error: true,
            message: Resource.msg('error.alert.productnotfound', 'product', null)
        };
    }

    var availableToSell;
    var defaultShipment = currentBasket.defaultShipment;
    var perpetual;
    var productInCart;
    var productLineItems = currentBasket.productLineItems;
    var productQuantityInCart;
    var quantityToSet;
    var optionModel = productHelper.getCurrentOptionModel(product.optionModel, options);
    var result = {
        error: false,
        message: Resource.msg('text.alert.addedtobasket', 'product', null)
    };

    var totalQtyRequested = 0;
    var canBeAdded = false;

    if (product.bundle) {
        canBeAdded = base.checkBundledProductCanBeAdded(childProducts, productLineItems, quantity);
    } else {
        totalQtyRequested = quantity + base.getQtyAlreadyInCart(productId, productLineItems);
        perpetual = product.availabilityModel.inventoryRecord.perpetual;
        canBeAdded = (perpetual || totalQtyRequested <= product.availabilityModel.inventoryRecord.ATS.value);
    }

    if (!canBeAdded) {
        result.error = true;
        result.message = Resource.msgf(
            'error.alert.selected.quantity.cannot.be.added.for',
            'product',
            null,
            product.availabilityModel.inventoryRecord.ATS.value,
            product.name
        );
        return result;
    }

    productInCart = base.getExistingProductLineItemInCart(
        product, productId, productLineItems, childProducts, options
    );

    if (productInCart) {
        productQuantityInCart = productInCart.quantity.value;
        quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
        availableToSell = productInCart.product.availabilityModel.inventoryRecord.ATS.value;

        if (availableToSell >= quantityToSet || perpetual) {
            productInCart.setQuantityValue(quantityToSet);
            result.uuid = productInCart.UUID;
        } else {
            result.error = true;
            result.message = availableToSell === productQuantityInCart
                ? Resource.msg('error.alert.max.quantity.in.cart', 'product', null)
                : Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
        }
    } else {
        var productLineItem;

        try {
            productLineItem = base.addLineItem(
                currentBasket,
                product,
                quantity,
                childProducts,
                optionModel,
                defaultShipment
            );

            result.uuid = productLineItem.UUID;
        } catch (error) {
            result.error = true;
            result.message = Resource.msg('error.alert.network', 'product', null);
        }
    }

    if (result.uuid) {
        result.message = Resource.msgf('success.alert.product', 'product', null, quantity, product.name);
        if (product.isVariant()) {
            var variantAttributes = productHelper.getVariationAttributes(productId);

            if (variantAttributes.length) {
                var variantAttributesValues = variantAttributes.map(function (attribute) {
                    return attribute.value;
                }).join(', ');
                result.message = Resource.msgf(
                    'success.alert.product.variant',
                    'product',
                    null,
                    quantity,
                    product.name,
                    variantAttributesValues
                );
            }
        }
    }

    return result;
}

/**
 * @description Creates basket from the order
 * @param {dw.order.Order} order - order instance
 * @returns {null|dw.order.Basket} basket or null
 */
function reCreateBasket(order) {
    var OrderMgr = require('dw/order/OrderMgr');
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');

    if (!order) {
        return null;
    }

    Transaction.wrap(function () {
        OrderMgr.failOrder(order, true);
    });

    return BasketMgr.getCurrentBasket();
}

module.exports = base;
module.exports.reCreateBasket = reCreateBasket;
module.exports.addProductToCart = addProductToCart;
