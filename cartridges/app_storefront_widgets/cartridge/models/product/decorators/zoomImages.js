var ImageModel = require('*/cartridge/models/product/productImages');

module.exports = function (object, product) {
    Object.defineProperty(object, 'zoomImages', {
        enumerable: true,
        value: new ImageModel(product, { types: ['zoom'], quantity: 'all' })
    });
};
