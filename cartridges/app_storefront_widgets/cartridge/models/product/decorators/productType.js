/**
 * @description Sets dynamic product type property to be accessible on storefront
 * <br>and easilly used in Mustache templates.
 * <br>For example, product will have: `product.productTypeStandard` or `productTypeMaster`
 * @param {object} object - assembled product object
 * @param {dw.catalog.Product} product - api product object
 * @param {string} productType - product type
 */
module.exports = function (product, apiProduct, productType) {
    var productTypeProperty = productType !== ''
        ? 'productType' + (productType.charAt(0).toUpperCase() + productType.slice(1))
        : 'productTypeUnknown';

    Object.defineProperty(product, productTypeProperty, {
        enumerable: true,
        value: true
    });
};
