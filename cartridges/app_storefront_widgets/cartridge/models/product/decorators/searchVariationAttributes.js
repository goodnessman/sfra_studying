var ATTRIBUTE_NAME = 'color';
var collections = require('*/cartridge/scripts/util/collections');
var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');
var URLUtils = require('dw/web/URLUtils');

/**
 * @param {object} object product object
 * @param {dw.catalog.Product} apiProduct product
 * @description Model was overriden to add `code` property
 * <br>to variation attribute values, which contains hex color of the swatch
 */
module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'variationAttributes', {
        enumerable: true,
        value: (function () {
            var variationModel = apiProduct.getVariationModel();
            var colorAttr = variationModel.getProductVariationAttribute(ATTRIBUTE_NAME);
            var colors = variationModel.getAllValues(colorAttr);
            var selectedColor = variationModel.getSelectedValue(colorAttr);
            var ID = variationModel.getMaster().getID();

            return [{
                attributeId: 'color',
                id: 'color',
                swatchable: true,
                values: collections.map(colors, function (color) {
                    var apiImage = color.getImage('swatch', 0);
                    var attributeObject = {
                        id: color.ID,
                        description: color.description,
                        displayValue: color.displayValue,
                        value: color.value,
                        selectable: true,
                        selected: selectedColor ? selectedColor.ID === color.ID : false,
                        code: productHelpers.getColorCode(color.ID.toLowerCase()),
                        url: URLUtils.url(
                            'Product-Show',
                            'pid',
                            ID,
                            'dwvar_' + ID + '_color',
                            color.value
                        ).toString()
                    };

                    if (apiImage) {
                        attributeObject.images = {
                            swatch: [{
                                alt: apiImage && apiImage.alt,
                                url: apiImage && apiImage.URL.toString(),
                                title: apiImage && apiImage.title
                            }]
                        };
                    }

                    return attributeObject;
                }).sort(function sortColors(colorA, colorB) {
                    // selected color should go first
                    return +colorB.selected - +colorA.selected;
                })
            }];
        }())
    });
};
