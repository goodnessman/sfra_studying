var base = module.superModule;

module.exports = base;
module.exports.deliveryAndReturns = require('*/cartridge/models/product/decorators/deliveryAndReturns');
module.exports.zoomImages = require('*/cartridge/models/product/decorators/zoomImages');
module.exports.link = require('*/cartridge/models/product/decorators/link');
module.exports.productType = require('*/cartridge/models/product/decorators/productType');
