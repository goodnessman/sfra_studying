
module.exports = function (object, product) {
    Object.defineProperty(object, 'deliveryAndReturns', {
        enumerable: true,
        value: (function () {
            var ContentModel = require('*/cartridge/models/content');
            var deliveryAndReturns = null;
            var primaryCategory = product.variant
                ? product.masterProduct.primaryCategory
                : product.primaryCategory;
            var contentModel;

            if (primaryCategory && primaryCategory.custom.deliveryAndReturnsAssetId) {
                contentModel = new ContentModel(primaryCategory.custom.deliveryAndReturnsAssetId);
                deliveryAndReturns = contentModel && contentModel.getMarkup();
            }

            return deliveryAndReturns;
        }())
    });
};
