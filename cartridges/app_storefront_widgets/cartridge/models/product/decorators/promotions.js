var collections = require('*/cartridge/scripts/util/collections');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * @description In addition to SFRA promotions decorator, was added:
 * - JSON-safe promotion details text to use in templates / mustache templates
 * @param {object} object - decorable product object
 * @param {array} promotions - array of product promotions
 */
module.exports = function (object, promotions) {
    Object.defineProperty(object, 'promotions', {
        enumerable: true,
        value: promotions.length === 0 ? null : collections.map(promotions, function (promotion) {
            return {
                calloutMsg: promotion.calloutMsg ? promotion.calloutMsg.markup : '',
                details: promotion.details ? promotion.details.markup : '',
                detailsJSONSafe: renderTemplateHelper.getRenderedHtml({
                    unsafeString: promotion.details ? promotion.details.markup : ''
                }, 'components/utils/jsonSafeString'),
                enabled: promotion.enabled,
                id: promotion.ID,
                name: promotion.name,
                promotionClass: promotion.promotionClass,
                rank: promotion.rank
            };
        })
    });
};
