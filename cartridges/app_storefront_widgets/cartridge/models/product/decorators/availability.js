module.exports = function (object, quantity, minOrderQuantity, availabilityModel) {
    Object.defineProperty(object, 'availability', {
        enumerable: true,
        value: (function () {
            var Resource = require('dw/web/Resource');
            var availability = {};
            var productQuantity = quantity ? parseInt(quantity, 10) : minOrderQuantity;
            var availabilityModelLevels = availabilityModel.getAvailabilityLevels(productQuantity);
            var inventoryRecord = availabilityModel.inventoryRecord;

            availability.messages = [];

            if (inventoryRecord && inventoryRecord.inStockDate) {
                availability.inStockDate = inventoryRecord.inStockDate.toDateString();
            } else {
                availability.inStockDate = null;
            }

            if (availabilityModelLevels.inStock.value > 0) {
                availability.stock = availabilityModelLevels.inStock.value;

                if (availabilityModelLevels.inStock.value === productQuantity) {
                    availability.class = 'm-instock';
                    availability.messages.push(Resource.msg('label.instock', 'common', null));
                } else {
                    availability.class = 'm-lowinstock';
                    if (availabilityModelLevels.inStock.value < productQuantity) {
                        availability.isReachedLimit = true;
                        // eslint-disable-next-line max-len
                        availability.isReachedLimitMessage = Resource.msgf('availability.isreachedlimit.msg', 'cart', null, inventoryRecord.allocation.value);
                        availability.inStockMsg = [];
                        availability.messages.push(
                            Resource.msg(
                                'availability.isreachedlimit.error',
                                'cart',
                                null
                            )
                        );
                        availability.inStockMsg.push(
                            Resource.msgf(
                                'availability.instock.msg',
                                'cart',
                                null,
                                availabilityModelLevels.inStock.value
                            )
                        );
                    } else {
                        availability.messages.push(
                            Resource.msgf(
                                'availability.instock.msg',
                                'cart',
                                null,
                                availabilityModelLevels.inStock.value
                            )
                        );
                    }
                }
            }

            if (availabilityModelLevels.preorder.value > 0) {
                if (availabilityModelLevels.preorder.value === productQuantity) {
                    availability.messages.push(Resource.msg('label.preorder', 'common', null));
                } else {
                    availability.messages.push(
                        Resource.msgf(
                            'label.preorder.items',
                            'common',
                            null,
                            availabilityModelLevels.preorder.value
                        )
                    );
                }
            }

            if (availabilityModelLevels.backorder.value > 0) {
                if (availabilityModelLevels.backorder.value === productQuantity) {
                    availability.messages.push(Resource.msg('label.back.order', 'common', null));
                } else {
                    availability.messages.push(
                        Resource.msgf(
                            'label.back.order.items',
                            'common',
                            null,
                            availabilityModelLevels.backorder.value
                        )
                    );
                }
            }

            if (availabilityModelLevels.notAvailable.value > 0) {
                if (availabilityModelLevels.notAvailable.value === productQuantity) {
                    availability.isOutOfStockError = Resource.msg('availability.outofstock.error', 'cart', null);
                    availability.class = 'm-outofstock';
                    availability.isOutOfStock = true;
                    availability.messages.push(Resource.msg('availability.outofstock.msg', 'cart', null));
                }
            }

            return availability;
        }())
    });

    Object.defineProperty(object, 'available', {
        enumerable: true,
        value: availabilityModel.isOrderable(parseFloat(quantity) || minOrderQuantity)
    });
};
