var URLUtils = require('dw/web/URLUtils');

/**
 * @description Sets link to current product
 * @param {object} object - assembled product object
 * @param {dw.catalog.Product} product - api product object
 */
module.exports = function (object, product) {
    Object.defineProperty(object, 'link', {
        enumerable: true,
        value: URLUtils.url('Product-Show', 'pid', product.ID).toString()
    });
};
