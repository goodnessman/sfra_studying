var collections = require('*/cartridge/scripts/util/collections');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var ImageModel = require('*/cartridge/models/product/productImages');

/**
 * @description Determines whether a product attribute has image swatches.  Currently, the only attribute that
 *     does is Color.
 * @param {string} dwAttributeId Id of the attribute to check
 * @returns {boolean} Flag that specifies if the current attribute should be displayed as a swatch
 */
function isSwatchable(dwAttributeId) {
    var imageableAttrs = [require('util/pref').get('product.color.attr.id')];
    return imageableAttrs.indexOf(dwAttributeId) > -1;
}

/**
 * @description Retrieve all attribute values
 *
 * @param {dw.catalog.ProductVariationModel} variationModel A product's variation model
 * @param {dw.catalog.ProductVariationAttributeValue} selectedValue Selected attribute value
 * @param {dw.catalog.ProductVariationAttribute} attr Attribute value
 * @param {string} endPoint The end point to use in the Product Controller
 * @param {string} selectedOptionsQueryParams Selected options query params
 * @param {string} quantity Quantity selected
 * @returns {object[]} List of attribute value objects for template context
 */
function getAllAttrValues(
    variationModel,
    selectedValue,
    attr,
    endPoint,
    selectedOptionsQueryParams,
    quantity
) {
    var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');

    var self = this;
    var attrValues = variationModel.getAllValues(attr);
    var actionEndpoint = 'Product-' + endPoint;

    return collections.map(attrValues, function (value) {
        var isSelected = (selectedValue && selectedValue.equals(value)) || false;
        var valueUrl = '';

        var processedAttr = {
            id: value.ID,
            description: value.description,
            displayValue: value.displayValue,
            value: value.value,
            selected: isSelected,
            selectable: variationModel.hasOrderableVariants(attr, value)
        };


        valueUrl = (isSelected && endPoint !== 'Show')
            ? variationModel.urlUnselectVariationValue(actionEndpoint, attr)
            : variationModel.urlSelectVariationValue(actionEndpoint, attr, value);
        processedAttr.url = urlHelper.appendQueryParams(valueUrl, [selectedOptionsQueryParams,
            'quantity=' + quantity]);


        if (self.isSwatchable(attr.attributeID)) {
            processedAttr.code = productHelpers.getColorCode(value.ID);

            if (!empty(value.getImages('swatch'))) {
                processedAttr.images = new ImageModel(value, {
                    types: ['swatch'],
                    quantity: 'all',
                    emptyIfNotExist: true
                });
            }
        }

        return processedAttr;
    });
}

/**
 * @description Gets the Url needed to relax the given attribute selection, this will not return
 * anything for attributes represented as swatches.
 *
 * @param {Array} values Attribute values
 * @param {string} attrID Id of the attribute
 * @returns {string} The Url that will remove the selected attribute.
 */
function getAttrResetUrl(values, attrID) {
    var urlReturned;
    var value;

    for (var i = 0; i < values.length; i++) {
        value = values[i];
        if (!value.images) {
            if (value.selected) {
                urlReturned = value.url;
                break;
            }

            urlReturned = value.url.replace(attrID + '=' + value.value, attrID + '=');
            break;
        }
    }

    return urlReturned;
}

/**
 * @class
 * @description Get a list of available attributes that matches provided config
 *
 * @param {dw.catalog.ProductVariationModel} variationModel Current product variation
 * @param {Object} attrConfig Attributes to select
 * @param {Array} attrConfig.attributes An array of strings,representing the
 *                                        id's of product attributes.
 * @param {string} attrConfig.attributes If this is a string and equal to '*' it signifies
 *                                         that all attributes should be returned.
 *                                         If the string is 'selected', then this is comming
 *                                         from something like a product line item, in that
 *                                         all the attributes have been selected.
 *
 * @param {string} attrConfig.endPoint The endpoint to use when generating urls for
 *                                       product attributes
 * @param {string} selectedOptionsQueryParams Selected options query params
 * @param {string} quantity Quantity selected
 * @returns {void}
 */
function VariationAttributesModel(variationModel, attrConfig, selectedOptionsQueryParams, quantity) {
    var allAttributes = variationModel.productVariationAttributes;
    var self = this;

    collections.forEach(allAttributes, function (attr) {
        var selectedValue = variationModel.getSelectedValue(attr);
        var values = self.getAllAttrValues(variationModel, selectedValue, attr, attrConfig.endPoint,
            selectedOptionsQueryParams, quantity);
        var resetUrl = self.getAttrResetUrl(values, attr.ID);

        if ((Array.isArray(attrConfig.attributes)
            && attrConfig.attributes.indexOf(attr.attributeID) > -1)
            || attrConfig.attributes === '*') {
            self.push({
                attributeId: attr.attributeID,
                displayName: attr.displayName,
                id: attr.ID,
                swatchable: self.isSwatchable(attr.attributeID),
                values: values,
                resetUrl: resetUrl
            });
        } else if (attrConfig.attributes === 'selected') {
            self.push({
                displayName: attr.displayName,
                displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
                attributeId: attr.attributeID,
                id: attr.ID
            });
        }
    });
}

VariationAttributesModel.prototype = [];
VariationAttributesModel.prototype.isSwatchable = isSwatchable;
VariationAttributesModel.prototype.getAllAttrValues = getAllAttrValues;
VariationAttributesModel.prototype.getAttrResetUrl = getAttrResetUrl;

module.exports = VariationAttributesModel;
