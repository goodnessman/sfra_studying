var base = module.superModule;

var productDecorators = require('*/cartridge/models/product/decorators/index');

/**
 * @description Decorate product with product line item information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
module.exports = function fullProduct(product, apiProduct, options) {
    var extProduct = base(product, apiProduct, options);

    productDecorators.deliveryAndReturns(extProduct, apiProduct);

    if (options.variationModel) {
        productDecorators.zoomImages(extProduct, options.variationModel);
    } else {
        productDecorators.zoomImages(extProduct, apiProduct);
    }

    return extProduct;
};
