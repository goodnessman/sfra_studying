var PromotionMgr = require('dw/campaign/PromotionMgr');
var decorators = require('*/cartridge/models/product/decorators/index');

/**
 * Decorate product with product tile information
 *
 * @description overrides native SFRA implementation: adds availability, promotions and fixes searchPrice calculation
 *
 * @param {object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 *
 * @returns {object} - Decorated product model
 */
module.exports = function productTile(product, apiProduct, productType) {
    var promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
    decorators.base(product, apiProduct, productType);
    decorators.availability(product, 1, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel);
    decorators.promotions(product, promotions);
    decorators.price(product, apiProduct, promotions);
    decorators.images(product, apiProduct, { types: ['medium'], quantity: 'single' });
    decorators.ratings(product);
    if (productType === 'set') {
        decorators.setProductsCollection(product, apiProduct);
    }

    if (apiProduct.isVariant() || apiProduct.isVariationGroup() || apiProduct.isMaster()) {
        decorators.searchVariationAttributes(product, apiProduct);
    }

    return product;
};
