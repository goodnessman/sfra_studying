var decorators = require('*/cartridge/models/product/decorators/index');

/**
 * @description Decorate product with full product information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options
 * @returns {Object} - Decorated product model
 */
module.exports = function fullProduct(product, apiProduct, options) {
    decorators.base(product, apiProduct, options.productType);
    decorators.quantity(product, apiProduct, options.quantity);
    decorators.availability(product, options.quantity, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel);
    decorators.quantitySelector(product, apiProduct.stepQuantity.value, options.variables, options.options);
    decorators.readyToOrder(product, options.variationModel);

    return product;
};
