var superModule = module.superModule;

/**
 * Order class that represents the current order
 *
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
    var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
    var listItemModel = require('*/cartridge/models/orderListItem');

    if (options && options.config && options.config.numberOfLineItems === 'single') {
        listItemModel.call(this, lineItemContainer);
    } else {
        superModule.call(this, lineItemContainer, options);

        if (lineItemContainer) {
            this.totalQuantity = ProductLineItemsModel.getTotalQuantity(
                lineItemContainer.productLineItems
            );
            this.customerName = lineItemContainer.customerName;
        }
    }
}

module.exports = OrderModel;
