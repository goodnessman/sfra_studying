var URLUtils = require('dw/web/URLUtils');

var collections = require('*/cartridge/scripts/util/collections');

/**
 * @description Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @class
 */
function Categories(items) {
    this.categories = [];
    collections.forEach(items, function (item) {
        if (item.custom && item.custom.showInMenu
            && (item.hasOnlineProducts() || item.hasOnlineSubCategories() || item.custom.alternativeUrl)) {
            this.categories.push(this.categoryToObject(item));
        }
    }, this);
}

/**
 * @description Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
Categories.prototype.getCategoryUrl = function (category) {
    return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
        ? (category.custom.alternativeUrl.toString()).replace(/&amp;/g, '&')
        : URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
};

/**
 * @description Get highlight flag
 * @param {dw.catalog.Category} category - Current category
 * @returns {boolean} - Highlight flag
 */
Categories.prototype.getHighlightFlag = function (category) {
    return category.custom && 'isHighlighted' in category.custom && category.custom.isHighlighted
        ? category.custom.isHighlighted
        : false;
};

/**
 * @description Get custom Menu ID
 * @param {dw.catalog.Category} category - Current category
 * @returns {string|null} - Custom Menu asset id
 */
Categories.prototype.getCustomMenuAssetId = function (category) {
    return category.custom && 'customMenuAssetId' in category.custom && category.custom.customMenuAssetId
        ? category.custom.customMenuAssetId
        : null;
};

/**
 * @description Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @returns {Object} plain object that represents a category
 */
Categories.prototype.categoryToObject = function (category) {
    var self = this;

    if (!category.custom || !category.custom.showInMenu) {
        return null;
    }

    var result = {
        name: category.getDisplayName(),
        url: self.getCategoryUrl(category),
        id: category.ID,
        isHighlighted: self.getHighlightFlag(category),
        customMenuAssetId: self.getCustomMenuAssetId(category),
        raw: category
    };

    var subCategories = category.hasOnlineSubCategories()
        ? category.getOnlineSubCategories() : null;

    if (subCategories) {
        collections.forEach(subCategories, function (subcategory) {
            var converted = null;

            if (subcategory.hasOnlineProducts() || subcategory.hasOnlineSubCategories()) {
                converted = self.categoryToObject(subcategory);
            }

            if (converted) {
                if (!result.subCategories) {
                    result.subCategories = [];
                }
                result.subCategories.push(converted);
            }
        });

        if (result.subCategories) {
            result.complexSubCategories = result.subCategories.some(function (item) {
                return !!item.subCategories;
            });
        }
    }

    return result;
};

module.exports = Categories;
