var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

var addressBase = module.superModule;

function address(addressObject) {
    addressObject = addressObject && Object.prototype.hasOwnProperty.call(addressObject, 'raw')
        ? addressObject.raw
        : addressObject;
    addressBase.call(this, addressObject);

    if (this.address && this.address.countryCode && this.address.countryCode.value) {
        this.address.country = this.address.countryCode.value;
    }

    if (this.address) {
        this.address.addressUUID = addressObject.UUID;

        var template = require('util/pref').get('address.template.'
            + (require('util/object').get(this, 'address.country') || '').toLowerCase(),
        'components/address/displayAddress_default');
        this.address.rendered = renderTemplateHelper.getRenderedHtml({
            address: {
                address: this.address
            },
            template: template
        }, 'components/address/address');
    }
}

module.exports = address;
