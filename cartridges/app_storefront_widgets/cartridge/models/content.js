/**
 * @description Get Content Object
 *
 * @param  {dw.content.Content|dw.content.Folder|string} contentValue  Content Object, Content folder or ID
 * @returns {dw.content.Content} Content object
 */
function getContentObject(contentValue) {
    var ContentMgr = require('dw/content/ContentMgr');

    var contentObject = null;

    if (!empty(contentValue)) {
        if (contentValue instanceof dw.content.Content || contentValue instanceof dw.content.Folder) {
            contentObject = contentValue;
        } else if (typeof contentValue === 'string') {
            contentObject = ContentMgr.getContent(contentValue);
        }
    }
    return contentObject;
}

/**
 * @description Content model
 * @param  {dw.content.Content|dw.content.Folder|string} contentValue - Content Object, Content folder or ID
 * @param  {string} renderingTemplate - rendering template for the given content
 * @returns {Content} return instance of class
 * @class
 */
function Content(contentValue, renderingTemplate) {
    var contentObject = getContentObject(contentValue);

    if (!contentObject) {
        require('dw/system/Logger').error('Content asset with id = "' + contentValue + '" doesn\'t exist');
        return null;
    }

    if (!contentObject.online) {
        return null;
    }

    var usedRenderingTemplate = renderingTemplate || 'components/content/contentAssetInc';

    this.body = (contentObject && contentObject.custom && contentObject.custom.body) || null;
    this.UUID = contentObject.UUID;
    this.ID = contentObject.ID;
    this.template = contentObject.template || usedRenderingTemplate;
    this.pageTitle = contentObject.pageTitle;
    this.pageDescription = contentObject.pageDescription;
    this.pageKeywords = contentObject.pageKeywords;
    this.isFolder = false;
    this.raw = contentObject;

    if (contentObject instanceof dw.content.Content) {
        this.name = contentObject.name;
        this.pageMetaTags = contentObject.pageMetaTags;
    } else if (contentObject instanceof dw.content.Folder) {
        var ContentSearchModel = require('dw/content/ContentSearchModel');
        var contentSearchModel = new ContentSearchModel();

        contentSearchModel.setFolderID(this.ID);
        this.name = contentObject.displayName;
        this.pageMetaTags = contentSearchModel.pageMetaTags;
        this.isFolder = true;
    }
    return this;
}

/**
 * @description Get content asset markup
 * Support parsing velocity template that defined in content asset body
 * @param {object} [contextObject] option context object used in velocity template as $object
 * @see {@link module:velocityHelper~getScope}
 * @returns {string} rendered asset markup
 */
Content.prototype.getMarkup = function (contextObject) {
    var markup = this.body && this.body.getMarkup();
    if (markup) {
        var velocityHelper = require('*/cartridge/scripts/helpers/velocityHelper');
        return velocityHelper.render(markup, contextObject || this);
    }
    return '';
};

/**
 * @description Creates content nav element
 * @param  {dw.content.Content} folder - API content object
 * @param  {array} path - top-bottom inheritance points
 * @returns {object} content nav element
*/

Content.prototype.getContentNavElement = function (content, path) {
    var URLUtils = require('dw/web/URLUtils');

    return {
        UUID: content.UUID,
        ID: content.ID,
        name: content.name,
        active: path ? path.indexOf(content) > -1 : false,
        url: URLUtils.url('Page-Show', 'cid', content.ID).toString(),
        children: []
    };
};

/**
 * @description Creates folder nav element
 * @param  {dw.content.Folder} folder - API folder object
 * @param  {array} path - top-bottom inheritance points, all points from this list should be created
 * @param  {array} includeChildren - folder element with sub folders should be returned
 * @returns {object} folder nav element
 */
Content.prototype.getFolderNavElement = function (folder, path, includeChildren) {
    var URLUtils = require('dw/web/URLUtils');
    var collections = require('*/cartridge/scripts/util/collections');
    var isActive = empty(path) ? includeChildren : path.indexOf(folder) > -1;
    var that = this;

    var result = {
        UUID: folder.UUID,
        ID: folder.ID,
        name: folder.displayName,
        online: folder.online,
        active: isActive
    };

    if (folder.onlineSubFolders.length) {
        result.url = URLUtils.url('Search-ShowContent', 'fdid', folder.ID).toString();
        if (isActive) {
            result.children = collections.map(folder.onlineSubFolders, function (childFolder) {
                return that.getFolderNavElement(childFolder, path);
            });
        }
    } else if (folder.onlineContent.length === 1) {
        result.url = URLUtils.url('Page-Show', 'cid', folder.onlineContent[0].ID).toString();
    } else {
        result.url = URLUtils.url('Search-ShowContent', 'fdid', folder.ID).toString();
        if (isActive) {
            result.children = collections.map(folder.onlineContent, function (childFolder) {
                return that.getContentNavElement(childFolder, path);
            });
        }
    }

    return result;
};

/**
 * @description Creates folders hierarchy model for current folder or content
 * @returns {object} Top-level parent folder with lower-level parents and siblings as children
 */
Content.prototype.getNavStructure = function () {
    var path = [];
    var result = null;
    var folder;

    if (this.isFolder) {
        folder = this.raw;
    } else {
        var content = this.raw;

        path.push(content);
        folder = content.getClassificationFolder();
        if (!folder && content.folders.length) {
            folder = content.folders[0];
        }
    }

    while (folder && !folder.root) {
        path.unshift(folder);
        folder = folder.parent;
    }

    if (path.length) {
        var firstPathElement = path[0];

        if (firstPathElement instanceof dw.content.Content) {
            result = this.getContentNavElement(firstPathElement, path);
        } else if (firstPathElement instanceof dw.content.Folder) {
            result = this.getFolderNavElement(firstPathElement, path);
        }
    }

    return result;
};

/**
 * @description Checks if rendering template of content or folder has navigation
 * @param {string} template rendering template for check
 * @returns {boolean} does rendering template has navigation
 */
Content.prototype.templateHasNav = function (template) {
    if (!template) {
        template = this.template;
    }

    return String(template).indexOf('rendering/content/with-nav') > -1;
};


module.exports = Content;
