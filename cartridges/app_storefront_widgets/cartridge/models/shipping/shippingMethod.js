var Money = require('dw/value/Money');
var ShippingMgr = require('dw/order/ShippingMgr');
var formatCurrency = require('*/cartridge/scripts/util/formatting').formatCurrency;

/**
 * @override Comparing to SFRA, fixed displaying of the correct price
 * for basket\order instances in the getShippingCost method
 */

/**
 * @description Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 * @param {string} containerView - a container view (order or basket)
 * @param {dw.order.Basket | dw.order.lineItemContainer} lineItemContainer Current users's basket/order
 */
function ShippingMethodModel(shippingMethod, shipment, containerView, lineItemContainer) {
    this.ID = shippingMethod ? shippingMethod.ID : null;
    this.displayName = shippingMethod ? shippingMethod.displayName : null;
    this.description = shippingMethod ? shippingMethod.description : null;
    this.estimatedArrivalTime = shippingMethod && shippingMethod.custom
        ? shippingMethod.custom.estimatedArrivalTime : null;
    this.default = shippingMethod ? shippingMethod.defaultMethod : null;

    // Mix in dynamically transformed properties
    if (shipment) {
        // Optional model information available with 'shipment' parameter
        this.shippingCost = this.getShippingCost(shippingMethod, shipment, containerView, lineItemContainer);
        this.selected = this.getIsSelected(shippingMethod, shipment);
    }
}

/**
 * @description Returns shippingCost property for a specific Shipment / ShippingMethod pair
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} shipment - a shipment of the current basket
 * @param {string} containerView - a container view (order or basket)
 * @param {dw.order.Basket | dw.order.lineItemContainer} lineItemContainer Current users's basket/order
 * @returns {string} String representation of Shipping Cost
 */
ShippingMethodModel.prototype.getShippingCost = function (shippingMethod, shipment, containerView, lineItemContainer) {
    if (containerView === 'order') {
        return this.getShippingPrice(lineItemContainer);
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var Logger = require('dw/system/Logger');
    var basket = BasketMgr.getCurrentBasket();

    if (!basket) {
        try {
            return this.getShippingPrice(lineItemContainer);
        } catch (err) {
            Logger.error('ShippingMethodModel: Unable to get shipping cost');

            return {};
        }
    }

    var Transaction = require('dw/system/Transaction');
    var PromotionMgr = require('dw/campaign/PromotionMgr');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var defaultShippingMethod = basket.getDefaultShipment().getShippingMethod();
    var shippingCost;

    Transaction.begin();
    // Sets shipping method.
    shipment.setShippingMethod(shippingMethod);
    PromotionMgr.applyDiscounts(basket);
    ShippingMgr.applyShippingCost(basket);

    shippingCost = this.getShippingPrice(basket);

    Transaction.rollback();

    Transaction.wrap(function () {
        shipment.setShippingMethod(defaultShippingMethod);
        basketCalculationHelpers.calculateTotals(basket);
    });

    return shippingCost;
};

/**
 * @description Return object with shipping base, adjusted and total price.
 * @param {dw.order.Basket | dw.order.Order} basket - basket\order instance
 * @returns {object} result
 */
ShippingMethodModel.prototype.getShippingPrice = function (basket) {
    var Resource = require('dw/web/Resource');
    var priceAdjustments = this.getShippingPriceAdjustments(basket);
    var priceAdjustmentsTotal = priceAdjustments.priceAdjustmentsTotal;
    var shippingPriceBase = basket.getShippingTotalPrice();
    var shippingPriceAdj = priceAdjustments.priceAdjustmentsTotal >= 0
        ? shippingPriceBase.subtract(priceAdjustmentsTotal)
        : shippingPriceBase.add(priceAdjustmentsTotal);
    var shippingPrice = {};

    shippingPrice.shippingPriceBase = formatCurrency(shippingPriceBase.value, shippingPriceBase.currencyCode);

    if ((new Money(0.0, basket.getCurrencyCode()).equals(shippingPriceAdj))
        || new Money(0.0, basket.getCurrencyCode()).equals(shippingPriceBase)) {
        shippingPrice.shippingPriceBase = Resource.msg('totals.shipping.free', 'cart', null);
    } else if (!shippingPriceAdj.equals(shippingPriceBase)) {
        shippingPrice.shippingPriceAdjusted = formatCurrency(shippingPriceAdj.value, shippingPriceAdj.currencyCode);
    }

    shippingPrice.priceFinal = shippingPrice.shippingPriceAdjusted
        ? shippingPrice.shippingPriceAdjusted
        : shippingPrice.shippingPriceBase;

    shippingPrice.priceAdjustments = priceAdjustments.priceAdjustmentsArray;

    return shippingPrice;
};

/**
 * @description Returns isSelected property for a specific Shipment / ShippingMethod pair
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} shipment - a shipment of the current basket
 * @returns {boolean} true is shippingMethod is selected in Shipment
 */
ShippingMethodModel.prototype.getIsSelected = function (shippingMethod, shipment) {
    var selectedShippingMethod = shipment.shippingMethod || ShippingMgr.getDefaultShippingMethod();
    var selectedShippingMethodID = selectedShippingMethod ? selectedShippingMethod.ID : null;

    return (selectedShippingMethodID && shippingMethod.ID === selectedShippingMethodID);
};

/**
 * @description Returns shupping price adjustments and price adjustments total
 * @param {dw.order.Basket} basket - current basket
 * @returns {object} result
 */
ShippingMethodModel.prototype.getShippingPriceAdjustments = function (basket) {
    var priceAdjustmentsTotal = new Money(0.0, basket.getCurrencyCode());
    var priceAdjustments = basket.getDefaultShipment().getShippingPriceAdjustments().iterator();
    var result = [];

    while (priceAdjustments.hasNext()) {
        var priceAdjustment = priceAdjustments.next();

        if (priceAdjustment && priceAdjustment.promotion) {
            priceAdjustmentsTotal = priceAdjustmentsTotal.add(priceAdjustment.price);

            result.push({
                price: priceAdjustment.price,
                calloutMsg: priceAdjustment.promotion.calloutMsg.getMarkup()
            });
        }
    }

    return {
        priceAdjustmentsArray: result,
        priceAdjustmentsTotal: priceAdjustmentsTotal
    };
};

module.exports = ShippingMethodModel;
