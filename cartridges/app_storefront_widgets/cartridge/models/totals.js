var base = module.superModule;

var Money = require('dw/value/Money');
var Resource = require('dw/web/Resource');
var formatMoney = require('dw/util/StringUtils').formatMoney;

function getTotalShippingCost(shippingTotalPrice) {
    return {
        value: shippingTotalPrice.value,
        formatted: formatMoney(shippingTotalPrice),
        isFree: shippingTotalPrice.value === 0
    };
}

/**
 * @description totals model
 * @classdesc represents the totals of the current line item container
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
    base.call(this, lineItemContainer);

    this.isZeroTotal = lineItemContainer && lineItemContainer.totalGrossPrice.available
        && (new Money(0, lineItemContainer.getCurrencyCode()).equals(lineItemContainer.totalGrossPrice));

    this.totalShippingCostObject = {
        isFree: false
    };

    if (this.grandTotal === '-') {
        this.grandTotal = Resource.msg('totals.grandtotal.tbd', 'cart', null);
    }

    this.hasShippingCost = true;

    if (this.totalShippingCost === '-') {
        this.hasShippingCost = false;
    } else {
        this.totalShippingCostObject = getTotalShippingCost(lineItemContainer.shippingTotalPrice);
    }

    if (this.discounts) {
        var couponDiscountsCount = 0;

        this.discounts.forEach(function (dicsount) {
            if (dicsount.relationship) {
                dicsount.relationship.forEach(function (relationshipItem) {
                    if (relationshipItem.callOutMsg) {
                        relationshipItem.callOutMsgMarkup = relationshipItem.callOutMsg.getMarkup();
                    } else {
                        relationshipItem.callOutMsgMarkup = '';
                    }
                });
            }

            if (dicsount.type === 'coupon') {
                dicsount.isCouponDiscount = true;
                couponDiscountsCount++;
            }
        });

        this.onlyOneCouponDiscount = couponDiscountsCount === 1;
        this.hasCouponDisconts = couponDiscountsCount > 0;
    }
}

module.exports = totals;
