var AddressModel = require('*/cartridge/models/address');
var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');

/**
 * @override Comparing to SFRA, fixed displaying of the correct price
 * for basket\order instances in the getSelectedShippingMethod method
 */

/**
 * @description Model that represents shipping information.
 * Overidden due to the bug with selected shipping method prices for the specified containerView
 *
 * @param {dw.order.Shipment} shipment - the default shipment of the current basket
 * @param {object} address - the address to use to filter the shipping method list
 * @param {object} customer - the current customer model
 * @param {string} containerView - the view of the product line items (order or basket)
 * @param {dw.order.Basket | dw.order.lineItemContainer} lineItemContainer Current users's basket/order
 */
function ShippingModel(shipment, address, customer, containerView, lineItemContainer) {
    var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');

    // Simple properties
    this.UUID = this.getShipmentUUID(shipment);

    // Derived properties
    this.productLineItems = this.getProductLineItemsModel(shipment, containerView);
    this.applicableShippingMethods = shippingHelpers.getApplicableShippingMethods(
        shipment,
        address,
        containerView,
        lineItemContainer
    );
    this.selectedShippingMethod = this.getSelectedShippingMethod(shipment, containerView, lineItemContainer);
    this.matchingAddressId = this.getAssociatedAddress(shipment, customer);

    // Optional properties
    if (this.emptyAddress(shipment)) {
        this.shippingAddress = new AddressModel(shipment.shippingAddress).address;
    } else {
        this.shippingAddress = address;
    }

    this.isGift = shipment ? shipment.gift : null;
    this.giftMessage = shipment ? shipment.giftMessage : null;
}

/**
 * @description Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {string} containerView - the view of the product line items (order or basket)
 * @returns {ProductLineItemsModel} an array of ShippingModels
 */
ShippingModel.prototype.getProductLineItemsModel = function (shipment, containerView) {
    if (!shipment) {
        return null;
    }

    return new ProductLineItemsModel(shipment.productLineItems, containerView);
};

/**
 * @description Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * CHANGE: Comparing to SFRA, added containerView and lineItemContainer parameters for the ShippingMethodModel
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {string} containerView - the view of the product line items (order or basket)
 * @param {dw.order.Basket | dw.order.lineItemContainer} lineItemContainer Current users's basket/order
 * @returns {object || null} result - a ShippingMethodModel object
 */
ShippingModel.prototype.getSelectedShippingMethod = function (shipment, containerView, lineItemContainer) {
    if (!shipment) {
        return null;
    }

    return shipment.shippingMethod
        ? new ShippingMethodModel(shipment.shippingMethod, shipment, containerView, lineItemContainer)
        : null;
};

/**
 * @description Returns Shipment UUID or null
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @returns {string} the Shipment UUID or null
 */
ShippingModel.prototype.getShipmentUUID = function (shipment) {
    if (!shipment) return null;

    return shipment.UUID;
};

/**
 * @description Returns the matching address ID or UUID for a shipping address
 * @param {dw.order.Shipment} shipment - line items model
 * @param {object} customer - customer model
 * @returns {string|boolean} returns matching ID or false
*/
ShippingModel.prototype.getAssociatedAddress = function (shipment, customer) {
    var address = shipment ? shipment.shippingAddress : null;
    var matchingId;
    var anAddress;

    if (!address) return false;

    // If we still haven't found a match, then loop through customer addresses to find a match
    if (!matchingId && customer && customer.addressBook && customer.addressBook.addresses) {
        for (var j = 0, jj = customer.addressBook.addresses.length; j < jj; j++) {
            anAddress = customer.addressBook.addresses[j];

            if (anAddress && anAddress.isEquivalentAddress(address)) {
                matchingId = anAddress.ID;
                break;
            }
        }
    }

    return matchingId;
};

/**
 * @description Returns a boolean indicating if the address is empty
 * @param {dw.order.Shipment} shipment - A shipment from the current basket
 * @returns {boolean} a boolean that indicates if the address is empty
 */
ShippingModel.prototype.emptyAddress = function (shipment) {
    if (shipment && shipment.shippingAddress) {
        return [
            'firstName',
            'lastName',
            'address1',
            'address2',
            'phone',
            'city',
            'postalCode',
            'stateCode'
        ].some(function (key) {
            return shipment.shippingAddress[key];
        });
    }

    return false;
};

module.exports = ShippingModel;
