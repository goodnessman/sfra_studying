
/**
 * @class
 * @classdesc Model that encapsulates product sort options<br/>
 * FIXED: sz parameter was not correct and was different for each applied refinement, that cause cache miss<br/>
 * CHANGE: Moved all method to prototype to be able override on brand level
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {string|null} sortingRuleId - HTTP Param srule value
 * @param {dw.util.List.<dw.catalog.SortingOption>} sortingOptions - Sorting rule options
 * @param {dw.catalog.Category} rootCategory - Catalog's root category
 * @param {dw.web.PagingModel} pagingModel - The paging model for the current search context
 */
function ProductSortOptions(
    productSearch,
    sortingRuleId,
    sortingOptions,
    rootCategory,
    pagingModel
) {
    this.options = this.getSortingOptions(productSearch, sortingOptions, pagingModel);
    this.ruleId = sortingRuleId || this.getSortRuleDefault(productSearch, rootCategory);
}

ProductSortOptions.ACTION_ENDPOINT = 'Search-ShowAjax';

/**
 * @description Retrieves sorting options
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {dw.util.List.<dw.catalog.SortingOption>} sortingOptions - List of sorting rule options
 * @param {dw.web.PagingModel} pagingModel - The paging model for the current search context
 * @returns {SortingOption} - Sorting option
 */
ProductSortOptions.prototype.getSortingOptions = function (productSearch, sortingOptions, pagingModel) {
    var collections = require('*/cartridge/scripts/util/collections');
    var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');

    return collections.map(sortingOptions, function (option) {
        var baseUrl = productSearch.urlSortingRule(ProductSortOptions.ACTION_ENDPOINT, option.sortingRule);
        var pagingParams = {
            start: '0',
            sz: pagingModel.pageSize
        };
        return {
            displayName: option.displayName,
            id: option.ID,
            url: urlHelper.appendQueryParams(baseUrl.toString(), pagingParams).toString()
        };
    });
};

/**
 * @description Retrieves refined or default category sort ID
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {dw.catalog.Category} rootCategory - Catalog's root category
 * @returns {string} - Sort rule ID or null if no default sorting rule specified
 */
ProductSortOptions.prototype.getSortRuleDefault = function (productSearch, rootCategory) {
    var category = productSearch.category ? productSearch.category : rootCategory;
    return category.defaultSortingRule ? category.defaultSortingRule.ID : null;
};

module.exports = ProductSortOptions;
