var base = module.superModule;

var preferences = require('*/cartridge/config/preferences');
var DEFAULT_PAGE_SIZE = (preferences && preferences.defaultProductPageSize) || 24;

var ACTION_ENDPOINT_AJAX = 'Search-ShowAjax';
var ACTION_ENDPOINT_UPDATE_GRID = 'Search-UpdateGrid';
var ACTION_ENDPOINT_SHOW = 'Search-Show';

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

/**
 * @description Retrieves search refinements. Added hit count for category.
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {dw.catalog.ProductSearchRefinements} refinements - Search refinements
 * @param {ArrayList.<dw.catalog.ProductSearchRefinementDefinition>} refinementDefinitions - List of
 *     product search refinement definitions
 * @returns {Refinement[]} - List of parsed refinements
 */
function getRefinements(productSearch, refinements, refinementDefinitions) {
    var collections = require('*/cartridge/scripts/util/collections');
    var searchRefinementsFactory = require('*/cartridge/scripts/factories/searchRefinements');
    var pref = require('util/pref');

    return collections.map(refinementDefinitions, function (definition) {
        var refinementValues = refinements.getAllRefinementValues(definition);
        var values = searchRefinementsFactory.get(productSearch, definition, refinementValues);
        var selectedFilters = [];
        var refinementId;
        var expanded;

        if (definition.categoryRefinement) {
            refinementId = 'category';
        } else if (definition.priceRefinement) {
            refinementId = 'price';
        } else {
            refinementId = definition.attributeID;
        }

        expanded = pref.is('refinements.' + refinementId + '.expanded');

        if (!definition.categoryRefinement) {
            values.forEach(function (value) {
                if (value.selected) {
                    selectedFilters.push(value.displayValue);
                }
            });
        }

        return {
            displayName: definition.displayName,
            refinementName: definition.displayName.toLowerCase().replace(/ /g, '-'),
            isColor: definition.attributeID === pref.get('refinements.color.attr.id'),
            isSize: definition.attributeID === pref.get('refinements.size.attr.id'),
            isCategoryRefinement: definition.categoryRefinement,
            isAttributeRefinement: definition.attributeRefinement,
            isPriceRefinement: definition.priceRefinement,
            selectedFiltersText: selectedFilters.join(', '),
            values: values,
            expanded: expanded
        };
    });
}

/**
 * @description Returns the refinement values that have been selected
 *
 * @param {Array.<CategoryRefinementValue|AttributeRefinementValue|PriceRefinementValue>} refinements - List of all relevant refinements for this search
 * @returns {object[]} - List of selected filters
 */
function getSelectedFilters(refinements) {
    var selectedFilters = [];
    var selectedValues = [];

    refinements.forEach(function (refinement) {
        if (!refinement.isCategoryRefinement) {
            selectedValues = refinement.values.filter(function (value) { return value.selected; });
            selectedValues.forEach(function (value) {
                value.displayName = refinement.displayName;
            });

            if (selectedValues.length) {
                selectedFilters.push.apply(selectedFilters, selectedValues);
            }
        }
    });

    return selectedFilters;
}

/**
 * Configures and returns a PagingModel instance
 *
 * @param {dw.util.Iterator} productHits - Iterator for product search results
 * @param {number} count - Number of products in search results
 * @param {number} pageSize - Number of products to display
 * @param {number} startIndex - Beginning index value
 * @return {dw.web.PagingModel} - PagingModel instance
 */
function getPagingModel(productHits, count, pageSize, startIndex) {
    var PagingModel = require('dw/web/PagingModel');
    var paging = new PagingModel(productHits, count);

    paging.setStart(startIndex || 0);
    paging.setPageSize(pageSize || DEFAULT_PAGE_SIZE);

    return paging;
}

/**
 * Generates URL for "Load previous" button
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @return {string} - Prev button URL or empty string if not applicable
 */
function getShowPrev(productSearch, httpParams) {
    if (httpParams.ajax && !httpParams.loadPrev && !httpParams.back) {
        return {};
    }

    var currentStart = parseInt(httpParams.start || 0, 10);
    if (currentStart === 0) {
        return {};
    }

    var pageSize = httpParams.sz || DEFAULT_PAGE_SIZE;
    var prevStart = currentStart - pageSize;

    if (prevStart < 0) {
        prevStart = 0;
    }

    var paging = getPagingModel(
        productSearch.productSearchHits,
        productSearch.count,
        DEFAULT_PAGE_SIZE,
        prevStart
    );

    var baseUrl = productSearch.url(ACTION_ENDPOINT_UPDATE_GRID);
    var historyUrl = paging.appendPaging(productSearch.url(ACTION_ENDPOINT_SHOW));
    var showAjaxUrl = paging.appendPaging(productSearch.url(ACTION_ENDPOINT_AJAX));

    var prevCanonicalPageURL = '';
    if (productSearch.categoryID) {
        prevCanonicalPageURL = URLUtils.abs('Search-Show', 'cgid', productSearch.categoryID);
    } else if (httpParams.q) {
        prevCanonicalPageURL = URLUtils.abs('Search-Show', 'q', httpParams.q);
    }
    if (prevCanonicalPageURL && paging.currentPage > 0) {
        prevCanonicalPageURL = paging.appendPaging(prevCanonicalPageURL);
    }

    return {
        historyUrl: historyUrl,
        ajaxUrl: showAjaxUrl.append('back', true),
        url: paging.appendPaging(baseUrl.append('loadPrev', true)),
        prevCanonicalPageURL: prevCanonicalPageURL,
        count: DEFAULT_PAGE_SIZE
    };
}

/**
 * Generates URL for "Load more" button
 * Also return search pagination indicator
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @return {object} - Next button related parameters (pages indicator, `load more` button)
 */
function getShowMore(productSearch, httpParams) {
    if (httpParams.loadPrev) {
        return {};
    }

    var currentStart = httpParams.start || 0;
    var pageSize = httpParams.sz || DEFAULT_PAGE_SIZE;
    var hitsCount = productSearch.count;
    var nextStart;

    var paging = getPagingModel(
        productSearch.productSearchHits,
        hitsCount,
        pageSize,
        currentStart
    );

    if (pageSize >= hitsCount) {
        return {
            total: hitsCount,
            viewed: hitsCount,
            viewedPercents: 100
        };
    } else if (pageSize > DEFAULT_PAGE_SIZE) {
        nextStart = pageSize;
    } else {
        var endIdx = paging.getEnd();
        nextStart = endIdx + 1 < hitsCount ? endIdx + 1 : null;

        if (!nextStart) {
            return {
                total: hitsCount,
                viewed: hitsCount,
                viewedPercents: 100
            };
        }
    }

    paging.setStart(nextStart);

    var count = hitsCount - nextStart;
    if (count >= pageSize) {
        count = pageSize;
    }

    var baseUrl = productSearch.url(ACTION_ENDPOINT_UPDATE_GRID);
    var finalUrl = paging.appendPaging(baseUrl);
    var historyUrl = paging.appendPaging(productSearch.url(ACTION_ENDPOINT_SHOW));
    var showAjaxUrl = paging.appendPaging(productSearch.url(ACTION_ENDPOINT_AJAX));
    var viewedPercents = (nextStart / hitsCount) * 100;

    var nextCanonicalPageURL = '';
    if (productSearch.categoryID) {
        nextCanonicalPageURL = URLUtils.abs('Search-Show', 'cgid', productSearch.categoryID);
    } else if (httpParams.q) {
        nextCanonicalPageURL = URLUtils.abs('Search-Show', 'q', httpParams.q);
    }
    if (nextCanonicalPageURL) {
        nextCanonicalPageURL = paging.appendPaging(nextCanonicalPageURL);
    }

    return {
        url: finalUrl,
        historyUrl: historyUrl,
        ajaxUrl: showAjaxUrl.append('back', true),
        nextCanonicalPageURL: nextCanonicalPageURL,
        count: count,
        total: hitsCount,
        viewed: nextStart,
        viewedPercents: viewedPercents
    };
}

/**
 * @description Generates URL that removes refinements, essentially resetting search criteria
 *
 * @param {dw.catalog.ProductSearchModel} search - Product search object
 * @param {object} httpParams - Query params
 * @param {string} [httpParams.q] - Search keywords
 * @param {string} [httpParams.cgid] - Category ID
 * @returns {string} - URL to reset query to original search
 */
function getResetLink(search, httpParams) {
    var url = search.categorySearch
        ? URLUtils.url(ACTION_ENDPOINT_AJAX, 'cgid', httpParams.cgid)
        : URLUtils.url(ACTION_ENDPOINT_AJAX, 'q', httpParams.q);

    if (search.sortingRule) {
        url.append('srule', search.sortingRule.ID);
    }

    return url.toString();
}

/**
 * @description Prepare object for JSON
 *
 * @returns {object} - Prepared object
 */
function toJSON() {
    var tmp = {};

    for (var key in this) {
        if (typeof this[key] !== 'function') {
            tmp[key] = this[key];
        }
    }

    tmp.refinements = this.refinements;
    tmp.selectedFilters = this.selectedFilters;

    return tmp;
}
/**
 * Generates URL current search URL
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {object} httpParams - HTTP query parameters
 * @param {boolean} isAjax - AJAX flag
 * @returns {string} - Show Url
 */
function getShowURL(productSearch, httpParams, isAjax) {
    var url = isAjax ? productSearch.url('Search-ShowAjax') : productSearch.url('Search-Show');

    if (httpParams.sz && httpParams.start) {
        url.append('sz', httpParams.sz);
        url.append('start', httpParams.start);
    }


    return url.toString();
}

/**
 * @description Returns first suggested search phrase from current search
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @returns {string} - first suggested search phrase or empty string
 */
function getSuggestedSearchPhrase(productSearch) {
    var suggestions = productSearch.getSearchPhraseSuggestions();
    var phrase = '';

    if (suggestions && suggestions.hasSuggestedPhrases()) {
        phrase = suggestions.getSuggestedPhrases().next().getPhrase();
    }

    return phrase;
}

/**
 * @class
 * @classdesc ProductSearch class
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {object} httpParams - HTTP query parameters
 * @param {string} sortingRule - Sorting option rule ID
 * @param {dw.util.ArrayList.<dw.catalog.SortingOption>} sortingOptions - Options to sort search
 *     results
 * @param {dw.catalog.Category} rootCategory - Search result's root category if applicable
 */
function ProductSearch(productSearch, httpParams, sortingRule, sortingOptions, rootCategory) {
    base.call(this, productSearch, httpParams, sortingRule, sortingOptions, rootCategory);

    this.showPrev = getShowPrev(productSearch, httpParams);
    this.showMore = getShowMore(productSearch, httpParams);

    this.resetLink = this.getResetLink(productSearch, httpParams);
    this.hasSelectedFilters = this.selectedFilters && this.selectedFilters.length > 0;
    this.resultText = Resource.msgf('refinements.showresults', 'search', null, this.count);
    this.refineBarUrl = this.isCategorySearch
        ? URLUtils.url('Search-Refinebar', 'ajax', true, 'cgid', this.category.id).toString()
        : URLUtils.url('Search-Refinebar', 'ajax', true).toString();
    this.historyUrl = this.getShowURL(productSearch, httpParams, false);
    this.ajaxUrl = this.getShowURL(productSearch, httpParams, true);
    this.firstPage = !httpParams.start || !parseInt(httpParams.start, 10);
    this.isRefinedSearch = productSearch.isRefinedSearch();
    this.suggestedSearchPhrase = getSuggestedSearchPhrase(productSearch);
}

Object.defineProperty(ProductSearch.prototype, 'refinements', {
    get: function () {
        if (!this.cachedRefinements) {
            this.cachedRefinements = this.getRefinements(
                this.productSearch,
                this.productSearch.refinements,
                this.productSearch.refinements.refinementDefinitions
            );
        }

        return this.cachedRefinements;
    }
});

Object.defineProperty(ProductSearch.prototype, 'selectedFilters', {
    get: function () {
        return this.getSelectedFilters(this.refinements);
    }
});

ProductSearch.prototype.getRefinements = getRefinements;
ProductSearch.prototype.getSelectedFilters = getSelectedFilters;
ProductSearch.prototype.getResetLink = getResetLink;
ProductSearch.prototype.toJSON = toJSON;
ProductSearch.prototype.getShowURL = getShowURL;

module.exports = ProductSearch;
