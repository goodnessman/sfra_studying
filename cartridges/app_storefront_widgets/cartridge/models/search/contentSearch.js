var base = module.superModule;

var preferences = require('*/cartridge/config/preferences');
var DEFAULT_PAGE_SIZE = (preferences && preferences.defaultContentPageSize) || 10;

/**
 * @constructor
 * @classdesc ContentSearch class
 * <br>Core class was overwritten, because we need
 * - extended functionality for `load more` button - number of content left
 * @param {dw.util.Iterator<dw.content.Content>} contentSearchResult - content iterator
 * @param {number} count - number of contents in the results
 * @param {string} queryPhrase - request queryPhrase
 * @param {number} startingPage - The index for the start of the content page
 * @param {number | null} pageSize - The index for the start of the content page
 *
 */
function ContentSearch(contentSearchResult, count, queryPhrase, startingPage, pageSize) {
    base.call(this, contentSearchResult, count, queryPhrase, startingPage, pageSize);

    var ps = pageSize || DEFAULT_PAGE_SIZE;
    var moreContentCount = count - parseInt((startingPage || 0), 10);

    if (moreContentCount > ps * 2) {
        moreContentCount = ps;
    } else {
        moreContentCount -= ps;
    }

    var viewed = (startingPage || ps) % ps;
    if (viewed === 0) {
        viewed = parseInt((startingPage || 0), 10) + ps;
    }

    if (viewed > count) {
        viewed = count;
    }

    this.moreContentCount = moreContentCount;

    if (count > 0) {
        this.progressIndicator = {
            viewedPercents: (viewed / count) * 100,
            viewed: viewed,
            total: count
        };
    }
}

module.exports = ContentSearch;
