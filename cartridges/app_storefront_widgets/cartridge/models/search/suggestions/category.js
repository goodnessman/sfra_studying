// TO BE REVIEWED
var URLUtils = require('dw/web/URLUtils');
var endpoint = 'Search-Show';

/**
 * @class
 * @classdesc CategorySuggestions class
 *
 * @param {dw.suggest.SuggestModel} suggestions - Suggest Model
 * @param {number} maxItems - Maximum number of categories to retrieve
 */
function CategorySuggestions(suggestions, maxItems) {
    this.categories = [];

    if (!suggestions.categorySuggestions) {
        this.available = false;
        return;
    }

    var categorySuggestions = suggestions.categorySuggestions;
    var iter = categorySuggestions.suggestedCategories;

    this.available = categorySuggestions.hasSuggestions();

    for (var i = 0; i < maxItems; i += 1) {
        var category = null;

        if (iter.hasNext()) {
            category = iter.next().category;

            var parentID = '';
            var parentName = category.parent.displayName;

            var parent = category.parent;
            while (parent && parent.ID !== 'root') {
                parentID = parent.ID;
                parentName = parent.displayName;
                parent = parent.parent;
            }

            this.categories.push({
                name: category.displayName,
                imageUrl: category.image ? category.image.url + '' : '',
                url: URLUtils.url(endpoint, 'cgid', category.ID).toString(),
                parentID: parentID,
                parentName: parentName
            });
        }
    }
}

module.exports = CategorySuggestions;
