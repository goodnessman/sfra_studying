// TO BE REVIEWED
var URLUtils = require('dw/web/URLUtils');
var ACTION_ENDPOINT = 'Product-Show';

var ObjectUtils = require('util/object');
var decorators = require('*/cartridge/models/product/decorators/index');
var PromotionMgr = require('dw/campaign/PromotionMgr');

/**
 * Compile a list of relevant suggested products
 *
 * @param {dw.util.Iterator<dw.suggest.SuggestedProduct>} suggestedProducts - Iterator to retrieve SuggestedProducts
 * @param {number} maxItems - Maximum number of products to retrieve
 * @param {object} productModelInstance - instance of ProductSuggestions
 * @returns {object[]} - Array of suggested products
 */
function getProducts(suggestedProducts, maxItems, productModelInstance) {
    var product = null;
    var products = [];

    for (var i = 0; i < maxItems; i += 1) {
        if (suggestedProducts.hasNext()) {
            var productSearchHit = suggestedProducts.next().productSearchHit;
            product = productSearchHit.product;
            // eslint-disable-next-line no-use-before-define
            products.push(productModelInstance.getSuggestionsProductModel(product));
        }
    }

    return products;
}

/**
 * @typedef SuggestedPhrase
 * @type Object
 * @property {boolean} exactMatch - Whether suggested phrase is an exact match
 * @property {string} value - Suggested search phrase
 */

/**
 * Compile a list of relevant suggested phrases
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedPhrase>} suggestedPhrases - Iterator to retrieve
 *                                                                           SuggestedPhrases
 * @param {number} maxItems - Maximum number of phrases to retrieve
 * @returns {SuggestedPhrase[]} - Array of suggested phrases
 */
function getPhrases(suggestedPhrases, maxItems) {
    var phrase = null;
    var phrases = [];

    for (var i = 0; i < maxItems; i += 1) {
        if (suggestedPhrases.hasNext()) {
            phrase = suggestedPhrases.next();
            phrases.push({
                exactMatch: phrase.exactMatch,
                value: phrase.phrase
            });
        }
    }

    return phrases;
}

/**
 * @class
 * @classdesc ProductSuggestions class
 *
 * @param {dw.suggest.SuggestModel} suggestions - Suggest Model
 * @param {number} maxItems - Maximum number of items to retrieve
 */
function ProductSuggestions(suggestions, maxItems) {
    var productSuggestions = suggestions.productSuggestions;

    if (!productSuggestions) {
        this.available = false;
        this.phrases = [];
        this.products = [];
        return;
    }

    var searchPhrasesSuggestions = productSuggestions.searchPhraseSuggestions;
    this.available = productSuggestions.hasSuggestions();
    this.phrases = getPhrases(searchPhrasesSuggestions.suggestedPhrases, maxItems);
    this.products = getProducts(productSuggestions.suggestedProducts, maxItems, this);
}

/**
 * Function to get formatted price from product object decorated with price
 *
 * @param {object} product product object decorated with price
 * @param {string} keySales key to retrieve salesPrice from product object
 * @param {string} keyList key to retrieve listPrice from product object
 * @returns {object} object containing listPrice & salesPrice
 */
function getFormattedPrice(product, keySales, keyList) {
    var salesPrice = ObjectUtils.get(product, keySales);
    var listPrice = ObjectUtils.get(product, keyList);
    return {
        listPrice: salesPrice && listPrice,
        salesPrice: salesPrice || listPrice
    };
}

/**
 * @description Method to obtaine search suggestion product model
 * <br>Having it here in place - brands could overwrite it
 * @param {dw.catalog.Product} apiProduct - an API product
 * @returns {object} - assembled and decorated suggestions product model
 */
ProductSuggestions.prototype.getSuggestionsProductModel = function (apiProduct) {
    var product = Object.create(null);
    var promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);

    decorators.base(product, apiProduct);
    decorators.price(product, apiProduct, promotions);
    decorators.images(product, apiProduct, { types: ['searchsuggestion'], quantity: 'single' });

    if (product.price.type === 'range') {
        product.priceRange = {
            minPrice: getFormattedPrice(product, 'price.min.sales.formatted', 'price.min.list.formatted'),
            maxPrice: getFormattedPrice(product, 'price.max.sales.formatted', 'price.max.list.formatted')
        };
    } else {
        var price = getFormattedPrice(product, 'price.sales.formatted', 'price.list.formatted');
        product.listPrice = price.listPrice;
        product.salesPrice = price.salesPrice;
    }
    product.url = URLUtils.url(ACTION_ENDPOINT, 'pid', apiProduct.ID).toString();

    return product;
};

module.exports = ProductSuggestions;
