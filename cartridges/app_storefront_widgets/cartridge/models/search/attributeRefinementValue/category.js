var collections = require('*/cartridge/scripts/util/collections');

/**
 * @description Get hit count
 *
 * @param {string} categoryID Category ID
 * @param {dw.util.Collection} refinementValues Raw DW refinement value
 * @returns {number} Hit count
 */
function getHitCount(categoryID, refinementValues) {
    var hitCount = 0;
    var refinementValue;

    if (!empty(refinementValues)) {
        refinementValue = collections.find(refinementValues, function (value) {
            return value.value === categoryID;
        });

        if (refinementValue) {
            hitCount = refinementValue.hitCount;
        }
    }

    return hitCount;
}
/**
 * @class
 * @classdesc Category attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.Category} refinementValue Raw DW refinement value
 * @param {boolean} selected Selected flag
 * @param {dw.util.Collection} refinementValues Raw DW refinement value
 * @returns {void}
 */
function CategoryRefinementValueWrapper(productSearch, refinementDefinition, refinementValue, selected, refinementValues) {
    module.superModule.call(this, productSearch, refinementDefinition, refinementValue, selected);
    this.hitCount = this.getHitCount(this.id, refinementValues);
}

CategoryRefinementValueWrapper.prototype.getHitCount = getHitCount;

module.exports = CategoryRefinementValueWrapper;
