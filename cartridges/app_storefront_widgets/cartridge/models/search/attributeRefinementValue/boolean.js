/**
 * @class
 * @description Extended boolean attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 */

function BooleanRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
    var util = require('util/pref');

    module.superModule.call(this, productSearch, refinementDefinition, refinementValue);

    this.hitCount = refinementValue.hitCount;
    this.isBoolean = this.id !== util.get('refinements.color.attr.id')
        && this.id !== util.get('refinements.color.attr.id');
    this.value = refinementValue.value;
}

module.exports = BooleanRefinementValueWrapper;
