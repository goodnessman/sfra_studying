/**
 * @description Get Color Code
 *
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue Raw DW refinement value
 * @returns {string|null} class flag
 */
function getColorCode(refinementValue) {
    var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');

    var colorCode = null;

    if (refinementValue.presentationID) {
        colorCode = refinementValue.presentationID;
    } else {
        colorCode = productHelpers.getColorCode(refinementValue.value);
    }

    return colorCode;
}

/**
 * @description Has rresentation class
 *
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue Raw DW refinement value
 * @returns {boolean} class flag
 */
function hasPresentationClass(refinementValue) {
    return !empty(refinementValue.presentationID) && refinementValue.presentationID.charAt(0) !== '#';
}

/**
 * @class
 * @classdesc Color attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue Raw DW refinement value
 * @returns {void}
 */
function ColorRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
    module.superModule.call(this, productSearch, refinementDefinition, refinementValue);

    this.hasClass = this.hasPresentationClass(refinementValue);
    this.colorCode = this.getColorCode(refinementValue);
    this.hitCount = refinementValue.hitCount;
    this.isColor = this.id === require('util/pref').get('refinements.color.attr.id');
    this.value = refinementValue.value;
}

ColorRefinementValueWrapper.prototype.hasPresentationClass = hasPresentationClass;
ColorRefinementValueWrapper.prototype.getColorCode = getColorCode;

module.exports = ColorRefinementValueWrapper;
