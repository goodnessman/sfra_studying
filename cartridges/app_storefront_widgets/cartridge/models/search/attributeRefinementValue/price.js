/**
 * @class
 * @classdesc Price refinement value class
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 */
function PriceRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
    module.superModule.call(this, productSearch, refinementDefinition, refinementValue);
    this.valueFrom = refinementValue.valueFrom;
    this.valueTo = refinementValue.valueTo;
}

module.exports = PriceRefinementValueWrapper;
