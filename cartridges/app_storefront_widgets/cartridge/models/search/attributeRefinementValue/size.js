/**
 * @class
 * @description Extended size attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue Raw DW refinement value
 */
function SizeRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
    module.superModule.call(this, productSearch, refinementDefinition, refinementValue);

    this.hitCount = refinementValue.hitCount;
    this.isSize = this.id === require('util/pref').get('refinements.size.attr.id');
    this.value = refinementValue.value;
}

module.exports = SizeRefinementValueWrapper;
