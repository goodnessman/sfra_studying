var DefaultPrice = require('*/cartridge/models/price/default');

function getOrderLineItemPrice(lineItem) {
    var salesPrice = lineItem.adjustedPrice.divide(lineItem.quantity.value);
    var listPrice = lineItem.getBasePrice();

    if (salesPrice.equals(listPrice)) {
        listPrice = null;
    }

    return new DefaultPrice(salesPrice, listPrice);
}

module.exports = function (object, lineItem) {
    var price = getOrderLineItemPrice(lineItem);

    Object.defineProperty(object, 'price', {
        enumerable: true,
        value: price
    });
};
