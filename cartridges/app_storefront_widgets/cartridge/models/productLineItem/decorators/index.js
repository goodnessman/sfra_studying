var baseDecorators = module.superModule;

baseDecorators.emailImages = require('*/cartridge/models/productLineItem/decorators/emailImages');
baseDecorators.price = require('*/cartridge/models/productLineItem/decorators/price');

module.exports = baseDecorators;
