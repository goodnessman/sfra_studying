var preferences = require('*/cartridge/config/preferences');
var DEFAULT_MAX_ORDER_QUANTITY = preferences.maxOrderQty || 10;

/**
 * @description Rewritten SFRA decorator that sets quantityOptions to pli. quantityOptions.maxOrderQuantity is calculated as on PDP
 *
 * @param {object} object - assembled product object
 * @param {dw.order.productLineItem} productLineItem - api productlineitem object
 */
module.exports = function (object, productLineItem) {
    Object.defineProperty(object, 'quantityOptions', {
        enumerable: true,
        value: {
            minOrderQuantity: productLineItem.product.minOrderQuantity.value || 1,
            maxOrderQuantity: DEFAULT_MAX_ORDER_QUANTITY
        }
    });
};
