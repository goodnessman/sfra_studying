var ImageModel = require('*/cartridge/models/product/productImages');

/**
 * @description Adds suitable images to decorated object to reflect in emails.
 * @param {object} object - object to decorate
 * @param {dw.catalog.Product} product - API product instance
 */
module.exports = function (object, product) {
    Object.defineProperty(object, 'emailImages', {
        enumerable: true,
        value: new ImageModel(product, {
            types: [
                require('util/pref').get('product.img.viewtype.email', 'email')
            ],
            quantity: 'single'
        })
    });
};
