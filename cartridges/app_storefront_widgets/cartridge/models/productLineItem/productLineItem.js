var baseProductLineItem = module.superModule;

var productDecorators = require('*/cartridge/models/product/decorators/index');

/**
 * @description Decorate product with product line item information
 * @param {object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {object} options - Options passed in from the factory
 * @property {dw.catalog.ProductVarationModel} options.variationModel - Variation model returned by the API
 * @property {object} options.lineItemOptions - Options provided on the query string
 * @property {dw.catalog.ProductOptionModel} options.currentOptionModel - Options model returned by the API
 * @property {dw.util.Collection} options.promotions - Active promotions for a given product
 * @property {number} options.quantity - Current selected quantity
 * @property {object} options.variables - Variables passed in on the query string
 *
 * @returns {object} - Decorated product model
 */
module.exports = function productLineItem(product, apiProduct, options) {
    var extProduct = baseProductLineItem(product, apiProduct, options);

    productDecorators.link(extProduct, apiProduct);
    productDecorators.productType(extProduct, apiProduct, options.productType);

    return extProduct;
};
