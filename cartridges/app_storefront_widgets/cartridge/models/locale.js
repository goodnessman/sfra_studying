var ApiLocale = require('dw/util/Locale');

/**
 * Represents current locale information in plain object
 * @param {dw.util.Locale} currentLocale - current locale of the request
 * @param {string} allowedLocales - list of allowed locales for the site
 * @param {string} siteId - id of the current site
 * @constructor
 */
function Locale(currentLocale, allowedLocales, siteId) {
    var countries = this.getLocalesConfig();
    var Site = require('dw/system/Site');
    var currentSiteID = Site.getCurrent().getID();
    var currentCurrencyCode = session.currency.currencyCode;
    var currentCountry = !this.isLocaleValid(currentLocale) ? countries[0]
        : countries.filter(function (country) {
            return country.locale === currentLocale.ID
                && currentSiteID === country.siteID
                && country.currencyCode === currentCurrencyCode;
        })[0];

    var localeLinks = this.getLocaleLinks(allowedLocales, siteId, currentLocale.ID);
    this.locale = {
        countryCode: currentLocale.country,
        name: currentLocale.displayCountry,
        localeLinks: localeLinks,
        currencyCode: currentCountry.currencyCode,
        displayName: currentCountry.displayName,
        language: currentLocale.language,
        displayLanguage: currentLocale.displayLanguage
    };
}

Locale.prototype.getLocalesConfig = function () {
    if (this.localesConfig) {
        return this.localesConfig;
    }
    this.localesConfig = require('util/sitepref').getJSONValue('LocalesConfig');
    return this.localesConfig;
};

/**
 * returns object needed to render links to change the locale of the site
 * @param {string} allowedLocales - list of allowed locales for the site
 * @param {string} siteId - id of the current site
 * @param {string} currentLocaleID - id of the current loale
 * @returns {Array} - array of Objects representing available locales
 */
Locale.prototype.getLocaleLinks = function (allowedLocales, siteId, currentLocaleID) {
    var localeOption;
    var apiLocale;
    var localeOptions = [];
    var countries = this.getLocalesConfig();

    if (countries && countries.length) {
        var Site = require('dw/system/Site');
        var currentCurrencyCode = session.currency.currencyCode;
        countries.forEach(function (locale) {
            var isOtherSite = Site.getCurrent().getID() !== locale.siteID;
            if (
                (allowedLocales.indexOf(locale.locale) > -1
                    && (locale.locale !== currentLocaleID || locale.currencyCode !== currentCurrencyCode)
                )
                || isOtherSite
            ) {
                apiLocale = ApiLocale.getLocale(locale.locale);

                localeOption = {
                    localeID: locale.locale,
                    country: (apiLocale && apiLocale.country) || '',
                    displayCountry: (apiLocale && apiLocale.displayCountry) || '',
                    currencyCode: locale.currencyCode,
                    displayName: locale.displayName || '',
                    language: (apiLocale && apiLocale.language) || '',
                    displayLanguage: (apiLocale && apiLocale.displayLanguage) || '',
                    isOtherSite: isOtherSite,
                    siteID: locale.siteID || ''
                };
                localeOptions.push(localeOption);
            }
        });
    }

    return localeOptions;
};

/**
 * Performs a deeper check on a plain locale object
 * @param {dw.util.Locale} currentLocale - current locale of the request
 * @return {boolean} - returns true
 */
Locale.prototype.isLocaleValid = function (currentLocale) {
    return currentLocale && currentLocale.ID;
};

module.exports = Locale;
