var accountBase = module.superModule;

/**
 * Account class that represents the current customer's profile dashboard
 * SFRA behavior was changed to filter addresses by current country
 * @param {Object} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 */
function account(currentCustomer, addressModel, orderModel) {
    accountBase.call(this, currentCustomer, addressModel, orderModel);

    if (this.addresses && this.addresses.length) {
        var Locale = require('dw/util/Locale');
        var locale = Locale.getLocale(request.locale);
        if (locale) {
            var allowedCountries = require('util/pref')
                .get('address.account.allowed.' + (locale.country || '').toLowerCase(), '');

            if (!empty(allowedCountries)) {
                this.addresses = this.addresses.filter(function (address) {
                    return allowedCountries.split(',').indexOf(address.country) > -1;
                });
            }
        }
    }
}

module.exports = account;
