/**
 * Order class that represents the current order in list
 *
 * @param {dw.order.Order} order - Current users's order
 * @constructor
 */
function OrderListItemModel(order) {
    var TotalsModel = require('*/cartridge/models/totals');
    var ImageModel = require('*/cartridge/models/product/productImages');
    var collections = require('*/cartridge/scripts/util/collections');

    var totalsModel = new TotalsModel(order);
    this.orderNumber = order.orderNo;
    this.priceTotal = totalsModel.grandTotal;
    this.creationDate = order.creationDate;
    this.orderEmail = order.customerEmail;
    this.orderStatus = order.status.value;
    this.orderItemsImages = collections.reduce(order.productLineItems, function (result, pli) {
        if (pli.product) {
            var imageModel = new ImageModel(pli.product, { types: ['small'], quantity: 'single' });

            if (!empty(imageModel.small)) {
                result.push(imageModel.small[0]);
            }
        }

        return result;
    }, []);
}

module.exports = OrderListItemModel;
