<div
    class="b-variations_item m-swatch m-${attr.id}"
    data-widget="colorSwatch"
    data-widget-event-change="changeAttribute"
    data-id="attr-${attr.id}"
    data-skip-validation="true"
>
    <div class="b-variations_item-label">
        <div id="variation-label-${attr.id}">
            ${attr.displayName}:
        </div>
    </div>

    <div class="b-variations_item-empty_value">
        ${Resource.msg('form.resource.emptyoption', 'forms', null)}
    </div>
    <div
        role="listbox"
        aria-labelledby="variation-label-${attr.id}"
        aria-describedby="variation-error-${attr.id}"
        class="b-variations_item-content m-list"
    >
        <isloop items="${attr.values}" var="attrValue">
            <isif condition="${attr.swatchable}">
                <isif condition="${!(isBundle && product.productType === 'variant' && !attrValue.selected) }">
                    <button
                        id="variation-swatch-button-${attrValue.id}"
                        role="option"
                        class="b-variation_swatch m-swatch ${ !attrValue.selectable ? 'm-disabled' : '' }"
                        aria-label="${attr.displayName} ${attrValue.displayValue}"
                        data-event-click.prevent="selectColor"
                        data-attr-url="${attrValue.url}"
                        data-attr-is-selected="${attrValue.selected}"
                        title="${attrValue.id} ${ !attrValue.selectable ? '(not available)' : '' }"
                        aria-selected="${attrValue.selected}"
                        aria-disabled="${!attrValue.selectable}"
                    >
                        <span
                            data-attr-value="${attrValue.value}"
                            class="b-variation_swatch-value"
                        >
                            <span
                                class="b-variation_swatch-color_value"
                                <isif condition="${attrValue.images && attrValue.images.swatch[0]}">
                                    style="background-image: url('${attrValue.images.swatch[0].url}')"
                                <iselse/>
                                    style="background-color: ${attrValue.code}"
                                </isif>
                            ></span>
                        </span>
                        <span class="b-variation_swatch-name">${attrValue.displayValue}</span>
                    </button>
                </isif>
            <iselse/>
                <button
                    id="variation-swatch-button-${attrValue.id}"
                    role="option"
                    class="b-variation_swatch ${ !attrValue.selectable ? 'm-disabled' : '' }"
                    aria-label="${attr.displayName} ${attrValue.displayValue}"
                    data-event-click.prevent="selectColor"
                    data-attr-url="${attrValue.url}"
                    data-attr-is-selected="${attrValue.selected}"
                    title="${attrValue.id} ${ !attrValue.selectable ? '(not available)' : '' }"
                    aria-selected="${attrValue.selected}"
                    aria-disabled="${!attrValue.selectable}"
                >
                    <span class="b-variation_swatch-value">${attrValue.displayValue}</span>
                    <span class="b-variation_swatch-name">${attrValue.id}</span>
                </button>
            </isif>
        </isloop>
    </div>
    <div
        role="alert"
        id="variation-error-${attr.id}"
        class="b-variations_item-error"
        data-ref="errorFeedback"
        hidden="hidden"
    ></div>
    <script type="template/mustashe" data-ref="template">
        <div
            class="b-variations_item m-swatch"
            data-widget="colorSwatch"
            data-widget-event-change="changeAttribute"
            data-id="attr-${attr.id}"
            data-skip-validation="true"
        >
            <div class="b-variations_item-label">
                <div id="variation-label-${attr.id}">
                    ${attr.displayName}:
                </div>
            </div>
            <div class="b-variations_item-empty_value">
                ${Resource.msg('form.resource.emptyoption', 'forms', null)}
            </div>
            <div
                role="listbox"
                aria-labelledby="variation-label-${attr.id} variation-error-${attr.id}"
                class="b-variations_item-content m-list"
            >
            {{${'#'}attr.values}}
                <isif condition="${attr.swatchable}">
                    <isif condition="${!(isBundle && product.productType === 'variant' && !attrValue.selected) }">
                        <button
                            id="variation-swatch-button-{{id}}"
                            role="option"
                            class="b-variation_swatch m-swatch {{${'#'}disabled}}m-disabled{{/disabled}}"
                            aria-label="{{attr.displayName}} {{displayValue}}"
                            aria-selected="{{selected}}"
                            title="{{displayValue}} {{${'#'}disabled}}(not available){{/disabled}}"
                            data-event-click.prevent="selectColor"
                            data-attr-url="{{url}}"
                            data-attr-is-selected="{{selected}}"
                            aria-disabled="{{disabled}}"
                        >
                            <span
                                data-attr-value="{{value}}"
                                class="b-variation_swatch-value"
                            >
                                <span
                                    class="b-variation_swatch-color_value"
                                    {{${'#'}images.swatch.0}}
                                        style="background-image: url('{{images.swatch.0.url}}')"
                                    {{/images.swatch.0}}
                                    {{^images.swatch.0}}
                                        style="background-color: {{code}}"
                                    {{/images.swatch.0}}
                                ></span>
                            </span>
                            <span class="b-variation_swatch-name">{{displayValue}}</span>
                        </button>
                    </isif>
                <iselse/>
                    <button
                        id="variation-swatch-button-{{id}}"
                        role="option"
                        class="b-variation_swatch {{${'#'}disabled}}m-disabled{{/disabled}}"
                        aria-label="{{attr.displayName}} {{displayValue}}"
                        data-event-click.prevent="selectColor"
                        data-attr-url="{{url}}"
                        data-attr-is-selected="{{selected}}"
                        title="{{displayValue}} {{${'#'}disabled}}(not available){{/disabled}}"
                        aria-selected="{{selected}}"
                        aria-disabled="{{disabled}}"
                    >
                        <span class="b-variation_swatch-name">{{value}}</span>
                        <span class="b-variation_swatch-value">{{displayValue}}</span>
                    </button>
                </isif>
            {{/attr.values}}
            </div>
            <div
                role="alert"
                id="variation-error-${attr.id}"
                class="b-variations_item-error"
                hidden="hidden"
                data-ref="errorFeedback"
            ></div>
        </div>
    </script>
</div>

<isif condition="${attr.id === 'size' && product.sizeChartId && !isQuickView}">
    <div class="b-variations_item-label_link">
        <isinclude template="product/components/sizeChart" />
    </div>
</isif>
