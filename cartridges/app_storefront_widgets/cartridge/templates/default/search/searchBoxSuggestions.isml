<iscontent charset="UTF-8" compact="true" type="text/html"/>

<script data-ref="template" type="template/mustache">
    <div
        role="none"
        class="b-suggestions-inner"
        data-ref="listboxInner"
    >
        {{${'#'}suggestions.product.phrases}}
            {{^exactMatch}}
            <div role="none" class="b-suggestions-section m-guess">
                <a
                    role="option"
                    aria-label="${Resource.msg('suggestions.selected.wai', 'search', null)} ${Resource.msg('suggestions.title.correction', 'search', null)} {{value}}"
                    id="result-item-{{posinset}}"
                    class="b-suggestions-option b-suggestions_guess"
                    href="${URLUtils.url('Search-Show')}?q={{value}}"
                    data-ref="result-item-{{posinset}}"
                    data-event-keydown="selectItem"
                    data-suggestion-value="{{value}}"
                >
                    ${Resource.msg('suggestions.title.correction', 'search', null)}
                    <span class="b-suggestions_guess-correction_wrap">-&nbsp;<q class="b-suggestions_guess-correction">{{value}}</q>?</span>
                </a>
            </div>
            {{/exactMatch}}
        {{/suggestions.product.phrases}}

        {{${'#'}suggestions.category.available}}
            <div role="none" class="b-suggestions-section m-categories">
                <div role="none" class="b-suggestions-title">
                    ${Resource.msg('suggestions.title.categories', 'search', null)}
                </div>
                {{${'#'}suggestions.category.categories}}
                    <a
                        role="option"
                        aria-label="${Resource.msg('suggestions.selected.wai', 'search', null)} ${Resource.msg('suggestions.title.categories', 'search', null)} {{name}}"
                        id="result-item-{{posinset}}"
                        class="b-suggestions-option b-suggestions_category"
                        href="{{url}}"
                        data-ref="result-item-{{posinset}}"
                        data-event-keydown="selectItem"
                        data-suggestion-value="{{name}}"
                    >
                        <span class="b-suggestions_category-title">{{name}}</span>
                        {{${'#'}parentID}}
                            <span class="b-suggestions_category-parent_title">
                                ${Resource.msg('suggestions.in', 'search', null)} {{parentName}}
                            </span>
                        {{/parentID}}
                    </a>
                {{/suggestions.category.categories}}
            </div>
        {{/suggestions.category.available}}

        {{${'#'}suggestions.product.available}}
            <div role="none" class="b-suggestions-section m-products">
                <div role="none" class="b-suggestions-title">
                    ${Resource.msg('suggestions.title.products', 'search', null)}
                </div>
                {{${'#'}suggestions.product.products}}
                    <a
                        role="option"
                        aria-label="${Resource.msg('suggestions.selected.wai', 'search', null)} ${Resource.msg('suggestions.title.products', 'search', null)} {{name}}"
                        id="result-item-{{posinset}}"
                        class="b-suggestions-option b-suggestions_product"
                        href="{{url}}"
                        data-ref="result-item-{{posinset}}"
                        data-event-keydown="selectItem"
                        data-suggestion-value="{{name}}"
                    >
                        {{${'#'}images.searchsuggestion}}
                            <div class="b-suggestions_product-picture_wrap">
                                <picture class="b-suggestions_product-picture">
                                    <img
                                        src="{{images.searchsuggestion.0.url}}"
                                        srcset="{{images.searchsuggestion.0.sources.0.srcset}}"
                                        alt="{{images.searchsuggestion.0.title}}"
                                    />
                                </picture>
                            </div>
                        {{/images.searchsuggestion}}

                        <div class="b-suggestions_product-info">
                            <h3 class="b-suggestions_product-title">{{productName}}</h3>

                            <div class="b-price b-suggestions_product-price">
                                {{${'#'}priceRange}}
                                    {{${'#'}minPrice}}
                                        {{${'#'}listPrice}}
                                            <span class="b-price-item m-old">{{{listPrice}}}</span>
                                        {{/listPrice}}
                                        <span class="b-price-item b-suggestions_product-price_item">{{{salesPrice}}}</span>
                                    {{/minPrice}}              
                                    <span class="b-price-divider">-</span>
                                    {{${'#'}maxPrice}}
                                        {{${'#'}listPrice}}
                                            <span class="b-price-item m-old">{{{listPrice}}}</span>
                                        {{/listPrice}}
                                        <span class="b-price-item b-suggestions_product-price_item">{{{salesPrice}}}</span>
                                    {{/maxPrice}}
                                {{/priceRange}}
                                {{^priceRange}}
                                    {{${'#'}listPrice}}
                                        <span class="b-price-item m-old">{{{listPrice}}}</span>
                                    {{/listPrice}}
                                    <span class="b-price-item b-suggestions_product-price_item">{{{salesPrice}}}</span>
                                {{/priceRange}}
                            </div>
                        </div>
                    </a>
                {{/suggestions.product.products}}
            </div>
        {{/suggestions.product.available}}

        <isif condition="${require('util/pref').is('search.content.enabled')}">
        {{${'#'}suggestions.content.available}}
            <div role="none" class="b-suggestions-section m-content">
                <div role="none" class="b-suggestions-title">
                    ${Resource.msg('suggestions.title.content', 'search', null)}
                </div>
                {{${'#'}suggestions.content.contents}}
                <a
                    role="option"
                    aria-label="${Resource.msg('suggestions.selected.wai', 'search', null)} ${Resource.msg('suggestions.title.content', 'search', null)} {{name}}"
                    id="result-item-{{posinset}}"
                    class="b-suggestions-option b-suggestions-link"
                    href="{{url}}"
                    data-ref="result-item-{{posinset}}"
                    data-event-keydown="selectItem"
                    data-suggestion-value="{{name}}"
                >
                    {{name}}
                </a>
                {{/suggestions.content.contents}}
            </div>
        {{/suggestions.content.available}}
        </isif>

        <div role="none" class="b-suggestions-section">
            <a
                class="b-button m-outline b-suggestions-view_results"
                role="option"
                id="result-item-{{suggestions.viewAllResultsPosinset}}"
                data-ref="result-item-{{suggestions.viewAllResultsPosinset}}"
                href="${URLUtils.url('Search-Show')}?q={{suggestions.searchPhrase}}"
                data-event-keydown="selectItem"
                data-suggestion-value="${Resource.msg('suggestions.viewallresults', 'search', null)}"
            >
                <isprint value="${Resource.msg('suggestions.viewallresults', 'search', null)}" />
            </a>
        </div>

        <p role="alert" class="b-suggestions-message" id="search-result-count">
            {{suggestions.total}} ${Resource.msg('suggestions.resultscount.wai', 'search', null)}
        </p>
    </div>
</script>
<script data-ref="templateError" type="template/mustache">
    <div
        role="alert"
        class="b-suggestions-inner m-empty"
        data-ref="listboxInner"
    >
        ${Resource.msg('suggestions.unavailable', 'search', null)}
    </div>
</script>
