<iscontent charset="UTF-8" compact="true" type="text/html"/>

<div
    data-ref="container"
    class="b-dialog m-search_suggestions"
    data-event-click.self="closeModal"
    hidden
>
    <div class="b-search_dialog" data-ref="dialog">
        <div
            class="b-search_dialog-inner"
            data-widget="searchBox"
            data-ref-first-focus-element="submit"
            data-ref-last-focus-element="closeSearch"
            data-focus-loop="true"
            data-event-keydown="handleKeydown"
            data-url="${URLUtils.url('SearchServices-GetSuggestions')}"
            data-widget-event-closesearch="closeSearch"
            data-close-from-outside="false"
            data-close-on-tab="false"
            role="dialog"
            aria-modal="true"
            aria-label="${Resource.msg('header.search.label', 'search', null)}"
        >
            <div class="b-search_dialog-inner_top">
                <div class="b-search_dialog-inner_top_content">
                    <div class="b-search_dialog-form_wrap">
                        <form
                            role="search"
                            method="get"
                            action="${URLUtils.url('Search-Show')}"
                            name="simpleSearch"
                            class="b-search_input"
                            data-event-submit="handleSubmit"
                            itemprop="potentialAction"
                            itemscope
                            itemtype="https://schema.org/SearchAction"
                        >
                            <meta content="${URLUtils.url('Search-Show')}?q={q}" itemprop="target"/>

                            <isinclude template="components/modal/focusTrapFirst">
                                <button
                                    class="b-search_input-submit"
                                    data-ref="submit"
                                    type="submit"
                                    enterkeyhint="search"
                                    title="${Resource.msg('header.search.submit', 'search', null)}"
                                    aria-label="${Resource.msg('header.search.submit', 'search', null)}"
                                    tabindex="0"
                                >
                                    <isinclude template="/common/icons/standalone/search" />
                                </button>

                                <iscomment>
                                    by ARIA specs wrapper of input should be combobox,
                                    but most screen readers not announce it, so we move combobox to input
                                </iscomment>
                                <input
                                    role="combobox"
                                    aria-activedescendant
                                    aria-autocomplete="list"
                                    aria-controls="search-suggestions-list"
                                    aria-expanded="false"
                                    aria-haspopup="listbox"
                                    aria-label="${Resource.msg('header.search.label.wai', 'search', null)}"
                                    aria-owns="search-suggestions-list"
                                    data-ref="input"
                                    data-event-blur="handleBlur"
                                    data-event-focus="handleFocus"
                                    data-event-input="handleInput"
                                    data-event-keydown="handleKeydown"
                                    id="header-search-input"
                                    class="b-search_input-input"
                                    type="search"
                                    name="q"
                                    value=""
                                    placeholder="${Resource.msg('header.search.placeholder', 'search', null)}"
                                    autocapitalize="off"
                                    autocomplete="off"
                                    autocorrect="false"
                                    spellcheck="false"
                                    itemprop="query-input"
                                    maxlength="50"
                                />
                                <button
                                    aria-hidden="true"
                                    class="b-search_input-clear"
                                    data-ref="clearButton"
                                    data-event-click="clearInput"
                                    type="reset"
                                    title="${Resource.msg('header.search.clear', 'search', null)}"
                                    tabindex="-1"
                                >
                                    <isinclude template="/common/icons/standalone/searchClear" />
                                </button>
                                <input type="hidden" name="lang" value="${pdict.locale}"/>
                        </form>
                        <button
                            type="button"
                            class="b-search_dialog-cancel"
                            data-event-click="closeSearch"
                            data-last-focused
                            data-ref="closeSearch"
                            tabindex="0"
                        >
                            <isprint value="${Resource.msg('common.cancel', 'common', null)}" />
                        </button>
                        <isinclude template="components/modal/focusTrapLast">
                    </div>
                </div>
            </div>

            <div
                role="listbox"
                aria-labelledby="header-search-input"
                id="search-suggestions-list"
                class="b-suggestions"
                data-ref="listbox"
                data-event-mouseenter="markHover"
                data-event-mouseleave="unMarkHover"
            >
                <div role="none" class="b-suggestions-inner" data-ref="listboxInner"></div>
            </div>
            <isinclude template="search/searchBoxInitialSuggestions"/>
            <isinclude template="search/searchBoxSuggestions"/>
        </div>
    </div>
</div>
