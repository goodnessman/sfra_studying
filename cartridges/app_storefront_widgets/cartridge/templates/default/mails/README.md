# Mails

## Check and preview mails

Check your mails at: `/on/demandware.store/Sites-Boilerplate-NA-Site/en_US/MailStub-Start`

#### Two preview modes:
1. Develop
    * Resulting HTML contains usual CSS classes with styles from `mails.scss` file.
    * Livereload will refresh your preview page if it detects any changes

2. Preview
    * Resulting HTML contains compiled CSS to properties from `mails.scss` in `style="..."` attributes
    * Livereload is disabled
    * Class error notifications are enabled. You will see a *dashed red outline* over an element with the *invalid class*. **Invalid class** is a class that does not exist in `mails.scss` file.

## Develop mails

run `npm run dev:mails:watch` to start dev server and compile css to properties

## How to add a class to HTML

#### 1. Adding classes in ISML file by using `<isstyle/>` tag.
Needed classes should be added in `p_bind` attribute with whitespaces as a divider.

```html
Example:

<div <isstyle p_bind="text text_24 text_black text_bold"/>>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
    Tempor incididunt ut labore et dolore magna aliqua.
</div>
```

#### 2. Adding classes in HTML file of asset by using `$tools.bindStyles` attribute.
`$tools.bindStyles` should be added as an attribute to HTML tag and classes should be added as paramaters `$tools.bindStyles(...params...)` with whitespaces as a divider.

```html
Example:

<div $tools.bindStyles('text text_16 text_black')>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.
    Tempor incididunt ut labore et dolore magna aliqua.
</div>
```

## CSS development

1. In `mails.scss` file, we can use **only classes**. We cannot use id selectors, attribute selectors, etc. Added to the HTML classes will be compiled into properties of style="..." attribute.

2. Any changes should be done via adding a new class with properties instead of manual adding `style="..."` attribute in HTML.

3. We can add as much as you like classes into `mails.scss`. Since only classes that are attached to HTML will be compiled.
