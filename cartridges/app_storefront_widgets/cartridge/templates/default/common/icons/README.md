# Icons

This is folder to store SVG icons resources. This icons used as includes across the site.

## Preview used icons

You could preview all icons by this URL `/on/demandware.store/Sites-Boilerplate-NA-Site/en_US/Icons-Show`

To rebuild preview template run `npm run build:icons:preview`. It update `common/icons/priveiw.isml` with all actual icons.

## Create individual icon

1. Just place SVG into isml template in `icons/standalone`

```xml
<svg width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
    <path fill="currentColor" d="M1 9h14M9 2l7 7-7 7"/>
</svg>
```

2. use it in template

```html
<isinclude template="common/icons/standalone/arrow"/>
```

3. Run `npm run build:icons:preview` to rebuild preview template

After that you could preview it with `/on/demandware.store/Sites-Boilerplate-NA-Site/en_US/Icons-Show`

## Reuse icons in several places

To reuse icon you need to place it into sprite, to do so just add icon to appropriate sprite file ex. global, search etc.

Imagine that you have arrow icon

```xml
<svg width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
    <path fill="currentColor" d="M1 9h14M9 2l7 7-7 7"/>
</svg>
```

and you need to use it on each PLP tile.

1. Pleace it inside `common/icons/spritePLP.isml`

```xml
<svg
    width="100"
    height="200"
    style="position:absolute;width:1px;height:1px;"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
>
    <symbol id="arrow">
        <path fill="currentColor" d="M1 9h14M9 2l7 7-7 7"/>
    </symbol>
</svg>
```

2. Use it in template

```xml
<svg class="b-some-component-icon" width="14" height="14" role="presentation" focusable="false">
    <use href="${'#'}arrow"></use>
</svg>
```

Please note: do not use `xlink:href` namespace on use tag since diffDOM could not handle attribute namespaces.

3. Run `npm run build:icons:preview` to rebuild preview template

After that you could preview it with `/on/demandware.store/Sites-Boilerplate-NA-Site/en_US/Icons-Show`
