<section
    data-ref="billingStep"
    class="b-checkout_step"
>
    <header
        class="b-checkout_step-header"
        data-widget="checkoutStepHeader"
        data-widget-event-editstep="handleEditStep"
        data-listen-events="order.updated"
    >
        <script data-ref="template" type="template/mustache">
            <header
                class="b-checkout_step-header {{${'#'}isBilling}}m-active{{/isBilling}} {{${'#'}isSummary}}m-applied{{/isSummary}}"
                data-widget-event-editstep="handleEditStep"
                data-widget="checkoutStepHeader"
                data-listen-events="order.updated"
            >

                <h2 class="b-checkout_step-title">
                    {{^isSummary}}
                        <span class="b-checkout_step-counter">
                            2
                        </span>
                    {{/isSummary}}

                    {{${'#'}isSummary}}
                        <span class="b-checkout_step-apply_icon">
                            <isinclude template="/common/icons/standalone/applyStep" />
                        </span>
                    {{/isSummary}}

                    ${Resource.msg('checkout.payment.title', 'checkout', null)}
                </h2>

                {{${'#'}isSummary}}
                    <button
                        class="b-button m-outline m-medium"
                        data-event-click="editStep"
                        data-ref="editStepBtn"
                        data-step="billing"
                        type="button"
                    >
                        ${Resource.msg('common.edit', 'common', null)}
                    </button>
                {{/isSummary}}
            </header>
        </script>
    </header>

    <section
        class="b-checkout_step-item"
        data-ref="billingSection"
    >
        <isset name="billingFields" value="${pdict.forms.billingForm.addressFields}" scope="page"/>
        <isset name="contactInfo" value="${pdict.forms.billingForm.contactInfoFields}" scope="page"/>

        <div class="b-checkout_step-content">
            <form
                class="b-form"
                autocomplete="on"
                novalidate
                method="POST"
                action="${URLUtils.url('CheckoutServices-SubmitPayment')}"
                data-widget="billingForm"
                data-address-selector="billingAddressSelector"
                data-widget-event-submit="billingSubmitted"
                data-address-fields="${[
                    billingFields.firstName.htmlName,
                    billingFields.lastName.htmlName,
                    billingFields.address1.htmlName,
                    billingFields.address2.htmlName,
                    billingFields.country.htmlName,
                    billingFields.states.stateCode.htmlName,
                    billingFields.city.htmlName,
                    billingFields.postalCode.htmlName,
                    contactInfo.phone.htmlName
                ].join(',')}"
                data-form-definition-url="${URLUtils.url('Forms-FormDefinition', 'form', 'billing')}"
                <isprint value=${pdict.forms.billingForm.attributes} encoding="off" />
            >
                <div
                    role="alert"
                    class="b-form-message"
                    hidden="hidden"
                    data-ref="errorMessageLabel"
                ></div>

                <div class="b-billing_address">
                    <h2 class="b-billing_address-title">
                        ${Resource.msg('billing.fieldset.address', 'checkout', null)}
                    </h2>
                    <p class="b-billing_address-description">
                        ${Resource.msg('billing.fieldset.address.caption', 'checkout', null)}
                    </p>

                    <isprint value="${
                        formElement(contactInfo.email).render()
                    }" encoding="off"/>

                    <!-- check box to show\hide address selector + billing address with printed shipping address -->
                    <isprint value="${
                        formElement(billingFields.useShipping).render()
                    }" encoding="off"/>

                    <div
                        data-widget="label"
                        data-id="useShippingDescription"
                    >
                        <script data-ref="template" type="template/mustache">
                            <div class="b-summary_address" data-widget="label" data-id="useShippingDescription">
                                {{${'#'}order.shipping.0.shippingAddress}}
                                    <isinclude template="m/checkout/addressSummary" />
                                {{/order.shipping.0.shippingAddress}}
                            </div>
                        </script>
                    </div>
                </div>

                <!-------------------------------------------------------------------------->
                <!-- Billing Address Selector                                             -->
                <!-------------------------------------------------------------------------->
                <isinclude template="m/checkout/billing/addressSelector" />

                <!-------------------------------------------------------------------------->
                <!-- Billing Address                                                      -->
                <!-------------------------------------------------------------------------->
                <fieldset class="b-address_form b-form-set m-hide" data-ref="addressForm" hidden>
                    <legend class="b-form-set_label m-wai">
                        ${Resource.msg('billing.fieldset.address', 'checkout', null)}
                    </legend>
                    <isif condition="${!empty(pdict.customer.addresses) || (!empty(pdict.order.shipping) && pdict.order.shipping[0].shippingAddress)}">
                        <button
                            type="button"
                            class="b-button m-link m-small b-address_form-back"
                            data-event-click.prevent="backToAddresses"
                        >
                            <span class="b-address_form-icon">
                                <isinclude template="/common/icons/standalone/arrowBack" />
                            </span>
                            ${Resource.msg('form.shipment.backsavedaddress', 'forms', null)}
                        </button>
                    </isif>
                    <isinclude template="checkout/billing/billingAddressInner" />
                </fieldset>

                <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}" />
                <input type="hidden" name="localizedNewAddressTitle" value="${Resource.msg('msg.new.address', 'checkout', null)}"/>

                <!-------------------------------------------------------------------------->
                <!-- Payment Options                                                      -->
                <!-------------------------------------------------------------------------->
                <fieldset
                    class="b-form-set"
                >
                    <legend class="b-form-set_label" ${pdict.order.totals.isZeroTotal ? 'hidden' : ''}>
                        ${Resource.msg('billing.fieldset.paymentchoose', 'checkout', null)}
                    </legend>
                    <isinclude template="checkout/billing/paymentOptions" />
                </fieldset>
            </form>
        </div>

        <div
            class="b-checkout_step-controls"
            data-widget="label"
            data-listen-events="checkout.step.updated"
            data-widget-event-click="submitCurrentStep"
        >
            <script data-ref="template" type="template/mustache">
                <div
                    class="b-checkout_step-controls"
                    data-widget="label"
                    data-listen-events="checkout.step.updated"
                    data-widget-event-click="submitCurrentStep"
                >

                    {{${'#'}billing}}
                        <button
                            class="b-button m-width_full"
                            type="submit"
                            name="submit"
                            value="submit-payment"
                            data-ref="submitButton"
                            data-event-click="handleClick"
                        >
                            ${Resource.msg('checkout.paymentNext', 'checkout', null)}
                        </button>
                    {{/billing}}
                </div>
            </script>
        </div>
    </section>

    <section
        class="b-checkout_step-item m-summary"
        data-ref="billingSummary"
    >
        <div
            class="b-checkout_step-content"
            data-widget="label"
            data-listen-events="order.updated"
        >
            <script data-ref="template" type="template/mustache">
                <div
                    class="b-checkout_step-content"
                    data-widget="label"
                >
                    <isinclude template="m/checkout/billing/billingSummary" />
                </div>
            </script>
        </div>
    </section>
</section>
