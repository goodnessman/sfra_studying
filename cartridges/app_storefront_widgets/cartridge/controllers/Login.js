var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

var server = require('server');

server.extend(module.superModule);

server.append('Show', pageMetaData.computedPageMetaData, function (req, res, next) {
    var viewData = res.getViewData();
    var loginForm = server.forms.getForm('login');
    if (viewData.rememberMe && !empty(viewData.userName)) {
        loginForm.rememberMe.checked = viewData.rememberMe;
        loginForm.email.value = viewData.userName;
    }
    res.setViewData({
        loginForm: loginForm
    });

    next();
});

/**
 * Action was fully replaced cause in base controller this action pass hardcoded rememberMe value(false in all case) to logoutCustomer method.
 * That's why remember me functionality doesn't work. We need to pass correct value corresponded to value that customers selected in loginForm.
 * For more information look in base Login-Logout
 */
server.replace('Logout', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var loginForm = server.forms.getForm('login');
    var rememberMe = Boolean(loginForm.rememberMe.checked);

    CustomerMgr.logoutCustomer(rememberMe);
    res.redirect(URLUtils.url('Login-Show'));
    next();
});

module.exports = server.exports();
