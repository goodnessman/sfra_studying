var server = require('server');

server.extend(module.superModule);

server.prepend('Check', function (req, res, next) {
    // eslint-disable-next-line
    var cookieAccepted = request.getHttpCookies()['dw_consent_cookie'];

    if (cookieAccepted) {
        req.session.privacyCache.set('consent', cookieAccepted.getValue() === '1');
    }

    next();
});

module.exports = server.exports();
