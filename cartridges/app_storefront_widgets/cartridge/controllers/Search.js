var server = require('server');

var URLUtils = require('dw/web/URLUtils');

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var breadcrumbs = require('*/cartridge/scripts/middleware/breadcrumbs');
var search = require('*/cartridge/scripts/middleware/search');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

server.extend(module.superModule);

/**
 * @description Added AJAX repsonse
 */
server.replace('Refinebar', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');

    var ProductSearch = require('*/cartridge/models/search/productSearch');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');

    var apiProductSearch = new ProductSearchModel();

    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();

    var productSearch = new ProductSearch(
        apiProductSearch,
        req.querystring,
        req.querystring.srule,
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );

    if (req.querystring.ajax && req.httpHeaders.get('x-requested-with') === 'XMLHttpRequest') {
        res.json(productSearch);
    } else {
        res.render('/search/searchRefine', {
            productSearch: productSearch,
            querystring: req.querystring
        });
    }

    next();
});

/**
 * @description Middlware appended to Search-Show controller to generate breadcrumbs model for rendering it on Search and PLP page
 * <br>Middleware `search.contentSearch` was added in order to execute content search due to storefront logic (quantity of founded content)
 * <br>Middleware `search.searchCount` - injects data into action, which reflects if product/content search exists and total items found counter
 * <br>Middleware `search.redirect` - checks, if only one entity found and perfroms redirect to related entity page
 * <br>Middleware `breadcrumbs.generatePLPBreadcrumbs` - generates breadcrumbs model and adds it to the view
 * <br>Page Metadata amendments - include absolute canonical URL depending on search, adding pagination metadata if needed
 */
server.append('Show', function (req, res, next) {
    res.setViewData({
        querystring: req.querystring
    });

    next();
}, breadcrumbs.generateListSchema, search.contentSearch, search.searchCount, search.redirect,
pageMetaData.paginationMetadata, pageMetaData.canonicalUrl, pageMetaData.hrefLang, breadcrumbs.generatePLPBreadcrumbs);

/**
 * @description Added content search for action `ShowAjax` in middleware `search.contentSearch`
 * <br>Storefront logic (quantity of founded content) requires content to be rendered as well
 * <br>Middleware `search.searchCount` - injects data into action, which reflects if product/content search exists and total items found counter
 */
server.append('ShowAjax', search.contentSearch, function (req, res, next) {
    var viewData = res.getViewData();

    if (!viewData.productSearch.count) {
        res.setStatusCode(204);
    }

    next();
}, search.searchCount, breadcrumbs.generatePLPBreadcrumbs);

/**
 * @description Added:
 * <br>Middleware `search.searchCount` - injects data into action, which reflects if product/content search exists and total items found counter
 */
server.append('UpdateGrid', search.searchCount);
server.append('Refinebar', search.searchCount);

/**
 * @description Handles folder search, dynamically renders the content asset page for the folder searched
 */
server.get('ShowContent',
    cache.applyDefaultCache,
    consentTracking.consent,
    /* eslint-disable consistent-return */
    function (req, res, next) {
        var ContentMgr = require('dw/content/ContentMgr');
        var ContentSearchModel = require('dw/content/ContentSearchModel');
        var Logger = require('dw/system/Logger');
        var ContentModel = require('*/cartridge/models/content');
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

        var folderId = req.querystring[ContentSearchModel.FOLDERID_PARAMETER];
        var apiFolder = ContentMgr.getFolder(folderId);

        if (apiFolder) {
            if (empty(apiFolder.onlineSubFolders) && apiFolder.onlineContent.length === 1) {
                res.redirect(URLUtils.url('Page-Show', 'cid', apiFolder.onlineContent[0].ID));
                return next();
            }
            var content = new ContentModel(apiFolder, 'rendering/content/simple');

            pageMetaHelper.setPageMetaData(req.pageMetaData, content);
            pageMetaHelper.setPageMetaTags(req.pageMetaData, content);

            if (content.template) {
                var context = {
                    content: content
                };

                if (content.templateHasNav()) {
                    context.navStructure = content.getNavStructure();
                }
                res.render(content.template, context);
            } else {
                Logger.warn('Content folder with ID {0} is offline', folderId);
                res.redirect(URLUtils.url('Home-ErrorNotFound'));
            }
        } else {
            Logger.warn('Content folder with ID {0} was included but not found', folderId);
        }
        next();
    },
    pageMetaData.computedPageMetaData,
    breadcrumbs.generateContentBreadcrumbs,
    pageMetaData.hrefLang);

module.exports = server.exports();
