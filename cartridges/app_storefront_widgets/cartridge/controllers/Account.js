var server = require('server');

var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var accountNavigation = require('*/cartridge/scripts/middleware/accountNavigation');
var breadcrumbs = require('*/cartridge/scripts/middleware/breadcrumbs');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.extend(module.superModule);
/**
 *  That action was fully replaced because we use Form Framework and it doesn't support dynamic input name in the base controller.
 *  As we use Form Framework we add input name dynamically , but base action  use name from ISML (f.e. loginEmail, loginPassword, loginRememberMe) , so have no
 *  possibility to handle it. (See base loginForm.isml and Account.js)
 */

server.replace(
    'Login',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    /* eslint-disable consistent-return */
    function (req, res, next) {
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Resource = require('dw/web/Resource');
        var Site = require('dw/system/Site');
        var Transaction = require('dw/system/Transaction');

        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
        var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        var loginForm = server.forms.getForm('login');
        var formErrors = require('*/cartridge/scripts/formErrors');

        if (loginForm.valid) {
            var email = loginForm.email.htmlValue;
            var password = loginForm.password.htmlValue;
            var rememberMe = loginForm.rememberMe.checked
                ? (!!loginForm.rememberMe.checked)
                : false;

            var customerLoginResult = Transaction.wrap(function () {
                var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

                if (authenticateCustomerResult.status !== 'AUTH_OK') {
                    var errorCodes = {
                        ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
                        ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
                        ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
                        ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
                        ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
                        ERROR_UNKNOWN: 'error.message.error.unknown',
                        default: 'error.message.login.form'
                    };

                    var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
                    var errorMessage = Resource.msg(errorMessageKey, 'login', null);

                    return {
                        error: true,
                        errorMessage: errorMessage,
                        status: authenticateCustomerResult.status,
                        authenticatedCustomer: null
                    };
                }

                return {
                    error: false,
                    errorMessage: null,
                    status: authenticateCustomerResult.status,
                    authenticatedCustomer: CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe)
                };
            });

            if (customerLoginResult.error) {
                if (customerLoginResult.status === 'ERROR_CUSTOMER_LOCKED') {
                    var context = {
                        customer: CustomerMgr.getCustomerByLogin(email) || null
                    };

                    var emailObj = {
                        to: email,
                        subject: Resource.msg('subject.account.locked.email', 'login', null),
                        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                        type: emailHelpers.emailTypes.accountLocked
                    };

                    hooksHelper('app.customer.email',
                        'sendEmail', [emailObj, 'account/accountLockedEmail', context], function () {});
                }

                res.json({
                    error: [customerLoginResult.errorMessage || Resource.msg('error.message.login.form', 'login', null)]
                });

                return next();
            }

            if (customerLoginResult.authenticatedCustomer) {
                res.setViewData({ authenticatedCustomer: customerLoginResult.authenticatedCustomer });
                res.json({
                    success: true,
                    redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, false)
                });

                req.session.privacyCache.set('args', null);
            } else {
                res.json({ error: [Resource.msg('error.message.login.form', 'login', null)] });
            }

            return next();
        } else {
            res.json({
                success: false,
                fields: formErrors.getFormErrors(loginForm)
            });
        }
    }
);

/**
 *  That action was fully replaced because we use Form Framework and it doesn't support dynamic input name in the base controller.
 *  As we use Form Framework we add input name dynamically , but base action  use name from ISML (f.e. passwordResetForm) , so have no
 *  possibility to handle it. (See base passwordResetForm.isml and Account.js)
 */

server.replace('PasswordResetDialogForm', server.middleware.https, function (req, res, next) {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');
    var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
    var validator = require('*/cartridge/scripts/forms/validator');
    var resetPasswordForm = server.forms.getForm('profile').resetPassword;
    var emailInputName = resetPasswordForm.email.htmlName;
    var formErrors = require('*/cartridge/scripts/formErrors');

    if (resetPasswordForm.valid) {
        var email = resetPasswordForm.email.htmlValue;
        var errorMsg = Resource.msg('error.message.passwordreset', 'login', null);
        var isValid;
        var resettingCustomer;
        var mobile = req.querystring.mobile;
        var receivedMsgHeading = Resource.msg('passrordReset.confirmationSentTitle', 'login', null);
        var receivedMsgBody = Resource.msg('passwordReset.confirmationSentMessage', 'login', null);
        var buttonText = Resource.msg('button.text.loginform', 'login', null);
        var returnUrl = URLUtils.url('Login-Show').toString();
        var response = {
            success: true,
            fields: {}
        };
        if (email) {
            isValid = validator.validateEmail(email);
            if (isValid) {
                resettingCustomer = CustomerMgr.getCustomerByLogin(email);
                if (resettingCustomer) {
                    accountHelpers.sendPasswordResetEmail(email, resettingCustomer);
                }
                res.json({
                    success: true,
                    receivedMsgHeading: receivedMsgHeading,
                    receivedMsgBody: receivedMsgBody,
                    buttonText: buttonText,
                    mobile: mobile,
                    returnUrl: returnUrl
                });
            } else {
                response.success = false;
                response.fields[emailInputName] = errorMsg;
                res.json(response);
            }
        } else {
            errorMsg = Resource.msg('error.message.required', 'login', null);
            response.success = false;
            response.fields[emailInputName] = errorMsg;
            res.json(response);
        }
        return next();
    } else {
        res.json({
            success: false,
            fields: formErrors.getFormErrors(resetPasswordForm)
        });
    }
});

/**
 *  Append to ViewData correct form.
 */

server.append('SetNewPassword', function (req, res, next) {
    var viewData = res.getViewData();
    var newPasswordForm = server.forms.getForm('profile').login.newpasswords;
    if (viewData.passwordForm) {
        res.setViewData({
            newPasswordForm: newPasswordForm
        });
    }
    next();
}, pageMetaData.computedPageMetaData);

/**
 *  Append to ViewData correct form.
 */
server.append('PasswordReset', server.middleware.https, function (req, res, next) {
    var resetPasswordForm = server.forms.getForm('profile').resetPassword;
    res.setViewData({
        resetPasswordForm: resetPasswordForm
    });
    next();
});

/**
 *  That action was fully replaced because we use Form Framework and AjaxForm Widget.
 *  It doesn't support Ajax in the base controller.
 *  Base controller grab incorrect form as well.
 */

server.replace('SaveNewPassword', server.middleware.https, function (req, res, next) {
    var Transaction = require('dw/system/Transaction');
    var Resource = require('dw/web/Resource');
    var formErrors = require('*/cartridge/scripts/formErrors');

    var passwordForm = server.forms.getForm('profile');
    var newPasswordsForm = passwordForm.login.newpasswords;
    var newPassword = passwordForm.login.newpasswords.newpassword;
    var newPasswordConfirm = passwordForm.login.newpasswords.newpasswordconfirm;
    var token = req.querystring.Token;

    if (newPassword.value !== newPasswordConfirm.value) {
        newPasswordsForm.valid = false;
        newPassword.valid = false;
        newPasswordConfirm.valid = false;
        newPasswordConfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
    }

    if (newPasswordsForm.valid) {
        var result = {
            success: true,
            newPassword: newPassword.value,
            newPasswordConfirm: newPasswordConfirm.value,
            token: token,
            newPasswordsForm: newPasswordsForm
        };
        res.json(result);
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var CustomerMgr = require('dw/customer/CustomerMgr');
            var URLUtils = require('dw/web/URLUtils');
            var Site = require('dw/system/Site');
            var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

            var formInfo = res.getViewData();
            var status;
            var resettingCustomer;
            Transaction.wrap(function () {
                resettingCustomer = CustomerMgr.getCustomerByToken(formInfo.token);
                status = resettingCustomer.profile.credentials.setPasswordWithToken(
                    formInfo.token,
                    formInfo.newPassword
                );
            });
            if (status.error) {
                newPassword.valid = false;
                newPasswordConfirm.valid = false;
                newPassword.error = Resource.msg(
                    'error.message.resetpassword.invalidformentry',
                    'forms',
                    null
                );
                res.json({
                    newPassword: newPassword,
                    success: false,
                    fields: formErrors.getFormErrors(newPasswordsForm),
                    token: token
                });
            } else {
                var email = resettingCustomer.profile.email;
                var url = URLUtils.https('Login-Show').toString();
                var objectForEmail = {
                    firstName: resettingCustomer.profile.firstName,
                    lastName: resettingCustomer.profile.lastName,
                    url: url
                };

                var emailObj = {
                    to: email,
                    subject: Resource.msg('subject.profile.resetpassword.email', 'login', null),
                    from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                    type: emailHelpers.emailTypes.passwordReset
                };

                emailHelpers.sendEmail(emailObj, 'account/password/passwordChangedEmail', objectForEmail);
                res.json({
                    success: true,
                    redirectUrl: url
                });
            }
        });
    } else {
        res.json({
            newPasswordsForm: newPasswordsForm,
            success: false,
            fields: formErrors.getFormErrors(newPasswordsForm),
            token: token
        });
    }
    next();
});

/**
 *  That action was fully replaced because we don't use Email and Email Confirm fields on our Profile Details page.
 *   In base conttroller (see Account.js) SaveProfile method use those fields.
 */

server.replace(
    'SaveProfile',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Transaction = require('dw/system/Transaction');
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Resource = require('dw/web/Resource');
        var URLUtils = require('dw/web/URLUtils');
        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

        var formErrors = require('*/cartridge/scripts/formErrors');

        var profileForm = server.forms.getForm('profile');
        var currentCustomer = req.currentCustomer;

        var result = {
            firstName: profileForm.customer.firstname.value,
            lastName: profileForm.customer.lastname.value,
            phone: profileForm.customer.phone.value,
            email: currentCustomer.profile.email,
            password: profileForm.login.currentpassword.value,
            profileForm: profileForm
        };
        if (profileForm.valid && currentCustomer) {
            res.setViewData(result);
            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var formInfo = res.getViewData();
                var customer = CustomerMgr.getCustomerByCustomerNumber(
                    req.currentCustomer.profile.customerNo
                );
                var profile = customer.getProfile();
                var customerLogin;
                var status;

                Transaction.wrap(function () {
                    status = profile.credentials.setPassword(
                        formInfo.password,
                        formInfo.password,
                        true
                    );

                    if (status.error) {
                        formInfo.profileForm.login.currentpassword.valid = false;
                        formInfo.profileForm.login.currentpassword.error = Resource.msg(
                            'error.message.currentpasswordnomatch', 'forms', null
                        );
                    } else {
                        customerLogin = profile.credentials.setLogin(
                            formInfo.email,
                            formInfo.password
                        );
                    }
                });

                delete formInfo.password;

                if (customerLogin) {
                    Transaction.wrap(function () {
                        profile.setFirstName(formInfo.firstName);
                        profile.setLastName(formInfo.lastName);
                        profile.setEmail(formInfo.email);
                        profile.setPhoneHome(formInfo.phone);
                    });

                    // Send account edited email
                    accountHelpers.sendAccountEditedEmail(customer.profile);

                    delete formInfo.profileForm;
                    delete formInfo.email;

                    res.json({
                        success: true,
                        redirectUrl: URLUtils.url('Account-Show').toString()
                    });
                } else {
                    delete formInfo.profileForm;

                    res.json({
                        success: false,
                        fields: formErrors.getFormErrors(profileForm)
                    });
                }
            });
        } else {
            res.json({
                success: false,
                fields: formErrors.getFormErrors(profileForm)
            });
        }
        return next();
    }
);

/**
 * Get form and it to the template
 */
server.append('EditPassword', function (req, res, next) {
    var viewData = res.getViewData();
    var newPasswordForm = server.forms.getForm('changePassword');
    if (viewData.profileForm) {
        res.setViewData({
            newPasswordForm: newPasswordForm
        });
    }
    next();
});

/**
 *  That action was fully replaced because we use separate form for this action.
 *  Base controller use same form in few place.
 */
server.replace(
    'SavePassword',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Transaction = require('dw/system/Transaction');
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Resource = require('dw/web/Resource');
        var URLUtils = require('dw/web/URLUtils');

        var formErrors = require('*/cartridge/scripts/formErrors');

        var newPasswordForm = server.forms.getForm('changePassword');

        // form validation
        if (newPasswordForm.newpassword.value !== newPasswordForm.newpasswordconfirm.value) {
            newPasswordForm.valid = false;
            newPasswordForm.newpassword.valid = false;
            newPasswordForm.newpasswordconfirm.valid = false;
            newPasswordForm.newpasswordconfirm.error = Resource.msg(
                'form.profile.passwordconfirm.mismatch', 'forms', null
            );
        }

        var result = {
            currentPassword: newPasswordForm.currentpassword.value,
            newPassword: newPasswordForm.newpassword.value,
            newPasswordConfirm: newPasswordForm.newpasswordconfirm.value,
            newPasswordForm: newPasswordForm
        };

        if (newPasswordForm.valid) {
            res.setViewData(result);
            this.on('route:BeforeComplete', function () { // eslint-disable-line no-shadow
                var formInfo = res.getViewData();
                var customer = CustomerMgr.getCustomerByCustomerNumber(
                    req.currentCustomer.profile.customerNo
                );
                var status;
                Transaction.wrap(function () {
                    status = customer.profile.credentials.setPassword(
                        formInfo.newPassword,
                        formInfo.currentPassword,
                        true
                    );
                });
                if (status.error) {
                    formInfo.newPasswordForm.currentpassword.valid = false;
                    formInfo.newPasswordForm.currentpassword.error = Resource.msg(
                        'error.message.currentpasswordnomatch', 'forms', null
                    );

                    delete formInfo.currentPassword;
                    delete formInfo.newPassword;
                    delete formInfo.newPasswordConfirm;
                    delete formInfo.newPasswordForm;

                    res.json({
                        success: false,
                        fields: formErrors.getFormErrors(newPasswordForm)
                    });
                } else {
                    delete formInfo.currentPassword;
                    delete formInfo.newPassword;
                    delete formInfo.newPasswordConfirm;
                    delete formInfo.newPasswordForm;

                    res.json({
                        success: true,
                        redirectUrl: URLUtils.url('Account-Show').toString()
                    });
                }
            });
        } else {
            res.json({
                success: false,
                fields: formErrors.getFormErrors(newPasswordForm)
            });
        }
        return next();
    }
);

server.append(
    'Show',
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems,
    breadcrumbs.addCurrentPageToBreadcrumbs('header.myaccount', 'account')
);
server.append(
    'EditProfile',
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems,
    breadcrumbs.addCurrentPageToBreadcrumbs('profileDetails.title', 'account')
);
server.append(
    'EditPassword',
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems,
    breadcrumbs.addCurrentPageToBreadcrumbs('changePassword.title', 'account')
);

module.exports = server.exports();
