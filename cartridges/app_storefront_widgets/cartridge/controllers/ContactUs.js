var server = require('server');

var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

server.extend(module.superModule);

/**
 * @description Need to be prepended and stop "not include" requests
 */
server.prepend('Landing', server.middleware.include);

/**
 * @description Add form, based on API form definition and necessary page meta data
 */
server.append('Landing',
    function (req, res, next) {
        var contactForm = server.forms.getForm('contactUs');
        var contentHelpers = require('*/cartridge/scripts/content/contentHelpers');
        var questionTypeOptions = contentHelpers.getContactUsQuestionTypeOptions(req.locale.id);

        contactForm.base.questionType.setOptions(questionTypeOptions.iterator());
        contactForm.clear();
        res.render('contactUs/contactUs.isml', {
            contactForm: server.forms.getForm('contactUs')
        });

        next();
    },
    pageMetaData.computedPageMetaData,
    pageMetaData.hrefLang);

/**
 * @description Action was overwritten, as for now form is based on API forms framework and it should be validated in a different way
 */
server.replace('Subscribe',
    server.middleware.post,
    server.middleware.https,
    function (req, res, next) {
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        var formErrors = require('*/cartridge/scripts/formErrors');

        var contactForm = server.forms.getForm('contactUs');

        if (contactForm.valid) {
            var questionType;
            // Session has been ended before submission
            if (empty(contactForm.questionType.options)) {
                var contentHelpers = require('*/cartridge/scripts/content/contentHelpers');
                var questionTypes = contentHelpers.getContactUsQuestionTypes(req.locale.id);

                if (Array.isArray(questionTypes)) {
                    questionType = questionTypes.filter(function (option) {
                        return option.id === contactForm.questionType.value;
                    })[0];
                }
            } else {
                questionType = contactForm.questionType.options.filter(function (option) {
                    return option.selected;
                })[0];
            }
            var contactDetails = {
                name: contactForm.name.value,
                email: contactForm.email.value,
                phone: contactForm.phone.value,
                orderNumber: contactForm.orderNumber.value,
                questionType: questionType ? questionType.label : null,
                message: contactForm.message.value
            };
            contactForm.clear();
            hooksHelper('app.contactUs.subscribe', 'subscribe', contactDetails, function () {});

            var ContentModel = require('*/cartridge/models/content');
            var successContentModel = new ContentModel('cp-contact-us-success');

            res.json({
                success: true,
                msg: successContentModel.getMarkup()
            });
        } else {
            res.json({
                success: false,
                fields: formErrors.getFormErrors(contactForm)
            });
        }

        next();
    });

module.exports = server.exports();
