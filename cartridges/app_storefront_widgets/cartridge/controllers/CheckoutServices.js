var page = module.superModule;
var server = require('server');

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.extend(page);

server.replace(
    'SubmitPayment',
    server.middleware.post,
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var paymentForm = server.forms.getForm('billing');
        var basePaymentMethods = require('util/pref').get('checkout.payments.base').split(',');

        if (basePaymentMethods.indexOf(paymentForm.paymentMethod.value) === -1) {
            // Go to the next middleware chain to handle different payment
            return next();
        }

        if (require('util/test').forceNegativeCase('general_error')) {
            throw new Error();
        }

        var Resource = require('dw/web/Resource');
        var PaymentManager = require('dw/order/PaymentMgr');
        var paymentMethodId = paymentForm.paymentMethod.value;
        var paymentProcessor = PaymentManager.getPaymentMethod(paymentMethodId).getPaymentProcessor();

        if (!paymentProcessor) {
            // respond with form data and errors
            res.json({
                form: paymentForm,
                fieldErrors: [],
                serverErrors: [Resource.msg('error.payment.processor.missing', 'checkout', null)],
                error: true
            });

            return next();
        }

        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

        // verify billing form data
        var billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);
        var contactInfoFormErrors = COHelpers.validateFields(paymentForm.contactInfoFields);
        var formFieldErrors = [];
        var viewData = {};

        if (Object.keys(billingFormErrors).length) {
            formFieldErrors.push(billingFormErrors);
        }

        viewData.address = COHelpers.getBillingAddressObject(paymentForm);

        if (Object.keys(contactInfoFormErrors).length) {
            formFieldErrors.push(contactInfoFormErrors);
        } else {
            viewData.email = {
                value: paymentForm.contactInfoFields.email.value
            };

            viewData.phone = { value: paymentForm.contactInfoFields.phone.value };
        }

        var processPaymentFormResult = COHelpers.processPaymentForm(
            paymentProcessor,
            req,
            paymentForm,
            viewData
        );

        if (processPaymentFormResult.error && processPaymentFormResult.fieldErrors) {
            formFieldErrors.push(processPaymentFormResult.fieldErrors);
        }

        if (formFieldErrors.length || processPaymentFormResult.serverErrors) {
            // respond with form data and errors
            res.json({
                form: paymentForm,
                fieldErrors: formFieldErrors,
                serverErrors: processPaymentFormResult.serverErrors ? processPaymentFormResult.serverErrors : [],
                error: true
            });

            return next();
        }

        res.setViewData(processPaymentFormResult.viewData);

        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var BasketMgr = require('dw/order/BasketMgr');
            var PaymentMgr = require('dw/order/PaymentMgr');
            var Transaction = require('dw/system/Transaction');
            var AccountModel = require('*/cartridge/models/account');
            var OrderModel = require('*/cartridge/models/order');
            var URLUtils = require('dw/web/URLUtils');
            var Locale = require('dw/util/Locale');
            var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
            var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');

            var currentBasket = BasketMgr.getCurrentBasket();
            var billingData = res.getViewData();

            if (!currentBasket) {
                delete billingData.paymentInformation;

                res.json({
                    error: true,
                    cartError: true,
                    fieldErrors: [],
                    serverErrors: [],
                    redirectUrl: URLUtils.url('Cart-Show').toString()
                });
                return;
            }

            var validatedProducts = validationHelpers.validateProducts(currentBasket);

            if (validatedProducts.error) {
                delete billingData.paymentInformation;

                res.json({
                    error: true,
                    cartError: true,
                    fieldErrors: [],
                    serverErrors: [],
                    redirectUrl: URLUtils.url('Cart-Show').toString()
                });
                return;
            }

            var billingForm = server.forms.getForm('billing');
            var paymentMethodID = billingData.paymentMethod.value;

            COHelpers.clearBillingFormSensetiveData(billingForm);

            var handleBillingAddressResult = COHelpers.handleBillingAddress(req, currentBasket, billingData);

            if (handleBillingAddressResult.error) {
                delete billingData.paymentInformation;

                res.json({
                    form: billingForm,
                    fieldErrors: [],
                    serverErrors: [Resource.msg('error.no.saved.billing.address', 'payment', null)],
                    error: true
                });
                return;
            }

            // if there is no selected payment option and balance is greater than zero
            if (!paymentMethodID && currentBasket.totalGrossPrice.value > 0) {
                var noPaymentMethod = {};

                noPaymentMethod[billingData.paymentMethod.htmlName] = Resource.msg(
                    'error.no.selected.payment.method',
                    'payment',
                    null
                );

                delete billingData.paymentInformation;

                res.json({
                    form: billingForm,
                    fieldErrors: [noPaymentMethod],
                    serverErrors: [],
                    error: true
                });
                return;
            }

            // check to make sure there is a payment processor
            var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();

            if (!processor) {
                throw new Error(Resource.msg(
                    'error.payment.processor.missing',
                    'checkout',
                    null
                ));
            }

            COHelpers.updatePaymentInformation(req, billingData);

            var paymentProcessorResult = COHelpers.handlePaymentProcessor(
                processor,
                currentBasket,
                billingData.paymentInformation
            );

            // need to invalidate credit card fields
            if (paymentProcessorResult.error) {
                delete billingData.paymentInformation;

                res.json({
                    form: billingForm,
                    fieldErrors: paymentProcessorResult.fieldErrors,
                    serverErrors: paymentProcessorResult.serverErrors,
                    error: true
                });
                return;
            }

            // Save payment to customer wallet
            COHelpers.savePaymentInformation(req, processor, currentBasket, billingData);

            // Calculate the basket
            Transaction.wrap(function () {
                basketCalculationHelpers.calculateTotals(currentBasket);
            });

            // Re-calculate the payments.
            var calculatedPaymentTransaction = COHelpers.calculatePaymentTransaction(
                currentBasket
            );

            if (calculatedPaymentTransaction.error) {
                res.json({
                    form: paymentForm,
                    fieldErrors: [],
                    serverErrors: [Resource.msg('error.technical', 'checkout', null)],
                    error: true
                });

                return;
            }

            COHelpers.handleEmailSubscription(paymentForm);

            var accountModel = new AccountModel(req.currentCustomer);
            var basketModelData = {
                usingMultiShipping: COHelpers.handleMultishippingUse(req, currentBasket),
                countryCode: Locale.getLocale(req.locale.id).getCountry(),
                containerView: 'basket'
            };
            var basketModel = new OrderModel(currentBasket, basketModelData);

            delete billingData.paymentInformation;

            res.json({
                customer: accountModel,
                order: basketModel,
                form: billingForm,
                error: false
            });
        });

        return next();
    }
);

server.replace(
    'PlaceOrder',
    server.middleware.post,
    server.middleware.https,
    function (req, res, next) {
        if (require('util/test').forceNegativeCase('general_error')) {
            throw new Error();
        }

        var BasketMgr = require('dw/order/BasketMgr');
        var OrderMgr = require('dw/order/OrderMgr');
        var Resource = require('dw/web/Resource');
        var Transaction = require('dw/system/Transaction');
        var URLUtils = require('dw/web/URLUtils');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        var cartHelpers = require('*/cartridge/scripts/cart/cartHelpers');
        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
        var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');

        var currentBasket = BasketMgr.getCurrentBasket();

        if (!currentBasket) {
            if (session.privacy.isPaymentRedirectInvoked && session.privacy.orderID) {
                var existingOrder = OrderMgr.getOrder(session.privacy.orderID);
                cartHelpers.reCreateBasket(existingOrder);
                res.redirect(URLUtils.url('Cart-Show'));

                return next();
            }

            res.json({
                error: true,
                cartError: true,
                fieldErrors: [],
                serverErrors: [],
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });

            return next();
        }

        var validatedProducts = validationHelpers.validateProducts(currentBasket);

        if (validatedProducts.error) {
            res.json({
                error: true,
                cartError: true,
                fieldErrors: [],
                serverErrors: [],
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });

            return next();
        }

        if (req.session.privacyCache.get('fraudDetectionStatus')) {
            res.json({
                error: true,
                cartError: true,
                redirectUrl: URLUtils.url('Error-ErrorCode', 'err', '01').toString(),
                errorMessage: Resource.msg('error.technical', 'checkout', null)
            });

            return next();
        }

        var validationOrderStatus = hooksHelper(
            'app.validate.order',
            'validateOrder',
            currentBasket,
            require('*/cartridge/scripts/hooks/validateOrder').validateOrder
        );

        if (validationOrderStatus.error) {
            res.json({
                error: true,
                errorMessage: validationOrderStatus.message
            });

            return next();
        }

        // Check to make sure there is a shipping address
        if ((currentBasket.defaultShipment.shippingAddress === null)
            || require('util/test').forceNegativeCase('no_shipping_address')
        ) {
            res.json({
                error: true,
                errorStage: {
                    stage: 'shipping',
                    step: 'address'
                },
                errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
            });

            return next();
        }

        // Check to make sure billing address exists
        if (!currentBasket.billingAddress || require('util/test').forceNegativeCase('no_billing_address')) {
            res.json({
                error: true,
                errorStage: {
                    stage: 'payment',
                    step: 'billingAddress'
                },
                errorMessage: Resource.msg('error.no.billing.address', 'checkout', null)
            });

            return next();
        }

        // Calculate the basket
        Transaction.wrap(function () {
            basketCalculationHelpers.calculateTotals(currentBasket);
        });

        // Re-validates existing payment instruments
        var validPayment = COHelpers.validatePayment(req, currentBasket);

        if (validPayment.error) {
            res.json({
                error: true,
                errorStage: {
                    stage: 'payment',
                    step: 'paymentInstrument'
                },
                errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
            });

            return next();
        }

        // Re-calculate the payments.
        var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);

        if (calculatedPaymentTransactionTotal.error) {
            res.json({
                error: true,
                errorMessage: Resource.msg('error.technical', 'checkout', null)
            });
            return next();
        }

        // Creates a new order.
        var order = COHelpers.createOrder(currentBasket);

        if (!order) {
            res.json({
                error: true,
                errorStage: {
                    stage: 'payment',
                    step: 'paymentInstrument'
                },
                errorMessage: Resource.msg('error.technical', 'checkout', null)
            });

            return next();
        }

        // Handles payment authorization
        var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo);

        if (handlePaymentResult.error) {
            if (handlePaymentResult.renderTemplate) {
                res.render(handlePaymentResult.renderViewPath, handlePaymentResult.renderData);

                return next();
            }

            if (handlePaymentResult.redirect) {
                res.redirect(handlePaymentResult.redirectURL);

                return next();
            }

            if (handlePaymentResult.errorMessage) {
                res.json({
                    error: true,
                    errorStage: {
                        stage: 'payment',
                        step: 'paymentInstrument'
                    },
                    errorMessage: handlePaymentResult.errorMessage
                });

                return next();
            }

            res.json({
                error: true,
                errorStage: {
                    stage: 'payment',
                    step: 'paymentInstrument'
                },
                errorMessage: Resource.msg('error.technical', 'checkout', null)
            });

            return next();
        }

        var fraudDetectionStatus = hooksHelper(
            'app.fraud.detection',
            'fraudDetection',
            currentBasket,
            require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection
        );

        if (fraudDetectionStatus.status === 'fail') {
            Transaction.wrap(function () {
                OrderMgr.failOrder(order, true);
            });

            // fraud detection failed
            req.session.privacyCache.set('fraudDetectionStatus', true);

            res.json({
                error: true,
                cartError: true,
                redirectUrl: URLUtils.url('Error-ErrorCode', 'err', fraudDetectionStatus.errorCode).toString(),
                errorMessage: Resource.msg('error.technical', 'checkout', null)
            });

            return next();
        }

        // Places the order
        if (handlePaymentResult.authorized) {
            var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);

            if (placeOrderResult.error) {
                res.json({
                    error: true,
                    errorStage: {
                        stage: 'payment',
                        step: 'paymentInstrument'
                    },
                    errorMessage: Resource.msg('error.technical', 'checkout', null)
                });

                return next();
            }
        }

        var shippingForm = server.forms.getForm('shipping');

        if (shippingForm.shippingAddress.addressFields.saveAddress.checked) {
            COHelpers.saveOrderAddressForCustomer(order, req.currentCustomer);
        }

        COHelpers.sendConfirmationEmail(order, req.locale.id);

        // Reset usingMultiShip after successful Order placement
        req.session.privacyCache.set('usingMultiShipping', false);

        res.json({
            error: false,
            orderID: order.orderNo,
            orderToken: order.orderToken,
            continueUrl: URLUtils.url('Order-Confirm').toString()
        });

        return next();
    }
);

module.exports = server.exports();
