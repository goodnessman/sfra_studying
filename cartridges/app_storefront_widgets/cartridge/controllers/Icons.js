var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

server.get(
    'Show',
    cache.applyDefaultCache,
    consentTracking.consent,
    function (req, res, next) {
        var Site = require('dw/system/Site');
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

        pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);
        res.render('common/icons/preview');
        next();
    }, pageMetaData.computedPageMetaData
);

module.exports = server.exports();
