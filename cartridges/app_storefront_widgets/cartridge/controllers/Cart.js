var server = require('server');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.extend(module.superModule);

/**
 * @description Need to be prepended and stop further flow and render related error, when requested PLI was not found in a basket.
 * Otherwise default SFRA implementation will cause non-handable output for storefront
 */
server.prepend('GetProduct', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Resource = require('dw/web/Resource');
    var basket = BasketMgr.getCurrentBasket();
    var requestPLI = null;

    if (basket) {
        var collections = require('*/cartridge/scripts/util/collections');

        var requestUuid = req.querystring.uuid;
        requestPLI = collections.find(basket.allProductLineItems, function (item) {
            return item.UUID === requestUuid;
        });
    }

    if (!requestPLI) {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product', 'cart', null)
        });
        this.emit('route:Complete', req, res);
        return;
    }

    next();
});

/**
 * @description Add to response ID of product to be able show error for correct product in Wish List
 */
server.append('AddProduct', function (req, res, next) {
    var pid = req.form.pid;
    res.json({
        pid: pid
    });
    next();
});

/**
 * @description Changes output format from json to html in order to be able to process remote includes in underlying templates
 */
server.append('GetProduct', function (_req, _res, next) {
    this.on('route:BeforeComplete', function (__req, res) {
        res.render('product/quickView.isml');
    });
    next();
});

server.append('Show', pageMetaData.computedPageMetaData);

/**
 * @description Action `AddCoupon` was replaced in order to:
 * - implement logic with SFCC form `coupon` support
 * - be more specific in different coupon application error messages
 */
server.replace(
    'AddCoupon',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var couponForm = server.forms.getForm('coupon');
        var formErrors = require('*/cartridge/scripts/formErrors');

        if (!couponForm.valid) {
            res.json({
                error: true,
                fieldErrors: formErrors.getFormErrors(couponForm)
            });
            couponForm.clear();
            return next();
        }

        var couponCode = couponForm.couponCode.value;
        couponForm.clear();

        var BasketMgr = require('dw/order/BasketMgr');
        var Resource = require('dw/web/Resource');
        var Transaction = require('dw/system/Transaction');
        var URLUtils = require('dw/web/URLUtils');
        var CartModel = require('*/cartridge/models/cart');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

        var currentBasket = BasketMgr.getCurrentBasket();

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });

            return next();
        }

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({ errorMessage: Resource.msg('error.add.coupon', 'cart', null) });
            return next();
        }

        var error = false;
        var errorMessage;

        try {
            Transaction.wrap(function () {
                return currentBasket.createCouponLineItem(couponCode, true);
            });
        } catch (e) {
            error = true;
            var errorCodes = {
                COUPON_CODE_ALREADY_IN_BASKET: 'form.promoCode.errors.alreadyapplied',
                COUPON_ALREADY_IN_BASKET: 'error.coupon.cannot.be.combined',
                COUPON_CODE_ALREADY_REDEEMED: 'error.coupon.already.redeemed',
                COUPON_CODE_UNKNOWN: 'form.promoCode.errors.notrecognised',
                COUPON_DISABLED: 'form.promoCode.errors.notrecognised',
                REDEMPTION_LIMIT_EXCEEDED: 'error.unable.to.add.coupon',
                TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED: 'form.promoCode.errors.notrecognised',
                CUSTOMER_REDEMPTION_LIMIT_EXCEEDED: 'form.promoCode.errors.notrecognised',
                NO_ACTIVE_PROMOTION: 'form.promoCode.errors.notrecognised',
                NO_APPLICABLE_PROMOTION: 'form.promoCode.errors.notrecognised',
                default: 'form.promoCode.errors.notrecognised'
            };

            var errorMessageKey = errorCodes[e.errorCode] || errorCodes.default;
            errorMessage = Resource.msg(errorMessageKey, 'forms', null);
        }

        if (error) {
            res.json({
                error: error,
                errorMessage: errorMessage
            });
            return next();
        }

        Transaction.wrap(function () {
            basketCalculationHelpers.calculateTotals(currentBasket);
        });

        var basketModel = new CartModel(currentBasket);

        res.json(basketModel);
        return next();
    }
);

/**
 * @description Rewritten SFRA implementation. Changes condition of pli to be updated, which allows to show error message that limit is reached and Only N-items in stock.
 */
server.replace('UpdateQuantity', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var CartModel = require('*/cartridge/models/cart');
    var collections = require('*/cartridge/scripts/util/collections');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var productId = req.querystring.pid;
    var updateQuantity = parseInt(req.querystring.quantity, 10);
    var uuid = req.querystring.uuid;
    var productLineItems = currentBasket.productLineItems;
    var matchingLineItem = collections.find(productLineItems, function (item) {
        return item.productID === productId && item.UUID === uuid;
    });
    var availableToSell = 0;

    var totalQtyRequested = 0;
    var qtyAlreadyInCart = 0;
    var minOrderQuantity = 0;
    var perpetual = false;
    var canBeUpdated = false;
    var bundleItems;
    var bonusDiscountLineItemCount = currentBasket.bonusDiscountLineItems.length;

    if (matchingLineItem) {
        if (matchingLineItem.product.bundle) {
            bundleItems = matchingLineItem.bundledProductLineItems;
            canBeUpdated = collections.every(bundleItems, function (item) {
                var quantityToUpdate = updateQuantity
                    * matchingLineItem.product.getBundledProductQuantity(item.product).value;
                qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                    item.productID,
                    productLineItems,
                    item.UUID
                );
                totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
                availableToSell = item.product.availabilityModel.inventoryRecord.ATS.value;
                perpetual = item.product.availabilityModel.inventoryRecord.perpetual;
                minOrderQuantity = item.product.minOrderQuantity.value;
                return (totalQtyRequested <= availableToSell || perpetual)
                    && (quantityToUpdate >= minOrderQuantity);
            });
        } else {
            qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                productId,
                productLineItems,
                matchingLineItem.UUID
            );
            totalQtyRequested = updateQuantity + qtyAlreadyInCart;
            minOrderQuantity = matchingLineItem.product.minOrderQuantity.value;
            // only check if total quantity is greater than min.
            // exceeded quantity check will be performed by availability product decorator
            canBeUpdated = totalQtyRequested >= minOrderQuantity;
        }
    }

    if (canBeUpdated) {
        Transaction.wrap(function () {
            matchingLineItem.setQuantityValue(updateQuantity);

            var previousBounsDiscountLineItems = collections.map(
                currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
                    return bonusDiscountLineItem.UUID;
                }
            );

            basketCalculationHelpers.calculateTotals(currentBasket);
            if (currentBasket.bonusDiscountLineItems.length > bonusDiscountLineItemCount) {
                var prevItems = JSON.stringify(previousBounsDiscountLineItems);

                collections.forEach(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
                    if (prevItems.indexOf(bonusDiscountLineItem.UUID) < 0) {
                        bonusDiscountLineItem.custom.bonusProductLineItemUUID = matchingLineItem.UUID;
                        matchingLineItem.custom.bonusProductLineItemUUID = 'bonus';
                        matchingLineItem.custom.preOrderUUID = matchingLineItem.UUID;
                    }
                });
            }
        });
    }

    if (matchingLineItem && canBeUpdated) {
        var basketModel = new CartModel(currentBasket);
        res.json(basketModel);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product.quantity', 'cart', null)
        });
    }

    return next();
});

module.exports = server.exports();
