var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

var server = require('server');

server.extend(module.superModule);

server.append('Show', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');

    var structuredDataHelper = require('*/cartridge/scripts/helpers/structuredDataHelper');
    var schemaData = [];

    schemaData.push(structuredDataHelper.getWebSiteSchema());
    schemaData.push(structuredDataHelper.getOrganization());
    schemaData.push(structuredDataHelper.getSearch());

    res.setViewData({
        schemaData: schemaData,
        canonicalUrl: URLUtils.abs('Home-Show')
    });

    next();
}, pageMetaData.hrefLang);


module.exports = server.exports();
