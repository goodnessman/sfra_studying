var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var accountNavigation = require('*/cartridge/scripts/middleware/accountNavigation');


var server = require('server');

server.extend(module.superModule);

/**
 * @description Overloaded, as history page should have pagination, so order model hasn't to be created for all search result
 * <br>Also, we can't return just page elements from helper, as paging info will be lost
 * <br>Added paging info to the rendering context
 */
server.replace(
    'History',
    server.middleware.get,
    consentTracking.consent,
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var Resource = require('dw/web/Resource');
        var URLUtils = require('dw/web/URLUtils');
        var OrderHelpers = require('*/cartridge/scripts/order/orderHelpers');
        var objectUtils = require('util/object');

        var ordersResult = OrderHelpers.getOrders(
            req.currentCustomer,
            req.querystring
        );

        var breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.url('Account-Show').toString()
            },
            {
                htmlValue: Resource.msg('label.orderhistory', 'account', null),
                url: URLUtils.url('Order-History').toString()
            }
        ];

        res.render('account/order/history', objectUtils.assign(ordersResult, {
            accountlanding: false,
            breadcrumbs: breadcrumbs
        }));
        next();
    },
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems
);

/**
 * @description Overloaded, as history page should have pagination, so order model should be created just for current page
 * <br>Also, we can't return just page elements from helper, as paging info will be lost
 * <br>Added paging info to the rendering context
 */
server.replace(
    'Filtered',
    server.middleware.get,
    server.middleware.https,
    consentTracking.consent,
    userLoggedIn.validateLoggedInAjax,
    function (req, res, next) {
        var OrderHelpers = require('*/cartridge/scripts/order/orderHelpers');
        var objectUtils = require('util/object');

        var data = res.getViewData();
        if (data && !data.loggedin) {
            res.json();
            return next();
        }

        var ordersResult = OrderHelpers.getOrders(
            req.currentCustomer,
            req.querystring
        );

        res.render('account/order/historyOrders', objectUtils.assign(ordersResult, {
            accountlanding: false
        }));
        return next();
    }
);

/**
 * @description Returns page parts for load more action
 */
server.get(
    'LoadItems',
    server.middleware.https,
    consentTracking.consent,
    userLoggedIn.validateLoggedInAjax,
    function (req, res, next) {
        var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
        var OrderHelpers = require('*/cartridge/scripts/order/orderHelpers');
        var objectUtils = require('util/object');

        var data = res.getViewData();
        if (data && !data.loggedin) {
            res.json();
            return next();
        }

        var ordersResult = OrderHelpers.getOrders(
            req.currentCustomer,
            req.querystring
        );

        res.json({
            orderListItems: renderTemplateHelper.getRenderedHtml(objectUtils.assign(ordersResult, {
                accountlanding: false
            }), 'account/order/orderList'),
            orderListFooter: renderTemplateHelper.getRenderedHtml(ordersResult, 'account/order/historyListFooter')
        });
        return next();
    }
);

server.get('TrackForm', function (req, res, next) {
    var userProfile = req.currentCustomer.profile;
    var trackOrderForm = server.forms.getForm('trackOrder');
    if (userProfile) {
        trackOrderForm.orderEmail.value = userProfile.email;
    }
    res.render('account/components/trackOrder', {
        trackOrderForm: trackOrderForm
    });
    next();
}, pageMetaData.computedPageMetaData);

server.append('Confirm', pageMetaData.computedPageMetaData);

server.append('Details', pageMetaData.computedPageMetaData);


server.prepend(
    'CreateAccount',
    /**
     * @description Handles agree to privacy during account creation
     * @param {object} req - request object
     * @param {object} res - response object
     * @param {object} next - next function
     * @returns {Function} result
     */
    function (req, res, next) {
        var Resource = require('dw/web/Resource');
        var passwordForm = server.forms.getForm('newPasswords');

        if (!passwordForm.valid || !passwordForm.agreeToPrivacy.valid) {
            passwordForm.valid = false;
            passwordForm.agreeToPrivacy.valid = false;
            passwordForm.agreeToPrivacy.error = Resource.msg(
                'error.message.required',
                'forms',
                null
            );

            res.json({
                error: true
            });

            this.emit('route:Complete');
            return;
        }

        return next(); // eslint-disable-line consistent-return
    }
);

module.exports = server.exports();
