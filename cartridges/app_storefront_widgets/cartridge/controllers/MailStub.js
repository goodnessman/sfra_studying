var server = require('server');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var environment = require('*/cartridge/scripts/middleware/environment');

var emailTypes = {
    registration: 'registration',
    passwordReset: 'passwordReset',
    passwordChanged: 'passwordChanged',
    orderConfirmation: 'orderConfirmation',
    accountLocked: 'accountLocked',
    accountEdited: 'accountEdited'
};

/**
 * @description Meta of the mail page
 * @param {string} type - type of the mail
 * @param {object} args - string for insertions in message
 * @returns {{preheader: string, theme: string}}
 */
function getMailMeta(type, args) {
    var Resource = require('dw/web/Resource');

    return {
        theme: Resource.msg(type + '.theme', 'mails', ''),
        preheader: Resource.msgf(type + '.preheader', 'mails', '', args)
    };
}

/**
 * @description - Prepares User object for emails. If in params `email` will be specified - will
 * try to find a relevant user object. In case params are empty - prepares object with predefined mock data.
 * @param {string} type - type of the mail
 * @param {boolean} [urlPasswordReset] - If we need to get user object for Password Reset email
 * @returns {object} - User object to use in email templates
 */
function getUserObject(type, urlPasswordReset) {
    var userObject = {};
    var url = URLUtils.https('Login-Show');
    var passwordResetUrl = URLUtils.https('Account-SetNewPassword', 'Token');

    var customerEmail = request.httpParameterMap.email.value || request.httpParameterMap.mailTo.value || 'john_doe@example.com';
    var customer = CustomerMgr.getCustomerByLogin(customerEmail);

    if (customer) {
        userObject = {
            email: customer.profile.email,
            firstName: customer.profile.firstName,
            lastName: customer.profile.lastName,
            url: url
        };

        if (urlPasswordReset) {
            var Transaction = require('dw/system/Transaction');
            Transaction.wrap(function () {
                passwordResetUrl.append('Token', customer.profile.credentials.createResetPasswordToken());
                userObject.url = passwordResetUrl;
            });
        }
    } else {
        userObject = {
            email: 'john_doe@example.com',
            firstName: 'John',
            lastName: 'Doe',
            url: urlPasswordReset ? passwordResetUrl.append('Token', 'fakeToken') : url
        };
    }

    userObject.meta = getMailMeta(type, customer ? customer.profile.firstName : '');

    return userObject;
}

/**
 * @description - Prepares Order object for emails.
 * @param {number} orderID - ID of the order to get data
 * @param {string} locale - locale string. If not provided it would computed as default locale
 * @returns {object} - Order object to use in email templates
 */
function getOrderModel(orderID, locale) {
    var OrderMgr = require('dw/order/OrderMgr');

    var order = OrderMgr.getOrder(orderID);
    if (!order) {
        return 'No order ID (orderID) specified!';
    }

    if (!locale) {
        locale = Site.getCurrent().defaultLocale;
    }

    var OrderModel = require('*/cartridge/models/order');
    var Locale = require('dw/util/Locale');
    var currentLocale = Locale.getLocale(locale);

    return new OrderModel(order, { countryCode: currentLocale.country, containerView: 'order' });
}

/**
 * @description Renders email template with data into a string
 * @param {string} template - email template to use
 * @param {object} context - data container for email
 * @returns {string} rendered template
 */
function renderMail(template, context) {
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    return renderTemplateHelper.getRenderedHtml(context, template);
}

/**
 * @description Sends email content to defined email
 * @param {string} content - parsed email content string
 * @param {string} to - target email address
 * @param {string} type - type of the mail. Need to get proper meta
 */
function sendMail(content, to, type) {
    var Mail = require('dw/net/Mail');

    var email = new Mail();
    email.addTo(to);
    email.setSubject(getMailMeta(type).theme);
    email.setFrom('mailstub@astoundcommerce.com');
    email.setContent(content, 'text/html', 'UTF-8');
    email.send();
}

function orderConfirmationEmail(type) {
    var orderID = request.httpParameterMap.orderID.value;
    var locale = request.httpParameterMap.locale.value;

    return renderMail(
        'checkout/confirmation/confirmationEmail',
        {
            order: getOrderModel(orderID, locale),
            meta: getMailMeta(type, orderID)
        }
    );
}

function getMailContentString(type) {
    var contentString = '';

    switch (type) {
        case emailTypes.accountEdited:
            contentString = renderMail('account/components/accountEditedEmail', getUserObject(type));
            break;
        case emailTypes.accountLocked:
            contentString = renderMail('account/components/accountLockedEmail', getUserObject(type));
            break;
        case emailTypes.passwordChanged:
            contentString = renderMail('account/password/passwordChangedEmail', getUserObject(type));
            break;
        case emailTypes.passwordReset:
            contentString = renderMail('account/password/passwordResetEmail', getUserObject(type, true));
            break;
        case emailTypes.registration:
            contentString = renderMail('checkout/confirmation/accountRegisteredEmail', getUserObject(type));
            break;
        case emailTypes.orderConfirmation:
            contentString = orderConfirmationEmail(type);
            break;
        default:
            contentString = '';
    }

    return contentString;
}

function mailsArray() {
    var result = [];

    for (var prop in emailTypes) {
        if (Object.prototype.hasOwnProperty.call(emailTypes, prop)) {
            result.push(prop);
        }
    }

    return result;
}

var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

/**
 * @description Sends chosen email type to specified email
 */
server.post('Send', environment.ensureNotProduction,
    function (req, res, next) {
        var mailID = req.form.mailid;
        var to = req.form.mailTo;
        var message = 'Please provide mail address';

        if (to && mailID) {
            sendMail(getMailContentString(mailID), to, mailID);
            message = 'Mail successfully send to ' + to;
        }

        res.redirect(URLUtils.url('MailStub-Start', 'message', message));
        next();
    });

/**
 * @description Preview chosen email in browser.
 * In case if `useLocal` parameter specified in request - forces email showing
 * in local environment, with CSS classes and hot-reload available
 */
server.get('Preview', environment.ensureNotProduction,
    function (req, res, next) {
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

        pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);

        res.setContentType('text/html');
        res.print(getMailContentString(req.querystring.mid));

        next();
    }, pageMetaData.computedPageMetaData);

/**
 * @description Renders list of possible email types with an ability to trigger send or preview
 */
server.get('Start', environment.ensureNotProduction,
    function (req, res, next) {
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

        pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);

        res.render('mails/index', {
            message: req.querystring.message,
            mails: mailsArray()
        });

        next();
    }, pageMetaData.computedPageMetaData);

module.exports = server.exports();
