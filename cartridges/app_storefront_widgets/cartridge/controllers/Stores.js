var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

var server = require('server');

server.extend(module.superModule);

server.append('Find', pageMetaData.computedPageMetaData, pageMetaData.hrefLang);

module.exports = server.exports();
