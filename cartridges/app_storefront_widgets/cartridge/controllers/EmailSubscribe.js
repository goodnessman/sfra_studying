var Resource = require('dw/web/Resource');

var server = require('server');

server.extend(module.superModule);

server.replace('Subscribe', function (req, res, next) {
    var emailsubscribeForm = server.forms.getForm('emailsubscribe');
    var formErrors = require('*/cartridge/scripts/formErrors');

    if (emailsubscribeForm.valid) {
        var email = emailsubscribeForm.email.htmlValue;
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        var hookResult = hooksHelper('app.mailingList.subscribe', 'subscribe', [email], function () {});

        if (hookResult && hookResult.error) {
            res.json({
                success: false,
                msg: hookResult.msg
            });
        } else {
            res.json({
                success: true,
                msg: Resource.msg('subscribe.email.success', 'homePage', null)
            });
        }
    } else {
        res.json({
            success: false,
            fieldErrors: formErrors.getFormErrors(emailsubscribeForm)
        });
    }

    emailsubscribeForm.clear();

    next();
});

module.exports = server.exports();
