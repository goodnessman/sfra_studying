var server = require('server');

server.extend(module.superModule);

var breadcrumbs = require('*/cartridge/scripts/middleware/breadcrumbs');
var cache = require('*/cartridge/scripts/middleware/cache');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

/**
 * @description Renders PDP
 * @returns {void}
 */
server.append('Show', breadcrumbs.generateListSchema, pageMetaData.hrefLang);

/**
 * @description Renders availability related part of PDP
 * @returns {void}
 */
server.get('Availability',
    server.middleware.include,
    function (req, res, next) {
        var productHelper = require('*/cartridge/scripts/helpers/productHelpers');

        var showProductAvailabilityHelperResult = productHelper.showProductAvailability(req.querystring);

        res.render(showProductAvailabilityHelperResult.template, {
            product: showProductAvailabilityHelperResult.product,
            addToCartUrl: showProductAvailabilityHelperResult.addToCartUrl,
            updateCartUrl: req.querystring.updateCartUrl,
            updateWishlistUrl: req.querystring.updateWishlistUrl,
            uuid: req.querystring.uuid
        });

        next();
    });

/**
 * @description Overrided Product-SizeChart to support velocity rendering of size guide asset.
 * Also added caching that was missed in base cartridge
 */
server.replace('SizeChart', cache.applyDefaultCache, function (req, res, next) {
    var ContentModel = require('*/cartridge/models/content');

    var content = new ContentModel(req.querystring.cid);

    if (content.body) {
        res.json({
            success: true,
            content: content.getMarkup()
        });
    } else {
        res.json({});
    }
    next();
});

/**
 * @description Replacement of SFRA core method was done due to inability SFCC to render remote includes
 * <br>in templates, which are rendered not directly, but using `renderTemplateHelper.getRenderedHtml`
 * <br>in our implementation, controller action `ShowQuickView` should output only rendered markup, but not json
 */
server.replace('ShowQuickView', cache.applyPromotionSensitiveCache, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var Resource = require('dw/web/Resource');

    var params = req.querystring;
    var product = ProductFactory.get(params);
    var addToCartUrl = URLUtils.url('Cart-AddProduct');
    var template = product.productType === 'set'
        ? 'product/setQuickView.isml'
        : 'product/quickView.isml';

    var context = {
        product: product,
        addToCartUrl: addToCartUrl,
        resources: productHelper.getResources(),
        quickViewFullDetailMsg: Resource.msg('link.quickview.viewdetails', 'product', null),
        closeButtonText: Resource.msg('link.quickview.close', 'product', null),
        enterDialogMessage: Resource.msg('msg.enter.quickview', 'product', null),
        template: template
    };

    res.setViewData(context);

    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var viewData = res.getViewData();
        res.render(viewData.template, viewData);
    });

    next();
});

module.exports = server.exports();
