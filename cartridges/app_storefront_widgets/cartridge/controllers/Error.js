var server = require('server');

server.extend(module.superModule);

server.post('JsLogger', function (req, res, next) {
    var Logger = require('dw/system/Logger').getLogger('error-js', 'error-js');

    let stack = req.form.stack || '';
    // Clean up full URL and left only relative part
    stack = stack.replace(/http.*?\/v[0-9]+\//g, '/');

    Logger.error(
        '\nType: "{0}"\nMsg: "{1}"\nUrl: {2}\nUser Agent: {3}\nLine: {4}, Column: {5}\nStack trace: {6}',
        req.form.type,
        req.form.message,
        req.form.url,
        request.httpUserAgent,
        req.form.line,
        req.form.column,
        stack
    );

    res.print('');
    next();
});

module.exports = server.exports();
