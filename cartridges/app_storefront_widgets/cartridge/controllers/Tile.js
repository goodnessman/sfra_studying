var server = require('server');

server.extend(module.superModule);

/**
 * Added tile position
 */
server.append('Show', function (req, res, next) {
    res.setViewData({
        first: req.querystring.first === 'true'
    });


    next();
});

module.exports = server.exports();
