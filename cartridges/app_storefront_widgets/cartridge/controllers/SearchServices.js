/**
 * @description Controller was overwritten to have additional capabilities
 * <br>for storefront availability requirements. For example - adding `posinset` index
 * <br>to define concrete suggestion a position in a suggestions set.
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');

var totalSuggestions = 0;
function insertSuggestionIndex(suggestions) {
    if (suggestions.product
        && !suggestions.product.exactMatch
        && suggestions.product.phrases
        && suggestions.product.phrases[0]
    ) {
        suggestions.product.phrases[0].posinset = totalSuggestions;
        totalSuggestions++;
    }

    var categorySuggestions = suggestions.category;
    if (categorySuggestions.available) {
        categorySuggestions.categories.forEach(function (category) {
            if (!category.exactMatch) {
                category.posinset = totalSuggestions;
                totalSuggestions += 1;
            }
        });
    }

    var productSuggestions = suggestions.product;
    if (productSuggestions && productSuggestions.available) {
        productSuggestions.products.forEach(function (product) {
            product.posinset = totalSuggestions;
            totalSuggestions += 1;
        });
    }

    var contentSuggestions = suggestions.content;
    if (contentSuggestions && contentSuggestions.available && require('util/pref').is('search.content.enabled')) {
        contentSuggestions.contents.forEach(function (content) {
            if (!content.exactMatch) {
                content.posinset = totalSuggestions;
                totalSuggestions += 1;
            }
        });
    }

    suggestions.viewAllResultsPosinset = totalSuggestions;
    suggestions.total = totalSuggestions + 1;
    return suggestions;
}

server.get('GetSuggestions', cache.applyDefaultCache, function (req, res, next) {
    var SuggestModel = require('dw/suggest/SuggestModel');
    var CategorySuggestions = require('*/cartridge/models/search/suggestions/category');
    var ContentSuggestions = require('*/cartridge/models/search/suggestions/content');
    var ProductSuggestions = require('*/cartridge/models/search/suggestions/product');
    var SearchPhraseSuggestions = require('*/cartridge/models/search/suggestions/searchPhrase');
    var categorySuggestions;
    var contentSuggestions;
    var productSuggestions;
    var recentSuggestions;
    var popularSuggestions;
    var brandSuggestions;
    var searchTerms = req.querystring.q;
    var suggestions;
    var minChars = 3;
    var maxSuggestions = 3;

    if (searchTerms && searchTerms.length >= minChars) {
        suggestions = new SuggestModel();
        suggestions.setSearchPhrase(searchTerms);
        suggestions.setMaxSuggestions(maxSuggestions);
        categorySuggestions = new CategorySuggestions(suggestions, maxSuggestions);
        contentSuggestions = new ContentSuggestions(suggestions, maxSuggestions);
        productSuggestions = new ProductSuggestions(suggestions, maxSuggestions);
        recentSuggestions = new SearchPhraseSuggestions(suggestions.recentSearchPhrases, maxSuggestions);
        popularSuggestions = new SearchPhraseSuggestions(suggestions.popularSearchPhrases, maxSuggestions);
        brandSuggestions = new SearchPhraseSuggestions(suggestions.brandSuggestions, maxSuggestions);

        if (productSuggestions.available
            || contentSuggestions.available
            || categorySuggestions.available
            || recentSuggestions.available
            || popularSuggestions.available
            || brandSuggestions.available) {
            res.json({
                // eslint-disable-next-line max-len
                suggestions: insertSuggestionIndex({ // Note template should much this order (prod, cat, cont, recent) to have proper indexes
                    product: productSuggestions,
                    category: categorySuggestions,
                    content: contentSuggestions,
                    recent: recentSuggestions,
                    popular: popularSuggestions,
                    brand: brandSuggestions,
                    searchPhrase: searchTerms
                })
            });
        } else {
            res.json({});
        }
    } else {
        // Return an empty object that can be checked on the client.  By default, rendered
        // templates automatically get a diagnostic string injected into it, making it difficult
        // to check for a null or empty response on the client.
        res.json({});
    }

    next();
});

module.exports = server.exports();
