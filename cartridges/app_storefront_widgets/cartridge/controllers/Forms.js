var server = require('server');

var forms = require('*/cartridge/scripts/middleware/forms');

server.get(
    'FormDefinition',
    forms.updateFormDefinition
);

module.exports = server.exports();
