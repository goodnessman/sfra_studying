var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

var server = require('server');

server.extend(module.superModule);

server.append('AddPayment', pageMetaData.computedPageMetaData);

module.exports = server.exports();
