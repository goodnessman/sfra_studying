var server = require('server');

var forms = require('*/cartridge/scripts/middleware/forms');
var accountNavigation = require('*/cartridge/scripts/middleware/accountNavigation');
var breadcrumbs = require('*/cartridge/scripts/middleware/breadcrumbs');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

server.extend(module.superModule);

/**
 * Pass to template set as default checkbox value
 */
server.append('EditAddress', function (req, res, next) {
    var viewData = res.getViewData();
    var addressForm = viewData.addressForm;
    var addressId = viewData.addressId;
    var preferredAddress = req.currentCustomer.addressBook.preferredAddress;
    if (addressId === preferredAddress.ID) {
        addressForm.setAsDefault.checked = true;
    }
    res.setViewData({
        formDefinitionContext: 'address',
        preferredAddress: preferredAddress
    });
    next();
}, forms.updateFormOptions);

/**
 * Add set as default Address functionality
 */
server.append('SaveAddress', function (req, res, next) {
    var UUIDUtils = require('dw/util/UUIDUtils');
    var Transaction = require('dw/system/Transaction');
    var addressId = UUIDUtils.createUUID();

    var viewData = res.getViewData();

    if (viewData) {
        res.setViewData({
            addressId: addressId
        });
    }

    this.on('route:BeforeComplete', function () {
        var formInfo = res.getViewData();
        if (formInfo.setAsDefault) {
            var customer = req.currentCustomer.raw;
            var addressBook = customer.getProfile().getAddressBook();
            var address = addressBook.getAddress(formInfo.addressId);
            Transaction.wrap(function () {
                addressBook.setPreferredAddress(address);
            });
            res.setViewData({
                isDefault: true
            });
        }
    });
    next();
});

server.append(
    'AddAddress',
    function (req, res, next) {
        res.setViewData({
            formDefinitionContext: 'address'
        });
        next();
    },
    forms.updateFormOptions,
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems,
    breadcrumbs.addCurrentPageToBreadcrumbs('addressBook.new', 'addressBook')
);

server.append(
    'EditAddress',
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems,
    breadcrumbs.addCurrentPageToBreadcrumbs('addressBook.title.edit', 'addressBook')
);

/**
 * @description Possibility to filter addresses per specific country.
 * If we want to explicitly allow some addresses with respectfull countries in Address List,
 * we should specify them as 'allowed' in `_preferences.properties` like shown below
 * `address.account.allowed.us=US,CA` or `address.account.allowed.gb=GB`.
 * In case if preference is empty - there will be no any changes in address list
 */
server.append(
    'List',
    pageMetaData.computedPageMetaData,
    accountNavigation.generateNavigationMenuItems,
    breadcrumbs.addCurrentPageToBreadcrumbs('addressBook.title', 'addressBook'),
    function (req, res, next) {
        var addressBook = res.getViewData().addressBook;
        if (addressBook && addressBook.length) {
            var Locale = require('dw/util/Locale');
            var locale = Locale.getLocale(req.locale.id);
            if (locale) {
                var allowedCountries = require('util/pref')
                    .get('address.account.allowed.' + (locale.country || '').toLowerCase(), '');
                if (!empty(allowedCountries)) {
                    addressBook = addressBook.filter(function (address) {
                        return allowedCountries.split(',').indexOf(address.address.country) > -1;
                    });
                    res.setViewData({
                        addressBook: addressBook
                    });
                }
            }
        }
        next();
    }
);

module.exports = server.exports();
