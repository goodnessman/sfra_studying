var server = require('server');
var forms = require('*/cartridge/scripts/middleware/forms');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

server.extend(module.superModule);

server.append('Begin', function (req, res, next) {
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    var profileForm = server.forms.getForm('profile');
    var viewData = res.getViewData();

    if (require('util/test').forceNegativeCase('general_error')) {
        throw new Error();
    }

    COHelpers.prefillCustomerEmail(viewData, req.currentCustomer, currentBasket);

    profileForm.clear();

    viewData.profileForm = profileForm;
    viewData.formDefinitionContext = 'shipping,billing';

    res.setViewData(viewData);

    return next();
}, forms.updateFormOptions, pageMetaData.computedPageMetaData);

/**
 * @description Prepare login page forms
 * @returns {void}
 */
server.append(
    'Login',
    pageMetaData.computedPageMetaData,
    function (req, res, next) {
        var viewData = res.getViewData();
        var loginForm = server.forms.getForm('login');
        var profileForm = server.forms.getForm('profile');

        if (require('util/test').forceNegativeCase('general_error')) {
            throw new Error();
        }

        profileForm.clear();

        loginForm.rememberMe.checked = viewData.rememberMe;

        res.setViewData({
            loginForm: loginForm,
            profileForm: profileForm
        });

        next();
    }
);

/**
 * @description Handle guest checkout by preselect guest email address
 * @returns {void}
 */
server.post(
    'Guest',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');
        var Resource = require('dw/web/Resource');
        var loginForm = server.forms.getForm('login');

        if (require('util/test').forceNegativeCase('general_error')) {
            throw new Error();
        }

        if (loginForm.email.valid) {
            var BasketMgr = require('dw/order/BasketMgr');
            var currentBasket = BasketMgr.getCurrentBasket();

            if (!currentBasket) {
                res.redirect(URLUtils.url('Cart-Show'));
                return next();
            }

            var Transaction = require('dw/system/Transaction');

            Transaction.wrap(function () {
                currentBasket.setCustomerEmail(loginForm.email.value);
            });

            res.json({
                success: true,
                redirectUrl: URLUtils.https('Checkout-Begin').toString()
            });
        } else {
            res.json({
                error: [Resource.msg('form.payment.email.tooltip', 'forms', null)]
            });
        }

        return next();
    }
);

module.exports = server.exports();
