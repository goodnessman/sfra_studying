var server = require('server');

var cache = require('*/cartridge/scripts/middleware/cache');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var breadcrumbs = require('*/cartridge/scripts/middleware/breadcrumbs');

server.extend(module.superModule);

server.append('IncludeHeaderMenu', function (req, res, next) {
    res.render('/components/header/pageHeader');

    next();
});

/**
 * @description Adds navigation structure for menu if template requires it
 */
server.append('Show', function (req, res, next) {
    var viewData = res.getViewData();

    if (viewData.content && viewData.content.templateHasNav()) {
        viewData.navStructure = viewData.content.getNavStructure();
    }

    if (req.querystring.ajax) {
        viewData.isAjax = true;
    }

    res.setViewData(viewData);

    next();
});

server.get('IncludeFooter',
    server.middleware.include,
    cache.applyDefaultCache,
    function (req, res, next) {
        res.render('/components/footer/pageFooter');

        next();
    });

/**
 * @description Action, executes to return an URL to target external site,
 * specifying currency code as a URL parameter to be recognized and applied on external site.
 */
server.get('SetSite',
    function (req, res, next) {
        var URLAction = require('dw/web/URLAction');
        var URLUtils = require('dw/web/URLUtils');

        var targetSiteID = req.querystring.siteID;
        var targetLocale = req.querystring.code;

        var redirectUrl = '';

        if (targetLocale && targetSiteID) {
            redirectUrl = URLAction('Page-SiteRedirect', targetSiteID, targetLocale);
            redirectUrl = URLUtils.abs(redirectUrl);
            redirectUrl = redirectUrl.append('CurrencyCode', req.querystring.CurrencyCode);
        }

        if (!empty(redirectUrl)) {
            res.json({
                success: true,
                redirectUrl: redirectUrl.toString()
            });
        }

        next();
    });

/**
 * @description Action, which will be executed when customer comes from external site.
 * Changes locale and currency as specified in request parameters,
 * and then redirects to Home Page.
 */
server.get('SiteRedirect',
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');
        var Currency = require('dw/util/Currency');
        var Site = require('dw/system/Site');
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');

        var requestLocaleId = req.locale.id;
        var currentBasket = BasketMgr.getCurrentBasket();
        var currentSite = Site.getCurrent();
        var allowedCurrencies = currentSite.allowedCurrencies;

        var setLocaleResult = req.setLocale(requestLocaleId);
        if (setLocaleResult) {
            var currency = Currency.getCurrency(req.querystring.CurrencyCode);
            if (allowedCurrencies.indexOf(req.querystring.CurrencyCode) > -1) {
                req.session.setCurrency(currency);

                if (currentBasket && currency && currentBasket.currencyCode !== currency.currencyCode) {
                    Transaction.wrap(function () {
                        currentBasket.updateCurrency();
                    });
                }
            }
        }

        res.redirect(URLUtils.url('Home-Show'));
        next();
    });

server.append('Show', pageMetaData.hrefLang, breadcrumbs.generateContentBreadcrumbs);

module.exports = server.exports();
